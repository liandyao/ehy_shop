$(function(){
	laodMyShare();
	setTimeout(delay,1000)
	$(".div_share_curtain").load("pages/front/share.jsp #curtain");
	$(".div_share").load("pages/front/share.jsp #share-layer");
	$("#Section4").load("pages/front/myShare.jsp .myShare");   
})
//分享的最小百分比
var minPrice=10;
//分享的最大百分比
var maxPrice=10; 
//分享商品的规格的最低价格
var proMinPrice=0; 
//分享商品的规格的最高价格
var proMaxPrice=0;
//根据订单id查询订单的方法 
function showShare(oritmId){
	$.ajax({
		url:'order/front/findById.action?mxId='+oritmId,
		data:null,
		type:'POST',
		async: false,  
        cache: false,
        contentType: false,  
        processData: false,  
        success: function (oritm) {
        	
        	if(oritm.mes.state==1){
        		if(oritm.share!=null){
	        	document.getElementById("share-layer").style="display:block;";
	        	document.getElementById("curtain").style="display:block;";
	        	$("#mxId").val(oritmId)
	        	$(".max-img").empty();
	        	$(".max-img").append("<img src="+oritm.share.photo+" />")
	        	$(".factory").html("(￥"+oritm.share.proMinPrice+"-￥"+oritm.share.proMaxPrice+")")
	        	proMaxPrice=oritm.share.proMaxPrice;
	        	proMinPrice=oritm.share.proMinPrice;
	        	$(".moneyFact").html(oritm.share.moneyFact)
	        	$(".min-price").html(oritm.share.minPrice)
	        	minPrice=oritm.share.minPrice;
	        	$(".max-price").html(oritm.share.maxPrice)
	        	maxPrice=oritm.share.maxPrice;
        		}else{
        			alert("该商品已下架")
        		}
        	}else{
        		alert(oritm.mes.mes);
        	}
        },  
        error: function () {  
            alert("失败");  
        }  
	})

}
//格式化分享的值
function setShare(title, url,  pic, summary, hideMore) {  
    jiathis_config.title = title;//分享的标题  
    jiathis_config.url = url;//分享的网址
    jiathis_config.pic = pic;//分享的图片
    jiathis_config.summary = summary;//分享的内容 
    jiathis_config.hideMore = hideMore;     
}  
//分享的值  
var jiathis_config = {};  

//清除的方法
function clean(){
	$(".div_share").empty();
	$(".div_share").load("pages/front/share.jsp #share-layer")
	setTimeout(delay,1000)
	hide();
}
/*隐藏的方法*/
function hide(){
	document.getElementById("share-layer").style="display:none;";
	document.getElementById("curtain").style="display:none;";
	clean();
}
/*评论和分享的方法*/
function comment(){
	var data=new FormData($( "#comment" )[0]);
	 var str="";
	 $(".code").each(function(){
		 var type=$(this).parents("ul").siblings("p").text();
		 if(str!=""){
			 str+="_"
		 }
		 str+=type+"："+$(this).text();
	 })
	 $.ajax({  
         url: 'comment/front/comment.action?commLevel='+str ,
         type: 'POST',  
         data: data,  
         async: false,  
         cache: false,  
         contentType: false,  
         processData: false,  
         success: function (mes) {
        	 alert("成功")
        	 hide();
         },  
         error: function () {  
             alert("失败");  
         }  
    });  
}
//需要延时加载的东西
function delay(){
	$("[name=shaPrice]").on('keyup',function(e){
		shaPrice=$(this).val();
		this.value = this.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
		/*this.value = this.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
*/		/*this.value = this.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
		this.value = this.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
		this.value = this.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");*/

	}).on('blur',function(){
		if($("[name=shaPrice]").val()==""){
			$("[name=shaPrice]").val(minPrice)
			};
		if(parseFloat(shaPrice)>parseFloat(maxPrice)){
			$("[name=shaPrice]").val(maxPrice)
		}else if(parseFloat(shaPrice)<parseFloat(minPrice)){
			$("[name=shaPrice]").val(minPrice)
		}
		//收益区间的最低值
		var a=(proMinPrice*(10*0.01)).toFixed(2);
		//收益区间的最高值
		var b=(proMaxPrice*(10*0.01)).toFixed(2);
		
		//显示在我的收益的最低值
		a=a-(proMinPrice*($("[name=shaPrice]").val()*0.01)).toFixed(2);
		//显示在我的收益的最高值
		b=b-(proMaxPrice*($("[name=shaPrice]").val()*0.01)).toFixed(2);
		
		$("#shaYield").val("("+a+"-"+b+")");
		/*$("#shaYield").val(($("[name=shaPrice]").val()-minPrice).toFixed(2));*/
	})
	//我的收益输入框的计算方法
	/*$("#shaYield").on('keyup',function(e){
		
		this.value = this.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
		this.value = this.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
		this.value = this.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
		this.value = this.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");

	}).on('blur',function(){
		shaYield=$(this).val();
		if($("#shaYield").val()==""){
			$("#shaYield").val(0)
		}
		if(parseFloat(shaYield)>(maxPrice-minPrice)){
			$("#shaYield").val(maxPrice-minPrice)
		}else if(parseFloat(shaYield)<0){
			$("#shaYield").val(0)
		}
		$("[name=shaPrice]").val((parseFloat($("#shaYield").val())+parseFloat(minPrice)).toFixed(2));
	})*/
	$(".ommLevel li").on('click',function(){
		$(this).parents("ul").children("li").each(function(){
			$(this).attr("class","");
		}
		);
		$(this).attr("class","code")
	})
	$("#xFile").on('change', function () {
		 
		   //获取上传文件的数量
		   var countFiles = $(this)[0].files.length;
		 
		   var imgPath = $(this)[0].value;
		   var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		   var upload_img = $(".photoAdd");
		   $(".photo").remove();
		 
		   if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		       if (typeof (FileReader) != "undefined") {
		 
		           // 循环所有要上传的图片
		           for (var i = 0; i < countFiles; i++) {
		 
		               var reader = new FileReader();
		               reader.onload = function (e) {
		            	   upload_img.before("<div class='photo' style='width: 223px; height: 296px; float: left;margin-right:20px;'><div class='' style='width: 223px; height: 215px; border: 1px solid #d5d5d5; border-bottom: none;'><div style='height:215px;padding:2px;border-bottom: none; line-height: 215px; text-align: center; color: red; font-size: 18px;'><img style='max-width:100%;max-height:100%;padding:2px;' src='"+e.target.result+"'/></div></div><textarea name='photoDesc' style='width: 223px; height: 70px;'placeholder='可输入描述文字'></textarea></div>");
		               }
		 
		               upload_img.show();
		               reader.readAsDataURL($(this)[0].files[i]);
		           }
		 
		       } else {
		    	   upload_img.before("<div class='photo' style='width: 223px; height: 296px; float: left;'><div class='' style='width: 223px; height: 215px; border: 1px solid #d5d5d5; border-bottom: none;'> <div style='border-bottom: none; line-height: 215px; text-align: center; color: red; font-size: 18px;'>上传照片</div></div><textarea name='photoDesc' style='width: 223px; height: 70px;' placeholder='可输入描述文字'></textarea></div>")
		       }
		   } else {
		       alert("请选择图像文件。");
		   }
		});
}
/*加载我的分享的方法*/
function laodMyShare(){
	$(".span [href=#Section4]").on("click",function(){
		$.ajax({
			url:"share/front/showAll.action",
			data:null,
			type:"post",
			async:true,
			cache:false,
			success:function(shareList){
				$(".myShare").empty();
				var str="";
				for(var i=0;i<shareList.length;i++){
					str+='<div class="shareBaby">'
					str+='<div class="baby">'
					str+='<div class="babyImg">'
					str+='<img class="img" src="'+shareList[i].photo.split(",")[0]+'">'
					str+='</div>'
					str+='<div class="babyMes">'
					str+='<ul class="mes">'
					str+='<li>宝贝标题</li>'
					str+='<li><span class="proName">'+shareList[i].proName+'</span></li>'
					str+='<li>厂家直销价：<span class="factoryPrice">￥'+shareList[i].proPrice0+'</span></li>'
					str+='<li>帮其他人优惠了：<span class="myPrice">'+shareList[i].shaPrice+'%</span></li>'
					str+='<li>下单时间：<span class="shareTime">'+shareList[i].mxDateTime+'</span></li>'
					str+='</ul>'
					str+='<div class="sm">'
					str+='<span class="smw">分享后，出现在宝贝详情页。带给更多买家参考，带来更多订单成交。</span>'
					str+='</div>'
					str+='</div>' 
					str+='</div>'
					str+='<div class="share">'
					str+='<input type="hidden" class="mxId" value="'+shareList[i].mxId+'">'
					str+='<input type="hidden" class="shaId" value="'+shareList[i].shaId+'">'
					str+='<div class="shareImg">'
					str+='<img class="img" src="'+shareList[i].photo.split(",")[1]+'">'
					str+='</div>'
					str+='<div class="shareMes">'
					str+='<ul class="mes">'
					str+='<li>分享的信息</li>'
					str+='<li>宝贝被游览：<span class="factoryPrice">'+shareList[i].look+'</span></li>'
					str+='<li>宝贝已成交：<span class="myPrice">'+shareList[i].buy+'</span></li>'
					str+='<li>宝贝的收益：<span class="myYield">￥'+shareList[i].earnings+'</span></li>'
					str+='<li>分享时间：<span class="shareTime">'+shareList[i].shareTime+'</span></li>'
					str+='</ul>'
					str+='<span class="fx"><span class="fxw">告诉朋友：</span>'
					//解决方案使用a标签直接动态生成传入地址进行分享
					str+='<div class="jiathis_style_32x32">'
					str+='<a class="jiathis_button_cqq"></a>'
					str+='<a class="jiathis_button_weixin"></a>'
					str+='<a class="jiathis_button_qzone"></a>' 
					str+='<a class="jiathis_button_tsina"></a>'
					str+='<a class="jiathis_button_renren"></a>' 
					str+='<a class="jiathis_button_kaixin001"></a>'
					str+='<a class="jiathis_counter_style"></a>'
					str+='</div></span></div></div></div>'
					
				}
				//增加隐藏框作为jiaThis分享动态取值的节点
				//分享的编号
				str+='<input type="hidden" name="jia_shareId" value="123">'
				//分享商品的订单号
				str+='<input type="hidden" name="jia_mxId" value="666">'
				//分享的商品标题
				str+='<input type="hidden" name="jia_title" value="666">'
				//分享的内容
				//str+='<input type="hidden" name="jia_summary">'
				//分享的百分比
				str+='<input type="hidden" name="jia_share" value="666">'
				//分享的图片	
				str+='<input type="hidden" name="jia_img" value="666">'
				$(".myShare").append(str);
				//绑定鼠标移入DIV的事件当鼠标移入div时进行一个分享数据的获取与重置
				$(".shareBaby").on('mouseover',function(){
					//得到当前shareBaby节点中的share节点
					var share = $(this).find(".share");
					//得到当前shareBaby节点中的baby节点
					var baby = $(this).find(".baby");
					//赋值
					$("[name=jia_shareId]").val(share.find(".shaId").val());
					$("[name=jia_mxId]").val(share.find(".mxId").val());
					$("[name=jia_title]").val(baby.find(".mes").find(".proName").text());
					$("[name=jia_share]").val(baby.find(".mes").find(".myPrice").text());
					$("[name=jia_img]").val(baby.find(".babyImg").find(".img").attr("src"));
					
				})
				//加载完成所有分享记录后将jiaThis.js引入到页面
				loadDemo("http://v3.jiathis.com/code/jia.js")
			},
			error:function(){
				/*alert("请求异常");*/
			}
			});
	})
}

//jiaThis分享代码动态加载分享的网址、标题、内容、图片
var jiathis_config = {
		siteName:"E货源",
		url:$("base").attr("href")+"pages/front/appraise.jsp?mxId="+$("[name=jia_mxId]").val()+"&shaId="+$("[name=jia_shareId]").val(),
	 	title:"E货源商品分享",
		summary:"您的好友将"+ $("[name=jia_title]").val()+"以"+$("[name=jia_share]").val()+"%优惠分享给你",
		/* summary:"您的好友将XXX"+"以5%的优惠分享给您",  */
		pic:$("[name=jia_img]").val()
		
}
//动态加载js的方法
function loadDemo(objSrc) {
    var iHead = document.getElementsByTagName('HEAD').item(0);
    var iScript = document.createElement("script");
    iScript.type = "text/javascript";
    iScript.src = objSrc;
    iHead.appendChild(iScript);
}