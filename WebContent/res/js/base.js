var stId = null; 

$(function() {
	//加载页面元素节点
	$.get("pages/front/base.jsp",function(data){
		$("#header").html($(data).filter("#header").html());//加载页眉
		$("#toolbar").html($(data).filter("#toolbar").html());//加载工具栏
		$("#footer").html($(data).filter("#footer").html());//加载页脚
		var url=window.location.href+"#main-top";//得到当前页面的URL地址
		$("#toolbar #ceiling").attr("href", url);//设置锚链接目标元素的URL地址
		stId=getParam("stId");
		loadStation(stId);//加载站点
	},"html");
	
});

var flag=false;

/**
 * 站点点击事件
 * @param obj 被点击的A标签
 * @param id 站点ID
 * @returns 
 */
function link(obj){
	var li=$("div.links-li li.active")[0];
	if(obj.title == $(li).find("a").attr("title")){
		return;
	}
	$(li).removeClass("active");
	$(obj).parents("li").addClass("active");
	$(".links-li li > div").removeClass("active");
	$(obj).parents("li").prev().find("div:last-child").addClass("active");	
	var id=$(obj).attr("data-value");
	if(getParam("proId") && flag){
		return window.location.href="pages/front/index.jsp?stId="+$(obj).attr("data-value");
	}
	if(typeof loadImages === 'function'){
		$(".show-list").remove();//移除上一个站点的产品
		loadImages(id);//加载产品图片
	}
	if(typeof loadSuperiorType === 'function'){
		loadSuperiorType(id);//加载产品类型
	}
	if(typeof shownews === 'function'){
		shownews(id);//加载新闻公告
	}
	
}

/**
 * 加载站点
 * @returns
 */
function loadStation(stId){
	var url="frontShow/front/findStationAll.action";
	var data={"intercept":true,};
	 
	$.post(url,data,function(arr){
		var ul=$("div.links-li > ul")[0];
		$(ul).find("li:not(:first-child)").remove();
		var index=2;
		for(var i=0;i<arr.length;i++){
			if(arr[i].stType==0){
				index=i+index;//记录默认站点所在的节点序号
			} 
			$(ul).append("<li><a href='javascript:void(0);' onclick='link(this)' "+
					" title="+arr[i].stName+
					" data-value="+arr[i].stId+" >"+arr[i].stName+"</a>"+
				"<div class='line-height'></div></li>");
		} 
		$(ul).find("li:nth-of-type(2)").prepend("<div class='line-height'></div>");
		var hq=$(ul).find("li:nth-of-type("+index+") > a")[0];//得到默认站点所在的节点
		if(stId){
			$("[data-value='"+stId+"']:first").click();
		}else{
			$(hq).click();
		}
		flag=true;
	},"json");
	 
}


/**
 * 获取URL参数的方法
 * @param name 属性名
 * @returns 如果存在返回对应的值,不存在返回null
 */
function getParam(name) { 
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //设置正则表达式的规则
  var url= window.location.search;//获取URL地址
  url=decodeURIComponent(url);//URL解码
  var r =url.substr(1).match(reg); 
  if (r != null) {  
    return unescape(r[2]); 
  } 
  return null; 
}

/**
 * jquery的方法,等待某个元素出现.直到加载完成才执行方法.例如
 * 
 * $(".links-li li.active a").wait(function(){
						var stId=$(".links-li li.active a").attr("data-value");
						 
						initLoad();
					})
	以上方法的意思是,当.links-li li.active a加载完成时,才执行function里面的方法
	
	该方法的文档说明:
	有3个参数。
	func 是回调函数，就是当指定元素出现后就执行的函数。
	times 是检测次数，默认是-1，一直检测直到出现为止。
	interval 是检测间隔，默认 20 毫秒一次。
 */
jQuery.fn.wait = function (func, times, interval) {
     var _times = times || -1, //100次
     _interval = interval || 20, //20毫秒每次 
     _self = this,
     _selector = this.selector, //选择器
     _iIntervalID; //定时器id
     if( this.length ){ //如果已经获取到了，就直接执行函数
         func && func.call(this);
     } else {
         _iIntervalID = setInterval(function() {
             if(!_times) { //是0就退出
                 clearInterval(_iIntervalID);
             }
             _times <= 0 || _times--; //如果是正数就 --
             
             _self = $(_selector); //再次选择
            if( _self.length ) { //判断是否取到
                 func && func.call(_self);
                 clearInterval(_iIntervalID);
             }
         }, _interval);
     }
     return this;
}
