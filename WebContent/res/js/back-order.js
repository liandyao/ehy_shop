/* version1.0 次js文件依赖于layui和jQuery若需使用请在layui,jQuery之后引入 */



/**
 * 判断obj的值是否为数字
 * @param obj input对象，自动清除非两位小数
 */
function number(obj){
	obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
	obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
	obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
	obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
	if(obj.value.indexOf(".")>-1){
		$(obj).attr("maxlength",obj.value.split(".")[0].length+3);
	}else{
		$(obj).removeAttr("maxlength");
	}
}

/**
 * 加载全部站点
 */
function showStation(stId){
	var url = "manager/back/showAllStation.action";
	$.post(url, null, function(info){
		for(i=0; i<info.length; i++){
			var obj = info[i];
			if(obj.stId==stId){
				$("#stId").append('<option value="'+obj.stId+'" selected >'+obj.stName+'</option>');
			}else{
				$("#stId").append('<option value="'+obj.stId+'">'+obj.stName+'</option>');
			}
		}
		$("#stId").append('<option value="">全部站点</option>');
		layui.use('form', function() {
			var form = layui.form;
			form.render('select');
		});
	});
}

/***
 * 格式化日期
 */
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度  
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}



/**
 * 查看商品详情
 * @param str
 * @returns
 */
function queryParticular(str){
	var content = "";
	str.split("_").forEach(function (val,index,arr){
		if(index==0){
			content+="<p>商品名称："+val+"</p>";
		}else {
			content+="<p>"+val+"</p>";
		}
	});
	$('.layui-layer-content').hide();
	layer.alert(content);
}

/**
 * 查看物流
 * @param express 物流公司编码
 * @param code 快递单号
 */
function ckeckWl(express, code){
	var arr = express.split("_");
	var url = 'https://m.kuaidi100.com/index_all.html?type=';
	if(arr.length>1){
		url += express.split("_")[1]+'&postid='+code+'#&ui-state=dialog';
	}else{
		url += express.split("_")[0]+'&postid='+code+'#&ui-state=dialog';
	}
	var index = layer.open({
        type: 2//样式
        ,skin: 'layui-layer-molv'//样式
        ,area: ['65%', '86%']
        ,title:"物流详情"//标题
        ,id: 'index' //防止重复弹出
        ,content: url
        ,shade: [0.8, '#393D49'] //显示遮罩
        ,shadeClose:true//点击也能遮罩层关闭
        ,anim:2//弹出动画
        ,success: function(layero, index){
           // var arr = $("iframe[name='iframe']").contents().find("#layui-layer-iframe3").contents().find("iframe").remover();
        }
    });
}