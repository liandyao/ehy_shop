function stateCnFun(state,data){
			var str = "";
			if(state==50){
				str+="<span>退款申请中,等待确认!</span>"
			}else if(state==51){
				
				str+="<span class='layui-btn-danger'>未发货,退款申请中!</span>"
			}else if(state==60){
				str+="<span class='layui-btn-danger'>已退款</span>"
			}
			else if(state==40){
				str+="<span>已签收</span>"
			}else if(state==30){
				str+="<span>待收货</span><span class='btn' onclick='ckeckWl(\""+data.ordExpress+"\",\""+data.ordExpressCode+"\");'>查看物流</span>"
			}else if(state==20){
				str+="<span>待发货</span>等待商家发货"
			}else if(state==10){
				str+="<span>待付款</span>"
			}else if(state==2){
				str+="<span>已退货</span>"
			}else if(state==1){
				str+="<span>退货中</span>等待商家确认"
			}
			return str ;
		}
		
		/**
		 * 显示订单状态
		 * @param ordMxId
		 * @returns
		 */
		function showState(data){
			 
			var str = "";
			 
			if(data.ordState>=50){
				var url = "returns/findByOrdId.action"; 
				$.ajax({
					url:url,
					data:{"ordId":data.ordId},
					type:"post", 
					dataType:"json",
					async:false,
					success:function(mes){
						if(mes.reDesc!=""){
							str+='<p style="color:red">退款原因：'+mes.reDesc+"</p>   <p>申请时间："+mes.reTime+"</p>";
						}
						
						if(mes.reAddress!=null){
							str+='<p>退货地址：'+mes.reAddress+'</p>';
						} 
						
						if(mes.reCode!=null){
							str+='<p>退款金额：'+(mes.reCode/100)+'</p>';
						} 
						if(mes.reExpressCode){
							str+="<p>退货快递单号:<a target='_blank' href='https://m.kuaidi100.com/index_all.html?postid="+mes.reExpressCode+"'>"+mes.reExpressCode+"</a></p>";
						} 
					}
				});
				
				if(data.ordState==50){
					str+="<span  class='layui-btn layui-btn-radius' onclick='fasongDizhi(\""+data.ordId+"\")'>发送退货地址</span><span class='layui-btn layui-btn-danger layui-btn-radius' onclick='tuikuanShenHe(\""+data.ordId+"\",\""+data.ordState+"\",\""+data.mxMoney+"\",\""+data.ordFreight+"\");' >审核</span>"
						
				}
				if(data.ordState==51){
					str+="<span class='layui-btn layui-btn-danger layui-btn-radius' onclick='tuikuanShenHe2(\""+data.ordId+"\",\""+data.ordState+"\",\""+data.mxMoney+"\",\""+data.ordFreight+"\");' >审核</span>"
						
				}
				
			}else{
				str="暂无信息";
			}
			
			layer.alert(str);
		}
		
		/**
		发送退货地址
		*/
		function fasongDizhi(ordId){
			
			layer.prompt({title: '请输入退货联系人,联系电话,地址',  formType: 2}, function(text, index){
				if(text==""){
					layer.msg("请输入正确的地址!");
					return ;
				}
				var url = "returns/sendAddress.action";
				$.post(url,{"ordId":ordId,"address":text},function(mes){
					if(mes.state==1){
						layer.msg("发送成功,等待买家发货!");
						layer.close(index); 
					}
				});
			});
			
			
		}
		
		/**
		退款审核
		*/
		function tuikuanShenHe(ordId,state,ordMoney,ordFreight){
			
			var url = "returns/findByOrdId.action";
			$.post(url,{"ordId":ordId},function(mes){
				if(mes){ 
					
					//询问框
					layer.confirm('通过之后退款将返回到买家账号,请确认？<p style="color:red">退款原因：'+mes.reDesc+"</p>   时间："+mes.reTime, {
					  btn: ['下一步','取消'] //按钮
					}, function(){
						
						
						
						layer.prompt({title: '【商品金额为：'+ordMoney+'，运费金额为：'+ordFreight+'】请输入需要退款的金额：',  formType: 0}, function(text, index){
						    if(isNaN(text)){
						    	layer.msg("请输入数字!");
						    	return ;
						    }
						    if(parseFloat(text)>(parseFloat(ordMoney)+parseFloat(ordFreight))){
						    	layer.msg("退款的金额已经超过付款的金额!");
						    	return ;
						    }
						    
							
							
						    $.ajax({
								url:"payAction/refund.action",
								data:{"ordId":ordId,"money":text},
								type:"post", 
								dataType:"json",
								cache:false,
								success:function(mes){
									
									if(mes.alipay_trade_refund_response.code!="10000"){
										layer.msg('退款失败!', {icon: 1});
									}else{
										
										//退款成功后,到ehuoyuan将状态改成已退款
										var url = "order/front/tuikuanShenHe.action";
										var dat = {"ordId":ordId,"money":text};
										$.post(url,dat,function(m){
											if(m.state==1){
												layer.close(index); 
												layer.msg('退款成功!', {icon: 1});
												reloadRec();
											}else{
												layer.msg('退款失败!', {icon: 1});
											}
										})
										
										
									}
									
									 
									 
								},
								error:function(){
									layer.msg('退款失败!', {icon: 1});
								}
							})
						  });
						
						
						
					 
					}, function(){
					 /*  layer.msg('暂不通过', {
					    time: 20000, //20s后自动关闭
					  }); */
						layer.msg('取消退款!', {icon: 1});
					});
				}
			}); 
			
			
			
			
		}
		
		/**
		未发货,退款审核
		*/
		function tuikuanShenHe2(ordId,state,ordMoney,ordFreight){
			
			var url = "returns/findByOrdId.action";
			$.post(url,{"ordId":ordId},function(mes){
				if(mes){ 
					
					//询问框
					layer.confirm('通过之后退款将返回到买家账号,请确认？<p style="color:red">退款原因：'+mes.reDesc+" 【商品金额为："+ordMoney+"，运费金额为："+ordFreight+"】</p>   时间："+mes.reTime, {
					  btn: ['下一步','取消'] //按钮
					}, function(){
						 
						    $.ajax({
								url:"payAction/refund.action",
								data:{"ordId":ordId,"money":parseFloat(ordMoney)+parseFloat(ordFreight)},
								type:"post", 
								dataType:"json",
								cache:false,
								success:function(mes){
									
									if(mes.alipay_trade_refund_response.code!="10000"){
										layer.msg('退款失败!', {icon: 1});
									}else{
										
										//退款成功后,到ehuoyuan将状态改成已退款
										var url = "order/front/tuikuanShenHe.action";
										var dat = {"ordId":ordId,"money":parseFloat(ordMoney)+parseFloat(ordFreight)};
										$.post(url,dat,function(m){
											if(m.state==1){
												 
												layer.msg('退款成功!', {icon: 1});
												reloadRec();
											}else{
												layer.msg('退款失败!', {icon: 1});
											}
										})
										
										
									}
									
									 
									 
								},
								error:function(){
									layer.msg('退款失败!', {icon: 1});
								}
							})
						  
						
						
						
					 
					}, function(){
					 /*  layer.msg('暂不通过', {
					    time: 20000, //20s后自动关闭
					  }); */
						layer.msg('取消退款!', {icon: 1});
					});
				}
			}); 
			  
		}