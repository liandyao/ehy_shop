var citiesArr={},areasArr={};

var address=new Object();

/**
 * 关闭时执行的功能函数
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月18日上午11:52:08
 * @version 1.0
 */
address.onHide=function(){
	
}
function btnCancel(){
	document.querySelector("#dialog-curtain").style.display="none";
	document.querySelector("#dialog-confirm").style.display="none";
}
function addrInitLoad(){
	pp();
	$("[data-event='address-show']").on("click",function(){
		$(".layer-box").find(".address-curtain, .address-layer").show();
	});
	$("[data-event='address-close']").on("click",function(){
		$(".address-curtain, .address-layer").hide();
		$(".address-layer .content input").val("");
		$("#titleP").click();
		$("#titleC,#titleD,#address .address-item").text("请选择");
		$("p.item span.font-blue").removeAttr("class");
		$("#address").removeAttr("data-value");
	});
	$("[data-event='address-save']").on("click",function(){
		saveAddress();
	});
	$("#titleP").click(function(){
		lineShow(this);
		$(this).text("请选择");
		$("#titleC,#cities,#titleD,#district").hide();
		$("#provinces").show();
	});
	$("#titleC").click(function(){
		ccClick(this);
	});
	$("#titleD").click(function(){
		aaClick(this);
	});
	
	$(".address-layer .content input[data-options*=phone]").keyup(function(){
		this.value=this.value.replace(/\D/g,'');
	});
	$(".address-layer .content input[data-options]").blur(function(){
		var json=this.getAttribute("data-options");
		if(!json) return;
		var data=eval("({"+json+"})");
		var value=this.getAttribute("data-value") || this.value;
		if(!data.hasOwnProperty("required") && !value) return $(this).next().text("");
		for(var key in data){
			var param=data[key];
			
			var flag=validate[key].validator(value,param);
			if(!flag){
				var error="收货人"+this.title+validate[key].message;
				if(key=="required") error="请您填写收货人"+this.title;
				if(error.indexOf("{0}")>-1){
					error=error.replace("{0}",param[0]);
				}
				if(error.indexOf("{1}")>-1) error=error.replace("{1}",param[1]);
				$(this).next().text(error);
				return;
			}
		}
		$(this).next().text("");
	});
}



/**
 * 保存收货地址
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:17:12
 * @version 1.0
 */
function saveAddress(){
	var arr=$(".address-layer .content input[data-options]");
	var flag=isValid(arr) & addressTip();
	if(!flag) return;
	var url="address/front/saveAddress.action";
	var data={"addProvince": $("#titleP").text(),"addCity": $("#titleC").text(),"addCounty": $("#titleD").text()};
	var arr=$(".address-layer .content input[name]");
	for(var i=0;i<arr.length;i++){
		var name=$(arr[i]).attr("name");
		var value=$(arr[i]).val();
		data[name]=value;
	}
	$.post(url,data,function(info){
		if(info>0){
			$("[data-event='address-close']:first").click();     
			//address.onHide();
		}else{
			alert("保存失败");
		}
	},"json");
}

/**
 * 给表单赋值
 * @param addId 收货地址ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月18日下午3:03:03
 * @version 1.0
 */
function setForm(addId){
	var url="address/front/selectByAddId.action";
	var data={"addId":addId};
	//return;
	$.post(url,data,function(data){
		var addr=data[0];
		var arr=$(".address-layer .content input[name]");
		for(var i=0;i<arr.length;i++){
			var name=$(arr[i]).attr("name");
			if(addr.hasOwnProperty(name)){
				$(arr[i]).val(addr[name]);	
			}
		}
		$(".layer-box").find(".address-curtain, .address-layer").show();
		$("#provinces > p:contains('"+addr.addProvince+"')").click();
		$("#cities > p:contains('"+addr.addCity+"')").click();
		$("#district > p:contains('"+addr.addCounty+"')").click();
		onOff();
	},"json");
}


function lineShow(id){
	$("#options > li > p span").removeClass("active");
	$(id).toggleClass("active");
}

/**
 * 关闭打开所在地址
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:17:31
 * @version 1.0
 */
function onOff(){
	$('#options').toggleClass('boxShow');
	$("#address").toggleClass('address-open');
}

/**
 * 加载省份
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:22:54
 * @version 1.0
 */
function pp(){
	var url="res/area/json/provinces.json";
	$.getJSON(encodeURI(url),function(arr){ 
		if(arr.length>0){
			for(var i=0;i<arr.length;i++){
				var p="<p class='item' old-name='"+arr[i].provinceId+"'><span>"+arr[i].province+"</span></p>";
				$("#provinces").append(p);
			}
		 itemClick("#provinces p","#titleP","#titleC");
		}
	});
	url="res/area/json/cities.json";
	$.getJSON(encodeURI(url),function(arr){ 
		citiesArr=arr;
	});
	url="res/area/json/areas.json";
	$.getJSON(encodeURI(url),function(arr){ 
		areasArr=arr;
	});
}

/**
 * 城市(标题)点击事件
 * @param obj
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:18:20
 * @version 1.0
 */
function ccClick(obj){
	lineShow(obj);
	$(obj).text("请选择");
	var id=$("#provinces p .font-blue").parents("p").attr("old-name");
	cc(id);
}

/**
 * 加载城市
 * @param id 
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:18:55
 * @version 1.0
 */
function cc(id){
	var arr=citiesArr[id];
	if(arr.length>0){
		$("#cities").html("");
		for(var i=0;i<arr.length;i++){
			var p="<p class='item' old-name='"+arr[i].cityId+"'><span>"+arr[i].city+"</span></p>";
			$("#cities").append(p);
		}
		itemClick("#cities p","#titleC","#titleD");
		$("#provinces,#titleD,#district").hide();
		$("#cities").show();
	}
}

/**
 * 区县(标题)点击事件
 * @param obj
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:19:27
 * @version 1.0
 */
function aaClick(obj){
	lineShow(obj);
	$(obj).text("请选择");
	var id=$("#cities p .font-blue").parents("p").attr("old-name");
	aa(id);
}

/**
 * 加载区县
 * @param id
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:20:12
 * @version 1.0
 */
function aa(id){
	var arr=areasArr[id];
	if(arr.length>0){
		$("#district").html("");
		for(var i=0;i<arr.length;i++){
			var p="<p class='item'><span>"+arr[i].area+"</span></p>";
			$("#district").append(p);
		}
		itemClick("#district p","#titleD","#options");
		$("#provinces,#cities").hide();
		$("#district").show();
	}
}

/**
 * 所在地址是否为空验证
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:20:26
 * @version 1.0
 */
function addressTip(){
	var address=$("#address").attr("data-value");
	var flag=address!=undefined && address!="";
	if(flag){
		$("#area-wrap").next().text("");
	}else {
		$("#area-wrap").next().text("请您选择收货人的所在地址");	
	}
	return flag;
}

/**
 * 选项点击事件
 * @param str
 * @param id
 * @param openId
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午4:21:53
 * @version 1.0
 */
function itemClick(str,id,openId){
	$(str).click(function(){
		$(str).find("span").removeClass("font-blue");
		$(this).find("span").addClass("font-blue");
		$(id).text($(this).text());
		if(openId!="#options"){
			$(openId).show();
			if(id=="#titleP"){
				ccClick("#titleC");
			}else{
				aaClick("#titleD");
			}
		}else{
			var address=$("#titleP").text()+" "+$("#titleC").text()+" "+$("#titleD").text();
			$(".address-item").text(address);
			$("#address").attr("data-value",address);
			onOff();//关闭打开
			addressTip();//验证地址是否为空
			
		}
		
	});
}