	var state=0;
	$(function(){
		//setTimeout('gsh()', 1000)
		zd();
		reveal(1);
	});
	//格式化日期框的方法
	/*function gsh(){
		$('.terminus').datepicker({
			todayBtn : "linked",  
			autoclose : true,  
		    endDate : new Date()  
		}).on('changeDate',function(e){
		    var endTime = e.date;  
		    $('.start').datepicker('setEndDate',endTime);
		    
		});
		$('.start').datepicker({  
			todayBtn : "linked",  
			autoclose : true,  
		    endDate : new Date()  
		}).on('changeDate',function(e){  
		    var startTime = e.date;  
		    $('.terminus').datepicker('setStartDate',startTime);  
		}); 
	}*/
	//显示隐藏的方法
	function Toggle() {
		var src = $(".jt").attr("src");
		if ("res/images/jt_s.png" == src) {
			$(".jt").attr("src", "res/images/jt_x.png")
		} else {
			$(".jt").attr("src", "res/images/jt_s.png")
			/* $("[name='keyword']").val(""); */
			$("[name='start']").val("");
			$("[name='terminus']").val("");
			$("[name='site']").find("option[value='']").attr("selected",true);
		}
		$(".hidn").slideToggle();
	}
	//选中的方法
	function pitchOn(obj,states){
		state=states;
		reveal(1);
		$(".ztBox .onddzt").attr("class","ddzt");
		$(obj).attr("class","onddzt")
	}
	function zd(){
		var url="manager/back/showAllStation.action?intercept=true"
		$.post(url,null,function(stList){
			for(var i=0;i<stList.length;i++){
				$(".site").append("<option value="+stList[i].stId+">"+stList[i].stName+"</option");
			}
			
		}) 
		
	}
	
	/**
	 * 确认收货
	 * @param ordId
	 * @param obj
	 * @returns
	 */
	function receipt(ordId,obj){
		$.ajax({
			url:"order/front/receipt.action",
			data:{"ordId":ordId},
			type:"post",
			async:true,
			cache:false,
			success:function(mes){
				if(mes.state>0){
					var tr=$(obj).parents("tr");
					tr.empty();
					tr.append("<td colspan='2'>订单状态：已签收</td><td colspan='3'><input class='ssbt'  onclick='tuikuan(\""+orderList[i].ordId+"\",this)'  type='button' value='我要退货'></td>");
				}
			},
			error:function(){
				/*alert("异常")*/
			}
		})
	}
	
	/**
	 * 未发货时,直接退货的申请
	 * @param ordId
	 * @param obj
	 * @returns
	 */
	function tuikuan2(ordId,obj){
		

		swal({
			title: "确认需要退款吗?",
			text: "您还未付款",
			icon: "warning",
			buttons: ["取消","确认"],
			dangerMode: true,
		})
		.then((willDelete) => {

			if (willDelete) {


				swal("请输入退款原因(可填写【不想买了】，【买错了】等):", {
					content: "input",
				})
				.then((value) => { 
					// swal(`You typed: ${value}`);
					$.ajax({
						url:"order/front/tuikuanShenqing2.action",
						data:{"ordId":ordId,"content":value,"state":51},
						type:"post",
						async:true,
						cache:false,
						success:function(mes){
							if(mes.state>0){
								var tr=$(obj).parents("tr");
								tr.empty();
								tr.append("<td colspan='2'>订单状态：申请退款中,等待审核</td><td colspan='3'></td>");

								// returnsMes(ordId,'returns_mes_'+ordId);

							}

						},
						error:function(){
							/* alert("异常") */
							swal("申请失败!");
						}
					})
				});
			}else{
				swal("取消申请!");
			}}
		);
	}
	
	/**
	 * 申请退款
	 * @param ordId
	 * @param obj
	 * @returns
	 */
	function tuikuan(ordId,obj){
		
		swal({
			  title: "确认需要退货吗?",
			  text: "不管是【质量问题】还是【非质量原因】退款请点击【确认】填写说明！请注意：如果是非质量原因则不会退运费。",
			  icon: "warning",
			  buttons: ["取消","确认"],
			  dangerMode: true,
			})
			.then((willDelete) => {
				 
			  if (willDelete) {
				  swal("请输入退货原因（非质量原因不退运费）:", {
					  content: "input",
					})
					.then((value) => {
					  if(value==""){
						  swal("没有填写说明,放弃退款!");
						  return ;
					  }
					  //swal(`You typed: ${value}`);
					  $.ajax({
							url:"order/front/tuikuanShenqing.action",
							data:{"ordId":ordId,"content":value},
							type:"post",
							async:true,
							cache:false,
							success:function(mes){
								if(mes.state>0){
									var tr=$(obj).parents("tr");
									tr.empty();
									tr.append("<td colspan='2'>订单状态：申请退款中,等待审核</td><td colspan='3' id='returns_mes_"+ordId+"'></td>");
									
									returnsMes(ordId,'returns_mes_'+ordId); 
									
								}
								
							},
							error:function(){
								/*alert("异常")*/
								swal("申请失败!");
							}
						})
					});
			  } else {
			    swal("放弃退款!");
			  }
			});
		
		
		
		
		 
		
		
	}
	
	/**
	 * 显示退货信息
	 * @returns
	 */
	function returnsMes(ordId,id){
		var url = "returns/findByOrdId.action";
		$.post(url,{"ordId":ordId},function(mes){
			//console.info(mes);
			if(mes){
				$("#"+id).html("我的退货原因："+mes.reDesc+"   "+mes.reTime);
				var desc = mes.reDesc.split("_")[1] ;
				
				//alert(desc);
				if(desc=="该商品未发货"){
					return ;
				}
				if(mes.reAddress){
					$("#"+id).append("<hr />");
					$("#"+id).append("  </p>退换货地址:"+mes.reAddress+"</p>");
					$("#"+id).append("  <p>请在快递中夹带一个小纸条!注明购买会员的手机号码!</p>");
					
				}
				
				if(mes.reExpressCode){
					$("#"+id).append("  <p>退货快递单号:<a  target='_blank' href='https://m.kuaidi100.com/index_all.html?postid="+mes.reExpressCode+"'>"+mes.reExpressCode+"</a></p>");
				}else{
					
					$("#"+id).append("  <p><button class='btn' onclick='returnKuaiDi(\""+ordId+"\")'>输入快递单号</button></p>");
					
				}
			}
		}); 
	}
	
	
	/**
	 * 输入退货快递单号
	 * @param curPage
	 * @returns
	 */
	function returnKuaiDi(ordId){
		swal("请输入退货的快递单号:", {
			  content: "input",
			})
			.then((value) => {
			  if(value==""){
				  swal("没有填写快递单号!");
				  return ;
			  }
			  //swal(`You typed: ${value}`);
			  $.ajax({
					url:"order/front/returnKuaiDi.action",
					data:{"ordId":ordId,"content":value},
					type:"post",
					async:true,
					cache:false,
					success:function(mes){
						if(mes.state>0){
							
							returnsMes(ordId,'returns_mes_'+ordId); 
							
						}else{
							swal(mes.mes);
						}
						
					},
					error:function(){
						/*alert("异常")*/
						swal("后台提交失败!");
					}
				})
			});
	}
	
	
	//显示的方法 
	function reveal(curPage){
		$.ajax({
			url:"order/front/showList.action?curPage="+curPage+"&ordState="+state,
			data:$("#for").serialize(),
			type:"post",
			async:true,
			cache:false,
			success:function(map){
			$(".orderb").empty();
			var orderList=map.orderList;	
			var pages=map.pages;
			var str="";
			if(orderList.length<=0){
				str="<div style='width: 100%;text-align: center;'><img style='margin-top:5%;' src='res/images/none_order.jpg'><p>没有找到订单</p></div>";
			}else{
			for(var i=0;i<orderList.length;i++){
				str="<div class='ordersBox'>"+
				"<table class='order'>"+
				"<tr class='dh'>"+
				"<td><div class='time'>下单日期："+new Date(orderList[i].opTime).format("yyyy-MM-dd")+"</div>"+
				"<div class='orderNum'>订单号："+orderList[i].ordId+"</div></td>"+
				"<td colspan='3'>收货人:"+orderList[i].ordMember+"  "+orderList[i].ordPhone+"  "+orderList[i].ordAddress+"</td>"+
				"<td colspan='2'><div class='kddh'><a href='https://m.kuaidi100.com/index_all.html?postid="+orderList[i].expressCode+"' target='_blank'>"+orderList[i].expressName+"："+orderList[i].expressCode+"</a></div></td>"
				var prolist=orderList[i].prolist;
				for(var j=0;j<prolist.length;j++){
					var payment=prolist[j].payment
					if(payment<=0){
						payment="未付款"
					}else{
						 
						payment="￥"+payment;
						 
						
					}
					str+="<tr class='details'>"+
					"<td class='baby'>"+
					"<a href='pages/front/info.jsp?proId="+prolist[j].proId+"' target='_blank'><div class='pro'></div>"+ //2018年7月16日 将<img class='pro_img'src="+prolist[j].proPhotoPath+">图片去掉,因为查询速度实在太慢了
					"<div class='pro_name'>"+prolist[j].proName+"</div></a></td>"+
					"<td class='price' style='text-decoration:none ;'>"+prolist[j].cost+"</td>"+
					"<td class='num'>"+prolist[j].mxNum+"件</td>"+
					"<td class='sj_price'>"+(prolist[j].ordFreight/100)+"</td>"+
					"<td class='sj_price'>"+payment+"</td>"+
					"<td class='cz'><div>"
					if(orderList[i].ordState>10){
						str+="<a href='javascript:showShare(\""+prolist[j].mxId+"\")'>分享商品</a>"
					} 
					
					str+="<div><a href='javascript:deleteOrderItem(\""+prolist[j].mxId+"\")'>删除订单</a></div>"
					 
					 
				}
				str+="<tr class='details'>"
				
				//0 已删除 1 申请退货 2 已退货 10 已确认待支付 20 已支付,待发货 30 已发货 40 已签收 50 退款申请 60 退款成功
				if(orderList[i].ordState==50){
					str+="<tr class='bom'><td colspan='2' style='color:red'>订单状态：申请退款中,等待审核!</td><td colspan='3' id='returns_mes_"+orderList[i].ordId+"'>"
					  
					
					
				}else if(orderList[i].ordState==60){
					str+="<tr class='bom'><td colspan='2'>订单状态：已退款</td><td colspan='3' id='returns_mes_"+orderList[i].ordId+"'>"
				}
				else if(orderList[i].ordState==40){
					str+="<tr class='bom'><td colspan='2'>订单状态：已确认收货</td><td colspan='3'><input class='ssbt' onclick='tuikuan(\""+orderList[i].ordId+"\",this)'  type='button'value='我要退货'>"
				}else if(orderList[i].ordState==30){ 
					str+="<tr class='bom'><td colspan='2'>订单状态：待收货</td><td colspan='3'><input class='ssbt' onclick='tuikuan(\""+orderList[i].ordId+"\",this)'  type='button'value='我要退货'><input class='ssbt' onclick='receipt(\""+orderList[i].ordId+"\",this)' type='button'value='确认收货'>"
				}else if(orderList[i].ordState==20){
					str+="<tr class='bom'><td colspan='2'>订单状态：待发货</td><td colspan='3'>等待商家发货<input class='ssbt' onclick='tuikuan2(\""+orderList[i].ordId+"\",this)'  type='button'value='我要退款'>"
				}else if(orderList[i].ordState==10){
					str+="<tr class='bom'><td colspan='2'>订单状态：待付款</td><td colspan='3'><a href=pages/front/pay.jsp?ordId="+orderList[i].ordId+" target='_blank' class='ssbt' type='button'>立即支付</a>"
				}else if(orderList[i].ordState==2){
					str+="<tr class='bom'><td colspan='2'>订单状态：已退货</td><td colspan='3'>已退货"
				}else if(orderList[i].ordState==1){
					str+="<tr class='bom'><td colspan='2'>订单状态：退货中</td><td colspan='3'>等待商家确认"
				}
				str+="</td></tr></table>";
				str=str.replace('null：null', '暂无数据快递数据');
				str=str.replace('null', '暂无数据');
				$(".orderb").append(str);
				returnsMes(orderList[i].ordId,'returns_mes_'+orderList[i].ordId); //显示退货信息
			}	
				var str="<div class='pages'><ul class='pagination'>"
				if(pages.curPage==1){
					str+="<li class='disabled'><span aria-label='Previous'><span aria-hidden='true'>&laquo;</span></span></li>"
				}else{
					str+="<li><a href='javascript:reveal("+(pages.curPage-1)+")' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a>"
				}
	 			for(var i=0;i<pages.totalPage;i++){
	 				if(pages.curPage==(i+1)){
	 					str+="<li class='active'><span>"+(i+1)+"</span></li>"
	 				}else{
	 					str+="<li><a href='javascript:reveal("+(i+1)+")'>"+(i+1)+"</a></li>"
	 				}
	 			}
	 			if(pages.curPage==pages.totalPage){
	 				str+="<li class='disabled'><span aria-label='Next'><span aria-hidden='true'>&raquo;</span></span></li>";
	 			}else{
	 				str+="<li><a href='javascript:reveal("+(pages.curPage+1)+")' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>"
	 			}
				str+="</ul></div>"
				}
				$(".orderb").append(str);
			}
			,
			error:function(){
				/*alert("失败")*/
			}
		});
		
	}
	function deleteOrderItem(mxId){
		
		swal({
			  title: "确认需要删除吗?",
			  text: "删除之后您将看不到该订单的显示,如果您未收到货,请不要删除!",
			  icon: "warning",
			  buttons: ["取消","确认"],
			  dangerMode: true,
			})
			.then((willDelete) => {
				 
			  if (willDelete) {
				  $.post("order/front/deleteOrderItem.action?mxId="+mxId,null,function(mes){
						if(mes.state>-1){
							reveal(1);
						}
						swal(mes.mes)
					})
			  } else {
			    swal("放弃删除!");
			  }
			});
		
		
	}
	//转换时间格式的方法
	Date.prototype.format = function(fmt) { 
	     var o = { 
	        "M+" : this.getMonth()+1,                 //月份 
	        "d+" : this.getDate(),                    //日 
	        "h+" : this.getHours(),                   //小时 
	        "m+" : this.getMinutes(),                 //分 
	        "s+" : this.getSeconds(),                 //秒 
	        "q+" : Math.floor((this.getMonth()+3)/3), //季度 
	        "S"  : this.getMilliseconds()             //毫秒 
	    }; 
	    if(/(y+)/.test(fmt)) {
	            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
	    }
	     for(var k in o) {
	        if(new RegExp("("+ k +")").test(fmt)){
	             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
	         }
	     }
	    return fmt; 
	}   