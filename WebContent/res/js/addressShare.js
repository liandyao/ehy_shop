$(function(){
	var shaId=getParam("shaId");
	findShareByShaId(shaId);
})
function findShareByShaId(shaId){
	$.ajax({
		url:"comment/front/findShareDetails.action",
		data:{"shaId":shaId},
		type:"post",
		async:true,
		cache:false,
		success:function(obj){
			$(".buyer").html(obj.mbName);
			var levels=obj.mbLevel;
			var str="";
			if(levels>0&&levels<8){
				for(var j=0;j<levels;j++){
					if(j>=4){
						break;
					}
					str+="<img style='width:19px;height:24px;' src='res/images/level_one.png'>";
					
				}
			}else if(levels>=8&&levels<50){
				var k=8;
				while(k<=levels&&k<=32){
					k=k+8;
					str+="<img style='width:19px;height:24px;' src='res/images/level_tow.png'>";
					
				}
			}else if(levels>=50){
				var k=50;
				while(k<=levels&&k<=400){
					k=k*2;
					str+="<img style='width:19px;height:24px;' src='res/images/level_three.png'>";
				}
			}
			$(".grade").html(str);
			$(".portrait").find("img").attr("src",obj.mbPhoto);
			$(".comment").html(obj.commDesc);
			$(".impress").html(obj.commLevel);
			var commtPhoto=obj.commtPhoto.split(",");
			
			for(var i=0;i<commtPhoto.length;i++){
				var commt=commtPhoto[i].split("|");
				$commt_photo_box=$("<div style='height:500px;'></div>");
				$commt_desc=$("<div style='height:100%;float:left;width:50%;display: table;'><div style='display:table-cell;vertical-align:middle;text-align:center;width:100%;padding:0% 20%;font-size:25px;text-indent:2em;font-weight:blod;font-family:Fantasy;color:#c30000;'>"+commt[1]+"</div></div>");
				$commt_photo=$("<div style='height:100%;float:left;width:50%;'><img style='height:500px' src='"+commt[0]+"'/></div>");
				if(i%2==0){
					$commt_desc.find("div").css("padding","0% 0% 0% 38%");
					$commt_photo.find("img").css("margin-left","-16%");
					$commt_photo_box.append($commt_desc);
					$commt_photo_box.append($commt_photo);
				}else{
					$commt_desc.find("div").css("padding","0% 38% 0% 0%");
					$commt_photo.find("img").css("margin-left","17%");
					$commt_photo_box.append($commt_photo);
					$commt_photo_box.append($commt_desc);
				}
				if(commtPhoto.length<=0)$(".photo").append("<div style='height:500px;border-bottom: 1px solid #d6d6d6;'>没有上传图片</div>")
				$(".photo").append($commt_photo_box);
			}
			
		},
		error(){
			ealert("出现了一些莫名奇妙的问题")
		}
	})
}