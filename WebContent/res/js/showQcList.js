var curPage = 1; // 当前页
var pageNum = 0; // 总页数
var limit = 10;
var url =""; // 查询展示产品和图片Action地址
var data =new Object(); // 查询参数

/**
 * 初始化加载
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function initLoad(){
	console.clear();
	console.info("onclick：initLoad 初始化加载");
	data.curPage = curPage;
	data.limit = limit;
	loadProduct(url,data);
	
}

/**
 * 关键字搜索
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function searchKey(){
	console.clear();
	console.info("onclick：searchKey 关键字搜索");
	curPage=1; // 从第一页开始加载
	data.curPage = curPage;
	data.limit = limit;
	data.keyword = $("#seek-key").val(); //输入的关键字
	loadProduct(url,data);
}

/**
 * 查询不到产品处理
 * @author 罗海兵
 * @dateTime 2018年09月07日下午0:47:42
 * @version 1.0
 */
function noFound(){
  console.info("onclick：noFound 查询不到产品处理");
  var noTip =""+
  "<div id='noFound'' align='center'>"+
  /* "<img style='margin-top:5%;' src='res/images/none_order.jpg' />"+
   "<a style='display:block;font-size:16px;color:blue;padding:20px 0px;' href='pages/back/login.jsp'>前往后台维护商品</a>"+*/
  	"查不到数据"+
  "</div>";
  $("#showList").append(noTip);	
}

/**
 * 正在查询商品
 * @author 罗海兵
 * @dateTime 2018年09月10日下午0:47:42
 * @version 1.0
 */
function onFound(){
  console.info("onclick：onFound 正在查询商品");
  $("#show").find("li").remove();
  $("#noFound").remove();
  var noTip =""+
  "<div id='onFound' class='onFound' align='center'>"+
  /* "<img style='margin-top:5%;' src='res/images/none_order.jpg' />"+
   "<a style='display:block;font-size:16px;color:blue;padding:20px 0px;' href='pages/back/login.jsp'>前往后台维护商品</a>"+*/
  	"正在查询商品"+
  "</div>";
  $("#showList").append(noTip);	
}

/**
 * 下一页
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function nextPage(){
	console.info("onclick：nextPage 下一页");
	if(curPage+1<=pageNum){
		curPage++;
		$("#curPage").val(curPage);
		initLoad();
	}else{
		swal("不要再点了，这已经是最后一页！");
	}
}

/**
 * 上一页
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function prvePage(){
	console.info("onclick：prvePage 上一页");
	if(curPage-1>0){
		curPage--;
		$("#curPage").val(curPage);
		initLoad();
	}else{
		swal("不要再点了，这已经是第一页！");
	}
}

/**
 * 第一页
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function firstPage(){
	console.info("onclick：firstPage 第一页");
	if(curPage!=1){
		curPage=1;
		$("#curPage").val(curPage);
		initLoad();
	}else{
		swal("不要再点了，这已经是最后一页！");
	}
}

/**
 * 最后一页
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function lastPage(){
	console.info("onclick：lastPage 最后一页");
	if(curPage!=pageNum){
		curPage=pageNum;
		$("#curPage").val(curPage);
		initLoad();
	}else{
		swal("不要再点了，这已经是最后一页！");
	}
}

/**
 * 加载产品
 * @param url 查询展示产品和图片Action地址
 * @param data 查询参数
 * @author 罗海兵
 * @dateTime 2018年11月9日下午4:53:54
 * @version 1.0
 */
function loadProduct(url,data){
	console.info("onclick：loadProduct 加载产品");
	console.info("正在访问："+url);
	
	onFound(); // 提示正在查询
	$.post(url,data,function(reulst){
		$("#onFound").remove(); // 查询完毕，删除提示
		curPage = reulst.curPage;
		pageNum = reulst.pageNum;
		var total = reulst.total;
		$("#curPage").attr("max", pageNum);
		$("#total").text(total);
		$("#pageNum").text(pageNum);
		if(reulst.list){
			var arr = reulst.list;
			liImgLoader(arr);//生成li元素列表
		}else{
			noFound(); // 提示查询不到数据
		}
		
	},"json");
}



/**
 * 产品展示列表的li元素生成器
 * @see li元素内含img
 * @param arr 产品展示的集合
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:54:34
 * @version 1.0
 */
function liImgLoader(arr){
	console.info("onclick：liImgLoader 正在加载产品信息");
	console.log("本页有产品"+arr.length+"个"); 
	for(var i=0;i<arr.length;i++){//循环产品展示的集合
		var obj=arr[i];//从集合中取出元素
		var stId=$(".links-li li.active a").attr("data-value");
		$("#show").append(
			"<li>"+
				"<a href='pages/front/info.jsp?proId="+obj.proId+"&stId="+stId+"&qc=1' target='_blank'>"+
					"<div class='img'>"+
						"<img alt='"+obj.proName+"' onError='imgError(this)' src='"+obj.imgPath+"'>"+
					"</div>"+
					"<div class='text'>"+
						"<div class='name'>"+obj.proName+"</div>"+
						"<div class='old-price'>市场价：<em>"+obj.proPrice+"</em></div>"+
						"<div class='new-price'>清仓抢购价：<em>"+obj.proPrice0+"</em></div>"+
					"</div>"+
				"</a>"+
			"</li>"	
		);
	}
}


/**
 * 页面底部的商品查询
 * @param type
 * @returns
 */
function buttomQueryproduct(typeId){
	var stId=$(".links-li li.active > a").attr("data-value");
	var data = {
			stId:stId,
			ptId:'f19e3e8ec60f11e78c1100163e02c911',
			typeId:typeId 
	};
	queryProduct(data);
	
	document.body.scrollTop = document.documentElement.scrollTop = 0;
	
}