/**
 * 创建一个XMLHttpReqeust对象
 * @returns 返回一个XMLHttpReqeust对象
 * @author 罗海兵
 * @dateTime 2017年11月16日下午6:50:31
 * @version 1.0
 */
function createXMLHttpReqeust (){	
	var xmlHttp;	
	if(window.ActiveXObject){	//判断是否为IE
		xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	}else if(window.XMLHttpRequest){		
		xmlHttp=new XMLHttpRequest();
	}		
	return xmlHttp;
}

/**
 * 同步Ajax请求
 * @param url 需要请求的URL地址
 * @param data 传入的参数
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月16日下午6:51:00
 * @version 1.0
 */
function Ajax(url,data){
	//使用Ajax技术向后台发送数据
	var val="";
	var xmlhttp=createXMLHttpReqeust();
	xmlhttp.open("POST",url+"?t="+Math.random(),false);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4){//是否发送成功
			if(xmlhttp.status==200){//判断Http状态响应码 200 OK 404 Not Found 500 Server errors 等等
				val=xmlhttp.responseText;
			}else{
				alert("发生错误："+xmlhttp.statusText);
			}
		}
	};//指定回调方法
	xmlhttp.send(data);//发送到服务器的参数
	return val;
};

/**
 * 验证对象
 * @see validator 验证方法
 * @see message 错误提示
 */
var validate={
	url: {
		validator: function (value, param) {
			var url=param[0];
			var data=param[1]+"="+value;
			return Ajax(url,data)=="0";
		},
		message: '已经存在'
    },
	required: {
			validator: function (value, param) {
	        return value;
	      },
	      message: '不能为空'
	    },
	number: {
		validator: function (value, param) {
        return /^[0-9]+.?[0-9]*$/.test(value);
      },
      message: '只允许数字'
    },
    eamil: {
		validator: function (value, param) {
        return /^[a-z0-9._%-]+@([a-z0-9-]+\.)+[a-z]{2,4}$/.test(value);
      },
      message: '邮箱格式不对'
    },
    greater:{
    	validator: function(value,param){  
    		return value.length >= param[0];
          },
          message: '长度必须大于{0}'
    },
	length:{
      validator: function(value,param){  
    	if(param.length==1) return value.length >= param[0];
        return value.length >= param[0] && value.length <= param[1];
      },
      message: '长度必须介于{0}和{1}之间'
    },
	loginName:{
      validator: function (value, param) {
        return /^[\u0391-\uFFE5\w]+$/.test(value);
      },
      message: '只允许汉字、英文字母、数字及下划线'
    },
    password:{
        validator: function (value, param) {
          return /^[\w]+$/.test(value);
        },
        message: '英文字母、数字及下划线'
     },
    equalTo: {
    	validator: function (value, param) {
			var pwdVal=document.querySelector(param).value;
			return value==pwdVal;
	    },
	    message: '和密码不一致'
	 },
	 equalTo2: {
		validator: function (value, param) {
			var pwdVal=document.querySelector(param).value;
			return value!=pwdVal;
	    },
	    message: '不能重复'
	 },
    phone: {
		validator: function(value, param){
			return /^1[34578]\d{9}$/.test(value);
		},
		message: '不正确'
    }
};

function isValid(its){
	var flag=true;
	for(var i=0;i<its.length;i++){
		var e = document.createEvent("HTMLEvents");
		e.initEvent("blur", true, true);　　//这里的click可以换成你想触发的行为
		its[i].dispatchEvent(e);　　　//这里的clickME可以换成你想触发行为的DOM结点
		var state=$(its[i]).next().text();
		if(state) flag=false;
	}
	return flag;
}