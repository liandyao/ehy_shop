/**
 * 加载公共新闻
 * @param id 站点ID
 * @returns
 * @author 刘东
 */
function shownews(id){
	var url="news/back/shownews.action";
	var data={"station":id};
	$(".notice .title > a").attr("href","pages/front/news.jsp?stId="+id);
	$.post(url,data,function(arr){
		var ul=$(".menu > .notice > ul")[0];
		$(ul).find("li:not(:first-child)").remove();
		for(var i=0;i<arr.length;i++){
			//字符串省略
			var str=arr[i].newsTitle;
			var s=str;
			if(str.length>8){
				  s=str.substring(0,8)+"...";
				}
			//时间截取
			var str =arr[i].optime ; 
			var sj=str.substring(5,10);
			$(ul).append(
				"<li><a onclick='show(this,&apos;"+arr[i].newsId+"&apos;);'>"+s+"</a><span align='right'>"+sj+"</span></li>"	
			);
		}
	},"json");
}

//公告详细
function show(obj,id){
	$("#shareDivs").css("display","block");
	$("#hide_div").css("display","block");
	var url="news/back/findById.action";
	var data={"newsId":id};
	$.post(url,data,function(mes){
		$("#div").html("<hr /><p><h1>"+mes.newsTitle+"</h1></P>"+
		"<font color='#6d6d6d' id='ziti'>时间:"+mes.optime+"&nbsp;&nbsp;&nbsp;作者:"+mes.oper+"</font><div class='divcss5-4'></div><br />");
        $("#div2").html("<font color='#6d6d6d' id='ziti'>"+mes.newsContent+"</font>");
	},"json");
}

/**
 * 加载站点的产品类型 
 * @param id 站点ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:53:32
 * @version 1.0
 */
function loadSuperiorType(id){
	console.info("onclick：loadSuperiorType 加载站点的产品类型");
	var url="frontShow/front/findByStId.action";
	var data={"stId":id};
	var li=$("#superior")[0];
	$(".menu > .box > .list > ul > li:nth-child(n+2)").not("#price-range").remove();
	$(li).find("a,div").remove();
	$.post(url,data,function(arr){
		 liLoader(li,arr);
		 $(li).find("a").first().click();
		 if(arr.length>0){
			 $(".opt > div.text > span").text(arr[0].typeName);
			 loadImages3(id);
		 }
	},"json");
}

/**
 * 加载站点的产品款式
 * @param id 产品类型ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:53:23
 * @version 1.0
 */
function loadSubordinateType(id){
	console.info("onclick：loadSubordinateType 加载站点的产品款式");
	var url="frontShow/front/findByPtId.action";
	var data={"ptId":id};
	var li=document.createElement("li");
	li.id="subordinate";
	$("#price-range select").prop('selectedIndex', 0);
	$.post(url,data,function(map){
		var arr=map.subordinateList;
		var typeName=$("#superior > a").first().text();
		$(li).html("<b>"+typeName+"款式：</b>");
		$("#superior").after(li);
		liLoader(li,arr);
		loadSpecification(map.specificationList);
	},"json");
}


/**
 * 首页初始化加载
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:53:10
 * @version 1.0
 */
function indexInit(){
	console.info("onclick：indexInit 首页初始化加载");
	//注册产品信息分类<a>点击事件
	/*$(".menu > .box > .list > ul").on("click","li a",function(){
		console.info("onclick：indexInit 产品信息分类点击");
		$(this).parents("li").find("a").removeAttr("class");
		$(this).addClass("active");
	});*/
	//注册产品属性点击<a>点击事件
	$(".menu > .box > .list > ul").on("click","li:nth-child(n+2) > a",function(){
		$(this).parents("li").find("a").removeAttr("class");
		$(this).addClass("active");
		console.info("onclick：indexInit 产品属性点击");
		itemClick();
	});
	//注册价格范围的<a>点击事件
	$(".menu > .box > .list > ul").on("change","#price-range",function(){
		console.info("onclick：indexInit 价格范围的点击");
		itemClick();
	});
	//已选择的'x'号点击事件
	$(".opt > div.text > i").click(function(){
		$("#tjsp").remove();
		console.clear();
		console.info("onclick：indexInit 'x'号点击");
		var pt=$(".box > .list > ul > li:first-child > a.active")[0];
		$(".opt > div.text > span").text(pt.title);
		$(".opt > div.text > input[name='ptId']").val($(pt).attr("data-value"));
		$(".menu > .box > .list > ul > li:not(:first-child) > a").removeAttr("class");
		var stId=$(".links-li li.active > a").attr("data-value");
		$("#price-range select").prop('selectedIndex', 0);
		loadImages(stId);
	});
	
	//注册产品信息分类第一个li的<a>点击事件
	$(".menu > .box > .list > ul").on("click","li:first-child a",function(){
		console.info("onclick：indexInit 产品信息分类第一个li的点击");
		$(this).parents("li").find("a").removeAttr("class");
		$(this).addClass("active");
		$(".menu > .box > .list > ul > li:nth-child(n+2)").not("li:last-child").remove();//删除产品类型和价格选择之外的所有li
		var id=$(this).attr("data-value");//得到a标签中的存放的产品类型ID
		loadSubordinateType(id);//加载产品类型的下级分类(产品款式)
	});
	
	//给搜索框添加键盘回车点击事件
	$(document).on("keydown","#seek-key",function(event){
		if(event.keyCode == "13" && this.value){  
			console.clear();
			console.info("onclick：searchKey 搜索框添加键盘回车的点击");
			searchKey();//当搜索框得到焦点并且关键字不为空时, 触发搜索按钮点击事件
	    }
	});
}


/**
 * 属性点击事件
 * @see 点击属性,查询产品
 * @see 点击[圆头],查询[鞋头款式]是[圆头]的[成都站][女鞋]
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:52:55
 * @version 1.0
 */
function itemClick(){
	console.clear();
	console.info("onclick：itemClick 属性点击事件");
	var ul=$('#tjsp').find("ul")[0];
	$(ul).find("li").remove();
	onFound($("#tjsp")[0]);
	$("#cartList").remove();
	var stId=$(".links-li li.active > a").attr("data-value");
	var pt=$(".menu > .box > .list > ul > li:first-child > a.active")[0];
	var ptId=$(pt).attr("data-value");
	var ptName=pt.title;
	var typeId=$("li:nth-child(2) a.active").attr("data-value");
	var typeName=$("li:nth-child(2) a.active").attr("title") || "";
	var arr=$(".menu > .box > .list > ul > li:nth-child(n+3) a.active");
	var spValues=ptName+" "+typeName;
	var spNames = new Array();
	for(var i=0;i<arr.length;i++){
		var title=arr[i].title;
		spValues+=" "+title;
		spNames[i]=title;
	}
	var priceRange=$("#price-range > select").val();
	var data={"stId":stId,"ptId":ptId,"typeId":typeId,"spValues":spNames};
	if(priceRange){ 
		spValues+=" "+priceRange
		var priceArr=priceRange.split("-");
		if(priceArr.length>1){
			data.minPrice=priceArr[0];
			data.maxPrice=priceArr[1];
		}else{
			data.maxPrice=priceArr[0];
		}
	};
	$(".opt > div.text > span").text(spValues);
	queryProduct(data);
	
}

/**
 * 查询商品
 * @param data
 * @returns
 */
function queryProduct(data){
	var ul=$('#tjsp').find("ul")[0];
	$(ul).find("li").remove();
	onFound($("#tjsp")[0]);
	$("#cartList").remove();
	
	var url="frontShow/front/searchAll.action";
	console.info("正在访问："+url);
	$.post(url,data,function(arr){
		$(".onFound").remove();
		liImgLoader(ul,arr); 
	},"json");
	
	//隐藏轮播
	hideLunbo();
}


/**
 * 搜索框点击事件
 * @see 输入关键字[平底鞋],查询[产品名称]含有关键字[平底鞋]的[成都站][女鞋]
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:52:38
 * @version 1.0
 */
function searchKey(){
	console.clear();
	console.info("onclick：searchKey 搜索框点击事件");
	var str = document.getElementById('seek-key').value.replace(/\s+/g,"").trim();    
    if(str.length==0){    
    	console.info("搜索关键字为空，不发往后台！");
        return;//请将“文本框”改成你需要验证的属性名称!    
    } 
    $("#tjsp > ul").find("li").remove(); 
    onFound($("#tjsp")[0]); // 提示正在查询产品
	$("#cartList").remove();
	//$('div.show-list:eq(1)').remove();
	var stId=$(".links-li li.active > a").attr("data-value");
	var ptId=$(".menu > .box > .list > ul > li:first-child > a.active").attr("data-value");
	var proName=$("#seek-key").val();
	var url="frontShow/front/searchAll.action";
	var data={"stId":stId,"ptId":ptId,"proName":proName};
	console.info("正在访问："+url);
	$.post(url,data,function(arr){
		$(".onFound").remove();
		var ul=$('#tjsp').find("ul")[0];
		liImgLoader(ul,arr); 
	},"json");
	
	//隐藏轮播
	hideLunbo();
	
}

/**
 * 隐藏轮播
 * @returns
 */
function hideLunbo(){
	$("#lunBo").hide();
}


/**
 * 产品类型和产品款式的li元素HTML生成器
 * @param li 需要生成HTML代码的元素节点
 * @param arr 需要加载的json集合(产品类型集合或者产品款式集合)
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:52:22
 * @version 1.0
 */
function liLoader(li,arr){
	console.info("onclick：liLoader 产品类型和产品款式的li元素HTML生成器");
	for(var i=0;i<arr.length;i++){
		var obj=arr[i];
		$(li).append(
			"<a href='javascript:void(0);' title="+obj.typeName+" data-value="+obj.typeId+">"+
			obj.typeName+"</a><div class='line-height'></div>"
		);
	}
	$(li).find("div.line-height:last-child").remove();
}

/**
 * 产品规格的li元素生成器
 * @param arr 产品规格的集合
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:54:46
 * @version 1.0
 */
function loadSpecification(arr){
	console.info("onclick：loadSpecification 产品规格的li元素生成器");
	var ul=$(".menu > .box > .list > ul")[0];
	
	for(var i=0;i<arr.length && i<5;i++){
		var spt=arr[i];
		var li=document.createElement("li");
		$("#price-range").before(li);
		$(li).append("<b>"+spt.sptName+"：</b>");
		for(var j=0,list=spt.spList;j<list.length && j<15;j++){
			var obj=list[j];
			$(li).append(
				"<a href='javascript:void(0);' title="+obj.spValue+" onclick=&apos;"+obj.spId+"&apos;>"+
				obj.spValue+"</a><div class='line-height'></div>"
			);
		}
		$(li).find("div.line-height:last-child").remove();
	}
}

/**
 * 产品展示列表的li元素生成器
 * @see li元素内含img
 * @param arr 产品展示的集合
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:54:34
 * @version 1.0
 */
function liImgLoader(ul,arr){
	console.info("onclick：liImgLoader 正在加载产品信息");
	if(!ul){ // "",null,undefined,NaN
        console.log("ul为空"); 
        noFound($("div.show-list:first")[0]);
    }
	
	$(ul).find("li").remove();//删除ul下面的所有li
	if(typeof arr === 'undefined'){
		console.info("arr的结果是："+arr);
		//alert("集合是空的， ==== "+arr);
	}
	if(!arr || arr.length==0){ // "",null,undefined,NaN
        console.log("产品集合为空"); 
        noFound($(ul).parent("div")[0]);
        return;
    }else{
    	console.log("查询到产品"+arr.length+"个"); 
    }
	for(var i=0;i<arr.length;i++){//循环产品展示的集合
		var obj=arr[i];//从集合中取出元素
		var stId=$(".links-li li.active a").attr("data-value");
		$(ul).append(
			"<li>"+
				"<a href='pages/front/info.jsp?proId="+obj.proId+"&stId="+stId+"' target='_blank'>"+
					"<div class='img'>"+
						"<img alt='"+obj.proName+"' onError='imgError(this)' src='"+obj.imgPath+"'>"+
					"</div>"+
					"<div class='text'>"+
						"<div class='name'>"+obj.proName+"</div>"+
						"<div class='old-price'>市场价：<em>"+obj.proPrice+"</em></div>"+
						"<div class='new-price'>厂家直销价：<em>"+obj.proPrice0+"</em></div>"+
					"</div>"+
				"</a>"+
			"</li>"	
		);
	}
}

  
/**
 * 加载图片失败处理
 * @param image 图片元素
 * @author 罗海兵
 * @dateTime 2018年09月07日下午0:37:44
 * @version 1.0
 */
function imgError(image){
	console.info("onclick：imgError 加载图片失败");
    $(image).hide();  
}

/**
 * 查询不到产品处理
 * @author 罗海兵
 * @dateTime 2018年09月07日下午0:47:42
 * @version 1.0
 */
function noFound(div){
  console.info("onclick：noFound 查询不到产品处理");
  $("#onFound").remove();// 删除正在查询商品提示
  var noTip =""+
  "<div id='noFound' align='center'>"+
  /* "<img style='margin-top:5%;' src='res/images/none_order.jpg' />"+
   "<a style='display:block;font-size:16px;color:blue;padding:20px 0px;' href='pages/back/login.jsp'>前往后台维护商品</a>"+*/
  	"查不到数据"+
  "</div>";
  $(div).append(noTip);	
}

/**
 * 正在查询商品
 * @author 罗海兵
 * @dateTime 2018年09月10日下午0:47:42
 * @version 1.0
 */
function onFound(div){
  console.info("onclick：onFound 正在查询商品");
  $("#noFound").remove(); // 删除查不到提示
  var noTip =""+
  "<div id='onFound' align='center'>"+
  /* "<img style='margin-top:5%;' src='res/images/none_order.jpg' />"+
   "<a style='display:block;font-size:16px;color:blue;padding:20px 0px;' href='pages/back/login.jsp'>前往后台维护商品</a>"+*/
  	"正在查询商品"+
  "</div>";
  $(div).append(noTip);	
}
	
/**
 * 加载图片
 * @param id 站点ID
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:53:54
 * @version 1.0
 */
function loadImages(id){
	console.info("onclick：loadImages 加载图片");
	var url="frontShow/front/showAllByType.action";//查询展示产品和图片Action地址
	var data={"maxResult":8,"stId":id,"showType":1};//查询参数
	var div=document.createElement("div");
	$("#main .content:first > .search").after(div);
	$(div).attr("id", "tjsp");
	$(div).addClass("show-list");
	$(div).html("" +
			"<div class='box_hd'>" +
				"<h3 class='box_tit'>厂家推荐</h3>" +
				"<i class='box_hd_arrow'></i>" +
				"<span class='box_subtit'>最火热的推荐产品</span>" +
			"</div>" +
		"<ul></ul>");
	var ul = $(div).find("ul:first")[0];
	$(ul).find("li").remove();
	$.post(url,data,function(map){ 
			//$(div).find("h3.box_tit").text(map.type);
			var list = map.list;
			liImgLoader(ul,list);//生成li元素列表
			 
	},"json");
}

/**
 * 加载图片
 * @param id 站点ID
 * @author 罗海兵
 * @dateTime 2017年11月9日下午4:53:54
 * @version 1.0
 */
function loadImages3(id){
	console.info("onclick：loadImages3 加载幸运产品");
	var ptId=$(".menu > .box > .list > ul > li:first-child > a.active").attr("data-value");
	var url="frontShow/front/show2.action";//查询展示产品和图片Action地址
	var data={"maxResult":20,"stId":id,"ptId":ptId};//查询参数
	
	var div=document.createElement("div");
	$(div).attr("id","lunBo");
	$(div).addClass("show-list");
	$(div).attr("style","position: relative;");
	$("#main .content:first").append(div);
	$(div).html("" +
			"<div class='slider_control_box'>" +
				"<span class='slider_control slider_control_left'><i class='glyphicon glyphicon-menu-left'></i></span>" +
				"<span class='slider_control slider_control_right'><i class='glyphicon glyphicon-menu-right'></i></span>" +
			"</div>" +
			"<div class='box_hd'>" +
				"<h3 class='box_tit'>系统推荐</h3>" +
				"<i class='box_hd_arrow'></i>" +
				"<span class='box_subtit'>这是轮播产品</span>" +
			"</div>" +
		"<ul></ul>" +
		"<div class='slider_indicators'>" +
			"<i class='slider_indicators_btn'></i><i class='slider_indicators_btn'></i><i class='slider_indicators_btn'></i><i class='slider_indicators_btn'></i>" +
		"</div>");
	var ul = $(div).find("ul:first")[0];
	$(ul).find("li").remove();
	console.info("正在访问："+url);
	$.post(url,data,function(arr){
		if(arr && arr.length>0){
			liImgLoader(ul,arr);//生成li元素列表
			
		}
		
	},"json");
}


/**
 * 页面底部的商品查询
 * @param type
 * @returns
 */
function buttomQueryproduct(typeId){
	var stId=$(".links-li li.active > a").attr("data-value");
	var data = {
			stId:stId,
			ptId:'f19e3e8ec60f11e78c1100163e02c911',
			typeId:typeId 
	};
	queryProduct(data);
	
	document.body.scrollTop = document.documentElement.scrollTop = 0;
	
}