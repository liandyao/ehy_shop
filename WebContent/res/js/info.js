$(function(){
	$.get("pages/front/memberLogin.jsp",function(data){
		$("#loginModal").html($(data).filter("#loginModal").html());
		$("#loginModal").on("click","span.modal-close",function(){
			hide();
		});
	});
	$.get("pages/front/comment.jsp",function(data){
		$("#comment").html($(data).filter("#comment").html());
	});
	$("#loginModal").on("click","#logins",function(){
		 	var url = "member/front/login.action";
			var data =$("#login_form").serialize();
			$.post(url,data,function(info){
				if(info.state==1){
					$("#loginModal .tips-error").css("display","none");
					$("#loginModal .tips-error").text("");
					ehy.isLogin=true;
					hide();
					$(".login-regist").html(str);
					//addCart();
					if(btnIndex==0){
						$(".cart.btn.btn-warning").click();
					}else{
						addShelf();
					}
				}else{
					$("#loginModal .tips-error").removeAttr("style");
					$("#loginModal .tips-error").text("用户名或者密码错误");
				}
			})
	 });
	infoInit();//产品详情页面初始化
});

var proPrice=new Object();//定义一个产品规格价格的对象
var pspId="";//定义一个产品规格价格的ID
var spNames="";
var btnIndex=0;
var str=" "+
	"<span class='glyphicon glyphicon-user'></span>"+
	"<a target='_blank' style='position: relative;' href='pages/front/UserCenter/userCenter.jsp'>个人中心</a>"+
	"<div class='header_account_hover' align='center'>"+
		"<span class='spanSJX'></span>"+
		"<a href='#'>"+
			"<i style='background-position:0 115px;'></i>"+
			"我的订单"+
		"</a>"+
		"<a href='#'>"+
			"<i style='background-position:0 93px;'></i>"+
			"售后服务"+
		"</a>"+
		"<a href='#'>"+
			"<i style='background-position:0 70px;'></i>"+
			"我的优惠"+
		"</a>"+
		"<a href='frontShow/front/logout.action'>"+
			"<i style='background-position:0 51px;'></i>"+
			"退出账户"+
		"</a>"+
	"</div>";

/**
 * 显示登陆窗口
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:45:15
 * @version 1.0
 */
function show(){
	document.getElementById("loginModal").style="display:block;";
	document.getElementById("curtain").style="display:block;";

}

/**
 * 隐藏登陆窗口
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:45:52
 * @version 1.0
 */
function hide(){
	document.getElementById("loginModal").style="display:none;";
	document.getElementById("curtain").style="display:none;";
}

/**
 * 加入购物车
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:46:03
 * @version 1.0
 */
function addCart(e){
	if(!ehy.isLogin){
		btnIndex=0;
		show();
		return;//如果没有登陆，则显示登陆窗口
	}
	if(pspId==""){ 
		alert("规格价格为空,后台维护出错"); 
		window.location.href="pages/common/error.html";
		return ;
	}
	var stName=$(".links-li li.active > a").text();//取出当前的站点名称
	var proId=getParam("proId");//从地址栏获取产品ID
	var proName=$("#proName").text();//取出产品名称
	var proFactoryPrice=$("#proFactoryPrice").text().substr(1);//获取产品出厂价
	var proPrice=$("#proPrice").text().substr(1);//获取产品市场价
	var cartNum=$("#cartNum").text();//获取购买数量
	var spValues=$("#spValues").text();//获取已选择的产品规格
	var key=spValues.replace(/[ ]/g,"@");//将产品规格字符串的空格符' '全部替换成分隔符'@'
	var url="cart/front/add.action";//定义加入购物请求的action地址
	var data={
			"stName":stName,
			"proId":proId,
			"proName":proName,
			"cartPrice":proPrice,
			"cartNum":cartNum,
			"spNames":spNames,
			"pspId":pspId
	};//定义请求发送的参数
	
	//执行Ajax请求,得到执行结果
	$.post(url,data,function(info){
		if(info>0){//如果返回值大于0,则加入购物车成功
			// 元素以及其他一些变量
			var eleFlyElement = document.querySelector("#flyItem"),
			    eleShopCart = document.querySelector("#shopCart");
			var numberItem = 0;
			// 抛物线运动
			var myParabola = funParabola(eleFlyElement, eleShopCart, {
			    speed: 400, //抛物线速度
			    curvature: 0.0008, //控制抛物线弧度
			    complete: function() {
			        eleFlyElement.style.visibility = "hidden";
			        eleShopCart.querySelector("span").innerHTML = ++numberItem;
			    }
			});
			var event = e || window.event;
			var src = $(".max-img > img").attr("src");
            $("#flyItem").find("img").attr("src", src);
            // 滚动大小
            var scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft || 0,
                scrollTop = document.documentElement.scrollTop || document.body.scrollTop || 0;
            eleFlyElement.style.left = event.clientX + scrollLeft + "px";
            eleFlyElement.style.top = event.clientY + scrollTop + "px";
            eleFlyElement.style.visibility = "visible";

            // 需要重定位
            myParabola.position().move();
            
			var span=$("#toolbar span.number")[0];//得到工具栏的购物车节点
			var num=parseInt($(span).text());//得到购物车数量
			$(span).text(num+1);//购物车数量加1
			$("#shopCart span.txt").css("display","none");
		}else{//否则，加入购物车失败
			alert("未登录,加入购物车失败");
		}
	});
	
}


/**
 * 加入购物车
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:46:03
 * @version 1.0
 */
function addOrder(e){
	if(!ehy.isLogin){
		btnIndex=0;
		show();
		return;//如果没有登陆，则显示登陆窗口
	}
	if(pspId==""){ 
		alert("规格价格为空,后台维护出错"); 
		window.location.href="pages/common/error.html";
		return ;
	}
	var stName=$(".links-li li.active > a").text();//取出当前的站点名称
	var proId=getParam("proId");//从地址栏获取产品ID
	var sheId=getParam("sheId");//从地址栏获取货架ID
	var proName=$("#proName").text();//取出产品名称
	var proFactoryPrice=$("#proFactoryPrice").text().substr(1);//获取产品出厂价
	var proPrice=$("#proPrice").text().substr(1);//获取产品市场价
	var cartNum=$("#cartNum").text();//获取购买数量
	var spValues=$("#spValues").text();//获取已选择的产品规格
	var key=spValues.replace(/[ ]/g,"@");//将产品规格字符串的空格符' '全部替换成分隔符'@'
	var url="order/front/addOrder.action";//定义加入购物请求的action地址
	var data={
			"stName":stName,
			"proId":proId,
			"proName":proName,
			"cartPrice":proPrice,
			"cartNum":cartNum,
			"spNames":spNames,
			"pspId":pspId,
			"sheId":sheId
	};//定义请求发送的参数
	
	//执行Ajax请求,得到执行结果
	$.post(url,data,function(info){
		if(info.state>0){//如果返回值大于0,则加入购物车成功
			// 获取协议
			var http = window.location.protocol;
			//获取域名
			var host = window.location.host;
			//获取主机地址之后的目录如：/Tmall/index.jsp
			var pathName=window.document.location.pathname;
			//获取带"/"的项目名，如：/Tmall
			var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1); 
			var newUrl = http+"//"+host+projectName+"/pages/front/cart2.jsp?"+info.mes;
			window.location.href=newUrl;
		}else{//否则，加入购物车失败
			alert("未登录,加入购物车失败");
		}
	});
	
}


/**
 * 加入货架
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:56:02
 * @version 1.0
 */
function addShelf(){
	if(!ehy.isLogin){
		btnIndex=1;
		show();
		return;//如果没有登陆，则显示登陆窗口
	}
	//alert("加入货架");
	var url="shelves/front/add.action";
	var data={"proId":getParam("proId"),"proStName":$(".links-li li.active a").attr("title")};
	$.post(url,data,function(info){
		if(info.state==0){
			swal(info.mes);
		}else if(info.state==1){
			swal("加入货架成功!");
		}
	},"json");
	
}

/**
 * 初始化加载
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:27:33
 * @version 1.0
 */
function infoInit(){
	//注册小图片点击事件
	$("div.min-img").on("click","li",function(){
		$("div.min-img li").removeClass("active");
		$(this).addClass("active");
		var src=$(this).find("img").attr("src");
		$("div.max-img > img").attr("src",src);
	});
	
	//注册规格点击事件
	$("#details").on("click",".specification > .list > ul > li",function(){
		$(this).parents("ul").find("li.active").removeAttr("class");
		$(this).addClass("active");
		
		var arr=$(".specification .list ul > li.active");
		var spValues="";
		var str="";
		for(var i=0;i<arr.length;i++){
			spValues+="@"+$(arr[i]).text();
			var sptName=$(arr[i]).parents(".specification").find(".text").text();
			var spName=$(arr[i]).text();
			str+=","+sptName+"："+spName;
		}
		spNames=str.substr(1);
		var key=spValues.substr(1);
		if(proPrice.hasOwnProperty(key)){
			var pro=proPrice[key];
			console.info(pro);
			$("#proFactoryPrice").text("￥"+pro.pspPrice);
			$("#proPrice").text("￥"+(parseFloat(pro.pspFactoryPrice)*1.5).toFixed(2));
			$("#proPriceCB").text("￥"+(parseFloat(pro.pspFactoryPrice)*1.05).toFixed(2));
			pspId=pro.pspId;
			$("#spValues").text(key.replace(/[@]/g," "));
		}
		
	});
	
	//注册标题点击事件
	$(".cont > .title > div").click(function(){
		$(".cont > .title > div").removeClass("active");
		$(this).addClass("active");
		var className=this.className;
		if(className.indexOf("dotey")>-1){
			$("#proInfo").show();
			$("#comment").hide();
		}else{
			$("#proInfo").hide();
			$("#comment").show();
		}
	});
	var proId=getParam("proId");//获取地址栏参数
	loadInfo(proId);//加载宝贝详情
	loadInfoImages(proId);//加载图片
	loadInfoSpecification(proId);//加载规格参数
	loadProAttr(proId);//加载产品属性
	loadInfoImages2(proId);//加载产品图片详情
}

/**
 * 加载产品属性
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:28:39
 * @version 1.0
 */
function loadProAttr(proId){
	var url="frontShow/front/findAttrByProId.action";
	var data={"intercept":true,"proId":proId};
	$.post(url,data,function(arr){//发送Ajax请求
		for(var i=0;i<arr.length;i++){
			var attrText=arr[i].attrName+"："+arr[i].attrValue;
			$("#attList").append("<li>"+attrText+"</li>");
		}
	});
}

/**
 * 加载产品的规格价格
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:29:34
 * @version 1.0
 */
function loadProPrice(proId){
	var url="frontShow/front/findPriceByProId.action";
	var data={"intercept":true,"proId":proId};
	$.post(url,data,function(map){//发送Ajax请求
		proPrice=map;
		$("div.specification li").click();
		$("div.specification li:first-child").click();
	});
}

/**
 * 增加或者减少购买数量
 * @param obj 被点击的元素节点
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:30:17
 * @version 1.0
 */
function addOrCut(obj){
	var span=document.querySelector(".number > .box > .num");//得到产品数量所在的文档节点
	var sign=obj.innerText;//获取点击的符号
	var num=parseInt(span.innerText);//将产品数量转化成整数型数字
	if(num==1 && sign=="-") return;//如果数量等于'1'并且点击的符号是'-',则直接返回
	if(sign=="+"){//如果点击的是'+'号
		span.innerText=num+1;//产品数量+1
	}else if(sign=="-"){//如果点击的是'-'号
		span.innerText=num-1;//产品数量-1
	}
}

/**
 * 加载产品规格
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:30:46
 * @version 1.0
 */
function loadInfoSpecification(proId){
	var url="frontShow/front/findByProId.action";
	var data={"intercept":true,"proId":proId};
	$.post(url,data,function(arr){//发送Ajax请求
		var str="";
		for(var i=0;i<arr.length;i++){
			var spt=arr[i];
			var spList=spt.spNames.split(",");
			var className="";
			if(spt.spRemark){ 
				str+=","+spt.spRemark;
				className="tip";
			}
			$("div#details div.number:first").before(
				"<div class='specification "+className+"'>"+
					"<div class='text'>"+spt.sptName+"</div>"+
					"<div class='list'>"+
						"<ul>"+
							builder(spList)+
						"</ul>"+
					"</div>"+
				"</div>"
			);
		}
		var spRemark=new Object();
		if(str) spRemark=eval("({"+str.substr(1)+"})");
		var liArr=$("#details > .specification > .list li");
		for(var i=0;i<liArr.length;i++){
			var li=liArr[i];
			var key=$(li).text();
			if(spRemark.hasOwnProperty(key)){
				var div=createTooltip(li,spRemark[key]);
				tipToggle(li,div);
			}
			
		}
		$("div.tooltip").css("display","none");
		loadProPrice(proId);//加载规格价格
	},"json");//设置返回参数类型
}

/**
 * 特殊规格的提示切换
 * @param obj 被点击的特殊规格
 * @param div 需要切换隐藏显示的文本容器
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:31:29
 * @version 1.0
 */
function tipToggle(obj,div){
	var ul=$(obj).parents("ul")[0];
	$(ul).find("li").click(function(){
		if($(this).text()==$(obj).text()){
			div.style.display="block";
			var w=obj.offsetWidth;
			var h= obj.offsetHeight;
			var pos=getPost(obj);
			var w2=div.offsetWidth;
			var h2= div.offsetHeight;
			div.style.left=(pos.x-w2/2+w/2)+"px";
			div.style.top=(pos.y+h+6)+"px";
			//alert("w:"+w+", h:"+", x:"+pos.x+", y:"+pos.y);
		}else{
			div.style.display="none";
		}
	});
}

/**
 * 规格的html生成器
 * @param arr 某一规格类型的规格值数组
 * @returns 
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:33:24
 * @version 1.0
 */
function builder(arr){
	var html="";
	for(var i=0;i<arr.length;i++){
		html+="<li>"+arr[i]+"</li>";
	}
	return html;
}

/**
 * 获取某个元素的坐标
 * @param obj 目标元素
 * @returns 返回一个js对象(包含了x坐标和y坐标)
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:34:11
 * @version 1.0
 */
function getPost(obj){
	var x=0,y=0;
    do { 
        x += obj.offsetLeft; 
        y += obj.offsetTop; 
    } while (obj = obj.offsetParent); 
	return {"x":x, "y":y};
}

/**
 * 获取某个元素的坐标
 * @param obj 目标元素
 * @returns 返回一个js对象(包含了x坐标和y坐标)
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:34:11
 * @version 1.0
 */
function getPost2(obj){
	var X= obj.getBoundingClientRect().left+document.documentElement.scrollLeft;
	var Y =obj.getBoundingClientRect().top+document.documentElement.scrollTop;
	return {"x":X, "y":Y};
}

function getPost3(obj){
	var x=getElementLeft(obj);
	var y=getElementTop(obj);
	return {"x":x, "y":y};
}

function getElementLeft(element){
　　　　var actualLeft = element.offsetLeft;
　　　　var current = element.offsetParent;

　　　　while (current !== null){
　　　　　　actualLeft += current.offsetLeft;
　　　　　　current = current.offsetParent;
　　　　}

　　　　return actualLeft;
　　}

　　function getElementTop(element){
　　　　var actualTop = element.offsetTop;
　　　　var current = element.offsetParent;

　　　　while (current !== null){
　　　　　　actualTop += current.offsetTop;
　　　　　　current = current.offsetParent;
　　　　}

　　　　return actualTop;
　　}

/**
 * 特殊规格的提示工具生成器
 * @param obj 需要生成提示的目标节点
 * @param text 提示文本
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:37:26
 * @version 1.0
 */
function createTooltip(obj,text){
	var div=document.createElement("div"); 
	div.innerHTML=" "+
		"<div class='tooltip-content'>"+text+"</div>"+
		"<div class='tooltip-arrow-outer'></div>"+
		"<div class='tooltip-arrow'></div>";
	document.body.appendChild(div);
	div.className="tooltip tooltip-bottom";
	return div;
}

/**
 * 加载产品的基本信息
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月14日下午6:38:30
 * @version 1.0
 */
function loadInfo(proId){
	var url="frontShow/front/findInfoById.action";//设置action请求地址
	var data={"intercept":true,"proId":proId};//设置请求参数
	$.post(url,data,function(map){//发送Ajax请求
		var pro=map;
		var isva=pro.proIsva==0?"<font color='red'>(未上架)</font>":"";
		$("#proName").html(pro.proName+isva);
	},"json");//设置返回参数类型
}

/**
 * 加载产品滚动图片
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月11日上午11:13:46
 * @version 1.0
 */
function loadInfoImages(proId){
	var url="frontShow/front/findImgByProId.action";
	var data={"intercept":true,"proId":proId,"imgType":1};
	$.post(url,data,function(arr){//发送Ajax请求
		var ul=$("div.min-img > ul")[0];
		for(var i=0;i<arr.length && i<6;i++){
			var imgPath=arr[i];
			$(ul).append("<li><img src="+imgPath+"></li>");
		}
		$(ul).find("li:first-child").click();
	},"json");//设置返回参数类型
}

/**
 * 加载产品图片详情
 * @param proId 产品ID
 * @returns
 * @author 罗海兵
 * @dateTime 2017年11月17日下午3:35:39
 * @version 1.0
 */
function loadInfoImages2(proId){
	var url="frontShow/front/findImgByProId.action";
	var data={"intercept":true,"proId":proId,"imgType":2};
	$.post(url,data,function(arr){//发送Ajax请求
		for(var i=0;i<arr.length;i++){
			var imgPath=arr[i];
			$("#imgList").append("<li><img src="+imgPath+"></li>");
		}
	},"json");//设置返回参数类型
}

