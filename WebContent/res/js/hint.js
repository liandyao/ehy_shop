	/*
	 * 使用说明：该js需要在jquery下方引入。提示内容需要传入。时间可不传入（不传入默认为2秒）
	 * 
	 */
	//可设置时间的提示方法
	function ealert(text,time){
		//创建新的div节点使用内联样式
		$div_box=$("<div style='border-radius:25px;text-align:center;display: none;position: fixed;top: 50%;left: 0;right: 0;height:auto;padding:10px 20px;white-space:normal;width:200px;color:white;background-color: black;opacity: 0.5;margin: 0 auto;z-index: 999px;'></div>");	
		/* //创建div容器用于存放显示内容
		$box_div=$("<div style='margin: 10px;padding: 10px 20px;'></div>"); */
		//在页面的body节点下创建盒子节点
		$("body").append($div_box);
		//在box节点下再次创建box节点
		$div_box.append($div_box);
		//在box节点新增内容
		$div_box.text(text);
		//调用jquery的如果显示则动态隐藏如果是隐藏状态就动态显示的方法
		$div_box.fadeToggle(500);
		//如果时间为空则定义默认时间2秒
		if(undefined==time)var time=2000;
		//调用隐藏删除提示框的方法
		deleteHint($div_box,time);
	}
	//隐藏删除提示框的方法
	function deleteHint(divBox,time){
		//定时调用的方法定时调用隐藏的方法
		setTimeout(function(){
			//调用jquery的如果显示则动态隐藏如果是隐藏状态就动态显示的方法
			$(divBox).fadeToggle(1000);
			//当隐藏完成后直接删除这个div
			setTimeout(function(){
				$(divBox).remove();
			},1000)
		},time);
	}