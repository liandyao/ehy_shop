<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,购物车,结算,前台" />
		<meta http-equiv="description" content="E货源网站前台购物车结算、选择收货地址、快递方式页面" />
		<title>E货源--结算页面</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/cart2.css" />
		<link rel="stylesheet" type="text/css" href="res/css/center2.css"/>
		<link rel="stylesheet" type="text/css"  href="res/css/dialog.css">  
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script src="res/js/address.js"></script>
		<script src="res/js/validUtils.js"></script>
		<style type="text/css">
		*{
			 
			user-select:text; 
		}
		</style>
		<script type="text/javascript">
			$(function(){
				$.get("pages/front/dialogDemo.jsp",function(data){
					$("body").prepend($(data).filter("#dialog-confirm").prop("outerHTML"));
					$("body").prepend($(data).filter("#dialog-curtain").prop("outerHTML"));
					$(".dialog-delorder .dialog-content p .dialog-text").text("确定删除此地址？");
					$("#dialog-confirm .console-btn-confirm").removeAttr("onclick");
				},"html");
				cart2Init();
				tabLoad();
				$.get("pages/front/UserCenter/address.jsp",function(data){
					$(document.body).prepend($(data).filter(".layer-box").prop("outerHTML"));
					addrInitLoad();
				},"html");
				$("#address-list").on("click","li span.operation > a:first-child",function(){
					var addId=$("#address-list li.active input").val();
					setForm(addId);
				});
			});
			
			function delAddress(){
				document.getElementById("dialog-curtain").style.display="block";
				document.getElementById("dialog-confirm").style.display="block";
				$(".console-btn-confirm").click(function(){
					var id=$("#address-list li.active input").val();
					var url="address/front/deleteByPrimaryKey.action";
					var data={"addId":id};
					$.post(url,data,function(mes){
						if(mes.state>0){
							btnCancel();
							$("#address-list li.active").remove();
							$("#address-list li label:first").click();
						}else{
							alert("删除失败");
						}
					},"json");
				});
			}
			
			function cart2Init(){
				$("#main .content > .address").on("click","#address-list li label",function(){
					$("#address-list li.active").removeClass("active");
					$(this).parents("li").addClass("active");
				});
				$("#product thead tr:last-of-type").on("click","span",function(){
					$("#product thead tr:last-of-type span").removeClass("active");
					$(this).addClass("active");
				});
				addrInit();
			}
			
			function stItemClick(obj,index){
				$("#product thead tr:last-of-type span").removeAttr("class");
				$(obj).addClass("active");
				$("#product tbody").css("display","none");
				$("#product tbody:nth-of-type("+index+")").removeAttr("style");
			}
			
			function addrInit(){
				var url="frontShow/front/findAddrByMbId.action";
				$.post(url,null,function(arr){
					var li=$("#address-list li")[0];
					$("#address-list li").not("#address-temp").remove();
					for(var i=0;i<arr.length;i++){
						var addId=arr[i].addId;
						var address=arr[i].address;
						$("#address-temp").before(
							"<li>"+
								"<label>"+
									"<span></span>"+
									"<input type='radio' value='"+addId+"' name='addId'/>"+
									"&nbsp;<span class='address'>"+address+"</span>"+
									"<span class='operation'>"+
										"<a href='javascript:void(0);'>修改</a>"+
										"<a href='javascript:void(0);' onclick='delAddress()'>删除</a>"+
									"</span>"+
								"<label>"+
							"</li>"		
						);
					}
					$("#address-list li label:first").click();
				},"json");
			}
			
			
			address.onHide=function(){
				addrInit();
			};
			
			function createOrder(){
				var url="frontShow/front/addOrder.action";
				var data="";
				var eleActive=$("#address-list li.active").find(".address")[0];
				if(eleActive.nodeName=="SPAN"){
					var addrText=eleActive.innerText;
					var arr=addrText.split("，");
					data+="ordMember="+arr[0];
					data+="&ordPhone="+arr[1];
					data+="&ordAddress="+arr[2];
					//alert("收货人："+arr[0]+"\n收货人电话："+arr[1]+"\n收货人地址："+arr[2]);
				}else{
					var addrText=eleActive.value.replace(/[，]/g,",");	
					if(addrText.replace(/(^\s*)|(\s*$)/g, "").length ==0) return alert("收货地址不能为空");
					//alert("临时地址："+addrText);
					data+="ordAddress="+addrText;
				}
				var uri=window.location.href;
				var index=uri.indexOf("?")+1;
				data+="&"+uri.substr(index);
				var mxRemark=$("#message-board").val();
				if(mxRemark) data+="&mxRemark="+mxRemark;
				$.post(url,data,function(info){
					if(info.state>0){
						//alert("增加订单成功");
						window.location.href="pages/front/pay.jsp?ordId="+info.mes;
					}
				},"json");
				
				
			}
			
			function createOrder2(){
				var url="frontShow/front/addOrder2.action";
				var data="";
				var eleActive=$("#address-list li.active").find(".address")[0];
				if(eleActive.nodeName=="SPAN"){
					var addrText=eleActive.innerText;
					var arr=addrText.split("，");
					data+="ordMember="+arr[0];
					data+="&ordPhone="+arr[1];
					data+="&ordAddress="+arr[2];
					//alert("收货人："+arr[0]+"\n收货人电话："+arr[1]+"\n收货人地址："+arr[2]);
				}else{
					var addrText=eleActive.value.replace(/[，]/g,",");	
					if(addrText.replace(/(^\s*)|(\s*$)/g, "").length ==0) return alert("收货地址不能为空");
					//alert("临时地址："+addrText);
					data+="ordAddress="+addrText;
				}
				var uri=window.location.href;
				var index=uri.indexOf("?")+1;
				data+="&"+uri.substr(index);
				var mxRemark=$("#message-board").val();
				if(mxRemark) data+="&mxRemark="+mxRemark;
				$.post(url,data,function(info){
					if(info.state>0){
						//alert("增加订单成功");
						window.location.href="pages/front/pay.jsp?ordId="+info.mes;
					}
				},"json");
				
				
			}
			
			function tabLoad(){
				var url="cart/front/findByCarIds.action";
				var uri=window.location.href;
				var index=uri.indexOf("?")+1;
				var data=uri.substr(index);
				$.post(url,data,function(cartArr){
					
					
					
					
					var st=$("#product thead tr:last-of-type td")[0];
					$(st).find("span").remove();
					$("#product tbody").remove();
					var tbody=document.createElement("tbody");
					var num=0;
					var freight=0;
					var total=0;
					for(var i=0;i<cartArr.length;i++){
						var obj=cartArr[i];
						var price=parseFloat(obj.cartPrice);
						var cartNum=parseInt(obj.cartNum);
						var subtotal=price*cartNum;
						var money=parseFloat(obj.money);
						var spNames=obj.spNames.replace(/[,]/g,"<br/>");
						$(tbody).append(
							"<tr>"+
								"<td>"+
									"<div class='img'><img alt='图片' src='"+obj.proImg+"'/></div>"+
									"<div class='txt'>"+
										"<span class='proName'>"+obj.proName+"</span>"+
									"</div>"+
								"</td>"+
								"<td class='info'>"+spNames+"</td>"+
								"<td class='price'>￥"+obj.cartPrice+"</td>"+
								"<td class='num'>"+obj.cartNum+"</td>"+
								"<td class='freight'>￥"+obj.money+"</td>"+
								"<td class='subtotal'>￥"+(subtotal+money).toFixed(2)+"</td>"+
							"</tr>"		
						);
						num+=obj.cartNum;
						freight+=money;
						total+=subtotal;
					}
					$(tbody).append(
						"<tr>"+
							"<td colspan='3'>合计：</td>"+
							"<td>"+num+"</td>"+
							"<td>￥"+freight.toFixed(2)+"</td>"+
							"<td>￥"+(total+freight).toFixed(2)+"</td>"+
						"</tr>"		
					);
					$("#product tfoot").before(tbody);
					$(st).find("span").first().click(); 
					
				},"json");
			}
		</script>
	</head>
	<body>
		<!-- 遮盖层 start -->
		<!-- <div id="curtain" class="address-curtain" style="display: none;"></div> -->
		<!-- 遮盖层 end -->
		
		<!-- 收货地址弹出层  start -->
		<!-- <div id="dialogs" class="address-layer" style="display: none;"></div> -->
		<!-- 收货地址弹出层  end -->
		
		<!-- 页眉 start -->
		<div id="header">
			<div class="content">
				<div class="logo">
					<a href="pages/front/index.jsp"><img alt="logo" src="res/images/eLOGO1.jpg" style="max-height: 80px;"></a>
				</div>
			</div>
		</div>
		<!-- 页眉 end -->
		
		<!-- 主体 start -->
		<div id="main">
			<div class="content">
				<div class="address">
					<p>
						<span class="ft">收货地址</span>
						<a href="javascript:void(0);" data-event="address-show" class="rt">增加收货地址</a>
					</p>
					
					<ul id="address-list">
						<li id="address-temp">
							<label>
								<div style="display: inline-block;line-height: normal;">
									<span style="transform: translateY(-50%);"></span><input type="radio" value="" name="addId"/>
								</div>
								<span class="txt">手动填写<br/>临时地址</span>
							</label>
							
							<textarea  class="address" placeholder="地址格式：张三,139****7537,浙江省 金华市 婺城区 城北街道 江北金茂百货负一楼140号济州岛"></textarea>
							
							
						</li>
						 
					</ul>
				</div>
				
				<div id="product" align="center">
				<!-- 
					<div style="text-align:  left; position:  relative;    top: -44px;    left: 100px;">
						<p>临时地址请按如下格式地址填写：</p>
						<p>张三,139****7537,浙江省 金华市 婺城区 城北街道 江北金茂10号</p>
						
					</div>
				 -->
					<table>
						<thead>
							<tr style="border-bottom: 1px solid #d5d5d5;">
								<th width="260">店铺宝贝</th>
								<th width="230">商品属性</th>
								<th width="140">单价</th>
								<th width="100">数量</th>
								<th width="140">运费</th>
								<th width="200">小计</th>
							</tr>
							<tr style="border: none;height: 20px;"></tr>
						</thead>
						
						<tbody>
							<tr>
								<td>
									<div class="img"><img alt="图片" src=""/></div>
									<div class="txt">
										<span class="proName">2016东过膝长靴高筒加绒保暖牛皮鞋带黑真皮磨砂女靴</span>
									</div>
								</td>
								<td class="info">
									颜色分类：浅棕色[单内里,适合秋冬]<br/>
									尺码：37
								</td>
								<td class="price">￥259.00</td>
								<td class="num">1</td>
								<td class="freight">￥36.00</td>
								<td class="subtotal">￥1036.00</td>
							</tr>
							<tr>
								<td colspan="3">合计：</td>
								<td>3</td>
								<td>￥210.00</td>
								<td>￥7036.00</td>
							</tr>
						</tbody>
						
						
						<tfoot>
							<tr>
								<td colspan="6" style="color:red;">
									温馨提示：默认发申通快递，特殊情况发其他快递！如需指定快递请留言，发顺丰或者EMS联系客服改邮费！
								</td>
								 
							</tr>
							<tr>
								<td colspan="4">
									<span style="letter-spacing: 1px;">有事您留言: </span>
									<textarea id="message-board"></textarea>
								</td>
								<td colspan="2" align="center">
									<c:if test="${empty param.ordId }">
										<input class="btn btn-warning" onclick="createOrder()" type="button" value="提交订单付款"/>
									</c:if>
									<c:if test="${!empty param.ordId }">
										<input class="btn btn-warning" onclick="createOrder2()" type="button" value="提交订单付款"/>
									</c:if>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<!-- 主体 end -->
		
		
		
		
	</body>
</html>