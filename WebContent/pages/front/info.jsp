<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,宝贝详情,前台" />
		<meta http-equiv="description" content="E货源网站前台宝贝详情页面" />
		<title>E货源--宝贝详情</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="res/css/info.css"/>
		<link rel="stylesheet" href="res/css/glyphicon.css">
		<link rel="stylesheet" href="res/css/memberLogin.css">
		<link rel="stylesheet" href="res/css/toolstip.css">
		<link rel="stylesheet" type="text/css" href="res/css/comment.css"/>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<style type="text/css">
			.fly_item {
			    width: 48px;
			    height: 48px;
			    overflow: hidden;
			    position: absolute;
			    visibility: hidden;
			    -webkit-border-radius: 100%;
			    border: 2px solid red;
			    -moz-border-radius: 100%;
			    -ms-border-radius: 100%;
			    -o-border-radius: 100%;
			    border-radius: 100%;
			    opacity: 0.9;
			}
		</style>
		
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script src="res/js/ehy_common.js?random=<%=Math.random()%>"></script>
		<script src="res/js/base.js"></script>
		<script src="res/js/info.js"></script>
		<script src="res/js/comment.js"></script>
		<script src="res/js/parabola.js"></script>
	</head>
	<body>
		<!-- 遮盖层 -->
		<div id="curtain" style="display:none;"></div>
		
		<!-- 会员登陆 -->
		<div id="loginModal" style="display:none;"></div>
		
		<!-- header 开始 -->
		<div id="header">
		</div>
		<!-- header 结束 -->
		
		<!-- 右边工具栏  start  -->
		<div id="toolbar">
		</div>
		<!-- 右边工具栏  end  -->
		
		<!-- main 开始 -->
		<div id="main">
			<div id="flyItem" class="fly_item">
	            <img src="images/item-pic.jpg" width="50" height="50" />
	        </div>
			<div class="content">
				<!-- info 开始 -->
				<div class="info">
					<!-- info 开始 -->
					<div class="left">
						<div class="max-img">
							<img alt="max" src="">
						</div>
						
						<div class="min-img">
							<ul></ul>
						</div>
					</div>
					<!-- info 结束 -->
					
					<!-- right 开始 -->
					<div class="right">
						<!-- 名称：字体加粗加黑 -->
						<div id="proName">产品名称</div>
						
						<!-- 价格：直销价/市场价 -->
						<div class="price">
							<b style="font-weight: 400;">价格：</b>
							<c:if test="${empty param.sheId && empty param.qc }">
								<span>厂家直销价<em id="proFactoryPrice"></em></span>
								
							</c:if>
							<c:if test="${!empty param.sheId && empty param.qc }">
								<span>&nbsp;&nbsp;VIP专享出厂成本价<em id="proPriceCB"></em></span>
							</c:if>
							
							<c:if test="${param.qc eq 1 }">
								<span>清仓价<em id="proFactoryPrice"></em></span>
							</c:if>
							<span class="market-price">&nbsp;&nbsp;市场价<em id="proPrice"></em></span>
						</div>
						
						<!-- 宝贝信息详情 -->
						<div id="details">
							<!-- 颜色：以图片的形式展示 
							<div class="img-color">
								<div class="text">颜色</div>
								<div class="list">
									<ul></ul>
								</div>
							</div>
							-->
							<!-- 尺码：提供多个尺码可供选择
							<div class="specification">
								<div class="text">尺码</div>
								<div class="list">
									<ul></ul>
								</div>
								<div class="explain">尺码说明：注意尺码选项的提示，有部分超小超大尺码的鞋不支持退换货</div>
							</div>
							 -->
							<!-- 数量：1~max存货量，以"+"和"-"进行增减 -->
							<div class="number">
								<div class="text">数量</div>
								<div class="box">
									<b class="add" onclick="addOrCut(this)">-</b>
									<span class="num" id="cartNum">1</span>
									<b class="cut" onclick="addOrCut(this)">+</b>
								</div>
							</div>
							
							<!-- 选择商品规程 -->
							<div class="goods">
								<div class="selected">
									<span>已选择：</span>
									<span id="spValues"></span>
								</div>
								
								<!-- 加入购物车-->
								<!-- 如果是货架点击进入的,则不显示 -->
								<c:if test="${empty param.sheId &&  empty param.qc }">
									<div class="cart btn btn-warning" onclick="addCart(event)">
										<span class="txt">加入采购车</span>
										<img src="res/images/cart.png" style="position: relative;top: 2px;"/>
									</div>
								 
								<!-- 放入货架 -->
								<!-- 如果是货架点击进入的,则不显示 -->
								 
									<div class="shelf btn btn-primary" onclick="addShelf()">
										<span class="txt">放入我的货架</span>
										<img src="res/images/shelf.png" style="position: relative;top: 2px;"/>
									</div>
								</c:if>
								<!-- 如果是货架点击进入的,则不显示 -->
								<c:if test="${!empty param.sheId &&  empty param.qc  }">
									<div class="cart btn btn-warning" onclick="addOrder(event)">
										<span class="txt">立即下单</span>
									</div>
								</c:if>
								
								
								<!-- 如果是清仓产品 -->
								<c:if test="${param.qc eq 1 }">
									<div class="cart btn btn-warning" onclick="addOrder(event)">
										<span class="txt">立即抢购</span>
									</div>
								</c:if>
							</div>
							
							<!-- 交易:购买方式/支付方式 -->
							<div class="deal">
								<div class="buy">
									<ul>
										<li>
											<i>批</i><span>实体批发</span>
										</li>
										<li>
											<i>团</i><span>企业个人团购</span>
										</li>
										<li>
											<i>销</i><span>网络销售代理</span>
										</li>
									</ul>
								</div>
								<div class="pay">支付方式：微信支付、支付宝支付</div>
							</div>
						</div>
						
						
					</div>
					<!-- right 结束 -->
				</div>
				<!-- info 结束 -->
				
				<!-- 海报 开始 -->
				<div class="poster">
					<img alt="海报" src="res/userDefinde/haibao.jpg" style="max-width: 100%;max-height: 100%;"/>
				</div>
				<!-- 海报 结束 -->
				
				<!-- cont 开始 -->
				<div class="cont">
					<div class="title">
						<div class="dotey active">厂家提供的宝贝详情</div>
						
						<div class="share">
							<div class="red-txt">(这里有真相)</div>
							<div class="big-txt">买家实物解析分享</div>
						</div>
					</div>
					
					<!-- 宝贝详情信息  start -->
					<div class="box" id="proInfo">
						<!-- 产品属性 -->
						<div class="specification">
							<ul id="attList"></ul>
						</div>
						
						<!-- 产品详情图片 -->
						<div class="images">
							<ul id="imgList"></ul>
						</div>
					</div>
					<!-- 宝贝详情信息  end -->
					
					<!-- 买家分享信息   start -->
					<div class="box" id="comment" style="display: none;"></div>
					<!-- 买家分享信息   end -->
				</div>
				<!-- cont 结束 -->
			</div>
		</div>
		<!-- main 结束 -->
		
		
		
		<!-- footer 开始 -->
		<div id="footer"></div>
		<!-- footer 结束 -->
	</body>
</html>