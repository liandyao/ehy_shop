<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<base href="<%=basePath %>"/>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="res/js/ehy_common.js"></script>
		<title>E货源-支付成功页面!</title>
		<style type="text/css">
			.main{
				text-align:center ;
				margin:0px auto ;
			}
			
			.main .logo{
				text-align:left ;
			}
			
			.main .center{
				text-align:center ;
				
			}
			
			ul{
				list-style: none;
				 
			}
			
			
			.btn {
			    text-decoration: none;
			    border-radius: 4px;
			    background-image: none;
			    border: 1px solid transparent;
			    vertical-align: middle;
			    font-size: 14px;
			    font-weight: 700;
			    line-height: 1.42857143;
			    text-align: center;
			    white-space: nowrap;
			    display: inline-block;
			    padding: 6px 12px;
			    color: #fff;
			    background-color: #fc4302;
			    cursor: pointer;
			}
			
			.btn-warning {
			    background-color: #fc4302;
			    border-color: #fe452d;
			}
			
			.btn-warning:hover {
			    color: #fff;
			    background-color: #fe7d52;
			    border-color: #fc4302;
			}
			
			
			.btn-primary:hover {
			    color: #fff;
			    background-color: #286090;
			    border-color: #204d74;
			}
			
			.btn-primary {
			    background-color: #0379f7;
			    border-color: #0379d2;
			}
			
			
		</style>
	</head>
	<body>
		<div class="main">
			<div class="logo">
				<img src="res/images/eLOGO1.jpg" alt="logo" width="210" height="70"/>
			</div>
			<div class="center">
				<h1>恭喜,支付成功!</h1>
				<ul>
					<li>商家订单编号:${out_trade_no }</li>
					<li>支付订单号:${trade_no }</li>
					<li>支付金额:${total_amount }</li>
				</ul>
				<a href="" class="cart btn btn-warning">继续购物</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="pages/front/UserCenter/userCenter.jsp#Section1" class="shelf btn btn-primary">查看订单</a>
			</div>
			<div>
				<img src="res/userDefinde/footer.jpg" alt="底部图片"/>
			</div>
		</div>
	  
	</body>
</html>