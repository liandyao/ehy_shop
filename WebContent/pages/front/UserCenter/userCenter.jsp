<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="res/js/jquery-2.1.4.min.js"></script>
<link href="res/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="res/css/bootstrap-datepicker3.css">
<link href="res/css/userCenter.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="res/css/common.css" />
<link rel="stylesheet" type="text/css" href="res/css/myShelves.css" />
<link rel="stylesheet" type="text/css" href="res/css/share.css" />
<link rel="stylesheet" type="text/css" href="res/css/myOrders.css">
<link
	href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css"
	rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="res/css/center2.css"/>
<link rel="stylesheet" type="text/css" href="res/css/common.css" />
<link rel="stylesheet" type="text/css" href="res/css/share.css" />
<link rel="stylesheet" type="text/css" href="res/css/cart.css" />
<link rel="stylesheet" type="text/css"  href="res/css/myShare.css">  
<link rel="stylesheet" type="text/css"  href="res/css/dialog.css">   

<!-- 弹出层样式 -->
<link href="https://cdn.bootcss.com/limonte-sweetalert2/7.21.1/sweetalert2.min.css" rel="stylesheet"> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--  复制到剪切板 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script src="res/js/validUtils.js"></script>
<script src="res/js/bootstrap.min.js"></script>
<script type="text/javascript" src="res/js/ehy_common.js"></script>
<script type="text/javascript" src="res/js/bootstrap-datepicker.min.js"></script>
 

<script type="text/javascript" src="res/js/myOrder.js"></script>

<script src="res/js/cart.js"></script>
<script type="text/javascript" src="res/js/share.js"></script>
<script type="text/javascript" src="res/js/center.js"></script>
<script type="text/javascript" src="res/js/address.js"></script>
<title>用户中心</title>
<style type="text/css">

</style>
<script type="text/javascript">
	/*引用jsp里面的一个层  */
	$(function() {
		
		$("#Section1").load("pages/front/myOrders.jsp .bigBox",function(){	
			var url=window.location.href;
			if(url.indexOf("#Section1")>-1){
				$("#mav > li:nth-child(1) > a").click();
				var index=getParam("select") || 0;
				
				$("#Section1 .ztBox > div:nth-child("+index+")").click();
				$("[name=keyword]").val(ordCode);
			}
		});
		cartInit();
		 
		$.get("pages/front/myShelves.html",function(data){
			$("#Section6").html(data);
		},"html");
		 
	});
	
	
	
	
/* 	$(function(){
		$(".Section8_div").load("pages/front/UserCenter/userCenter.jsp .add-btn",function(){
			$(".add-btn").css('display','block');
		})
	}) */
	//新增地址
	$(function(){
		$.get("pages/front/UserCenter/address.jsp",function(data){
			$(".Section8_div").prepend($(data));
			addrInitLoad();
		},"html");	
	})
	
	/* 判断手机号码的正则表达式 */
	function dian(){
		var reg = /^1[3|4|5|7|8][0-9]{9}$/;
		var account = $("#mbPhone").val();
		if(!reg.test(account)){
			$('#mbPhone').css('border-color','red');
			$('#mbPhone').attr('placeholder','请输入正确的手机号码');
		}else{
			$('#mbPhone').css('border-color','#DDDDDD');
		}
	}
	/* 判断手机号码的正则表达式 */
	function dians() {
		var reg = /^1[3|4|5|7|8][0-9]{9}$/;
		var account = $("#mbPhones").val();
		if(!reg.test(account)){
			$('#mbPhones').css('border-color','red');
			$('#mbPhones').attr('placeholder','请输入正确的手机号码');
			
		}else{
			$('#mbPhones').css('border-color','#DDDDDD');
		}
	}

	/*刷新页面  */
	function refresh() {
		parent.location.reload();
	}
	/*一访问这个页面自动定位在代理管理中心  */
	$(function() {
		var id = getParam("id");
		if (!id)
			id = "Section1";
		var href = "a[href$='#" + id + "']";
		$(href).click();
	});
	
	
</script>
</head>
<body>
		<!-- 遮盖层 start -->
		<div id="curtain" style="display: none;"></div>
		<!-- 遮盖层 end -->
		
		<!-- 弹出按钮 start -->
		
		<!-- 弹出按钮 end -->

			<div class="title">
				<a href="javascript:void(0);"  onclick="addHide()"></a>
			</div>

	<!-- 确认对话框  end -->
	<div class="div_share_curtain"></div>
	<!-- 上传图片 -->
	<form id="imgform">
		<input type="file" id="imgInput" name="file" onchange="upload(this)">
	</form>

	<div id="div">
		<div >
			<ul>
				<li>
					<a href="${pageContext.request.contextPath }/pages/front/index.jsp">
						<img alt="logo"  src="${pageContext.request.contextPath }/res/images/eLOGO1.jpg" style="max-height: 70px;">
					</a>
					<span style="font-size: 24px;">【用户中心】</span>
				</li>
			</ul>
		</div>

		<div style="float: right; margin-top: -30px;" class="spana">
			

			
		</div>

		<div class="div_emp">
			<div class="div_xx" >
				个人基础信息<img id="photo" style=" cursor: pointer;" src="res/images/icon_edit.png">
			</div>
			<div class="div_sss" style="border: 1px solid #DEDEDE;">

				<div class="div_da" align="center">

					<div class="div_bor" id="imageDiv"></div>
					<ul class="div_update_phone">
						<li><a><span id="updatePwd">修改手机号码</span></a></li>
						<li><a><span id="updatePwds">修改登录密码</span></a></li>
						<!-- <li ><a><span id="updatePayPwd">修改支付密码</span></a></li> -->
						<!-- <li><a href="pages/front/UserCenter/center.jsp"><span
								id="updateAddress">管理收货地址</span></a></li> -->
					</ul>
				</div>
					
				<div class="div_text" id="showList">
					<ul style="width: 280px">
						<li id="mbName">我的名称：</li>
						<li id="dengji">我的等级：0</li>
						
						<li id="mbIsbzj">会员资质：</li>
						
						<c:if test="${login.levelId eq 2 }">
							<li>银牌会员考核：当前功能暂未开启!</li>
						</c:if>	
						
							
						<c:if test="${login.levelId eq 4 }">
							<li id="invitationCode">我的邀请码：</li>
						</c:if>
						<li>${login.mbStation }</li>
						<c:if test="${login.levelId eq 1 }">
							<li style="color:red;">
							 
							限时活动说明：<br/>
						注册日起1个月内，成功交易一笔，<br/>
						普通会员自动升级为银牌VIP会员。<br/>
						可享受厂家批发价和30天退换货<br/>
							</li>
						</c:if>
					</ul>

				</div>
			</div>
		</div>

		<div class="div_baby">
			<div class="div_bb">我的宝贝动态</div>
			<div class="div_sy">
				<div class="div_top"></div>
				<div class="div_body">
					<ul>
						<li>我的收益</li>
					</ul>

					<div class="div_red">
						<li>￥:0</li>
					</div>
					<div class="div_red_down">
						<li>收益提现</li>
					</div>

					<div class="div_red_downs">
						<li>累计收益:￥</li>
					</div>
				</div>

				<div class="div_body2">
					<ul>
						<li>宝贝被浏览</li>
					</ul>

					<div class="div_red">
						<li>人气</li>
					</div>
				</div>

				<div class="div_body3">
					<ul>
						<li>宝贝已成交</li>
					</ul>
					<div class="div_red">
						<li>数量</li>
					</div>
				</div>
				<div class="div_foot"></div>
			</div>
		</div>
		<div><span style="color: red;">温馨提示：</span>如果您对我们网站上的商品，有任何不满意的地方，记得与我们联系哦。<br />我们会根据您反馈的宝贵意见，及时联系厂家，管控厂家商品或者下架厂家商品。确保为广大用户提供更好的厂家货源。</div>
		<div class="tab" role="tabpanel" id="tab_a">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" id="mav" role="tablist">
				<li class="span"><a href="#Section1" id="orders1s" aria-controls="order"
					role="tab" data-toggle="tab"><span>我的订单</span></a></li>
			<c:if test="${login.levelId eq 4 }">
				<li class="span"><a href="#Section3" aria-controls="homes"
					role="tab" data-toggle="tab"><span>代理管理中心</span></a></li>
			</c:if>
				
					
				<li class="span"><a href="#Section4" onclick="orderss" aria-controls="home"
					role="tab" data-toggle="tab"><span>分享的宝贝</span></a></li>
				<li class="span"><a href="#Section5" aria-controls="profile"
					role="tab" data-toggle="tab"><span>我的采购车</span></a></li>
				<c:if test="${login.levelId gt 1 }">
					<li class="span"><a href="#Section6" aria-controls="messages"
					role="tab" data-toggle="tab"><span>我的货架</span></a></li>
				</c:if>
				
				<li class="span"><a href="#Section7" aria-controls="messages"
					role="tab" data-toggle="tab"><span>我的浏览记录</span></a></li>
				<li class="span"><a href="#Section8" aria-controls="messages"
					role="tab" data-toggle="tab"><span>管理收货地址</span></a></li>
			</ul>      
			<!-- Tab panes -->
			<div class="tab-content tabs" id="tab-center">
				<div role="tabpanel" class="tab-pane fade" id="Section1"></div>

				<div role="tabpanel" class="tab-pane fade in active" id="Section3" >
					<div >
						<div style="width: 95%; margin-left: 25px;"  id="Section3_div">
							<span style="margin-left: 20px;  color:#ff4300; font-size: 15px;cursor: pointer;" id="registerTj"  onclick="registerTj()">注册人数统计</span>
							<span style="margin-left: 50px;  font-size: 15px;cursor: pointer;" onclick="orderTj()" id="orderTj">订单数量统计</span>
						</div>
						<div  id="registerTj1"  style="height: 100%; width: 100%; margin-left: 20px; border: 1px dashed #DEDEDE;border-top: hidden;border-right:hidden;border-left:hidden; 
	 overflow: hidden;">
							<div style="margin: 60px; width: 40%;">
								<div style="color: #ff4300;">今日注册人数：<span style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input"  readonly="readonly"></span></div>
							</div>
							<div style="float:right;  margin-top:-100px; margin-right:60px; width: 50%;height:100%; background-color: #D5D5D5;"  id="">
								<div style="width: 100%; text-align: center;" class="zhuce" >注册记录</div>
								<div style="width: 100%;"  float:right; class="zhuce" id="zc">
								</div>
								<div id="downAdnUp">
									
								</div>
								
								
							</div>
						</div>
						<div style="height: 60px;margin: 20px; margin: 30px; color:#ff4300 " id="registerTj2" >
							本月注册人数：<span  style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input1" style="color:#ff4300;border: none;background: none;" readonly="readonly"></span>
							<span style="margin-left: 60px;"></span>
							上月注册人数：<span style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input2" style="border: none;background: none;"  readonly="readonly"></span>
							<span style="margin:0 30px 0 60px;"></span>
							历史记录
						</div>
						
						
						<div  id="orderTj1"  style="display:none; overflow: hidden; height: 100%; width: 100%; margin-left: 20px; border: 1px dashed #DEDEDE;border-top: hidden;border-right:hidden;border-left:hidden; overflow :auto">
							<div style="margin: 60px; width: 40%;">
								<div style="color: #ff4300;">今日订单数：<span style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input3" style="border: none;background: none;" readonly="readonly"></span></div>
							</div>
							<div style="float:right;  margin-top:-100px; margin-right:60px;width: 50%;height:100%; background-color: #D5D5D5;">
								<div style="width: 100%; text-align: center;" class="zhuce">订单记录</div>
								<div style="width: 100%;  float:right; " class="zhuce" id="order">
								
								</div>
								<div id="shangAndXia"></div>
							</div>
						</div>
						<div style="display:none; color:#ff4300;   height: 60px;margin: 20px; margin: 30px;" id="orderTj2">
							当月已成交订单数：<span style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input4" style="border: none;background: none;"  readonly="readonly"></span>
							<span style="margin-left: 60px;"></span>
							上月订单数：<span style="border:  1px solid #ff4300;border-top: hidden;border-right:hidden;border-left:hidden;"><input type="text" size="7" id="input5" style="border: none;background: none;"  readonly="readonly"></span>
							<span style="margin:0 30px 0 60px;"></span>
							历史记录
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="Section4">
					<h3>分享的宝贝</h3>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="Section5" style="min-height: 200px;">
			
				</div>

				<div role="tabpanel" class="tab-pane fade" id="Section6">
					<h3>我的货架</h3>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="Section7">
					<h3>我的浏览记录</h3>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="Section8">
				
					<div class="Section8_div">
						
					
					
					</div>
					<div style="" >

	
	
					<table class="dizhi">
					<thead >
						<tr class="address" style="height: 30px; width:100%; background: #fafafa; text-align: center; border: 1px solid #DDDDDD;">
							<th width="300px;">收货人</th>
							<th width="400px">所在地区</th>
							<th width=400px">详细地区</th>
							<th width="200px">手机号码</th>
							<th width="150px">操作</th>
						</tr>
					</thead>
					
				</table>
					
					
				</div>

				</div>
			</div>
		</div>
  
	</div>
	<div id="hide_div" name="hide_div"></div>
	
	<div id="shareDiv" style="display: none;" name="shareDiv">
		<div style="width: 65px; height: 0%; float: left; line-height: 620px;" id="close" > 
			<!-- 关闭按钮  start -->
			<img src="res/images/close.png" style="width: 65px; height: 52px;" />
			<!-- 关闭按钮  start -->
		</div>
		<!-- 用户中心分享页面弹出层  start -->
		<div id="share" align="center">
			<form style="margin-top: 30px;" id="memberForm" name="memberForm">
				<input type="hidden" name="mbId" id="mbId" class="mbId">
				<table>
					<tr>
						<td>我的名称：</td>
						<td><input type="text" id="mbNames" name="mbName"
							class="form-control"></td>
					</tr>
					<tr>
						<td>我的地址：</td>
						<td><input type="text" id="mbAddrs" name="mbAddr"
							class="form-control"></td>
					</tr>
					<tr>
						<td>登录账号：</td>
						<td><input type="text" id="mbLogins" name="mbLogin"
							class="form-control"></td>
					</tr>
					<tr>
						<td>我的性别：</td>
						<td><select class="form-control" id="mbSexs" name="mbSex">
								<option>男</option>
								<option>女</option>
						</select></td>
					</tr>
   
					<tr>
						<td>会员生日：</td>
						<td><input type="date" id="mbBirthdays" name="mbBirthday"
							class="form-control"></td>
					</tr>
					<tr>
						<td>身份证号：</td>
						<td><input type="text" id="mbCardIds" name="mbCardId"
							class="form-control"></td>
					</tr>

				</table>
				<input type="button" value="提交" class="btn btn-primary"
					onclick="commit()">
			</form>
		</div>
		<!-- 用户中心分享页面弹出层   end -->
	</div>

	<!-- 修改手机号码弹出层  start-->
	<div id="shareDivs" name="shareDivs"  style="display: none;">
		<div style="width: 65px; height: 100%; float: left; line-height: 500px;"id="closes">
			<!-- 关闭按钮  start -->
			<img src="res/images/close.png" style="width: 65px; height: 52px;" />
			<!-- 关闭按钮  start -->
		</div>

		<div id="shares" align="center">
			<form style="margin-top: 30px;" id="memberForms">
				<input type="hidden" name="mbId" id="mbId" class="mbId">
				<table>
					<tr align="center" style="margin: 10px;">
						
					</tr>

					<tr>
						<td><input type="text" id="mbPhone" name="mbPhone"
							placeholder="手机号码" onblur="dian()" class="form-control"></td>

					</tr>
					<tr>
						<td><input type="text" id="mbPhoneYzm" name="mbPhoneYzm"
							placeholder="验证码" class="form-control"></td>
						<td><input type="button" class="btn btn-primary"
							value="获取验证码" id="Pho_btn" onclick="phoneYzm()"></td>
					</tr>
					<tr>
						<td><input type="text" id="mbPhones" name="mbPhones"
							onblur="dians()" placeholder="新手机号码" class="form-control"></td>
					</tr>

					<tr>
						<td><input type="password" id="mbLoginPwd" name="mbLoginPwd"
							placeholder="旧密码" class="form-control"></td>

					</tr>
					<tr>
						<td><input type="password" id="mbLoginPwds"
							name="mbLoginPwds" placeholder="新密码" class="form-control"></td>
					</tr>
					<tr>
						<td><input type="password" id="mbLoginPayPwd"
							name="mbLoginPayPwd" placeholder="旧支付密码" class="form-control"></td>
					</tr>
					<tr>
						<td><input type="password" id="mbLoginPayPwds"
							name="mbLoginPayPwds" placeholder="新支付密码" class="form-control"></td>
					</tr>
				</table>
				<input type="button" value="提交" class="btn btn-primary"
					onclick="commits()">
			</form>
		</div>

	</div>
	
	
	
	<!-- <div id="zhuCeDivs" style="display: none;">
		<div
			style="width: 65px; height: 100%; float: left; line-height: 620px;"
			id="closes">
			关闭按钮  start
			<img src="res/images/close.png" style="width: 65px; height: 52px;" />
			关闭按钮  start
		</div>


	</div> -->
	
	

	<div class="div_share"></div>
	
	<!-- 页脚 -->
	<div id="footer"></div>
	
	<script type="text/javascript">
	
	/*显示个人基础信息  */
	$(function() {
		var url = "${pageContext.request.contextPath }/member/front/findMemberById.action";
		$.post(url,null,function(member) {
			$("#imageDiv").append("<img id='sss' src='"+member[0].mbRemark+"'width='80px'/>");
			$("#mbName").append(member[0].mbName);
			$("#levelId").append(member[0].levelName);
			//$("#dengji").append(member[0].levelId);
			
			if(member[0].levelId==1){
				$("#mbIsbzj").append("普通会员");
			}else if(member[0].levelId==2){
				$("#mbIsbzj").append('<span style="background-color: #CCCCCC; color: white; border-radius:10%;">银牌会员</span>');
			}else if(member[0].levelId==3){
				$("#mbIsbzj").append('<span style="background-color: red; color: yellow; border-radius:10%;">金牌会员</span>');
			}else if(member[0].levelId==4){
				$("#mbIsbzj").append('<span style=" color:red;">【分站代理】</span>');
			}
			$("#invitationCode").append(member[0].invitationCode);
		}, "json");
			
			/* 修改个人资料*/
			$("#photo").click(function() {
				$('div[name="shareDiv"]').show();
				$('div[name="hide_div"]').show();
				var url = "${pageContext.request.contextPath }/member/front/findMemberById.action";
				$.post(url,null,function(member) {
					$(".mbId").val(member[0].mbId);
					$("#mbNames").val(member[0].mbName);
					$("#mbAddrs").val(member[0].mbAddr);
					$("#mbSexs").val(member[0].mbSex);
					$("#mbLogins").val(member[0].mbLogin);
					$("#mbBirthdays").val(member[0].mbBirthday);
					$("#mbCardIds").val(member[0].mbCardid);
				},"json");
			})
			
							
				/*会员分页  */
				var url = "member/front/selectMemberPage.action";
				var data  ={"intercept":true};
				$.post(url , data ,function(arr){
					for(i=0;i<arr.list.length;i++){
						$("#zc").append('<span style="margin:90px;">'+arr.list[i].MB_NAME+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
						new Date(arr.list[i].OPTIME).format("yyyy-MM-dd ")  +'</span>');
						
					}
					/* 如果页数等于1就禁止上一页 */
					 if(arr.pages.curPage==1){
						$("#downAdnUp").html(
								'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; color: #ff4300;width: 60px;" ><上一页</span>'+
								'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  >下一页></span>');
					}else{
						$("#downAdnUp").html(
								'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" onclick="good('+(arr.pages.curPage-1)+');"><上一页</span>'+
								'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; color: #ff4300;width: 60px;"  onclick="down('+(arr.pages.curPage+1)+');">下一页></span>');
					 }
					 /* 如果总页数等于1就禁止上一页和下一页 */
					if(arr.pages.totalPage==1){
							$("#downAdnUp").html(
									'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" ><上一页</span>'+
									'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" >下一页></span>');
						}
					       
				})
				
				/* 订单分页 */
				var url = "member/front/selectOrder.action";
				var data  ={"intercept":true};
				
				$.post(url , data ,function(arr){
					for(i=0;i<arr.list.length;i++){
						console.info(arr.list[i]);
						$("#order").append('<span style="margin:50px;"">'+arr.list[i].MB_NAME+'&nbsp;&nbsp;&nbsp;&nbsp;'+ arr.list[0].PRO_NAME.substring(0,20) +'&nbsp;&nbsp;&nbsp;'+new Date(arr.list[i].PAY_TIME).format("yyyy-MM-dd")+'</span>');
					}
					
					/* 如果页数等于1就禁止上一页 */
					 if(arr.pages.curPage==1){
						$("#shangAndXia").html(
								'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; color: #ff4300;width: 60px;" ><上一页</span>'+
								'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; color: #ff4300; width: 60px;">下一页></span>');
					}else{
						$("#shangAndXia").html(
								'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" onclick="shang('+(arr.pages.curPage-1)+');"><上一页</span>'+
								'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; color: #ff4300; width: 60px;"  onclick="xia('+(arr.pages.curPage+1)+');">下一页></span>');
					 }
					 /* 如果总页数等于1就禁止上一页和下一页 */
					if(arr.pages.totalPage==1){
							$("#shangAndXia").html(
									'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" ><上一页</span>'+
									'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" >下一页></span>');
						}
					
				})
							
				var url ="member/front/selectNumByTime.action";		
				$.post(url , null ,function(member){
						if(member==""){
							return;
						}else{
							$("#input").val(member);
						}

						
				})
				
				var url ="member/front/selectNumByMon.action";		
				$.post(url , null ,function(member1){
					
					if(member1==""){
						return;
					}else{
						$("#input1").val(member1);
					}
				})
				
				var url ="member/front/selectNumByMon1.action";		
				$.post(url , null ,function(member2){
					if(member2==""){
						return;
					}else{
						$("#input2").val(member2);
					}
				})
				
				var url ="member/front/selectNumByOrder.action";		
				$.post(url , null ,function(order){
					if(order==""){
						return;
					}else{
						$("#input3").val(order);
					}
				})
				
					var url ="member/front/selectNumByOrder1.action";		
				$.post(url , null ,function(order1){
					
					if(order1==""){
						return;
					}else{
						$("#input4").val(order1);
					}
				})
				
					var url ="member/front/selectNumByOrder2.action";		
				$.post(url , null ,function(order2){
					if(order2==""){
						return;
					}else{
						$("#input5").val(order2);
					}
				})

			$("#close").click(function() {
				$('div[name="shareDiv"]').css('display', 'none');
				$('div[name="hide_div"]').css('display', 'none');

			})
			
			$("#zhuCeSpan").click(function(){
				$("#zhuCeDivs").css('display', 'block');
				$("#hide_div").css('display', 'block');
			})

			$("#updatePwd").click(function() {
				$("#mbLoginPwd").val("");
				$("#mbLoginPwds").val("");
				$("#mbPhones").val("");
				$("#mbLoginPayPwd").val("");
				$("#mbLoginPayPwds").val("");
				$("#mbPhone").val("");
				$("#mbPhoneYzm").val("");
				
				$('div[name="hide_div"]').show();
				
				$("#shareDivs").css('display', 'block');
			
				$("#mbLoginPwd").css("display", "none");
				$("#mbLoginPwds").css("display", "none");
				$("#mbPhones").css("display", "block");
				$("#mbLoginPayPwd").css("display", "none");
				$("#mbLoginPayPwds").css("display", "none");
			})

			$("#updatePwds").click(function() {
				$("#mbLoginPwd").val("");
				$("#mbLoginPwds").val("");
				$("#mbPhones").val("");
				$("#mbLoginPayPwd").val("");
				$("#mbLoginPayPwds").val("");
				$("#mbPhone").val("");
				$("#mbPhoneYzm").val("");
				$("#shareDivs").css('display', 'block');
				$('div[name="hide_div"]').show();
				$("#mbPhones").css("display", "none");
				$("#mbLoginPwd").css("display", "block");
				$("#mbLoginPwds").css("display", "block");
				$("#mbLoginPayPwd").css("display", "none");
				$("#mbLoginPayPwds").css("display", "none");
			})

			

			$("#closes").click(function() {
				$('div[name="hide_div"]').css('display', 'none');
				$("#shareDivs").css('display', 'none');
			})

			$("#closePwd").click(function() {
				$("#shareDivPwd").css('display', 'none');

				$('div[name="hide_div"]').css('display', 'none');
			})
		
			$("#imageDiv").click(function() {
				$("#imgInput").click();

			})
			
		
		})
		/* 修改会员头像 */
		function upload(obj) {
			//判断文件格式
			photoExt = obj.value.substr(obj.value.lastIndexOf("."))
					.toLowerCase();//获得文件后缀名
			if (photoExt != '.jpg' && photoExt != '.png' && photoExt != '.gif'
					&& photoExt != '.jpg ') {
				alert("只能上传jpg/gif/png格式文件!");
				return false;
			}
			var form = new FormData(document.getElementById("imgform"));
			$.ajax({
						url : "${pageContext.request.contextPath }/member/front/updatePhoto.action",
						type : "post",
						data : form,
						processData : false,
		 				success : function(data) {
							if (data.state == 1) {
								$("#sss").attr("src", data.mes);
							} else {
								alert("修改头像失败");
							}
						},
						error : function(e) {
							alert("上传错误！！");
						}
					});
		}

		/*  修改会员信息*/        
		function commit() {
			var url = "member/front/updateMember.action";
			var data = $('form[name="memberForm"]').serialize();
			$.post(url, data, function(info) {
				if (info.state == 1) {
					alert("密码修改成功");
					setTimeout("refresh()", 500);
				} else if (info.state == 0) {
					alert("修改失败");
				}
			})
		}
		/* 发送验证码 */
		function phoneYzm() {
			var reg = /^1[3|4|5|7|8][0-9]{9}$/;
			var mbPhone = $("#mbPhone").val();//取出用户的填入的号码
			if (mbPhone == "") {
				$('#mbPhone').css('border-color','red');
			}else if(!reg.test(mbPhone)){
				$('#mbPhone').css('border-color','red');
				$('#mbPhone').attr('placeholder','请输入正确的手机号码');
				
			}else {
				var url = "${pageContext.request.contextPath}/member/front/yzm.action";
				var data = {'mbPhone' : mbPhone,intercept : true};
				$.post(url, data, function(info) {
					if (info.state == 1) {
						countDown(60);
					}
				})

			}
		}

		/*计时  */
		function countDown(secs) {
			var jumpTo = document.getElementById('Pho_btn');
			jumpTo.value = secs + "秒后重新发送";
			if (--secs > 0) {
				setTimeout("countDown(" + secs + ")", 1000);
				jumpTo.disabled = true;
			} else{
		    	jumpTo.value="点击获取验证码";
		    	jumpTo.disabled=false;
		    }
		}
		/*判断验证码  */
		function commits() {
			 
			var reg = /^1[3|4|5|7|8][0-9]{9}$/;
			var account = $("#mbPhone").val();
			var account1 = $("#mbPhones").val();
			var mbPhoneYzm = $("#mbPhoneYzm").val();//取出用户的填入的验证码
			console.info($("#mbPhones").css("display"));
			if (mbPhoneYzm == "") {
				$("#mbPhoneYzm").attr('placeholder','验证码不能为空');
				$("#mbPhoneYzm").css('border-color','red');
			
			}else if($("#mbPhone").css("display")!="none" && !reg.test(account)){
				$("#mbPhone").attr('placeholder','请输入正确的号码');
				$("#mbPhone").css('border-color','red');
			}else if($("#mbPhones").css("display")!="none" && !reg.test(account1)){
				$("#mbPhones").attr('placeholder','请输入正确的号码');
				$("#mbPhones").css('border-color','red');
			}else {
				
				var url = "${pageContext.request.contextPath}/member/front/loginPwdYzm.action";
				var data = {"mbPhoneYzm" : mbPhoneYzm};
				$.post(url, data, function(mes) {
					if (mes.state == 1) {
						updatePhone();
					} else if (mes.state == 0) {
						$("#mbPhoneYzm").val("");
						$("#mbPhoneYzm").attr('placeholder','验证码有误');
						$("#mbPhoneYzm").css('border-color','red');
					}
				})
				
			}
		}
		
		/*注册人数统计  */
	 	function registerTj(){
			$("#registerTj").css('color','#ff4300');
			$("#orderTj").css('color','#111');
			
			$("#registerTj1").css('display','block');
			$("#registerTj2").css('display','block');
			$("#orderTj1").css('display','none');
			$("#orderTj2").css('display','none');
		}
		/* 订单人数统计 */
		function orderTj(){
			$("#orderTj").css('color','#ff4300');
			$("#registerTj").css('color','#111');
			
			$("#registerTj1").css('display','none');
			$("#registerTj2").css('display','none');
			$("#orderTj1").css('display','block');

			$("#orderTj2").css('display','block');
		} 
		
		/* 修改手机号码 */
		function updatePhone() {
			var url = "${pageContext.request.contextPath}/member/front/updatePhone.action";
			var data = $("#memberForms").serialize();
			$.post(url, data, function(info) {
				if (info.state == 1) {
					setTimeout("refresh()", 500);
				} else if (info.state == 0) {
					alert("修改失败");
				}
			})
		}
		
		
		
		/* 注册人数分页----下一页 */
		function down(jia){
			var url ="member/front/selectMemberPage.action";
			var data  ={"intercept":true,"curPage":jia};
			$("#downAdnUp").html("");
			$("#zc").html("");
			$.post(url , data ,function(arr){
				for(i=0;i<arr.list.length;i++){
					$("#zc").append('<span style="margin:70px;">'+arr.list[i].MB_NAME+'&nbsp;&nbsp;&nbsp;'+  new Date(arr.list[i].OPTIME).format("yyyy-MM-dd ")  +'</span>');
					
				}
				/*如果页数等与总页数就禁止下一页  */
				if(arr.pages.curPage==arr.pages.totalPage){
					
					$("#downAdnUp").html(
						'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"><上一页</span>'+
						'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  >下一页></span>');
				}else{
				
					$("#downAdnUp").html(
							'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" onclick="good('+(arr.pages.curPage-1)+');"><上一页</span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="down('+(arr.pages.curPage+1)+');">下一页></span>');
				}
				
			})
		}
		/*注册人数分页----上一页 */
		function good(jia){
			var url ="member/front/selectMemberPage.action";
			var data  ={"intercept":true,"curPage":jia};
			$("#downAdnUp").html("");
			$("#zc").html("");
			
			$.post(url , data ,function(arr){
				for(i=0;i<arr.list.length;i++){
					$("#zc").append('<span style="margin:70px;">'+arr.list[i].MB_NAME+'&nbsp;&nbsp;&nbsp;'+  new Date(arr.list[i].OPTIME).format("yyyy-MM-dd ")  +'</span>');
					
				}
				/* 如果页数等于1就禁止上一页 */
				if(arr.pages.curPage==1){
					$("#downAdnUp").append('<span style="margin-left: 150px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" >上一页></span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; width: 60px;"  >下一页></span>');
				}else{
					$("#downAdnUp").append('<span style="margin-left: 150px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="good('+(arr.pages.curPage-1)+');">上一页></span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="down('+(arr.pages.curPage+1)+');">下一页></span>');
				}
					
				
			})
		}
		
		/* 订单分页----下一页 */
		function xia(jia){
			var url ="member/front/selectOrder.action";
			var data  ={"intercept":true,"curPage":jia};
			$("#shangAndXia").html("");
			$("#order").html("");
			$.post(url , data ,function(arr){
				for(i=0;i<arr.list.length;i++){
					$("#order").append('<span style="margin:70px;">'+arr.list[i].mbName+'&nbsp;&nbsp;&nbsp;成交了'+ arr.list[i].one +'单</span>');
					
				}
				/*如果页数等与总页数就禁止下一页  */
				if(arr.pages.curPage==arr.pages.totalPage){
					
					$("#shangAndXia").html(
						'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" onclick="shang('+(arr.pages.curPage-1)+');"><上一页</span>'+
						'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  >下一页></span>');
				}else{
					$("#shangAndXia").html(
							'<span style="margin-left: 100px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" onclick="shang('+(arr.pages.curPage-1)+');"><上一页</span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="xia('+(arr.pages.curPage+1)+');">下一页></span>');
				}
				
			})
		}
		
		/* 订单分页----上一页 */
		function shang(jia){
			var url ="member/front/selectOrder.action";
			var data  ={"intercept":true,"curPage":jia};
			$("#shangAndXia").html("");
			$("#order").html("");
			
			$.post(url , data ,function(arr){
				for(i=0;i<arr.list.length;i++){
					$("#order").append('<span style="margin:70px;">'+arr.list[i].mbName+'&nbsp;&nbsp;&nbsp;成交了'+ arr.list[i].one +'单</span>');
					
				}
				/* 如果页数等于1就禁止上一页 */
				if(arr.pages.curPage==1){
					$("#shangAndXia").append('<span style="margin-left: 150px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;" >上一页></span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300; width: 60px;"  onclick="xia('+(arr.pages.curPage+1)+');">下一页></span>');
				}else{
					$("#shangAndXia").append('<span style="margin-left: 150px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="shang('+(arr.pages.curPage-1)+');">上一页></span>'+
							'<span style="margin-left: 250px; cursor: pointer; background-color: #fff;border: 1px solid #ff4300;color: #ff4300; width: 60px;"  onclick="xia('+(arr.pages.curPage+1)+');">下一页></span>');
				}
					
				
			})
		}
		
		/*打开增加收货地址的弹出层  */
		function address(){
			$("#addressDiv").css("display",'block');
			$("#hide_div").css("display",'block');

			$("#address-item").html("");
			$("#addEmps").val("");
			$("#addAddresss").val("");
			$("#addPhones").val("");
			$("#addPhone2").val("");
		}

	
		
		/*关闭增加收货地址的弹出层  和遮罩层*/
		function closesss(){
			$("#addressDiv").css("display",'none');
			$("#hide_div").css("display",'none');
		}
		
		
		function UpdateAdd(id){
		
			setForm(id);
		}
		
		
		function deletes(id){
			var url = "address/front/deleteByPrimaryKey.action";
			var data ={"addId":id};
			$.post(url,data,function(mes){
				if(mes.state==1){
					setTimeout("refresh()", 0);
				}
			});
		}

		
		
		$(function(){
			var url = "address/front/selectByMbId.action";
			$.post(url,null,function(member){
				$.each(member,function(i,item){
					$(".dizhi").append(
							'<thead>'+
						'<tr style="border: 1px solid #DDDDDD; height: 30px; ">'+
							'<input type="hidden" name="addId" id="addId" value="'+item.addId+'">'+
							'<td id="addEmp" name="addEmp">'+item.addEmp+'</td>'+
							'<td >'+item.addProvince+'-'+item.addCity+'-'+item.addCounty+'</td>'+
							'<td id="addAddress" name="addAddress" value="">'+item.addAddress+'</td>'+
							'<td id="addPhone" name="addPhone" value="">'+item.addPhone+'</td>'+
							'<td><a id="update"  onclick="UpdateAdd(&apos;'+item.addId+'&apos;);">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;<a id="deletes"  onclick="deletes(&apos;'+item.addId+'&apos;);">删除</a></td>'+
						'</tr>'+
						'<thead>'
					);
				})
			});
		})
		
		
	</script>
	<script src="res/js/base.js"></script>
	<!-- 
	<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>
	 -->
</body>
</html>