<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
  <%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<base href="<%=basePath%>"/>
<link rel="stylesheet" type="text/css" href="res/css/address.css"><!-- 引用外部样式表  -->
<script type="text/javascript" src="res/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="res/js/address.js"></script>
<script type="text/javascript" src="res/js/center.js"></script>


<link href="res/css/common.css" rel="stylesheet" type="text/css">
<style type="text/css">
#share{
	width: 500px;
	height: 350px;
	background-color: #fff;
	 position:fixed;
	border: 1px solid #d3d3d0;
	top:200px;
	left:500px;
	z-index:10001; 
	box-shadow:2px 2px 2px #C4C4C4 inset;
	
}

/*设置遮障层  */
 #curtain{
  	background:#111;
  	width:100%;
  	height:100%;
  	border:1px solid white;
  	position: fixed;
  	
  	display:none;
  	top:0px;
  	left:0px;
  	z-index:99;
  	opacity: 0.5;/*背景半透明*/
 }
 
 #close{
	 position:fixed;
	margin: 0 auto;
	top:70px;
	left:440px;
	z-index: 10001;
	cursor: pointer;/*把鼠标状态变成手 */
}

#deletes{
	cursor: pointer;/*把鼠标状态变成手 */
	
}
#deletes:HOVER{
	color:#ff3300;
}

tr input{
	margin-top: 10px;
padding: 6px 12px;
border-radius: 4px;
width: 100%;
height: 24px;
text-align: center;
}

.dizhi{
margin: 0 0 0 10px;
font-size: 12px;
background: #ffaa45;
/* padding: 2px; */
color: #fff;
font-weight: 400;
}
a{
	color: #005ea7;
}

a:HOVER{
	text-decoration:none;
	color: #ff3300;
}

</style>


</head>
<body >
		<!-- 遮盖层 start -->
		<div id="curtain" style="display: none;"></div>
		<!-- 遮盖层 end -->
<div style="padding: 10px;" class="address1_div">
<!-- <input value="收货地址" id="adress" name="adress" type="button" onclick="address()"  class="btn btn-primary"> -->

<div style="width: 800px;height:auto;  font-size: 15px;"  id="div">

	
	
	<table class="dizhi">
	<thead>
		<tr class="address" style="height: 30px; width:100%; background: #fafafa; text-align: center;">
			<th width="300px;">收货人</th>
			<th width="400px">所在地区</th>
			<th width=400px">详细地区</th>
			<th width="200px">手机号码</th>
			<th width="150px">操作</th>
		</tr>
	</thead>
	
</table>
	
	
</div>


<div id="hide_div"></div>
	<div id="addressDiv"  style="display: none;">
	<div style="width: 65px;height: 0%;float: left;line-height: 620px;" id="close" onclick="closesss()" >
			<!-- 关闭按钮  start -->
			<img src="res/images/close.png" style="width: 65px;height: 52px;"/>
			<!-- 关闭按钮  start -->
	</div>
	<!-- 用户中心分享页面弹出层  start -->
		<div id="share"  align="center" >
			<form style="margin-top: 30px;" id="memberForm" method="post">
				<input type="hidden" name="addIid"  id="addIid">
				<table>
					<tr>
						<td><input ty8 pe="text" id="addEmps" name="addEmp" class="form-control" size="40" placeholder="收货人"></td>
					</tr>
					
		 <div class="box" style="background-color:#fff;position: relative;">
			<div class='address' id='address' onclick='onOff()' >
				送至
				<span class='address-item' id="address-item">请选择</span>
				<span class='caret'></span>
			</div>
			<ul id='options' class='menu'>
				<li>
					<p class='title' >
						<span id='titleP' class='txt active' name="addProvince"></span>
						<span id='titleC' class='txt'   name="addCity" style='display: none;'></span>
						<span id='titleD' class='txt' name="addCounty" style='display: none;'></span>
					</p>
					<div class='express' style="background-color: #fff;">
						<div id='provinces' class='cont' style='display:block;'>
						</div>
						<div id='cities' class='cont'>
						</div>
						<div id='district' class='cont'></div>
					</div>
				</li>
			</ul>
		 </div>
	
	
					<tr>
						<td><input type="text" id="addAddresss" name="addAddress"  class="form-control" placeholder="详细地址"></td>
					</tr>
					<tr>
						<td><input type="text" id="addPhones"  name="addPhone"  class="form-control" placeholder="手机号码"></td>
					</tr>
					<tr>
						<td><input type="text" id="addPhone2"  name="addPhone2"  class="form-control" placeholder="备用号码"></td>
					</tr>				
				</table>
					<input type="button" value="提交" class="btn btn-primary" id="button"  onclick="commit()" style="margin: 40px;">
			</form>	
		</div>
		<!-- 用户中心分享页面弹出层   end -->
		
		
		<div id="addressDiv"  style="display: none;">
	<!-- <div style="width: 65px;height: 0%;float: left;line-height: 620px;" id="close">
			关闭按钮  start
			<img src="res/images/close.png" style="width: 65px;height: 52px;"/>
			关闭按钮  start
	</div> -->
	<!-- 用户中心分享页面弹出层  start -->
		<div id="share"  align="center" >
			<form style="margin-top: 30px;" id="memberForm" method="post">
				<input type="hidden" name="addIid"  id="addIid">
				<table>
					<tr>
						<td><input type="text" id="addEmps" name="addEmp" class="form-control" size="40" placeholder="收货人"></td>
					</tr>
					
		 <div class="box" style="background-color:#fff;position: relative;">
			<div class='address' id='address' onclick='onOff()' >
				送至
				<span class='address-item' id="address-item">请选择</span>
				<span class='caret'></span>
			</div>
			<ul id='options' class='menu'>
				<li>
					<p class='title' >
						<span id='titleP' class='txt active' name="addProvince"></span>
						<span id='titleC' class='txt'   name="addCity" style='display: none;'></span>
						<span id='titleD' class='txt' name="addCounty" style='display: none;'></span>
					</p>
					<div class='express' style="background-color: #fff;">
						<div id='provinces' class='cont' style='display:block;'>
						</div>
						<div id='cities' class='cont'>
						</div>
						<div id='district' class='cont'></div>
					</div>
				</li>
			</ul>
		 </div>
	
	
					<tr>
						<td><input type="text" id="addAddresss" name="addAddress"  class="form-control" placeholder="详细地址"></td>
					</tr>
					<tr>
						<td><input type="text" id="addPhones"  name="addPhone"  class="form-control" placeholder="手机号码"></td>
					</tr>
					<tr>
						<td><input type="text" id="addPhone2"  name="addPhone2"  class="form-control" placeholder="备用号码"></td>
					</tr>				
				</table>
					<input type="button" value="提交" class="btn btn-primary"  onclick="commits()" style="margin: 40px;">
			</form>	
		</div>
		<!-- 用户中心分享页面弹出层   end -->
	</div>
	</div>
	</div>
</body>
<script type="text/javascript">
/*  $("#adress").click(function(){
	alert(123);
	$("#shareDiv").css("display",'block');
	$("#hide_div").css("display",'block');

	$("#address-item").html("");
	$("#addEmps").val("");
	$("#addAddresss").val("");
	$("#addPhones").val("");
	$("#addPhone2").val("");
}) */


</script>
</html>