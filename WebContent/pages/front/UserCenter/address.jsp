<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,登陆,前台" />
		<meta http-equiv="description" content="E货源网站前台登陆页面" />
		<title>E货源--会员登陆</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/center2.css"/>
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script src="res/js/address.js"></script>
		<script src="res/js/validUtils.js"></script>
		<script type="text/javascript">
			$(function(){
				addrInitLoad();
			});
		</script>
	</head>
	<body>
		<!-- 弹出按钮 start -->
		<a class="add-btn" href="javascript:void(0);" data-event="address-show">新增收货地址</a>
		<!-- 弹出按钮 end -->
	
	
		<div class="layer-box">
			<!-- 遮盖层 start -->
			<div id="curtain" class="address-curtain" style="display: none;"></div>
			<!-- 遮盖层 end -->
			
			<!-- 收货地址弹出层  start -->
			<div id="dialogs" class="address-layer" style="display: none;">
				<div class="title">
					<span>添加收货地址</span>
					<a href="javascript:void(0);" class="close" data-event="address-close"></a>
				</div>
				
				<div class="content">
					<input type="hidden" name="addId"/>
					<div class="item">
						<span class="label"><em>*</em>收货人：</span>
						<input type="text" class="text" title="姓名" name="addEmp" data-options="required:true"/>
						<span class="error"></span>
					</div>
					<div class="item">
						<span class="label"><em>*</em>所在地址：</span>
						<div id="area-wrap">
							<div class='address' id='address' onclick='onOff()'>送至
								<span class='address-item'>请选择</span>
								<span class='caret'></span>
							</div>
							<ul id='options' class='menu'>
								<li>
									<p class='title'>
										<span id='titleP' class='txt active'>请选择</span>
										<span id='titleC' class='txt' style='display: none;'>请选择</span>
										<span id='titleD' class='txt' style='display: none;'>请选择</span>
									</p>
									<div class='express'>
										<div id='provinces' class='cont' style='display:block;'>
										</div>
										<div id='cities' class='cont'>
										</div>
										<div id='district' class='cont'></div>
									</div>
								</li>
							</ul>
						</div>
						<span class="error"></span>
					</div>
					<div class="item">
						<span class="label"><em>*</em>详细地址：</span>
						<input type="text" class="text" title="详细地址" name="addAddress" data-options="required:true"/>
						<span class="error"></span>
					</div>
					<div class="item">
						<span class="label"><em>*</em>手机号码：</span>
						<input type="text" maxlength="11" class="text" title="手机号码" id="addPhone" name="addPhone" data-options="required:true,phone:true"/>
						<span class="error"></span>
					</div>
					<div class="item">
						<span class="label"><em></em>备用号码：</span>
						<input type="text" maxlength="11" class="text" title="备用号码" name="addPhone2" data-options="phone:true,equalTo2:'#addPhone'"/>
						<span class="error"></span>
					</div>
					<div class="save-btn">
						<a href="javascript:void(0);" class="add-btn" data-event="address-save">保存收货地址</a>
					</div>
				</div>
			</div>
			<!-- 收货地址弹出层  start -->
		</div>
		
		
	</body>
</html>