<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,支付,前台" />
		<meta http-equiv="description" content="E货源网站前台登支付面" />
		<title>E货源--支付页面</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/pay.css"/>
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript">
			$(function(){
				loadOrder();//加载订单信息
			});
			
			 
			function show(){
				document.getElementById("dialogs").style="display:block;";
				document.getElementById("curtain").style="display:block;";
				var id=document.querySelector("#main .content #pay ul li.active").getAttribute("data-value");
				document.getElementById(id).style="display:block;";
				//alert(id);
				if(id=="wechat"){ //如果是微信支付
					/**
					* 将二维码的地址加入到图片
					*/
					var url = "wxpayAction/pay.action";
					var data = {orderId:"${param.ordId }"};
					$.post(url,data,function(mes){
						//alert(mes);
						if(mes!=null){
							var info = mes.split("_");
							if(info[0]=='no'){
								$("#wx_qr").attr("alt","找不到该订单，或者该订单已支付，请重新生成订单！");//加载信息
								$("#wx_qr").attr("src","");//加载信息
							}else{
								$("#wx_qr").attr("src",info[1]);//加载支付图片
							}
						}
						
						
					});
				}
				
				
			}
			function hide(){
				document.getElementById("dialogs").style="display:none;";
				document.getElementById("curtain").style="display:none;";
				var id=document.querySelector("#main .content #pay ul li.active").getAttribute("data-value");
				document.getElementById(id).style="display:none;";
			}
			 
			
			function itemClick(obj){
				document.querySelector("#main .content #pay ul li.active").removeAttribute("class");
				obj.className="active";
				//if(obj.data-value=='alipay'){ //支付宝是直接跳转到支付宝官方页面支付
				show();//显示 
				//}else if(obj.data-value=='wxpay'){ //微信支付是直接弹出二维码
				//	show();
				//}
			}
			
			function loadOrder(){
				var uri=window.location.href;
				var index=uri.indexOf("?")+1;
				var data=uri.substr(index);
				var url="frontShow/front/selectPay.action";
				$.post(url,data,function(ord){
					$("#ordCode").text(ord.ordCode);
					$("#payMoney").text("￥"+ord.payMoney);
					$("#ordAddress").text(ord.ordAddress.replace(/[,]/g,"，"));
					$("#ordEndtime").text(ord.ordEndtime);
				},"json");
			}
		</script>
	</head>
	<body>
		<!-- 遮盖层 -->
		<div id="curtain" style="display: none;"></div>
		
		<!-- 弹出层 -->
		<div id="dialogs" style="display: none;">
			
			
			<div id="alipay" style="display: none;">
				<div class="title">
					<span class="txt">正在支付...</span>
					<img class="close" src="res/images/delete.png" onclick="hide()"/>
				</div>
				<form action="alipayAction/pay.action" method="post">
					<input type="hidden" value="${param.ordId }" name="orderId">
					<input id="payBtn" class="btn" type="submit" value="立即支付"/>
				</form>
				<ul class="pay-status" align="center">
					<li class="failed">
						<span>支付失败</span>
						<span>重新支付</span>
					</li>
					<li class="succeed" onclick="javacript:location.href='pages/front/UserCenter/userCenter.jsp?select=3#Section1';">
						<span>支付成功</span>
						<span>前往订单列表</span>
					</li>
				</ul>
			</div>
			
			<div id="wechat" style="display: none;">
				<div class="title">
					<span class="txt">微信支付</span>
					<img class="close" src="res/images/delete.png" onclick="hide()"/>
				</div>
			
				<div class="wechat-img-box">
					<img id="wx_qr" alt="扫码支付" src="res/images/qr_code.gif">
				</div>
				<div class="txt">请使用微信扫一扫扫描二维码支付</div>
				<div class="txt">
					二维码<span class="time">2</span>小时内有效，如过期请刷新页面重新获取二维码
				</div>
				<ul class="pay-status" align="center">
					<li class="failed">
						<span>支付失败</span>
						<span>重新支付</span>
					</li>
					<li class="succeed" onclick="javacript:location.href='pages/front/UserCenter/userCenter.jsp?select=3#Section1';">
						<span>支付成功</span>
						<span>前往订单列表</span>
					</li>
				</ul>
			</div>
			
		</div>
		
		<!--  头部 start  -->
		<div id="header">
			<div class="content">
				<a href="pages/front/index.jsp"><img src="res/images/eLOGO.png"></a>
				<img src="res/images/top_1.jpg">
				<img src="res/images/top_2.jpg"> 
				<img src="res/images/top_3.jpg"> 
				<img src="res/images/top_4.jpg">
			</div>
		</div>
		<!--  头部 start  -->
		
		<!--  主体 start  -->
		<div id="main">
			<div class="content">
				<div class="title">您的订单已提交成功，正在等待付款！</div>
				<table>
					<tbody>
						<tr>
						    <th colspan="4">订单信息:</th>
						</tr>
						<tr>
						    <td width="100"> 编号:</td>
						    <td width="340">
						        <strong id="ordCode"></strong>
						        <a href="pages/front/UserCenter/userCenter.jsp?select=2#Section1">[查看订单]</a>
						    </td>
						    <td width="100">应付金额:</td>
						    <td>
						        <strong id="payMoney"></strong>
						        <span class="hidden">
						            (支付手续费:<span id="fee">￥0.00</span>)
						        </span>
						    </td>
						</tr>
						
						<tr>
							<td>收货地址:</td>
						    <td id="ordAddress" style="width: 500px;"></td>
						    <td>配送方式:</td>
						    <td>普通快递</td>
						</tr>
						<tr>
						    <td colspan="4">
								请您在
								<span id="ordEndtime" style="color: blue;"></span>  
								之前支付订单款项，否则该订单将失效！
						    </td>
						</tr>
					</tbody>
				</table>
				
				<div id="pay">
					<h5>选择支付平台 </h5>
					<ul>
						<li class="active" data-value="alipay" onclick="itemClick(this)"><img src="res/images/alipay.png"/></li>
						<li data-value="wechat" onclick="itemClick(this)"><img src="res/images/wechat.png"/></li>
					</ul>
					 
				</div>
			</div>
		</div>
		<!--  主体 end  -->
		
		
		
		<!--  尾部  start  -->
		<div id="tfoot">
			<div class="content">
				<a href="#">关于我们</a><a>|</a><a href="#">联系我们</a><a>|</a> <a href="#">投诉建议</a><a>|</a>
				<a href="#">合作招商</a> <a href="#" class="a">Copyright@2004-2017e货源ehuoyuan.cn版权所有</a>
			</div>
		</div>
		<!--  尾部 end  -->
	</body>
</html>