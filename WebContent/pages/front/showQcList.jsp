<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="authors" content="罗海兵、刘东、欧阳锋、汪冠夫、邓丽杰、谢毅明、增任、李芬尧" />
		<meta http-equiv="keywords" content="E货源,e货源,ehuoyuan,女装,批发,女鞋,网上商城,分享" />
		<meta http-equiv="description" content="E货源在线批发商城，主要为淘宝等卖家提供一线货源！具有比其他商城更低的价格，质量更好的货源，会员购买之后还能分享给亲朋好友购买。并且提取佣金，让您买商品不花一分钱！" />
		<title>E货源--更多产品</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="res/css/index.css"/>
		<link rel="stylesheet" href="res/css/glyphicon.css">
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		
		<script src="res/js/base.js"></script>
		<script src="res/js/showQcList.js"></script>
		<style type="text/css">
			#closes {
			    position: fixed;
			    margin: 0 auto;
			    top: -3%;
    			left: 16%;
			    z-index: 10001;
			}
			#shares {
			    width: 64%;
				height: 72%;
			    background-color: #fff;
			    position: fixed;
			    border: 1px solid #d3d3d0;
			    top: 18%;
				left: 20%;
				overflow-x: hidden;
				overflow-y:scroll; 
			    z-index: 10001;
			}
			/*设置遮障层  */
			 #hide_div{
			  	background:#111;
			  	width:100%;
			  	height:100%;
			  	border:1px solid white;
			  	position: fixed;
			  	display:none;
			  	top:0px;
			  	left:0px;
			  	z-index:99;
			  	opacity: 0.5;/*背景半透明*/
			 }
			
			#ff{
				font-family: "微软雅黑";
				font-size: 24px;
			}
			
			#ziti{
				font-family: "微软雅黑";
				font-size: 15px;
			}
			
			input{
				font-family: "微软雅黑";
				font-size: 15px;
			}
			
			h1, h2, h3 {
			    font-size: 25px;
			    font-weight: 400;
			}
			 .divcss5-4{
				 border-bottom:1px dashed #b4aaab;
				 height:2px;
			 }
			 .box_hd{
			 	overflow: hidden;
			 }
			 .box_tit {
			    float: left;
			    font-weight: normal;
			    font-size: 22px;
			    color: #222;
			    line-height: 44px;
			}
			.box_hd_arrow {
			    background-position: 0px 0px;
			    float: left;
			    position: relative;
			    width: 23px;
			    height: 23px;
			    top: 11px;
			    left: 4px;
			    cursor: pointer;
			    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAAAklEQVR4AewaftIAAAHiSURBVJXBvUuUcQAA4MefclJIGmWDUGr9CU72ZZwEgjYoHIH2MbUUIQTpFHdj2hBK/QFSQtCpSEc1lCIF1trUIhaGgwhd4hAHaoIHYr73nj5PxVJLixICkuhBK5pQhzx+YB6TmMGmCFWidWEIBbzEGJawipM4jQt4ggQGkfOfKnslMIqreIgpbNlrGcv4iqfoxgiu4T4KioJdCUyjCS2YxJZ4W5hECxoxjYSiYNczFNCJvL1qxcujEwU8VxTs6MQV9GHDfq+QFm8DfbiMLtsCAobQj3XRbiOFtHjr6McwQkASBbxT2gqSSCEt3nv8RXtAD8aVt4IkUkiLN47ugPP47GBWkEQKaaXNoTWgEUsObgVJpJAWbQHNAXVYdThbdlSI9hu1AXmccHD1mEUWGdGO40/AT5xxMPWYRRYZpZ3FYsA8LiqvHrPIIiNeG74ETKBXvFOYRRYZ5fViKmAGR9ChtDFkkVFeB47iQ8AmBjCKGtGuI6O8GoxgEJvBjhzmMI5K+60prxLj+IQ3tgW77iGBHGodTi1yqMZdRZUPGhoUbeA1LuExfuG7eBXowQS+4QYKiqrsVcAddGEYj/ACc1hAHnU4hzbcRDX6kfOfKtFyeIt29OAWmnEMa1jEPAbwEZsi/APwnHjX1+uAaQAAAABJRU5ErkJggg==);
			}
			.box_subtit {
			    float: left;
			    font-size: 14px;
			    color: #999;
			    height: 20px;
			    line-height: 20px;
			    margin: 12px 0 0 10px;
			}
			.slider_control_box{
				width: 100%;
				position: absolute;
				top: 50%;
				transform: translateY(-50%);
			}
			.slider_control{
				display: inline-block;
				width: 20px;
				height: 40px;
				background-color: rgba(0,0,0,.15);
				text-align: center;
				line-height: 40px;
				font-size: 20px;
			}
			.slider_control_left{
				float: left;
			}
			.slider_control_right{
				float: right;
			}
			.slider_control > i{
				color: hsla(0,0%,100%,.4);
			}
			.slider_indicators{
				text-align: center;
			}
			.slider_indicators_btn {
			    position: relative;
			    display: inline-block;
			    width: 16px;
			    height: 16px;
			    margin-right: 1px;
			    -moz-border-radius: 50%;
			    border-radius: 50%;
			    -webkit-transition: background .2s ease;
			    -o-transition: background ease .2s;
			    -moz-transition: background ease .2s;
			    transition: background .2s ease;
			}
			.slider_indicators_btn::after {
			    content: "";
			    display: block;
			    position: absolute;
			    left: 3px;
			    top: 3px;
			    width: 10px;
			    height: 10px;
			    -moz-border-radius: 50%;
			    border-radius: 50%;
			    border: 2px solid #b9beba;
			    -webkit-transition: all .2s ease;
			    -o-transition: all ease .2s;
			    -moz-transition: all ease .2s;
			    transition: all .2s ease;
			}
			
			.slider_indicators_btn_active::after {
			    border: 2px solid transparent;
			    background: #eb3436;
			}
			.slider_indicators_btn_active {
			    background: #fdd9dd;
			}
		</style>
		<script type="text/javascript">
			$(function(){
				var loadType = getParam("loadType");
				if("qc"==loadType){
					url = "frontShow/front/showAllByType.action";
					$(".links-li li.active a").wait(function(){
						var stId=$(".links-li li.active a").attr("data-value");
						//alert(stId);
						data.stId=stId;
						data.showType=2; //显示清仓产品,类型为2
						limit=100;
						initLoad();
					})
					
				}
				
				//给搜索框添加键盘回车点击事件
				$(document).on("keydown","#seek-key",function(event){
					if(event.keyCode == "13" && this.value){  
						searchKey();//当搜索框得到焦点并且关键字不为空时, 触发搜索按钮点击事件
				    }
				});
			})
		</script>
	</head>
	<body>
		<!-- 遮罩层 -->
		<div id="hide_div"></div>
		<!-- 页眉 -->
		<div id="header"></div>
		
		<!-- 右边工具栏  start  -->
		<div id="toolbar"></div>
		
		<div id="main">
			<div class="content">
				<div class="poster">
					<!-- <h1 style="font-size: 40px;color: #ff6100">海报</h1> -->
					<img src="res/userDefinde/haibao_index.png" style="max-width: 100%;max-height: 100%;">
				</div>
				
				
				<div class="search">
					<ul>
						<li class="opt">
							<span class="title">清仓商品</span>
							<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
							<!-- 
							<div class="text" style="border: 0px;">
								<a href="javascript:firstPage();">第一页</a>
								<a href="javascript:prvePage();">上一页</a>
								<input type = "number" min="1" max="" id ="curPage" value="1" name="curPage" style="width: 30px;">
								<a href="javascript:nextPage();">下一页</a>
								<a href="javascript:lastPage();">最后一页</a>
								总页数 <a id ="pageNum" href="javascript:void(0);"></a>
								总记录数 <a id ="total" href="javascript:void(0);"></a>
							</div>
							 -->
						</li>
						<li class="seek">
							<span class="title">搜索本站宝贝</span>
							<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
							<div class="text">
								<input type="text" id="seek-key"/>
								<div onclick="searchKey()">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<!-- 
				<div class="show-list" style="position: relative;">
				    <div class="box_hd">
				        <h3 class="box_tit">
				            今日推荐
				        </h3>
				        <i class="box_hd_arrow">
				        </i>
				        <span class="box_subtit">
				            最火热的推荐产品
				        </span>
				    </div>
				    <div class="slider_control_box">
				    	<span class="slider_control slider_control_left"><i class="glyphicon glyphicon-menu-left"></i></span>
				    	<span class="slider_control slider_control_right"><i class="glyphicon glyphicon-menu-right"></i></span>
				    </div>
				    <ul>
				        <li>
				            <a href="pages/front/info.jsp?proId=9810bde7-0312-47cc-a3a9-7bae69401e95&amp;stId=20e79900c5b411e78c1100163e02c911"
				            target="_blank">
				                <div class="img">
				                    <img alt="" src="/upload/product/1510819105_qa6z3hyk.jpg">
				                </div>
				                <div class="text">
				                    <div class="name">
				                        2017秋冬款中筒真皮女靴休闲平底靴简约款
				                    </div>
				                    <div class="old-price">
				                        市场价：
				                        <em>
				                            225.00
				                        </em>
				                    </div>
				                    <div class="new-price">
				                        厂家直销价：
				                        <em>
				                            180.00
				                        </em>
				                    </div>
				                </div>
				            </a>
				        </li>
				        <li>
				            <a href="pages/front/info.jsp?proId=c579464d-1ef6-4dd2-9b5e-deb8dcb40441&amp;stId=20e79900c5b411e78c1100163e02c911"
				            target="_blank">
				                <div class="img">
				                    <img alt="" src="/upload/product/1510820799_h9b3sdsh.jpg">
				                </div>
				                <div class="text">
				                    <div class="name">
				                        全真皮休闲女鞋秋冬真皮短靴软底防滑
				                    </div>
				                    <div class="old-price">
				                        市场价：
				                        <em>
				                            202.50
				                        </em>
				                    </div>
				                    <div class="new-price">
				                        厂家直销价：
				                        <em>
				                            162.00
				                        </em>
				                    </div>
				                </div>
				            </a>
				        </li>
				        <li>
				            <a href="pages/front/info.jsp?proId=c579464d-1ef6-4dd2-9b5e-deb8dcb40441&amp;stId=20e79900c5b411e78c1100163e02c911"
				            target="_blank">
				                <div class="img">
				                    <img alt="" src="/upload/product/1510820799_h9b3sdsh.jpg">
				                </div>
				                <div class="text">
				                    <div class="name">
				                        全真皮休闲女鞋秋冬真皮短靴软底防滑
				                    </div>
				                    <div class="old-price">
				                        市场价：
				                        <em>
				                            202.50
				                        </em>
				                    </div>
				                    <div class="new-price">
				                        厂家直销价：
				                        <em>
				                            162.00
				                        </em>
				                    </div>
				                </div>
				            </a>
				        </li>
				        <li>
				            <a href="pages/front/info.jsp?proId=c579464d-1ef6-4dd2-9b5e-deb8dcb40441&amp;stId=20e79900c5b411e78c1100163e02c911"
				            target="_blank">
				                <div class="img">
				                    <img alt="" src="/upload/product/1510820799_h9b3sdsh.jpg">
				                </div>
				                <div class="text">
				                    <div class="name">
				                        全真皮休闲女鞋秋冬真皮短靴软底防滑
				                    </div>
				                    <div class="old-price">
				                        市场价：
				                        <em>
				                            202.50
				                        </em>
				                    </div>
				                    <div class="new-price">
				                        厂家直销价：
				                        <em>
				                            162.00
				                        </em>
				                    </div>
				                </div>
				            </a>
				        </li>
				    </ul>
				    <div class="slider_indicators">
				    	<i class="slider_indicators_btn"></i><i class="slider_indicators_btn"></i><i class="slider_indicators_btn"></i><i class="slider_indicators_btn"></i>
				    </div>
				</div>
				 -->
				<!-- 商品列表 	-->
				<div class="show-list" id="showList">
					<ul id="show">
						
					</ul>
				</div>
			
			</div>
		</div>
		
		<!-- 页脚 -->
		<div id="footer"></div>
	</body>
</html>