<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="${pageContext.request.contextPath}/">
<script src="res/js/jquery-2.1.4.min.js"></script>
<title>我的分享</title>
<link rel="stylesheet" type="text/css"  href="res/css/myShare.css">  
<script type="text/javascript" src="res/js/share.js"></script>
</head>
<body>
	<div class="myShare">
		<div class="shareBaby">
			<div class="baby">
				<div class="babyImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/kf5c72wequlmuthkklpu/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="babyMes">
					<ul class="mes">
						<li>宝贝标题</li>
						<li><span class="proName">精品上衣</span></li>
						<li>厂家直销价：<span class="factoryPrice">￥100</span></li>
						<li>分享价格：<span class="myPrice">￥95</span></li>
						<li>下单时间：<span class="shareTime">2017-11-13 15:59:16</span></li>
					</ul>
					<div class="sm">
						<span class="smw">分享后，出现在宝贝详情页。带给更多买家参考，带来更多订单成交。</span>
					</div>
				</div> 
			</div>
			<div class="share">
				<div class="shareImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="shareMes">
					<ul class="mes">
						<li>分享的信息</li>
						<li>宝贝被游览：<span class="factoryPrice">100</span></li>
						<li>宝贝已成交：<span class="myPrice">2</span></li>
						<li>宝贝的收益：<span class="myYield">￥10</span></li>
						<li>分享时间：<span class="shareTime">2017-11-13 15:59:16</span></li>
					</ul>
					<span class="fx"><span class="fxw">告诉朋友：</span> <!-- JiaThis Button BEGIN -->

						<div class="jiathis_style_32x32">
							<a class="jiathis_button_cqq"></a> <a
								class="jiathis_button_weixin"></a> <a
								class="jiathis_button_qzone"></a> <a
								class="jiathis_button_tsina"></a> <a
								class="jiathis_button_renren"></a> <a
								class="jiathis_button_kaixin001"></a> <a
								class="jiathis_counter_style"></a>
						</div> <!-- JiaThis Button END --> </span>
				</div>
			</div>
		</div><div class="shareBaby">
			<div class="baby">
				<div class="babyImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/kf5c72wequlmuthkklpu/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="babyMes">
					<ul class="mes">
						<li>宝贝标题</li>
						<li><span class="proName">精品上衣</span></li>
						<li>厂家直销价：<span class="factoryPrice">￥100</span></li>
						<li>分享价格：<span class="myPrice">￥95</span></li>
						<li>下单时间：<span class="shareTime">2017-11-13 15:59:16</span></li>
					</ul>
					<div class="sm">
						<span class="smw">分享后，出现在宝贝详情页。带给更多买家参考，带来更多订单成交。</span>
					</div>
				</div>
			</div>
			<div class="share">
				<div class="shareImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="shareMes">
					<ul class="mes">
						<li>分享的信息</li>
						<li>宝贝被游览：<span class="factoryPrice">100</span></li>
						<li>宝贝已成交：<span class="myPrice">2</span></li>
						<li>宝贝的收益：<span class="myYield">￥10</span></li>
						<li>分享时间：<span class="shareTime">2017-11-13 15:59:16</span></li>
					</ul>
					<span class="fx"><span class="fxw">告诉朋友：</span> <!-- JiaThis Button BEGIN -->

						<div class="jiathis_style_32x32">
							<a class="jiathis_button_cqq"></a><a
								class="jiathis_button_weixin"></a> <a
								class="jiathis_button_qzone"></a> <a
								class="jiathis_button_tsina"></a> <a
								class="jiathis_button_renren"></a> <a
								class="jiathis_button_kaixin001"></a> <a
								class="jiathis_counter_style"></a>
						</div> <!-- JiaThis Button END --> </span>
				</div>
			</div>
		</div>
		<!-- <div class="shareBaby">
			<div class="baby">
				<div class="babyImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/kf5c72wequlmuthkklpu/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="babyMes">
					<ul class="mes">
						<li>宝贝标题</li>
						<li><span class="proName">精品上衣</span></li>
						<li>厂家直销价：<span class="factoryPrice">￥100</span></li>
						<li>分享价格：<span class="myPrice">￥95</span></li>
					</ul>
					<div class="sm">
						<span class="smw">分享后，出现在宝贝详情页。带给更多买家参考，带来更多订单成交。</span>

					</div>
				</div>
			</div>
			<div class="share">
				<div class="shareImg">
					<img class="img"
						src="https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg">
				</div>
				<div class="shareMes">
					<ul class="mes">
						<li>分享的信息</li>
						<li>宝贝被游览：<span class="factoryPrice">100</span></li>
						<li>宝贝已成交：<span class="myPrice">2</span></li>
						<li>宝贝的收益：<span class="myYield">￥10</span></li>
						<li>分享时间：<span class="shareTime">2017-11-13 15:59:16</span></li>
					</ul>
					<div class="sm">
						<span class="smw">要告诉朋友：<script type="text/javascript">
							(
											function() {
												var p = {
													url : 'http://www.ehuoyuan.cn', /*获取URL，可加上来自分享到QQ标识，方便统计*/
													desc : '', /*分享理由(风格应模拟用户对话),支持多分享语随机展现（使用|分隔）*/
													title : 'e货源好友分享分享', /*分享标题(可选)*/
													summary : '您的好友给你分享了一件商品', /*分享摘要(可选)*/
													pics : 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg', /*分享图片(可选)*/
													flash : '', /*视频地址(可选)*/
													site : 'e货源分享', /*分享来源(可选) 如：QQ分享*/
													style : '201',
													width : 32,
													height : 32
												};
												var s = [];
												for ( var i in p) {
													s
															.push(i
																	+ '='
																	+ encodeURIComponent(p[i]
																			|| ''));
												}
												document
														.write([
																'<a class="qcShareQQDiv" href="http://connect.qq.com/widget/shareqq/index.html?',
																s.join('&'),
																'" target="_blank">分享到QQ</a>' ]
																.join(''));
											})();
						</script> <script type="text/javascript">
							(function() {
								var p = {
									url : 'http://www.ehuoyuan.cn',
									showcount : '0',/*是否显示分享总数,显示：'1'，不显示：'0' */
									desc : '',/*默认分享理由(可选)*/
									summary : '您的好友给你分享了一件商品',/*分享摘要(可选)*/
									title : 'e货源好友分享分享',/*分享标题(可选)*/
									site : 'e货源分享',/*分享来源 如：腾讯网(可选)*/
									pics : 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg', /*分享图片(可选)*/
									style : '201',
									width : 113,
									height : 39
								};
								var s = [];
								for ( var i in p) {
									s.push(i + '='
											+ encodeURIComponent(p[i] || ''));
								}
								document
										.write([
												'<a version="1.0" class="qzOpenerDiv" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?',
												s.join('&'),
												'" target="_blank">12321321321321321312</a>' ]
												.join(''));
							})();
						</script> <script
								src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201"
								charset="utf-8"></script></span>
					</div>
					<script src="http://connect.qq.com/widget/loader/loader.js"
						widget="shareqq" charset="utf-8"></script>
				</div>
			</div>
		</div> -->
	</div>
	<script type="text/javascript">
	/* $(".jiathis_style_32x32").on("mouseover",function(){
		setShare('您的好友邀请您加入。。。','http://www.baidu.com','http://www.baidu.com/logo.jpg','', false);
	})  */
	var jiathis_config = {
			siteName:"E货源",
			url:$("base").attr("href")+"pages/front/index.jsp",
			title:"E货源商品分享",
			summary:$("base").attr("href")+"pages/front/index.jsp",
			/* summary:"您的好友将XXX"+"以5%的优惠分享给您",  */
			pic:"https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/gxm2wpetkc4hhj4x9zve/therma-sphere-男子训练夹克-WyTAX82B.jpg"
			
	}
	</script>
	<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js"
		charset="utf-8"></script>
</body>
</html>