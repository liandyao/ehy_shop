<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,登陆,对话框,演示" />
		<meta http-equiv="description" content="E货源网站前台对话框演示页面" />
		<title>E货源--会员登陆</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/dialog.css"/>
		<script type="text/javascript">
			function delConfirm(){
				document.querySelector("#dialog-curtain").style.display="block";
				document.querySelector("#dialog-confirm").style.display="block";
			}
			
		
			function btnConfirm(){
				alert("删除了一条记录");
				btnCancel();
			}
			
			function btnCancel(){
				document.querySelector("#dialog-curtain").style.display="none";
				document.querySelector("#dialog-confirm").style.display="none";
			}
		</script>
	</head>
	<body>
		<input type="button" value="删除" onclick="delConfirm()"/>
	
		<!-- 遮盖层  start -->
		<div id="dialog-curtain"></div>
		<!-- 遮盖层  end -->
		
		<!-- 确认对话框  start -->
		<div id="dialog-confirm" class="dialog-delorder">
		    <div class="dialog-bar">
		       	<span class="dialog-title">温馨提示</span>
		       	<span class="dialog-close" onclick="btnCancel()">×</span>
		    </div>
		    <div class="dialog-content">
		        <p>
		        	<img src="res/images/doubt.png" class="dialog-icon"/>
		        	<span class="dialog-text">确定从购物车中删除此商品？</span>
		        </p>
		    </div>
		    <div class="dialog-console">
		        <a class="console-btn-confirm" href="javascript:void(0);" onclick="btnConfirm()" title="确定">确定</a>
		        <a class="console-btn-cancel" href="javascript:void(0);" onclick="btnCancel()" title="取消">取消</a>
		    </div>
		</div>
		<!-- 确认对话框  end -->
	</body>
</html>