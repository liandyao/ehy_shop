<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/res/js/login.js"></script>	
	
<script type="text/javascript"
	src="${pageContext.request.contextPath }/res/js/sweetalert-dev.js"></script>

<link href="${pageContext.request.contextPath }/res/css/login2.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath }/res/css/sweetalert.css"
	rel="stylesheet" type="text/css">
 
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/res/css/tooltip-classic.css" />

<title>登录界面</title>
<style type="text/css">
/* #switch a{
		color: #eee;
	} */
</style>
<script type="text/javascript">

/**
 * 获取URL参数的方法
 * @param name 属性名
 * @returns 如果存在返回对应的值,不存在返回null
 */
function getParam(name) { 
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //设置正则表达式的规则
  var url= window.location.search;//获取URL地址
  url=decodeURIComponent(url);//URL解码
  var r =url.substr(1).match(reg); 
  if (r != null) {  
    return unescape(r[2]); 
  } 
  return null; 
}

 
 function dian(){
		var reg = /^1[3|4|5|7|8][0-9]{9}$/;
		var account = $("#phone").val();
		if(!reg.test(account)){
			$('#phone').css('border-color','red');
			$('#phone').attr('placeholder','请输入正确的手机号码');
			
		}else{
			$('#phone').css('border-color','#DDDDDD');
			var url = "${pageContext.request.contextPath }/member/front/selectPhoneRow.action";
			var date = {"phone":account};
			$.post(url,date,function(info){
				if(info.state==1){
					$(".em").html("该手机号码已被注册").css('color','red').show();
					var jumpTo = document.getElementById('send_btn');
					jumpTo.disabled=true;
				}
			})
		}
	}
 
$(function(){
	var type=getParam("type");
	if(type && type=="regist"){
		$('#switch_login').click();
	}
	 
});






</script>
<style type="text/css">
.top {
	border: 0px red solid;
}

.top-img {
	margin-bottom: 10px;
}

.img img {
	border: 0px solid red;
}
</style>
</head>


<body>
 
	<div class="body">
		<div class="top">
			<div class="img">
				<a href="${pageContext.request.contextPath }/pages/front/index.jsp"><img src="${pageContext.request.contextPath }/res/images/eLOGO.png"
					width="190"></a> <img class="top-img"
					src="${pageContext.request.contextPath }/res/images/top_1.jpg"
					width="190"> <img class="top-img"
					src="${pageContext.request.contextPath }/res/images/top_2.jpg"
					width="190"> <img class="top-img"
					src="${pageContext.request.contextPath }/res/images/top_3.jpg"
					width="150"> <img class="top-img"
					src="${pageContext.request.contextPath }/res/images/top_4.jpg"
					width="190">
			</div>
		</div>
		<div class='bgimg'
			style="width: 1199px;height:530px; background-image: url('${pageContext.request.contextPath }/res/images/loginBack.jpg');50% 0 no-repeat;border:0px red solid;">
			<div class="login" style="margin-top: 76px;margin-right: 98px;">

				<div class="header">
					<div class="switch" id="switch">
						<a class="switch_btn_focus" id="switch_qlogin"
							href="javascript:void(0);" tabindex="7">账户登录</a> <a
							class="switch_btn" id="switch_login" href="javascript:void(0);"
							tabindex="8">账户注册</a>
						<div class="switch_bottom" id="switch_bottom"
							style="position: absolute; width: 64px; left: 10px;"></div>
					</div>
				</div>


				<div class="web_qr_login" id="web_qr_login"
					style="display: block; height:196px;">

					<!--登录-->
					<div class="web_login" id="web_login">


						<div class="login-box">


							<div class="login_form">
								<form name="loginform" accept-charset="utf-8" id="login_form"
									class="loginForm" method="post">
									<input name="did" value="0" type="hidden"> <input
										name="to" value="log" type="hidden">
									<div class="uinArea" id="uinArea">

										<div class="inputOuter" id="uArea">

											<input id="u" name="mbPhones" class="inputstyle" type="text"
												placeholder="用户名/手机号">
										</div>
									</div>
									<div class="pwdArea" id="pwdArea">

										<div class="inputOuter" id="pArea">

											<input id="p" name="mbLoginPwd" class="inputstyle"
							   					type="password" placeholder="密码">
										</div>
									</div>
										<span class="loginSpan"  style=" margin-left:-380px; display: none"></span>
									
									<div style="padding-left: -30px; margin-top: 20px;">
										<input value="登 录" style="width: 240px;" class="button_blue"
											type="button" id="logins">
									</div>
									 
									<span class="tooltip tooltip-effect-1" style="cursor:pointer;position: relative;top:10px;">
										<span class="tooltip-item" style="background: #fff;">找回密码</span>
										<span class="tooltip-content clearfix" style="background: #340101">
											<span class="tooltip-text" style="text-align: center;">
												<ul>
													<li>
														<input type='text' class="inputstyle" id="forgetPwd" name="forgetPwd" style="width:180px;" placeholder="请输入注册的手机号码" required="required">
														<input type='button'  id="forgetbit"   value="发送验证码" onclick="showForgetPwd()">
													</li>
													<li>
														<input type='text'   class="inputstyle" id="forgetPwdYzm" name="forgetPwdYzm" style="width:180px;margin-top:5px; " placeholder="请输入验证码">需要手机验证码
													</li>
													<li>
														<input type='button' class="button" style="background: #333" id="resetPwd" value="找回密码" disabled="true" onclick="forgetPwdClick()" >
													</li>
												</ul>
											</span>
											<script type="text/javascript">
											function showForgetPwd(){
												var reg = /^1[3|4|5|7|8][0-9]{9}$/;
											
												var mbPhone = $("#forgetPwd").val();//取出用户的填入的号码
												 if(mbPhone==""){
													swal({
														  title: "手机号码不能为空!",
														  text: "",
														  timer: 1000,
														  showConfirmButton: true
														});
												}else if(!reg.test(mbPhone)){
													$('#forgetPwd').val('');
													$('#forgetPwd').css({"borderColor":"#f00"});
													$('#forgetPwd').attr('placeholder','请输入正确的手机号码');
												}else{
													
													 var url="${pageContext.request.contextPath}/member/front/forgetPwd.action";
													 var data = {'mbPhone':mbPhone};
													$.post(url,data,function(info){
														if(info.state==1){
															$("#resetPwd").attr("disabled",false);
															$("#resetPwd").css({"background":"blue","cursor":"pointer"});
															countDown2(60);
															
														}else{
															swal(info.mes);
														}
														
													}) 
													
												} 
											}
												

											
											
											function countDown2(secs){     
												var jumpTo = document.getElementById('forgetbit');
												jumpTo.value=secs+"秒可点击";  
												if(--secs>0){
												    setTimeout("countDown2("+secs+")",1000);  
												    jumpTo.disabled=true;
												    
												}else{
											    	jumpTo.value="发生验证码";
											    	jumpTo.disabled=false;
											    }
											} 
											
											/**重置密码*/
											function forgetPwdClick(){
												if($("#forgetPwdYzm").val()==""){
													swal({
														  title: "验证码不能为空!",
														  text: "",
														  timer: 1000,
														  showConfirmButton: true
														});
													return ;
												}
												var url = "${pageContext.request.contextPath}/member/front/resetPwd.action";
												var data = {"phone":$('#forgetPwd').val(),"yzm":$("#forgetPwdYzm").val()};
												$.post(url,data,function(mes){
													 
														swal({
															  title: "提示",
															  text: mes.mes,
															  timer: 10000,
															  showConfirmButton: true
															});
													 
												},"json")
											}
											</script>
										</span>
									</span>
								</form>
								
							</div>

						</div>
						
					</div>
					<!--登录end-->
				</div>

				<!--注册-->
				<div class="qlogin" id="qlogin" style="display: none;">

					<div class="web_login">
						<form name="form2" id="regUser" accept-charset="utf-8"
							method="post"
							action="${pageContext.request.contextPath }/member/register.action">
							<input name="intercept" value="true" type="hidden" /> <input
								name="to" value="reg" type="hidden"> <input name="did"
								value="0" type="hidden">
							<ul class="reg_form" id="reg-ul">
								
								<li>

									<div class="inputOuter2">
										<input id="phone" name="mbPhone" maxlength="16" 
											class="inputstyle2" type="text"  onblur="dian()"
											placeholder="手机号">
									</div>

								</li >
								
										<span class="em" style=" margin-left:-150px; height: 5px; display: none"></span>
								
								<li>
									<div class="inputOuter2">
										<input id="passwd" name="mbLoginPwd" maxlength="16" minlength="6"
											class="inputstyle2"  type="password" placeholder="登录密码">
									</div>

								</li>
								
								<span class="pwdSpan" style=" margin-left:-150px; height: 5px; display: none">   </span>
								<li>
									<div class="inputOuter2">
										<input id="passwds" name="mbLoginPwds" maxlength="16" minlength="6"
											class="inputstyle2" type="password" placeholder="确认密码">
									</div>

								</li>
								
								<li>
									<div class="inputOuter2">
										<input id="passwd2" name="invitationCode" maxlength="16"
											class="inputstyle2" type="text" placeholder="邀请码(选填)">
									</div>

								</li>
								
								
									<span class="yqmTs" style=" margin-left:-150px; display: none"></span>
								
								
								<li>
									<div class="inputOuter2">
										<input style="margin-left: 10px; width: 125px;height: 38px;" id="send_btn"
											class="button_blue" value="点击获取验证码" type="button"
											onclick="show();"> <input id="yzm" name="yzm"
											maxlength="10" class="inputstyle2" type="text"
											placeholder="验证码" style="width: 111px;margin-left:-167px">
									</div>

								</li>

								<li>
									<div class="inputArea">
										<input
											style="margin-top: 10px; margin-left: 10px; width: 244px;"
											class="button_blue" value="同意协议并注册" type="button"
											id="register">
									</div>

								</li>

								<div class="cl"></div>
							</ul>
						</form>


					</div>


					<!--注册end-->
				</div>
			</div>



		</div>
		<div class="down">
		<div class="down-xia">
			<a href="#">关于我们</a><a>|</a> <a href="#">联系我们</a><a>|</a> <a href="#">投诉建议</a><a>|</a>
			<a href="#">合作招商</a> <a href="#" class="a">Copyright@2004-2017e货源ehuoyuan.cn版权所有</a>
			</div>
		</div>
	</div>
	
	<!-- 注意：请将要放入弹框的内容放在比如id="HBox"的容器中，然后将box的值换成该ID即可，比如：$(element).hDialog({'box':'#HBox'}); -->
		<div id="HBox"  style="display:none;">
			<form action="" method="post" onsubmit="return false;">
				<ul class="list">
					
					<li>
						<strong>手 机 :<font color="#ff0000">*</font></strong>
						<div class="fl"><input type="text" name="phone" value="" class="ipt phone" /></div>
					</li>
					<li>
						<strong>验证码: <font color="#ff0000">*</font></strong>
						<div class="fl"><input type="text" name="email" value="" class="ipt email" />
						
						<input style="margin-left: 10px; width: 125px;height: 38px;"  
											class="button_blue" value="点击获取验证码" type="button"
											onclick="show();">
						</div>
					</li>
					
					<li><input type="submit" value="确认提交" class="submitBtn" /></li>
				</ul>
			</form>
		</div><!-- HBox end -->
	<script type="text/javascript">
	function show(){
		var reg = /^1[3|4|5|7|8][0-9]{9}$/;
	
		var mbPhone = $("#phone").val();//取出用户的填入的号码
		 if(mbPhone==""){
			swal({
				  title: "手机号码不能为空!",
				  text: "",
				  timer: 1000,
				  showConfirmButton: true
				});
		}else if(!reg.test(mbPhone)){
			$('#phone').attr('placeholder','请输入正确的手机号码');
		}else{
			
			 var url="${pageContext.request.contextPath}/member/front/yzm.action";
			 var data = {'mbPhone':mbPhone,intercept:true};
			$.post(url,data,function(info){
				if(info.state==1){
					countDown(60);
				}
				
			}) 
			
		} 
	}
		

	
	
	function countDown(secs){     
		var jumpTo = document.getElementById('send_btn');
		jumpTo.value=secs+"秒后重新发送";  
		if(--secs>0){
		    setTimeout("countDown("+secs+")",1000);  
		    jumpTo.disabled=true;
		    
		}else{
	    	jumpTo.value="点击获取验证码";
	    	jumpTo.disabled=false;
	    }
	}     

	 $(function(){
		 
		 
		 
		 
		 $("#logins").click(function(){
			 var u =$("#u").val();
			 var p = $("#p").val();
			 if(u=="" && p==''){
				 $("#u").css('border-color','red');
				 $("#p").css('border-color','red');
				 $("#u").attr("placeholder","用户名和手机密码不能为空");
				 $("#u").css('color','#D5D5D5');
			 }else if(p==""){
				 $("#p").css('border-color','red');
				if(p!=""){
					$("#p").css('border-color','#111');
				} 
			 }else if(u==""){
				 $("#u").css('border-color','red');
			 }else{
			 var url = "${pageContext.request.contextPath }/member/front/login.action";
				var data =$("#login_form").serialize();
				$.post(url,data,function(info){
					if(info.state==1){
						var type=getParam("type");
						if(type && type=="isLogin"){
							window.history.back();
						}else{
							location.href="${pageContext.request.contextPath }/pages/front/index.jsp";
						}
						
					}else{
						$(".loginSpan").html("账号或密码错误！").css('color','red').show();
						 $("#u").css('border-color','red');
						 $("#p").css('border-color','red');
					}
				})
			 }
		 })
		
		 
		
		   
		$("#register").click(function(){
			var reg = /^1[3|4|5|7|8][0-9]{9}$/;
			
			var mbPhone = $("#phone").val();//取出用户的填入的号码
			var mbPwd = $("#passwd").val();
			var yzm = $("#yzm").val();
			var mbPwds = $("#passwds").val();
			
			 var url = "${pageContext.request.contextPath }/member/front/register.action";
				var data =$("#regUser").serialize();
				if(mbPhone=='' || mbPwd=='' || yzm=='' || mbPwds=='' ){
					swal("手机号码,密码,验证码等必填项不能为空!");
					return;
				}else if(mbPwd!=mbPwds){
					swal("两次密码不一致!");
					return;
				}else if(!reg.test(mbPhone)){
					$('#phone').css('border-color','red');
					$('#phone').attr('placeholder','请输入正确的手机号码');
				}else if(mbPwd==''){
					$('#passwd').attr('placeholder','密码不能为空');
				}else if(mbPwd!=''){
					$('#passwd').css('border-color','#dddddd');
				}
				/*
				else if(mbPwd.length<6){
					$(".pwdSpan").html("密码不能小于6位数").css('color','red').show();
				}
				*/
				$.post(url,data,function(info){
					if(info.state==1){
						location.href="${pageContext.request.contextPath }/pages/front/index.jsp";
					}else if(info.state==2){
						$(".yqmTs").html("邀请码输入错误!").css('color','red').show();
					}else if(info.state==3){
						$(".em").html("该手机号码已被注册").css('color','red').show();
						
					}else{
						if(info.mes){
							swal(info.mes);
						}
						
					}
						
					
				})
		 })
		 
		 $("#phone").click(function(){
			 $(".em").css('display','none');
		 })
		 
		 $('input[name="invitationCode"]').click(function(){
			 $(".yqmTs").css('display','none');
		 })
		 /*
		 $("#passwd").change(function(){
			 var mbPwd = $("#passwd").val();
			 if(mbPwd.length<6){
				 $(".pwdSpan").html("密码不能小于6位数").css('color','red').show();
			 }else{
				 $(".pwdSpan").css('display','none');
			 }
		 })
		 */
		 $("#passwd").click(function(){
			 $(".pwdSpan").css('display','none');
		 })
		 
		 $("input[name='mbPhones']").click(function(){
			 $(".loginSpan").css('display','none');
		 })
		 /* 判断密码跟确认密码是否相同 */
		 $("#passwds").change(function(){
			 var mbPwd = $("#passwd").val();
			 var mbPwds = $("#passwds").val();
			 if(mbPwd!=mbPwds){
				 $('#passwds').css('border-color','red');
				$('#passwds').attr('placeholder','请输入正确的手机号码');
			 }else if(mbPwd==mbPwds){
				 $('#passwds').css('border-color','#dddddd');
			 }
		 })
		  
	 })

</script>

</body>
</html>