<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% 
String url = request.getRequestURI()+"?"+request.getQueryString(); 
url=url.replace("?null", "");
%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="罗海兵" />
		<meta http-equiv="keywords" content="E货源,模板,前台" />
		<meta http-equiv="description" content="E货源网站前台页眉、页脚模板页面" />
		<title>E货源--布局模板</title>
		<base href="<%=basePath %>"/>
		<link rel="stylesheet" type="text/css" href="res/css/common.css"/>
		<link rel="stylesheet" href="res/css/glyphicon.css">
		<script src="res/js/jquery-2.1.4.min.js"></script>
		<style type="text/css">
			/* 主体内容样式区       start  */
			#main{
				width: 100%;
				margin-bottom: 10px;
				margin-left: auto;
				margin-right: auto;
			}
			#main .content{
				background-color: #eee;
				height: 800px;
			}
			
		</style>
		<script type="text/javascript">
			$(function() {
				loadStation();
			});
			function link(obj,id){
				$("div.links-li li").removeClass("active");
				$(obj).parents("li").addClass("active");
				$(".links-li li > div").removeClass("active");
				$(obj).parents("li").prev().find("div").addClass("active");	
				loadImages(id);
			}
			function loadStation(){
				var url="station/back/findStation.action";
				var data={"intercept":true,};
				$.post(url,data,function(arr){
					var ul=$("div.links-li > ul")[0];
					$(ul).find("li:not(:first-child)").remove();
					var index=2;
					for(var i=0;i<arr.length;i++){
						if(arr[i].stType==0){
							index=i+index;//记录默认站点所在的节点序号
						} 
						$(ul).append("<li><a href='javascript:void(0);' "+
							"onclick='link(this,&apos;"+arr[i].stId+"&apos;)'>"+arr[i].stName+"</a>"+
							"<div class='line-height'></div></li>");
					} 
					var hq=$(ul).find("li:nth-of-type("+index+") > a")[0];//得到默认站点所在的节点
					$(hq).click();
				},"json");
			}
		</script>
	</head>
	<body>
		
		<div id="header">
			<!-- 锚链接锚点 -->
			<a id="main-top" name="main-top"></a>
			<!-- 
			<div style="background-image:url('res/userDefinde/top_head.jpg'); background-position: 100%;height: 61px "></div>
			 -->
			 
			 <div style="width: 100%">
			 	<img src="res/userDefinde/top_head.jpg" width="100%"/>
			 </div>
			<div class="content">
				<!-- 
				<div class="head-top" style="border: 1px solid red;display: block;">
					 
				</div>
			   -->
				
				<div class="logo">
					<a href="pages/front/index.jsp"><img alt="logo" src="res/images/eLOGO1.jpg" style="max-height: 80px;"></a>
					<!-- <a href="pages/back/login.jsp" target="_blank" id="backAdmin">网站管理</a> -->
					<div class="login-regist font-color-white header_account">
						<c:if test="${empty login}">
						  	<a href="pages/front/login.jsp">登陆</a>
							<div class="line-height" style="margin: 0 5px;"></div> 
							<a href="pages/front/login.jsp?type=regist">注册</a>
						</c:if>
						
						<c:if test="${not empty login}">
								<span class="glyphicon glyphicon-user"></span>
						  		<a target="_blank" style="position: relative;" href="pages/front/UserCenter/userCenter.jsp">个人中心</a>
						  		<a href="pages/front/login.jsp">切换账号</a>
								<!-- 
								<div class="header_account_hover" align="center">
									<span class="spanSJX"></span>
									<a href="#">
										<i style="background-position:0 115px;"></i>
										我的订单
									</a>
									<a href="#">
										<i style="background-position:0 93px;"></i>
										售后服务
									</a>
									<a href="#">
										<i style="background-position:0 70px;"></i>
										我的优惠
									</a>
									<a href="frontShow/front/logout.action">
										<i style="background-position:0 51px;"></i>
										退出账户
									</a>
								</div> 
								 -->
						</c:if>
					</div>
				</div>
				<div class="links-li">
					<ul>
						<li class="text-img"><img alt="text-img" src="res/images/text-img.jpg"/></li>
						
					</ul>
				</div>
			</div>
			<div class="line-width bg-color"></div>
		</div>
		
		<!-- 右边工具栏  start  -->
		<div id="toolbar">
			<ul>
				<li>
					<a href="<%=basePath %>/pages/front/UserCenter/userCenter.jsp#Section5">
						<div align="center" id="shopCart">
							<div>
								<img src="res/images/buycar.png" /><br/>
								<span class="txt" style="position: relative;top: -3px;">采购车</span>
							</div>
							<div style="margin-top: 5px;">
								<c:if test="${empty cartNum }">
									<span class="number">0</span>
								</c:if>
								<c:if test="${!empty cartNum }">
									<span class="number">${cartNum }</span>
								</c:if>
							</div>
							
						</div>
					</a>
				</li>
				<li>
					<div align="center">
						<div onclick="javascript:window.open('pages/front/chat.html');">
							<img src="res/images/service.png"/><br/>
							<span class="txt">客服</span>
						</div>
					</div>
				</li>
				 
				<li>
					<a id="ceiling" href="<%=url %>#main-top">
						<div align="center">
							<div>
								<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
							</div>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<!-- 右边工具栏  end  -->
		
		
		<div id="main">
			<div class="content">
			</div>
		</div>
		
		
		<div id="footer">
			
			<div class="content">
				<img src="res/userDefinde/buttom1_01.jpg"  width="292"/>
				
				<a href="javascript:buttomQueryproduct('e973e7f7c6da11e78c1100163e02c911')">
					<img src="res/userDefinde/buttom1_02.jpg"  width="292"/>
				</a>
				<a href="javascript:buttomQueryproduct('befc9278749c11e8b61100163e0a8114')">
					<img src="res/userDefinde/buttom1_03.jpg"  width="292"/>
				</a>
				<a href="javascript:buttomQueryproduct('fb3a6b29a8c011e8886600163e0a8114')">
					<img src="res/userDefinde/buttom1_04.jpg" width="298"/>
				</a>
				
				<img src="res/userDefinde/buttom_2.jpg" />
				<img alt="页尾" src="res/userDefinde/footer.jpg" style="max-height: 100%;max-width: 100%;"/>
				<div align="center" style="width: 1200px;height:50px;">
					&copy;ehuoyuan.cn 版权所有 总店客服QQ(接受投诉建议和分店加盟)<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=113370249&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:113370249:51" alt="点击这里给我发消息" title="点击这里给我发消息"/></a> 蜀ICP备14021245号-2
				</div>
				
			</div>
		</div>
		
	</body>
</html>