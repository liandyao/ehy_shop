<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<base href="<%=basePath %>>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>所有订单</title>
		<link rel="stylesheet" href="res/layui/css/layui.css">
		<link rel="stylesheet" href="res/css/treeStyle_ouyang.css"> 
		<script src="res/js/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script src="res/js/ehy_common.js" type="text/javascript" ></script>
		<script src="res/layui/layui.js"></script>
		<script src="res/js/back-order.js"></script>
		<script src="res/js/orderReturn.js"></script>
		<style type="text/css">
		#sousuo .layui-input {
			height: 30px;
		}
		#sousuo td {
			padding-right: 5px;
			padding-top: 5px;
		}
		body .layui-elem-quote {
			padding: 7px;
		}
		table .layui-input-inline{
			width: 170px;
		}
		td button{
			float: right;
		}
		.laytable-cell-1-proName p{
			margin: 3px 0px 0px 0px;
			padding: 0px;
			font-size: 10px;
			line-height: 11px;
		}
		table * {
			font-size : 12px;
		}
		
		.btn {
		    background-color: #4CAF50; /* Green */
		    border-radius:20%;  
		    color: white;  
		    cursor:pointer; 
		    font-size: 14px;
		    padding-left:2px;
		    padding-right:2px;
		}
		
		.btn_red {
		    background-color: #ff00ff; /* Green */
		    border-radius:20%;  
		    color: white;  
		    cursor:pointer; 
		    font-size: 14px;
		    padding-left:2px;
		    padding-right:2px;
		}
		</style>
	</head>
	<body>
		<div class="demoTable layui-elem-quote layui-quote-nm" id="searchDiv">
			<div class="layui-inline">
			  	<form class="layui-form" id="sousuo" style="float: left;">
			  		<table>
						<tr>
							<td>
							  <div class="layui-input-inline">
				        		 	<input id="startMxDateTime" lay-verify="date" placeholder="请选择下单时间"  
				        		 	autocomplete="off" class="layui-input" type="text">
						      </div>
						  	   -
							  <div class="layui-input-inline">
							        <input id="endMxDateTime" lay-verify="date" placeholder="请选择下单时间"  
							        autocomplete="off" class="layui-input" type="text">
							  </div>
							</td>
							<td>
							  <div class="layui-input-inline">
				        		 	<input id="startPayTime" lay-verify="date" placeholder="请选择支付时间"  
				        		 	autocomplete="off" class="layui-input" type="text">
						      </div>
						  	   -
							  <div class="layui-input-inline">
							        <input id="endPayTime" lay-verify="date" placeholder="请选择支付时间"  
							        autocomplete="off" class="layui-input" type="text">
							  </div>
							</td>
							<td>
							  <div class="layui-input-inline">
				        		 	<input id="startFhTime" lay-verify="date" placeholder="请选择发货时间"  
				        		 	autocomplete="off" class="layui-input" type="text">
						      </div>
						  	   -
							  <div class="layui-input-inline">
							        <input id="endFhTime" lay-verify="date" placeholder="请选择发货时间"  
							        autocomplete="off" class="layui-input" type="text">
							  </div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="layui-input-inline">
							        <input id="minMxMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="maxMxMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);"/>
							    </div>
							</td>
							<td>
								<div class="layui-input-inline">
							        <input id="minMxMoneyFact" placeholder="实际付款金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="maxMxMoneyFact" placeholder="实际付款金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);"/>
							    </div>
							</td>
							<td>
								<div id="seachStation" class="layui-input-inline">
									<select name="stId" id="stId"> 
							 
									</select>
								</div>
								<button type="button" class="layui-btn layui-btn-small" onclick="reloadRec();"><i class="layui-icon">&#xe615;</i>搜索</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		
		
		<table class="layui-hide" id="LAY_table_user" lay-filter="demo"></table> 
			
		
		<script type="text/html" id="barDemo">
  			<a class="layui-btn layui-btn-mini" lay-event="detail">查看物流</a>
		</script>
		
		<script type="text/html" id="fhTime">
			<span>{{ new Date(d.fhTime).Format('yyyy-MM-dd hh:mm:ss') }}</span>
		</script>
		
		<script type="text/html" id="stateCn">
			 {{
				stateCnFun(d.ordState,d)
			 }} 
		</script>
		<script type="text/javascript">
		
		</script>
		<script type="text/html" id="proNameTemp">
			{{ d.proName.split('_')[0] }}	
		</script>
		
		<script type="text/javascript">
		var table;
		var laydate;
		var loadIndex;
		
		$(function (){
			showStation();
			var queryStation = parent.queryStation;
			if(queryStation){
				$("#searchDiv").show();
			}else{
				$("#searchDiv").hide();
			}
		});
		
		
		
		layui.use(['table','laydate'], function() {
			table = layui.table;
			loadIndex = layer.load();
			//方法级渲染
			  table.render({
				loading: true
				,elem: '#LAY_table_user'
			    ,url: 'orderItem/back/showList.action' 
			    ,method: 'post'
			    ,where:{
			    	"state" : '2'
			    }
			    ,cols: [[
			      {field:'ordId', title: '订单号', width:190, align:'center', fixed: 'left'}
			     
			      ,{field:'proName', title: '商品名称', width:260, align:'center', fixed: 'left', templet:'#proNameTemp', event: 'showName', style:'cursor: pointer;'}	
			      ,{field:'stName', title: '站点名称', width:80, align:'center'} 
			      ,{field:'ordExpress', title: '快递公司及编码', width:100, align:'center'}
			      ,{field:'ordExpressCode', title: '快递单号', width:120, align:'center'}
			      ,{field:'proCode', title: '商品编码', width:130, align:'center'}
			      ,{field:'mxNum', title: '数量', width:60, align:'center'}
			      ,{field:'ordState', title: '订单状态', width:180, align:'center', templet:'#stateCn',event: 'ordState', style:'cursor: pointer;'}
			      ,{field:'mxMoneyFact', title: '实际付款', width:80, align:'center'}
			      ,{field:'mxMoney', title: '商品总金额', width:100, align:'center'}
			      ,{field:'ordFreight', title: '运费', width:80, align:'center'}
			      ,{field:'mxDateTime', title: '下单时间', width:145, align:'center'}
			      ,{field:'payTime', title: '付款时间', width:145, align:'center'}
			      ,{field:'fhTime', title: '发货时间', width:145, align:'center', templet:'#fhTime'}
			      ,{field:'ordMember', title: '收货人', width:100, align:'center'}
			      ,{field:'ordPhone', title: '联系方式', width:110, align:'center'}
			      ,{field:'ordAddress', title: '收货地址', width:340, align:'center'}
			      ,{field:'mxRemark', title: '备注', width:200, align:'center'}
			      //,{fixed: 'right', width:100, align:'center', toolbar: '#barDemo'}
			    ]] 
			    ,id: 'd_order'
			    ,page: true
			    ,height: 400
			    ,size: "sm"
			    ,done: function(res, curr, count){
			    	layer.close(loadIndex);//加载层关闭  
			    }
			  });
			
			
			//监听工具条
			table.on('tool(demo)', function(obj){
			    var data = obj.data;
			    if(obj.event === 'detail'){
			    	ckeckWl(data.ordExpress, data.ordExpressCode);
			    } else if(obj.event === 'edit'){
			     	
			    } else if(obj.event === 'showName'){
			    	queryParticular(data.proName+'_商品编码：'+data.proCode);
			    } else if(obj.event === 'ordState'){
			    	showState(data);
			    }
			});
			
			laydate = layui.laydate;
			//下单时间的日期控件
			var startMxDateTime = laydate.render({
				elem : '#startMxDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endMxDateTime.config.min.year = date.year;
						endMxDateTime.config.min.month = date.month - 1;
						endMxDateTime.config.min.date = date.date;
					}else{
						endMxDateTime.config.min.year = '1900';
						endMxDateTime.config.min.month = '01';
						endMxDateTime.config.min.date = '01';
					}
				}
			});
			var endMxDateTime = laydate.render({
				elem : '#endMxDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startMxDateTime.config.max.year = date.year;
						startMxDateTime.config.max.month = date.month - 1;
						startMxDateTime.config.max.date = date.date;
					}else{
						startMxDateTime.config.max.year = '2099';
						startMxDateTime.config.max.month = '12';
						startMxDateTime.config.max.date = '31';
					}
				}
			});
			//付款时间的日期控件
			var startPayTime = laydate.render({
				elem : '#startPayTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endPayTime.config.min.year = date.year;
						endPayTime.config.min.month = date.month - 1;
						endPayTime.config.min.date = date.date;
					}else{
						endPayTime.config.min.year = '1900';
						endPayTime.config.min.month = '01';
						endPayTime.config.min.date = '01';
					}
				} 
			});
			var endPayTime = laydate.render({
				elem : '#endPayTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startPayTime.config.max.year = date.year;
						startPayTime.config.max.month = date.month - 1;
						startPayTime.config.max.date = date.date;
					}else{
						startPayTime.config.max.year = '2099';
						startPayTime.config.max.month = '12';
						startPayTime.config.max.date = '31';
					}
				}
			});
			
			//发货时间的日期控件
			var startFhTime = laydate.render({
				elem : '#startFhTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endFhTime.config.min.year = date.year;
						endFhTime.config.min.month = date.month - 1;
						endFhTime.config.min.date = date.date;
					}else{
						endFhTime.config.min.year = '1900';
						endFhTime.config.min.month = '01';
						endFhTime.config.min.date = '01';
					}
				} 
			});
			var endFhTime = laydate.render({
				elem : '#endFhTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startFhTime.config.max.year = date.year;
						startFhTime.config.max.month = date.month - 1;
						startFhTime.config.max.date = date.date;
					}else{
						startFhTime.config.max.year = '2099';
						startFhTime.config.max.month = '12';
						startFhTime.config.max.date = '31';
					}
				}
			});
			
		});
		
		function reloadRec(){
			table.reload('d_order', {
				where : {
					'state' : "2",
					'stId' : $("#stId").val(),
					'startMxDateTime' : $("#startMxDateTime").val(),
					'endMxDateTime' : $("#endMxDateTime").val()==""?"":$("#endMxDateTime").val()+" 23:59:59",
					'startPayTime' : $("#startPayTime").val(),
					'endPayTime' : $("#endPayTime").val()==""?"":$("#endPayTime").val()+" 23:59:59",
					'startFhTime' : $("#startPayTime").val(),
					'endFhTime' : $("#endPayTime").val()==""?"":$("#endPayTime").val()+" 23:59:59",
					'minMxMoney' : $("#minMxMoney").val(),
					'maxMxMoney' : $("#maxMxMoney").val(),
					'maxMxMoneyFact' : $("#maxMxMoneyFact").val(),
					'minMxMoneyFact' : $("#minMxMoneyFact").val()
				}
			});
		}
		
		var $ = layui.$, active = {
			reload : function() {
				reloadRec()
			}
		};
		
		$('.demoTable .layui-btn').on('click', function() {
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		</script>
	</body>
</html>