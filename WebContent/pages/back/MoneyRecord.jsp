<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>资金结算记录</title>
		<link rel="stylesheet" href="res/layui/css/layui.css">
		<script src="res/js/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script src="res/js/ehy_common.js" type="text/javascript" ></script>
		<script src="res/js/drag-sort.js" type="text/javascript" ></script>
		<script src="res/layui/layui.js"></script>
		<style type="text/css">
			input::-webkit-outer-spin-button,  
			input::-webkit-inner-spin-button{  
			    -webkit-appearance: none !important;  
			    margin: 0; 
			} 
			.demoTable input{
				height: 30px;
				width: 100px;
			}
			
			/* .demoTable button{
				height: 30px; 
			} */	
		</style>
	</head>
	<body>
		<div class="demoTable">
		    时间：
		  <div class="layui-input-inline">
		        <input name="startTime" id="startTime" lay-verify="date" placeholder="请选择起始日"  autocomplete="off" class="layui-input" type="text">
		      </div>
		   ~
		  <div class="layui-input-inline">
		        <input name="endTime" id="endTime" lay-verify="date" placeholder="请选择结束日"  autocomplete="off" class="layui-input" type="text">
		      </div>
		      金额:
		      <div class="layui-input-inline">
		        <input name="minMoney" id="minMoney" lay-verify="number" placeholder="请选择起金额"  autocomplete="off" class="layui-input" type="number">
		      </div>
		   ~
		  <div class="layui-input-inline">
		        <input name="maxMoney" id="maxMoney" lay-verify="number" placeholder="请选择结束金额"  autocomplete="off" class="layui-input" type="number">
		      </div>
		  <button class="layui-btn" data-type="reload"><!-- <i class="layui-icon">&#xe615;</i> -->搜索</button>
		</div>
	
		
		<table class="layui-table" lay-data="{height:383, size:'sm', url:'moneyRecord/back/showList.action', page:true, method:'post', id:'idTest', hidden:'true'}"  lay-filter="demo" id="tab">
		  <thead>
		    <tr>
		      <th lay-data="{field:'mrDatetime', width:250, sort: true}">结算时间</th>
		      <th lay-data="{field:'mrMoney', width:150, sort: true}">结算金额</th>
		      <th lay-data="{field:'mrRemark', width:300, sort: true}">结算说明</th>
		    </tr>
		  </thead>
		</table>
		<script type="text/javascript">
			var table;
			var laydate;
			layui.use([ 'table', 'laydate' ], function() {
				table = layui.table;
				table.on('tool(demo)', function(obj) {
					var data = obj.data;
					if (obj.event === 'detail') {

						layer.msg("查看的操作！");
					} else if (obj.event === 'fahuo') {

						layer.msg("发货的操作！");
					} else if (obj.event === 'tuihuo') {

						layer.msg("退货的操作！");
					}
				});

				laydate = layui.laydate;
				//日期
				var startTime = laydate.render({
					elem : '#startTime',
					trigger : 'click',
					done : function(value, date, endDate) {
						if(value!=''){
							endTime.config.min.year = date.year;
							endTime.config.min.month = date.month - 1;
							endTime.config.min.date = date.date + 1;
						}else{
							endTime.config.min.year = '1900';
							endTime.config.min.month = '01';
							endTime.config.min.date = '01';
						}
						reloadRec();
					}
				});
				//日期
				var endTime = laydate.render({
					elem : '#endTime',
					trigger : 'click',
					done : function(value, date, endDate) {
						if(value!=''){
							startTime.config.max.year = date.year;
							startTime.config.max.month = date.month - 1;
							startTime.config.max.date = date.date - 1;
						}else{
							startTime.config.max.year = '2099';
							startTime.config.max.month = '12';
							startTime.config.max.date = '31';
						}
						reloadRec();
					}
				});

			});
			
			function reloadRec(){
				table.reload('idTest', {
					where : {
						startTime : $('#startTime').val(),
						endTime : $("#endTime").val()==''?'':$("#endTime").val()+" 99:99:99",
						maxMoney : $('#maxMoney').val(),
						minMoney : $('#minMoney').val()
					}
				});
			}
			
			var $ = layui.$, active = {
				reload : function() {
					reloadRec()
				}
			};
			$('.demoTable .layui-btn').on('click', function() {
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
		</script>
	</body>
</html>