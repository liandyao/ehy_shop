<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>代理人管理</title>
<link rel="stylesheet" href="res/layui/css/layui.css">
<script src="res/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="res/layui/layui.js"></script>
<script src="res/js/ehy_common.js" type="text/javascript" ></script>
<script src="res/js/drag-sort.js" type="text/javascript"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="demoTable">
 站点名称：
  <div class="layui-inline">
    <input class="layui-input" name="stName" id="stName" style="width: 150px" autocomplete="off">
  </div>
   邀请码：
  <div class="layui-inline">
    <input class="layui-input" name="code" id="code" style="width: 150px" autocomplete="off">
  </div>
  会员名称：
  <div class="layui-inline">
    <input class="layui-input" name="mbName" id="mbName" style="width: 150px" autocomplete="off">
  </div>
 
  <button class="layui-btn" data-type="reload"><i class="layui-icon">&#xe615;</i>搜索</button>
  <button data-type="auto" class="layui-btn layui-btn-normal" onclick="showAddOrUpdate()"><i class="layui-icon">&#xe608;</i>新增</button>
</div>

	<table class="layui-table"
		lay-data="{ height:420,size:'sm', url:'invitationCode/back/showList.action', page:true, id:'idTest',method:'post'}"
		lay-filter="demo">
		<thead>
			<tr>
				<th lay-data="{field:'stName', width:117,sort: true, align:'center'}">站点名称</th>
				<th lay-data="{field:'code', width:125, sort: true, align:'center'}">邀请码</th>
				<th lay-data="{field:'mbName', width:125, align:'center'}">会员名称</th>	
				<th lay-data="{field:'mbWeixin', width:125, align:'center'}">会员名称</th>		
				<th lay-data="{field:'isva', width:120, align:'center',templet: '#isvaTpl'}">是否代理人</th>
				<th lay-data="{field:'count', width:120,sort: true, align:'center'}">邀请人数</th>
				<th lay-data="{field:'thisOrder', width:120,sort: true, align:'center',event: 'thisOrderMap'}">本月订单数</th>
				<th lay-data="{field:'oneOrder', width:120,sort: true, align:'center',event: 'oneOrderMap'}">上月订单数</th>
				<th lay-data="{fixed: 'right', width:290, align:'center', toolbar: '#barDemo'}"></th> 
			</tr>
		</thead>
	</table> 
	
	<!-- 判断是否为代理 -->
	<script type="text/html" id="isvaTpl">
  		{{#  if(d.isva === 2){ }}
    			<p class="layui-btn layui-btn-xs"> 是 </p>
  		{{#  } }}
		{{#  if(d.isva === 1){ }}
    		   <p class="layui-btn layui-btn-danger layui-btn-xs">否</p>
  		{{#  } }}  
	</script>
	<script type="text/html" id="barDemo">
	  <a class="layui-btn layui-btn-xs" lay-event="stick">置顶</a>
      <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="history">历史记录</a>
 	  {{#  if(d.isva == '1'){ }}
       		<a class="layui-btn layui-btn-xs" lay-event="setAgent">设为代理人</a>
      {{#  } else { }}
        	<a class="layui-btn layui-btn-xs" lay-event="cancelAgent">取消代理人</a>
      {{#  } }}
       
      <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
	</script>
 
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
	var table;
	function thisOrderTpl(code){ 
		var addCen= layer.open({
	        type: 2//样式
	        ,skin: 'layui-layer-molv'//样式
	        ,area: ['85%', '90%']
		    ,title: '统计图'
		    ,id : "mesFrom"
	        ,shade: [0.8, '#393D49'] //显示遮罩
	        ,shadeClose:true//点击也能遮罩层关闭
	        ,anim:2//弹出动画
	        ,maxmin: true //允许全屏最小化
	        ,content: 'pages/back/thisOrderMap.jsp?code='+code
	      }); 
	}
	function oneOrderTpl(code){
		var addCen= layer.open({
	        type: 2//样式
	        ,skin: 'layui-layer-molv'//样式
	        ,area: ['85%', '90%']
		    ,title: '统计图'
		    ,id : "mesFrom"
	        ,shade: [0.8, '#393D49'] //显示遮罩
	        ,shadeClose:true//点击也能遮罩层关闭
	        ,anim:2//弹出动画
	        ,maxmin: true //允许全屏最小化
	        ,content: 'pages/back/oneOrderMap.jsp?code='+code
	      });
	}
	
	function history(code){
		var addCen= layer.open({
	        type: 2//样式
	        ,skin: 'layui-layer-molv'//样式
	        ,area: ['85%', '95%']
		    ,title: '统计图'
		    ,id : "mesFrom"
	        ,shade: [0.8, '#393D49'] //显示遮罩
	        ,shadeClose:true//点击也能遮罩层关闭
	        ,anim:2//弹出动画
	        ,maxmin: true //允许全屏最小化
	        ,content: 'pages/back/history.jsp?code='+code
	      });
	}
	
	 
	
    layui.use(['table','laydate'], function(){
    	table = layui.table;
  		var laydate = layui.laydate;
  
  		//监听工具条
  		table.on('tool(demo)', function(obj){
    		var data = obj.data;
    		//置顶的方法
    		if(obj.event === 'stick'){
        		if(data.sort!=1){
        			layer.load();
    				var data = {"start":data.sort,
    						"end":1,
    						"inviId":data.inviId};
    				var url = "invitationCode/back/sortInvitationCode.action";
    				$.post(url, data, function(info){
    					layer.closeAll('loading');
    					if(info.state==1){
    						layer.msg(info.mes);
    						table.reload('idTest');
    						
    					}
    				});
    			}else{
    				layer.msg("该行已被置顶，请勿重复操作！");
    			} 
        	 //设置为为代理人的方法
       		 } else if(obj.event === 'setAgent'){
    			if(data.isva==2){
    		        layer.msg("该会员已是代理人，请勿重复设置！"); 
    	        }else{ 
    				layer.confirm('&nbsp;&nbsp;&nbsp;&nbsp;确认设为代理人吗？该会员将会<br/>设为代理会员。', function(index){
    					layer.load();
    					var url = "invitationCode/back/setAgent.action";
	    	       		var d ={inviId:data.inviId,mbId:data.mbId};
	    	        	$.post(url,d,function(mes){
	    	        		layer.closeAll('loading');
	    	        		if(mes.state==1){
	    	        			layer.msg(mes.mes); 
	    	        		}else{
	    	        			layer.msg(mes.mes); 
	    	        		}
	    	        		table.reload('idTest');
	    	       		 },"json")
    	        	 layer.close(index);
    	       		});
    			}
    	 	//取消代理人的方法
		    } else if(obj.event==='cancelAgent'){
		    	if(data.isva==1){
		    		layer.msg("该会员不是代理人，请勿重复操作！"); 
		    	}else{ 
		    		layer.confirm('&nbsp;&nbsp;&nbsp;&nbsp;确认取消代理人吗？该会员将会<br/>降为普通会员。', function(index){
		    			layer.load();
		    			var url = "invitationCode/back/cancelAgent.action";
		    	        var d ={inviId:data.inviId,mbId:data.mbId};
		    	        $.post(url,d,function(mes){
		    	        	layer.closeAll('loading');
		    	        	if(mes.state==1){
		    	        		layer.msg(mes.mes); 
		    	        	}else{
		    	        		layer.msg(mes.mes); 
		    	        	}
		    	        	table.reload('idTest');
		    	        },"json")
		    	        layer.close(index);
		    	      });
		    	} 
		    }else if(obj.event === 'del'){//删除的方法
            	layer.confirm('真的删除行么', function(index){
    	  			layer.load();
        			var url = "invitationCode/back/delete.action"; 
       				var d ={inviId:data.inviId,mbId:data.mbId};
			        $.post(url,d,function(mes){
			        	layer.closeAll('loading');
			        	if(mes.state==1){
			        		layer.msg(mes.mes); 
			        	}else{
			        		layer.msg(mes.mes); 
			        	}
			        	table.reload('idTest');
			        },"json")
       
        			layer.close(index);
         
     		     });
    		} else if(obj.event === 'thisOrderMap'){
    			thisOrderTpl(data.code);
    		}else if(obj.event === 'oneOrderMap'){
    			oneOrderTpl(data.code);
    		}else if(obj.event === 'history'){
    			history(data.code);
    		}
  	  });
   	  //条件查询的方法
  	  var $ = layui.$, active = { 
		    reload: function(){ 
		      var stName = $('#stName');
		      var code = $('#code');
		      var mbName = $('#mbName');
		      var invitationMember = $('#invitationMember'); 
		      table.reload('idTest', {
		        where: {
		        	stName: stName.val(),
		        	code: code.val(),
		        	mbName: mbName.val(),
		        	invitationMember: invitationMember.val()
		        } 
		      });
		    }
  		};
   	  //搜索按钮点击的方法
	  $('.demoTable .layui-btn').on('click', function(){ 
		    var type = $(this).data('type');
		    active[type] ? active[type].call(this) : '';
	  });
  
   	  
  });
    //显示维护层的方法
  function showAddOrUpdate(inviId,stId,mbId){ 
	var from = $("#fromStr").html(); 
	var addCen= layer.open({
        type: 2//样式
        ,skin: 'layui-layer-molv'//样式
        ,area: ['85%', '70%']
	    ,title: '邀请码维护'
	    ,id : "mesFrom"
        ,shade: [0.8, '#393D49'] //显示遮罩
        ,shadeClose:true//点击也能遮罩层关闭
        ,anim:2//弹出动画
        ,maxmin: true //允许全屏最小化
        ,content: 'pages/back/invitationCodeForm.jsp?inviId='+inviId+'&stId='+stId+'&mbId='+mbId
      }); 
  }

</script>

</body>
</html>