<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>清仓产品维护</title>
<!-- zTree -->
<link rel="stylesheet" href="res/zTree_v3/css/zTreeStyle/zTreeStyle.css"
	type="text/css">
<link rel="stylesheet" href="res/css/treeStyle_ouyang.css">
<link rel="stylesheet" href="res/layui/css/layui.css">
<script src="res/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="res/layui/layui.js"></script>
<!-- 判断是否登录的JS -->
<script src="res/js/ehy_common.js"></script>
<!-- zTree -->
<script type="text/javascript"
	src="res/zTree_v3/js/jquery.ztree.core.min.js"></script>
<style type="text/css">
body {
	padding: 5px;
}

#sousuo .layui-input {
	height: 30px;
}

#sousuo td {
	padding-right: 5px;
	padding-top: 5px;
}

body .layui-elem-quote {
	padding: 7px;
}
</style>
</head>
<body>
	<div class="demoTable layui-elem-quote layui-quote-nm">
		<div class="layui-inline">
			<form class="layui-form" id="sousuo" style="float: left;">
				<table>
					<tr>
						<td><input class="layui-input" name="priductData"
							maxlength="50" id="priductData" placeholder="产品编号/名称/品牌"
							autocomplete="off" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"></td>
						<td><input id="typeId" type="hidden" name="typeId"> <input
							type="text" id="typeName" onclick="showMenu(); return false;"
							placeholder="产品类型" autocomplete="off" class="layui-input"
							readonly="readonly"></td>
						<td><select id="stId" name="stId" lay-verify="" width="50px">
								<option value="">分站站点</option>
						</select></td>
						<td><select id="isva" name="isva" lay-verify="" width="50px">
								<option value="">是否上架</option>
								<option value="1">已上架</option>
								<option value="0">未上架</option>
						</select></td>
					</tr>
					<tr>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proFactoryPriceMix"
										name="proFactoryPriceMix" maxlength="10" placeholder="￥出厂价格"
										autocomplete="off" class="layui-input"
										onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proFactoryPriceMax"
										name="proFactoryPriceMax" maxlength="10" placeholder="￥出厂价格"
										autocomplete="off" class="layui-input"
										onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPriceMix" name="proPriceMix"
										maxlength="10" placeholder="￥市场价格" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPriceMax" name="proPriceMax"
										maxlength="10" placeholder="￥市场价格" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPrice0Mix" name="proPrice0Mix"
										maxlength="10" placeholder="￥产家直销价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPrice0Max" name="proPrice0Max"
										maxlength="10" placeholder="￥产家直销价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 120px;">
									<input type="text" id="proPrice1Mix" name="proPrice1Mix"
										maxlength="10" placeholder="￥银/金牌会员价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 120px;">
									<input type="text" id="proPrice1Max" name="proPrice1Max"
										maxlength="10" placeholder="￥银/金牌会员价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div class="layui-inline">
			<button style="margin-top: 5px;" class="layui-btn layui-btn-sm"
				data-type="reload"><i class="layui-icon">&#xe615;</i>搜索</button>
		    <button style="margin-top: 5px;" class="layui-btn layui-btn-sm layui-btn-warm"
				data-type="add"><i class="layui-icon">&#xe654;</i>增加</button>
		</div>
	</div>
	<table class="layui-table"
		lay-data="{height:400,size:'sm',skin:'row ', url:'product/back/findAll2.action?isQc=1',method:'post',page:true,limit:10, id:'tb'}"
		lay-filter="tb">
		<thead>
			<tr>
				<th
					lay-data="{field:'proName', sort: true,fixed:true,width:350,align:'left',templet:'#proNameTpl'}">产品名称</th>
				<th lay-data="{field:'proCode', sort: true, width:150}">产品编号</th>
				<th
					lay-data="{field:'isva', sort: true,width:110,align:'center',templet:'#isvaTpl'}">状态</th>
				<th
					lay-data="{field:'qcKc', sort: true,width:110,align:'center', event: 'setQcKc', style:'cursor: pointer;'}">库存数量</th>
				<th
					lay-data="{field:'proFactoryPrice', sort: true,width:150,align:'center'}">出厂价格(元)</th>
				<th
					lay-data="{field:'proPrice', sort: true,width:150,align:'center'}">市场价(元)</th>
				<th
					lay-data="{field:'proPrice0', sort: true,width:150,align:'center'}">产家直销价(元)</th>
				<th
					lay-data="{field:'proPrice1', sort: true,width:160,align:'center'}">银/金牌会员价(元)</th>
				<th
					lay-data="{fixed: 'right', width:100, align:'center', toolbar: '#barDemo1'}">操作</th>
			</tr>
		</thead>
	</table>
	<!-- 树 -->
	<div id="menuContent" class="menuContent"
		style="display: none; position: absolute;z-index: 99999;">
		<ul id="treeDemo" class="ztree" style="margin-top: 0;"></ul>
	</div>
	<script type="text/html" id="isvaTpl">
  			{{#  if(d.isva==0){ }}
    			<span class="layui-badge-rim layui-bg-gray">暂停销售</span>
  			{{#  } else { }}
    			<span class="layui-badge-rim layui-bg-blue">正常销售</span>
  			{{#  } }}
		</script>
	<script type="text/html" id="proNameTpl">
    		<a href="pages/front/info.jsp?proId={{d.proId}}" target="_blank" class="layui-table-link">{{d.proName}}</a>
		</script>
	<script type="text/html" id="barDemo1">
			<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete"><i class="layui-icon">&#xe640;</i>删除</a>
		</script>
	<script type="text/html" id="qcKcTpl">
  		<input id="{{d.proId}}" name ="qcKc" type="number" value="{{d.qcKc}}" min="1" style="width:50px;" />
	</script>
	<script>
			var form;//表单
			var table;//数据表格
			var loadIndex;//加载层
			layui.use('form', function(){
				form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
				//判断当前用户是否有查看所有站点的权限
				if(window.parent.queryStation==false){
					var isva = $("#stId").parent().next();
					$("#stId").parent().html(isva.html());
					isva.html("");
					form.render('select');//重新渲染
				}else{//如果有权限则加载所有的站点
					loadIndex = layer.load();//出现加载层
					var url = "station/back/findStation.action";
					$.post(url, null, function(data){
						$(data).each(function(i,element){
							$("#stId").append('<option value="'+element.stId+'">'+element.stName+'</option>');
							layer.close(loadIndex);//加载层关闭  
							form.render('select');//重新渲染
						});
					});
				}
			});
			//初始化数据表格
			layui.use('table', function(){
			  table = layui.table;
				//监听工具条
			  table.on('tool(tb)', function(obj){//tb是table原始容器的属性 lay-filter="对应的值"
				  var formdata = obj.data; //获得当前行数据
				  var layEvent = obj.event; //获得 lay-event 对应的值
				  if(layEvent === 'delete'){ //删除
					  var ts =layer.open({
						  type: 1, 
						  skin: 'layui-layer-molv'//样式
						  ,title:'清仓产品删除'//标题
						  ,area: ['300px', '150px']
						  ,content: '<p style="color:#01AAED;padding-top:5px;padding-left:20px;">温馨提示：删除之后该商品将从清仓页面消失</p><p style="color:#FF5722;padding-left:20px;">是否对该产品进行删除处理？</p>' //这里content是一个普通的String
						,btn: ['坚持删除', '算了'] //可以无限个按钮
					  	,yes: function(index, layero){
					  		layer.close(ts);
					  		loadIndex = layer.load();//出现加载层
					  		//下架操作
					  		var url = "productQc/back/delProductQc.action";
					  		var data = {"proId":formdata.proId}
							$.post(url, data, function(info){
								layer.close(loadIndex);//加载层关闭  
								if(info>0){
									layer.msg('清仓产品已成功删除！');
									obj.del();//刷新当前页
									//$(".layui-laypage-skip .layui-laypage-btn").click();//刷新当前页
								}else{
									layer.msg('操作失败！');
								}
							});
						  }
					});
				  }else if(obj.event === 'setQcKc'){
				      layer.prompt({
				          formType: 2
				          ,title: '修改库存'
				          ,value: formdata.qcKc
				        }, function(value, index){
				          layer.close(index);
				          
				          //这里一般是发送修改的Ajax请求
				          var url = "productQc/back/update.action";
				          var proId = formdata.proId;
				          var qckc = value;
				          var data = {"proId":proId, "qcAttr01":qckc};
				          $.post(url,data,function(info){
				        	if(info>>0){
				        		layer.msg('操作成功！');
				        		//同步更新表格和缓存对应的值
						          obj.update({
						        	 qcKc: value
						          });
				        	}else{
				        		layer.msg('操作失败！');
				        	} 
				          });
				        });
				      }
			  });
			  //搜索
			  var $ = layui.$, active = {
			    reload: function(){
			    	loadIndex = layer.load();//出现加载层
			    	tableReload();//重载表格的方法
			    	layer.close(loadIndex);//加载层关闭  
			    },
			    add: function(){// 增加
			    	var ts =layer.open({
						  type: 2, 
						  skin: 'layui-layer-molv'//样式
						  ,anim:1//弹出动画
						  ,shadeClose:true//点击弹出层关闭
						  ,maxmin:true//显示最大最小化
						  ,title:'增加清仓产品'//标题
						  ,area: ['75%', '80%']
						  ,content: 'pages/back/product/addBatchQc.jsp'
					}); 
			    }
			  };
			  
			  $('.demoTable .layui-btn').on('click', function(){
			    var type = $(this).data('type');
			    active[type] ? active[type].call(this) : '';
			  });
			});
			
			function tableReload(){
				table.reload('tb', {
			        where: {
			        	priductData: $('#priductData').val(),
			        	proTypeId:$("#typeId").val(),
			        	stId:$('#stId').val(),
			        	isva:$('#isva').val(),
			        	proFactoryPriceMix:$('#proFactoryPriceMix').val(),
			        	proFactoryPriceMax:$('#proFactoryPriceMax').val(),
			        	proPriceMix:$('#proPriceMix').val(),
			        	proPriceMax:$('#proPriceMax').val(),
			        	proPrice0Mix:$('#proPrice0Mix').val(),
			        	proPrice0Max:$('#proPrice0Max').val(),
			        	proPrice1Mix:$('#proPrice1Mix').val(),
			        	proPrice1Max:$('#proPrice1Max').val()
			        }
			      });
			}
			//树结构初始化
			var treeObj ;
		   var setting = {
			    async: {
			        enable: true,//采用异步加载
			        url : "ProductTypeAction/back/proTypeTree.action",
			        dataType : "json"
			    },
			    data : {
			        key : {  
			            name : "name"
			        },
			        simpleData : {
			            enable : true,
			            idKey : "id",
			            rootPid : null 
			        }
			    },
			    callback : {
			    	onClick: zTreeOnClick,//给每个节点点击事件
			        onAsyncSuccess: zTreeOnAsyncSuccess //异步加载完成调用
			    }
			};
		 //给每个节点点击事件
		   function zTreeOnClick(event, treeId, treeNode) {
			   $("#typeName").val(treeNode.name);
			   $("#typeId").val(treeNode.id);
			   hideMenu();	
			};
			//异步加载完成时运行，此方法将所有的节点打开
			function zTreeOnAsyncSuccess(event, treeId, msg) {
			    var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
			    treeObj.expandAll(true);
			}
		   $(document).ready(function(){
			   treeObj=$.fn.zTree.init($("#treeDemo"), setting);
		   });
		 //树形菜单相关方法
		    function showMenu() {
		      var cityObj = $("#typeName");
		      var cityOffset = $("#typeName").offset();
		      $("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		 
		      $("body").bind("mousedown", onBodyDown);
		    }
		    function hideMenu() {
		      $("#menuContent").fadeOut("fast");
		      $("body").unbind("mousedown", onBodyDown);
		    }
		    function onBodyDown(event) {
		      if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
		        hideMenu();
		      }
		    }
			//限制数字的输入
			function clearNoNum(obj){ 
			    obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符  
			    obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的  
			    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$","."); 
			    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数  
			    if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额 
			        obj.value= parseFloat(obj.value); 
			    }
			    if(obj.value.indexOf(".")==0){//第一个字符不能为.
			    	 obj.value="";
			    }
			}
		</script>
</body>
</html>