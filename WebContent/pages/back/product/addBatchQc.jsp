<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>增加清仓产品</title>
  <base href="<%=basePath%>"/>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <script src="res/js/jquery-2.1.4.min.js" type="text/javascript"></script>
  
  <!-- ztree start -->
  <link rel="stylesheet" href="res/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
  <link rel="stylesheet" href="res/css/treeStyle_ouyang.css">
  <script type="text/javascript" src="res/zTree_v3/js/jquery.ztree.core.min.js"></script>
  <!-- ztree end -->
  
  <!-- layui start -->
  <link rel="stylesheet" href="res/layui/css/layui.css"  media="all">
  <script src="res/layui/layui.js" charset="utf-8"></script>
  <!-- layui end -->
  
  <style type="text/css">
	body {
		padding: 5px;
	}
	
	#sousuo .layui-input {
		height: 30px;
	}
	
	#sousuo td {
		padding-right: 5px;
		padding-top: 5px;
	}
	
	body .layui-elem-quote {
		padding: 7px;
	}
	</style>
</head>
<body>

<!-- 顶部工具栏 start -->
 <div class="demoTable layui-elem-quote layui-quote-nm">
		<div class="layui-inline">
			<form class="layui-form" id="sousuo" style="float: left;">
				<table>
					<tr>
						<td><input class="layui-input" name="priductData"
							maxlength="50" id="priductData" placeholder="产品编号/名称/品牌"
							autocomplete="off" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"></td>
						<td><input id="typeId" type="hidden" name="typeId"> <input
							type="text" id="typeName" onclick="showMenu(); return false;"
							placeholder="产品类型" autocomplete="off" class="layui-input"
							readonly="readonly"></td>
						<td><select id="stId" name="stId" lay-verify="" width="50px">
								<option value="">分站站点</option>
						</select></td>
						<td><select id="isva" name="isva" lay-verify="" width="50px">
								<option value="">是否上架</option>
								<option value="1">已上架</option>
								<option value="0">未上架</option>
						</select></td>
						<td ></td>
					</tr>
					<tr>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proFactoryPriceMix"
										name="proFactoryPriceMix" maxlength="10" placeholder="￥出厂价格"
										autocomplete="off" class="layui-input"
										onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proFactoryPriceMax"
										name="proFactoryPriceMax" maxlength="10" placeholder="￥出厂价格"
										autocomplete="off" class="layui-input"
										onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPriceMix" name="proPriceMix"
										maxlength="10" placeholder="￥市场价格" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPriceMax" name="proPriceMax"
										maxlength="10" placeholder="￥市场价格" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPrice0Mix" name="proPrice0Mix"
										maxlength="10" placeholder="￥产家直销价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 100px;">
									<input type="text" id="proPrice0Max" name="proPrice0Max"
										maxlength="10" placeholder="￥产家直销价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>
						<td>
							<div class="layui-inline">
								<div class="layui-input-inline" style="width: 120px;">
									<input type="text" id="proPrice1Mix" name="proPrice1Mix"
										maxlength="10" placeholder="￥银/金牌会员价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
								<div class="layui-input-inline">-</div>
								<div class="layui-input-inline" style="width: 120px;">
									<input type="text" id="proPrice1Max" name="proPrice1Max"
										maxlength="10" placeholder="￥银/金牌会员价" autocomplete="off"
										class="layui-input" onkeyup="clearNoNum(this)">
								</div>
							</div>
						</td>

					</tr>
				</table>
			</form>
		</div>
		<div class="layui-inline">
			<button style="margin-top: 5px;" class="layui-btn layui-btn-sm" data-type="reload"><i class="layui-icon">&#xe615;</i>搜索</button>
		</div>
	</div>
<!-- 顶部工具栏 end -->
	
	<!-- 产品类型树  start-->
	<div id="menuContent" class="menuContent"
		style="display: none; position: absolute; z-index: 99999;">
		<ul id="treeDemo" class="ztree" style="margin-top: 0;"></ul>
	</div>
	<!-- 产品类型树  end-->
	
<!-- 数据表格 start -->	
<table class="layui-hide" id="test" lay-filter="test"></table>
<!-- 数据表格 end -->	 
<script type="text/html" id="toolbarDemo">
  <div class="layui-form-item">
    <div class="layui-inline">
    	<label class="layui-form-label">库存数</label>
    	<div class="layui-input-inline">
      		<input type="number" id="qcKc" placeholder="请输入库存数" class="layui-input">
    	</div>
    </div>
    <div class="layui-inline">
    	<button class="layui-btn layui-btn-sm" lay-event="updateBatch">批量修改</button>
	  	<button class="layui-btn layui-btn-sm" lay-event="addBatch">批量加入清仓</button>
    </div>
  </div>
</script>
 
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="edit">加入清仓</a>
</script>
              
 
<script>
layui.use('table', function(){
  var table = layui.table;
  
  table.render({
    elem: '#test'
    ,url:'product/back/findAll2.action?isQc=0'
    ,toolbar: '#toolbarDemo'
    ,title: '用户数据表'
    ,cols: [[
       {type:'numbers', fixed: 'left'}
      ,{type: 'checkbox', fixed: 'left'}
      ,{field:'proName',title:'产品名称', sort: true,fixed:true,width:300,align:'left',templet:'#proNameTpl'}
      ,{field:'proCode',title:'产品编号', sort: true, width:150}
      ,{field:'isva',title:'状态', sort: true,width:110,align:'center',templet:'#isvaTpl'}
      ,{field:'qcKc',title:'库存数', width:100,align:'center',templet:'#qcKcTpl'}
      ,{field:'proFactoryPrice', title:'出厂价格(元)',sort: true,width:150,align:'center'}
      ,{field:'proPrice',title:'市场价(元)', sort: true,width:150,align:'center'}
      ,{field:'proPrice0', title:'产家直销价(元)',sort: true,width:150,align:'center'}
      ,{field:'proPrice1',title:'银/金牌会员价(元)', sort: true,width:160,align:'center'}
      ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:100}
    ]]
  	,id:'testReload'
    ,page: true
  });
  
  
  var $ = layui.$, active = {
	    reload: function(){
	      //执行重载
	      table.reload('testReload', {
	        page: {
	          curr: 1 //重新从第 1 页开始
	        }
	        ,where: {
	        	priductData: $('#priductData').val(),
	        	proTypeId:$("#typeId").val(),
	        	stId:$('#stId').val(),
	        	isva:$('#isva').val(),
	        	proFactoryPriceMix:$('#proFactoryPriceMix').val(),
	        	proFactoryPriceMax:$('#proFactoryPriceMax').val(),
	        	proPriceMix:$('#proPriceMix').val(),
	        	proPriceMax:$('#proPriceMax').val(),
	        	proPrice0Mix:$('#proPrice0Mix').val(),
	        	proPrice0Max:$('#proPrice0Max').val(),
	        	proPrice1Mix:$('#proPrice1Mix').val(),
	        	proPrice1Max:$('#proPrice1Max').val()
	        }
	      });
	    }
	  };
	  
	  $('.demoTable .layui-btn').on('click', function(){
	    var type = $(this).data('type');
	    active[type] ? active[type].call(this) : '';
	  });
	
 	
	 //头工具栏事件
	  table.on('toolbar(test)', function(obj){
	    var checkStatus = table.checkStatus(obj.config.id);
	    var data = checkStatus.data;
	    if(obj.event=='addBatch'){ // 批量增加
			var dataArr = new Array();
	        for(var i=0; i<data.length; i++){
	        	var obj = new Object();
	        	var proId = data[i].proId;
	        	var qcKc = $("#"+proId).val();
	        	obj.proId=proId;
	        	obj.qcKc=qcKc;
	        	dataArr[i]=obj;
	        }
	        if(data.length==0){
	        	layer.msg('请选择一行数据！');
	        }else{
	        	loadIndex = layer.load();//出现加载层
	        	var url = "productQc/back/addBatchProductQc.action";
	        	var json=JSON.stringify(dataArr);//将对象数组转化成字符串
	        	$.post(url,{"dataArr":json},function(info){
	        		layer.close(loadIndex);//加载层关闭  
	        		if(info>0){
	        			layer.msg(info+'个产品已成功加入清仓！');
	        			$(".layui-laypage-skip .layui-laypage-btn").click();//刷新当前页
	        		}else{
	        			layer.msg('操作失败！');
	        		}
	        	});
	        }	
	    }else if(obj.event=='updateBatch'){ // 批量修改
	    	var qcKc = $("#qcKc").val();
	    	if(data.length==0){
	        	layer.msg('请选择一行数据！');
	        }else if(qcKc<1){
	        	layer.msg('库存数不能小于1！');
	        }else{
	        	for(var i=0; i<data.length; i++){
		        	var proId = data[i].proId;
		        	$("#"+proId).val(qcKc);
		        }
	        	
	        }
	    }
	  });	  
	  
	  
  
  //监听行工具事件
  table.on('tool(test)', function(obj){
	  var formdata = obj.data; //获得当前行数据
	  var layEvent = obj.event; //获得 lay-event 对应的值
    //console.log(obj)
    if(obj.event === 'edit'){
    	var ts =layer.open({
			  type: 1, 
			  skin: 'layui-layer-molv'//样式
			  ,title:'加入清仓'//标题
			  ,area: ['300px', '150px']
			  ,content: '<p style="color:#01AAED;padding-top:5px;padding-left:20px;">温馨提示：加入清仓该商品则进入清仓页面</p><p style="color:#FF5722;padding-left:20px;">是否对该产品进行清仓处理？</p>' //这里content是一个普通的String
			,btn: ['坚持清仓', '算了'] //可以无限个按钮
		  	,yes: function(index, layero){
		  		layer.close(ts);
		  		loadIndex = layer.load();//出现加载层
		  		//下架操作
		  		var url = "productQc/back/addProductQc.action";
		  		var proId = formdata.proId;
		  		var qcKc = $("#"+formdata.proId).val();
		  		var data = {"proId":proId, "qcAttr01":qcKc}
				$.post(url, data, function(info){
					layer.close(loadIndex);//加载层关闭  
					if(info>0){
						layer.msg('产品已成功加入清仓！');
						obj.del();//刷新当前页
					}else{
						layer.msg('操作失败！');
					}
				});
			  }
    	});	  
     }
  });

});
</script>
<script type="text/html" id="proNameTpl">
	<a href="pages/front/info.jsp?proId={{d.proId}}" target="_blank" class="layui-table-link">{{d.proName}}</a>
</script>
<script type="text/html" id="isvaTpl">
  			{{#  if(d.isva==0){ }}
    			<span class="layui-badge-rim layui-bg-gray">暂停销售</span>
  			{{#  } else { }}
    			<span class="layui-badge-rim layui-bg-blue">正常销售</span>
  			{{#  } }}
</script>
<script type="text/html" id="isvaTpl">
  			{{#  if(d.isva==0){ }}
    			<span class="layui-badge-rim layui-bg-gray">暂停销售</span>
  			{{#  } else { }}
    			<span class="layui-badge-rim layui-bg-blue">正常销售</span>
  			{{#  } }}
</script>
<script type="text/html" id="qcKcTpl">
  	<input id="{{d.proId}}" name ="qcKc" type="number" value="1" min="1" style="width:50px;" />
</script>
<script type="text/javascript">
// 站点加载
var form;//表单
var table;//数据表格
var loadIndex;//加载层
layui.use('form', function(){
	form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
	//判断当前用户是否有查看所有站点的权限
	if(window.parent.queryStation==false){
		var isva = $("#stId").parent().next();
		$("#stId").parent().html(isva.html());
		isva.html("");
		form.render('select');//重新渲染
	}else{//如果有权限则加载所有的站点
		loadIndex = layer.load();//出现加载层
		var url = "station/back/findStation.action";
		$.post(url, null, function(data){
			$(data).each(function(i,element){
				$("#stId").append('<option value="'+element.stId+'">'+element.stName+'</option>');
				layer.close(loadIndex);//加载层关闭  
				form.render('select');//重新渲染
			});
		});
	}
});

//树结构初始化
   var treeObj ;
   var setting = {
	    async: {
	        enable: true,//采用异步加载
	        url : "ProductTypeAction/back/proTypeTree.action",
	        dataType : "json"
	    },
	    data : {
	        key : {  
	            name : "name"
	        },
	        simpleData : {
	            enable : true,
	            idKey : "id",
	            rootPid : null 
	        }
	    },
	    callback : {
	    	onClick: zTreeOnClick,//给每个节点点击事件
	        onAsyncSuccess: zTreeOnAsyncSuccess //异步加载完成调用
	    }
	};
//给每个节点点击事件
function zTreeOnClick(event, treeId, treeNode) {
	   $("#typeName").val(treeNode.name);
	   $("#typeId").val(treeNode.id);
	   hideMenu();	
	};
	//异步加载完成时运行，此方法将所有的节点打开
	function zTreeOnAsyncSuccess(event, treeId, msg) {
	    var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
	    treeObj.expandAll(true);
	}
$(document).ready(function(){
	   treeObj=$.fn.zTree.init($("#treeDemo"), setting);
});
//树形菜单相关方法
function showMenu() {
  var cityObj = $("#typeName");
  var cityOffset = $("#typeName").offset();
  $("#menuContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");

  $("body").bind("mousedown", onBodyDown);
}
function hideMenu() {
  $("#menuContent").fadeOut("fast");
  $("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
  if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
    hideMenu();
  }
}
//限制数字的输入
function clearNoNum(obj){ 
    obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符  
    obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的  
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$","."); 
    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数  
    if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额 
        obj.value= parseFloat(obj.value); 
    }
    if(obj.value.indexOf(".")==0){//第一个字符不能为.
    	 obj.value="";
    }
}
</script>
</body>
</html>