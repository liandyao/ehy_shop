<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>待发货订单</title>
		<link rel="stylesheet" href="res/layui/css/layui.css">
		<link rel="stylesheet" href="res/css/treeStyle_ouyang.css"> 
		<script src="res/js/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script src="res/js/ehy_common.js" type="text/javascript" ></script>
		<script src="res/layui/layui.js"></script>
		<script src="res/js/back-order.js"></script>
		<style type="text/css">
		#sousuo .layui-input {
			height: 30px;
		}
		#sousuo td {
			padding-right: 5px;
			padding-top: 5px;
		}
		body .layui-elem-quote {
			padding: 7px;
		}
		.laytable-cell-1-proName p{
			margin: 3px 0px 0px 0px;
			padding: 0px;
			font-size: 10px;
			line-height: 11px;
		}
		table * {
			font-size : 12px;
		}
		</style>
	</head>
	<body>
		<div class="demoTable layui-elem-quote layui-quote-nm" id="searchDiv">
			<div class="layui-inline">
			  	<form class="layui-form" id="sousuo" style="float: left;">
			  		<table>
						<tr>
							<td>
							  <div class="layui-input-inline">
				        		 	<input id="startMxDateTime" lay-verify="date" placeholder="请选择下单时间"  
				        		 	autocomplete="off" class="layui-input" type="text">
						      </div>
						  	   -
							  <div class="layui-input-inline">
							        <input id="endMxDateTime" lay-verify="date" placeholder="请选择下单时间"  
							        autocomplete="off" class="layui-input" type="text">
							  </div>
							</td>
							<td>
								<div class="layui-input-inline">
				        		 	<input id="startPayTime" lay-verify="date" placeholder="请选择支付时间"  
				        		 	autocomplete="off" class="layui-input" type="text">
						      </div>
						  	   -
							  <div class="layui-input-inline">
							        <input id="endPayTime" lay-verify="date" placeholder="请选择支付时间"  
							        autocomplete="off" class="layui-input" type="text">
							  </div>
							</td>
							<td>
								
							</td>
						</tr>
						<tr>
							<td>
								<div class="layui-input-inline">
							        <input id="minMxMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="maxMxMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);"/>
							    </div>
							</td>
							<td>
								<div class="layui-input-inline">
							        <input id="minMxMoneyFact" placeholder="实际付款金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="maxMxMoneyFact" placeholder="实际付款金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);"/>
							    </div>
							</td>
								<td>
								<div id="seachStation" class="layui-input-inline" style="width:180px;">
									<select name="stId" id="stId"> 
							 
									</select>
								</div>
								<button type="button" class="layui-btn layui-btn-small" onclick="reloadRec();"><i class="layui-icon">&#xe615;</i>搜索</button>
								<button type="button" class="layui-btn layui-btn-small" data-type="getCheckData" onclick="showFrom()">发货</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<table class="layui-hide" id="LAY_table_user" lay-filter="demo"></table> 
		
		<div style="display:none; padding:40px;" id="fromdiv">
			<form class="layui-form" action="" id="from">
				<div class="layui-form-item">
					<label class="layui-form-label">快递公司</label>
					<div class="layui-input-inline">
						<select name="modules" lay-verify="required" lay-search=""
							id="ordExpress">
							<option value="">---请选择---</option>
						</select>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">快递单号</label>
					<div class="layui-input-block">
						<input type="text" name="title" lay-verify="title"
							autocomplete="off" placeholder="请输入快递单号" class="layui-input" id="ordExpressCode">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<input type="button" class="layui-btn" lay-submit="" lay-filter="demo" value="提交">
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
		<script type="text/html" id="barDemo">
  			<a class="layui-btn layui-btn-mini" lay-event="detail" 
				onclick="queryParticular('{{d.proName}}_商品编码：{{d.proCode}}_收件人：{{d.ordMember}}_联系方式：{{d.ordPhone}}_收货地址：{{d.ordAdderss}}')">
				查看详情</a>
		</script>
		<!-- <script type="text/html" id="proNameTemp">
			{{# var arr = d.proName; var str = ''; for(i=arr.length; i>0; i-=24){ var s = arr.substr(arr.length-i, arr.length-i+24); str += '<p>'+ s +'</p>' } }}
			{{ str }}
		</script> -->
		<script type="text/html" id="proNameTemp">
			{{ d.proName.split('_')[0] }}	
		</script>
		<script type="text/javascript">
		
		var table;
		var laydate;
		var form;
		var arrMX = "";
		var addCen;
		var loadIndex;
		
		$(function (){
			showExpress();
			showStation();
			var queryStation = parent.queryStation;
			if(queryStation){
				$("#seachStation").show();
			}else{
				$("#seachStation").hide();
			}
		});
		
		function showExpress(){
			var url = "express/back/showList.action";
			var data = {"page":1,"limit":9999}
			$.post(url, data, function(map){
				 for(i=0; i<map.data.length; i++){
					 var obj = map.data[i]; 
					 $("#ordExpress").append("<option value='"+obj.name+"'>"+obj.name+"</option>");
				 }
				form.render('select');
			});
		}
		
		function showFrom(mxId){
			arrMx = "";
			for(i=0; i<mxId.length; i++){
				arrMX += mxId[i]+",";
			}
			addCen = layer.open({
		        type: 1//样式
		        ,skin: 'layui-layer-molv'//样式
		        ,area: ['85%', '86%']
		        ,title:"发货"//标题
		        ,id: 'mesFrom' //防止重复弹出
		        ,content: $("#fromdiv")
		        ,shade: [0.8, '#393D49'] //显示遮罩
		        ,shadeClose:true//点击也能遮罩层关闭
		        ,anim:2//弹出动画
		      });
		}
		
		layui.use([ 'table', 'laydate', 'form' ], function() {
			table = layui.table;
			loadIndex = layer.load();
			//方法级渲染
		    table.render({
				loading: true
				,elem: '#LAY_table_user'
			    ,url: 'orderItem/back/showList.action' 
			    ,method: 'post'
			    ,where:{
			    	"state" : '1'
			    }
			    ,cols: [[
			         {type:'checkbox', checkbox: true, fixed: 'left'}
			        ,{field:'ordId', title: '订单号', width:190, align:'center', fixed: 'left'}
				    ,{field:'stName', title: '站点名称', width:80, align:'center'}
				    ,{field:'proName', title: '商品名称', width:260, align:'center', templet:'#proNameTemp', event: 'showName', style:'cursor: pointer;'}
				    ,{field:'proCode', title: '商品编码', width:100, align:'center'}
				    ,{field:'mxNum', title: '数量', width:60, align:'center'}
			        ,{field:'mxMoneyFact', title: '实际付款', width:80, align:'center'}
			        ,{field:'mxMoney', title: '总金额', width:80, align:'center'}
			        ,{field:'mxDateTime', title: '下单时间', width:145, align:'center'}
			        ,{field:'payTime', title: '付款时间', width:145, align:'center'}
			        ,{field:'ordMember', title: '收货人', width:100, align:'center'}
			        ,{field:'ordPhone', title: '联系方式', width:110, align:'center'}
			        ,{field:'ordAdderss', title: '收货地址', width:340, align:'center'}
			        ,{field:'mxRemark', title: '备注', width:200, align:'center'}
			        ,{fixed: 'right', width:100, align:'center', toolbar: '#barDemo'}
			    ]]
			    ,id: 'o_order'
			    ,page: true
			    ,height: 400
			    ,size: "sm"
			    ,done: function(res, curr, count){
			    	layer.close(loadIndex);//加载层关闭  
			    }
			});
			
			//监听表格复选框选择
			table.on('checkbox(demo)', function(obj){
				//console.log(obj);
				$("#from")[0].reset();
				arrMX="";
			});
			
			//监听工具条
			table.on('tool(demo)', function(obj){
			    var data = obj.data;
			    if(obj.event === 'detail'){
			    	ckeckWl(data.ordExpress, data.ordExpressCode);
			    } else if(obj.event === 'edit'){
			     	
			    } else if(obj.event === 'showName'){
			    	queryParticular(data.proName+'_商品编码：'+data.proCode);
			    }
			});
			
			var $ = layui.$, active = {
			    getCheckData: function(){ //获取选中数据    
			    	var checkStatus = table.checkStatus('o_order') ;
			    	var data = checkStatus.data;
			   		//layer.alert(JSON.stringify(data));
				    var mxId = new Array();
				    for(i=0; i<data.length; i++){
				    	mxId.push(data[i].mxId);
				    }
				    if(mxId.length>0){
				    	for(i=0; i<data.length; i++){
				    		for(j=0; j<data.length; j++){
				    			if(data[i].ordId!=data[j].ordId){
				    				layer.msg("只有同一订单的才能一起发货");
				    				return;
				    			}
				    		}
				    	}
				    	showFrom(mxId);
				    }else{
				    	layer.msg("请勾选订单");
				    }
				}
			};
			  
		   $('.demoTable .layui-btn').on('click', function(){
		    	var type = $(this).data('type');
		      	active[type] ? active[type].call(this) : '';
		    });
		    
		    laydate = layui.laydate;
			//下单时间的日期控件
			var startMxDateTime = laydate.render({
				elem : '#startMxDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endMxDateTime.config.min.year = date.year;
						endMxDateTime.config.min.month = date.month - 1;
						endMxDateTime.config.min.date = date.date;
					}else{
						endMxDateTime.config.min.year = '1900';
						endMxDateTime.config.min.month = '01';
						endMxDateTime.config.min.date = '01';
					}
				}
			});
			var endMxDateTime = laydate.render({
				elem : '#endMxDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startMxDateTime.config.max.year = date.year;
						startMxDateTime.config.max.month = date.month - 1;
						startMxDateTime.config.max.date = date.date;
					}else{
						startMxDateTime.config.max.year = '2099';
						startMxDateTime.config.max.month = '12';
						startMxDateTime.config.max.date = '31';
					}
				}
			});
			//付款时间的日期控件
			var startPayTime = laydate.render({
				elem : '#startPayTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endPayTime.config.min.year = date.year;
						endPayTime.config.min.month = date.month - 1;
						endPayTime.config.min.date = date.date;
					}else{
						endPayTime.config.min.year = '1900';
						endPayTime.config.min.month = '01';
						endPayTime.config.min.date = '01';
					}
				} 
			});
			var endPayTime = laydate.render({
				elem : '#endPayTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startPayTime.config.max.year = date.year;
						startPayTime.config.max.month = date.month - 1;
						startPayTime.config.max.date = date.date;
					}else{
						startPayTime.config.max.year = '2099';
						startPayTime.config.max.month = '12';
						startPayTime.config.max.date = '31';
					}
				}
			});
			form = layui.form;
			//自定义验证规则
			form.verify({
				
			});
			
			//监听提交 
			form.on('submit(demo)', function(data){
				var data = {"arr":arrMX,"ordExpress":$("#ordExpress").val(),"ordExpressCode":$("#ordExpressCode").val()};
				var url = "orderItem/back/deliverGoods.action";
				$.post(url, data, function(mes){
					$("#from")[0].reset();
					table.reload('o_order');
					if(mes.state==1){
						layer.close(addCen);
						layer.msg(mes.mes);  
			      	}else{
			      		layer.msg(mes.mes); 
			      	}
				});
			});
			
		});
		
		function reloadRec(){
			loadIndex = layer.load();
			table.reload('o_order', {
				where : {
					'state' : "1",
					'stId' : $("#stId").val(),
					'startMxDateTime' : $("#startMxDateTime").val(),
					'endMxDateTime' : $("#endMxDateTime").val()==""?"":$("#endMxDateTime").val()+" 23:59:59",
					'startPayTime' : $("#startPayTime").val(),
					'endPayTime' : $("#endPayTime").val()==""?"":$("#endPayTime").val()+" 23:59:59",
					'minMxMoney' : $("#minMxMoney").val(),
					'maxMxMoney' : $("#maxMxMoney").val(),
					'maxMxMoneyFact' : $("#maxMxMoneyFact").val(),
					'minMxMoneyFact' : $("#minMxMoneyFact").val()
				},
				done : function(){
					layer.close(loadIndex);
				}
			});
		}
		
		/* $('.demoTable .layui-btn').on('click', function() {
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		}); */
		
		</script>
	</body>
</html>