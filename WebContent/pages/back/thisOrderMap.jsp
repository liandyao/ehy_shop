<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>统计图</title>
	<link rel="stylesheet" href="res/layui/css/layui.css">
<script src="res/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="res/layui/layui.js"></script>
 <script src="res/js/ehy_common.js" type="text/javascript" ></script>
</head>
<body style="padding:10px 55px 0px 60px;">
	 <div id="main" style="height:400px"></div>
	<script src="res/js/echarts.js"></script>
	 <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        
        
        
        var code = GetQueryString('code');
        var data = [];
        
        var url = 'invitationCode/back/thisOrderMap.action';
        var d = {code:code};
        $.post(url, d, function(info){
        	for ( var i = 0; i < info.list.length; i++) {
				data.push([ info.list[i].mxDatetime, info.list[i].mxNum ]);
			}
        	  
            var dateList = data.map(function (item) {
                return item[0];
            });
            var valueList = data.map(function (item) {
                return item[1];
            });

            option = {

                // Make gradient line here
                visualMap: [{
                    show: false,
                    type: 'continuous',
                    seriesIndex: 0,
                    min: 0,
                    max: 5
                } ],


                title: [{
                    left: 'center',
                    text: info.year+'年'+info.month+'月订单数的统计折线图'
                }],
                tooltip: {
                    trigger: 'axis'
                },
                xAxis: [{
                    data: dateList
                    
                } ],
                yAxis: [{
                    splitLine: {show: false}
                } ],
                grid: [{
                    bottom: '20%'
                }, {
                    top: '0%'
                }],
                series: [{
                    type: 'line',
                    showSymbol: false,
                    data: valueList
                } ]
            }; 
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        });
        
        
      //获取从父页面传过来的参数的方法
    	function GetQueryString(name)
    	{
    	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    	     var r = window.location.search.substr(1).match(reg);
    	     if(r!=null)return  unescape(r[2]); return null;
    	}
    </script>
	
</body>
</html>