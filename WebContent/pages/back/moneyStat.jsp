<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>资金结算时间汇总</title>
		<link rel="stylesheet" href="res/layui/css/layui.css">
		<script src="res/js/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script src="res/js/ehy_common.js" type="text/javascript" ></script>
		<script src="res/layui/layui.js"></script>
		<style type="text/css">
		
		</style>
	</head>
	<body>
		<div>
		 统计人：
		  <div class="layui-inline" style="width: 140px;">
		    <input class="layui-input" name="msEmp" id="msEmp" autocomplete="off" placeholder="请输入统计人">
		  </div>
		   收款人：
		  <div class="layui-inline" style="width: 140px;">
		    <input class="layui-input" name="msGet" id="msGet" autocomplete="off" placeholder="请输入收款人">
		  </div>
		  统计时间：
		  <div class="layui-inline" style="width: 140px;">
		    <input class="layui-input" name="startDateTime" id="startDateTime" autocomplete="off" placeholder="统计时间">
		  </div>
		  ~
		  <div class="layui-inline">
		    <input class="layui-input" name="endDateTime" id="endDateTime" autocomplete="off" placeholder="统计时间">
		  </div>
		  
		  <button class="layui-btn" data-type="ordinaryBut" onclick="search();">搜索</button>
		  <button class="layui-btn" data-type="ordinaryBut" onclick="show_checkout_from();">结算</button>
		
		</div>
		
		<table class="layui-hide" lay-filter="demo" id="moneyStat"></table>
		
		
	<script type="text/html" id="barDemo">
  		<a class="layui-btn layui-btn-mini" lay-event="detail">查看</a>
  		<!--  -->
		{{# if(d.msIsjs==0){ }}
			<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
			<a class="layui-btn layui-btn-mini" lay-event="edit">结算</a>
		{{# } }}
  		
	</script>
	<script type="text/html" id="checkout">
		{{# if(d.msIsjs==1){ }}
			<span style='color:green;'>已结算</span>
		{{# }else if(d.msIsjs==0){ }}
			<span style='color:red;'>未结算</span>
		{{# } }}
	</script>
	<script type="text/javascript">
		
		var table, laydate, form;
		var endTimeStr = '', startTimeStr = '', endTime = null, startTime = null;
		
		var flag_time = false;
		
		layui.use(['table','form','laydate'], function() {
			table = layui.table;
			
			table.render({
				elem : '#moneyStat',
				url : 'stat/back/showList.action',
				height : 400,
				loading: true,
				method: 'post',
				size: "sm",
			   	id: 'moneyStat',
				cols : [ [ 
					{field : 'msStartTime',width : 145,title : '开始时间',align : 'center' }, 
					{field : 'msEndTime',width : 145,title : '结束时间',align : 'center' }, 
					{field : 'msYear',width : 80,title : '年份',align : 'center' },
					{field : 'msEmp',width : 80,title : '统计人',align : 'center' }, 
					{field : 'msDateTime',width : 145,title : '统计时间',align : 'center' }, 
					{field : 'msMoney',width : 80,title : '总金额',align : 'center' },
					{field : 'msIsjs',width : 80,title : '是否结算',align : 'center',templet : '#checkout'}, 
					{field : 'msGet',width : 80,title : '收款人',align : 'center' }, 
					{field : 'msGetType',width : 120,title : '收款方式',align : 'center' },
					{field : 'msGetTime',width : 145,title : '收款时间',align : 'center' },
					{fixed: 'right', width:160, align:'center', toolbar: '#barDemo'}
					] ],
				page : true
			});
			
			laydate = layui.laydate;
			startTime = laydate.render({
				elem : '#startTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					
					if(endTime && flag_time){
						endTimeStr = endTime.config.dateTime.year+'-'+(endTime.config.dateTime.month+1)+'-'+endTime.config.dateTime.date;
					}
					if(startTime){
						startTimeStr = startTime.config.dateTime.year+'-'+(startTime.config.dateTime.month+1)+'-'+startTime.config.dateTime.date;
					}
					computeMoney(startTimeStr,endTimeStr);
				}
			});
			endTime = laydate.render({
				elem : '#endTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startTime.config.max.year = date.year;
						startTime.config.max.month = date.month - 1;
						startTime.config.max.date = date.date;
					}
										
					flag_time = true;
					
					if(endTime){
						endTimeStr = endTime.config.dateTime.year+'-'+(endTime.config.dateTime.month+1)+'-'+endTime.config.dateTime.date;
					}
					if(startTime){
						startTimeStr = startTime.config.dateTime.year+'-'+(startTime.config.dateTime.month+1)+'-'+startTime.config.dateTime.date;
					}
					computeMoney(startTimeStr,endTimeStr);
				}
			});
			
			var startDateTime = laydate.render({
				elem : '#startDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endDateTime.config.min.year = date.year;
						endDateTime.config.min.month = date.month - 1;
						endDateTime.config.min.date = date.date;
					}else{
						endDateTime.config.min.year = '1900';
						endDateTime.config.min.month = '01';
						endDateTime.config.min.date = '01';
					}
				}
			});
			
			var endDateTime = laydate.render({
				elem : '#endDateTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startDateTime.config.max.year = date.year;
						startDateTime.config.max.month = date.month - 1;
						startDateTime.config.max.date = date.date;
					}else{
						startDateTime.config.max.year = '2099';
						startDateTime.config.max.month = '12';
						startDateTime.config.max.date = '31';
					}
				}
			});
			
			var getTime = laydate.render({
				elem : '#msGetTime',
				trigger : 'click'
			});
			
			form = layui.form;
			
			form.verify({
				sumMoney : function(value, item) {
					if(!(/^[1-9]\d*(\.\d+)?$/).test(value) || !(/^[1-9]\d*(\.\d+)?$/).test(value)){
						return '所选时间段没有可结算的资金';
					}
				}
			});
			
			
			form.on('submit(formDemo)', function(data) {
				//console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
				//console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
				//console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
				
				$.ajax({
					type : 'POST',
					url : 'stat/back/addCheckoutRecord.action',
					data : {
						'endTime' : endTimeStr,
						'startTime' : startTimeStr,
						'year' : new Date().getFullYear()
					},
					success : function(info){
						layer.closeAll();
						if(info.state==1){
							layer.msg(info.mes);
						}else{
							layer.msg('发生未知错误，请稍后再试!', {
								icon : 5,
								anim : 6
							});
						}
						search();
					},
					error : function(){
						
					}
				});
				
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
			});
			
			form.on('submit(formDemoTwo)', function(data) {
				/* alert($("#msId").val());
				return false;  */
				$.ajax({
					type : 'POST',
					url : 'stat/back/confirmCheckout.action',
					data : {
						'msGetEmp' : $("#msGetEmp").val(),
						'msGetType' : $("#msGetType").val(),
						'msGetTime' : $("#msGetTime").val(),
						'msId' : $("#msId").val()
					},
					success : function(info){
						layer.closeAll();
						if(info.state==1){
							layer.msg(info.mes);
						}else{
							layer.msg('发生未知错误，请稍后再试!', {
								icon : 5,
								anim : 6
							});
						}
						search();
					},
					error : function(){
						
					}
				});
				
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
			});
			
			//监听工具条
			table.on('tool(demo)', function(obj) {
				var data = obj.data;
				if (obj.event === 'detail') {
					displayTodo(data.msId);
				} else if (obj.event === 'del') {
					layer.confirm('真的删除行么', function(index) {
						dele(data.msId);
						obj.del();
						layer.close(index);
					});
				} else if (obj.event === 'edit') {
					show_confirm_from(data.msId);
				}
			});
			
			
			var $ = layui.$, active = {
				reload : function() {
					
				}
			};

			$('.demoTable .layui-btn').on('click', function() {
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});

		});

		/**
			计算金额
		 */
		function computeMoney(startTime, endTiem) {
			if (startTime != '' && startTime != null && endTiem != ''
					&& endTime != null) {
				$.ajax({
					type : 'POST',
					url : 'stat/back/computeMoney.action',
					data : {
						"startTime" : startTimeStr,
						"endTime" : endTimeStr
					},
					success : function(info) {
						$("#sumMoney").val(info.mes);
					},
					error : function() {
						layer.msg('发生未知错误，请稍后再试!', {
							icon : 5,
							anim : 6
						});
					}
				});
			}
		}

		/**
			显示结算的表单层
		 */
		function show_checkout_from() {

			var s = '';
			$.ajax({
				type : 'POST',
				url : 'stat/back/queryEndCheckoutTime.action',
				data : null,
				async : false,
				success : function(info) {
					var dat = info.mes.split(' ')[0];
					if(dat){
						s = '可选择最早结算时间：' + dat;
						//2017-11-21 11:55:42
						startTime.config.min.year = dat.split('-')[0];
						startTime.config.min.month = (dat.split('-')[1])-1;
						startTime.config.min.date = dat.split('-')[2];
					}
				},
				error : function() {
					layer.msg('发生未知错误，请稍后再试!', {
						icon : 5,
						anim : 6
					});
				}
			});

			layer.open({
				type : 1,
				skin : 'layui-layer-molv',
				area : [ '65%', '60%' ],
				title : '资金结算',
				id : 'show_checkout_from',
				content : $("#checkout_from"),
				shade : [ 0.8, '#393D49' ],
				shadeClose : true,
				anim : 2,
				success : function(index) {
					$('#start').text(s);
					
					var d = new Date();
					var nowYear = d.getFullYear();
					var nowMonth = d.getMonth()+1;
					var nowDay = d.getDate();
					
					if(nowMonth==1 && nowDay<15){//如果是一月份并且日期小于15
						endTime.config.max.year = nowYear-1;
						endTime.config.max.month = 10;
					}else if(nowMonth==1 && nowDay>=15){
						endTime.config.max.year = nowYear-1;
						endTime.config.max.month = 11;
					}else if(nowMonth==2 && nowDay<15){//如果是二月份并且日期小于15
						endTime.config.max.year = nowYear-1;
						endTime.config.max.month = 11;
					}else if(nowDay<15){//其他月份日期小于15
						endTime.config.max.month = nowMonth-2;
					}else if(nowDay>=15){//其他月份日期大于15
						endTime.config.max.month = nowMonth-1;
					}
					
				}
			});
		}
		
		/**
			显示确认的表单层
		*/
		function show_confirm_from(msId){
			layer.open({
				type : 1,
				skin : 'layui-layer-molv',
				area : [ '65%', '60%' ],
				title : '资金结算',
				id : 'show_confirm_from',
				content : $("#checkout_from_two"),
				shade : [ 0.8, '#393D49' ],
				shadeClose : true,
				anim : 2,
				success : function(){
					$("#msId").val(msId);
				}
			});
		}
		
		/**
			删除记录
		*/
		function dele(id){
			$.ajax({
				type : 'POST',
				url : 'stat/back/delete.action',
				data : {'id':id},
				async : false,
				success : function (info){
					layer.msg(info.mes);
				},
				error : function (){
					layer.msg('发生未知错误，请稍后再试!', {
						icon : 5,
						anim : 6
					});
				}
			});
		}
		
		function search(){
			table.reload('moneyStat', {
			   url: 'stat/back/showList.action'
			   ,where: {
				  'msEmp' : $('#msEmp').val(),
				  'msGet' : $('#msGet').val(),
				  'startDateTime' : $('#startDateTime').val(),
				  'endDateTime' : $('#endDateTime').val()
			   } //设定异步数据接口的额外参数
			});
		}
		
		
		function displayTodo(id){
			$.ajax({
				type : 'POST',
				url : 'stat/back/displayTodo.action',
				data : {'id':id},
				async : false,
				success : function (info){
					var str = '<div style="padding:10px;"><table  class="layui-table"><tr><td>结算时间</td><td>结算金额</td><td>商品</td><td>订单号</td><td>是否结算</td></tr>';
					for(i=0; i<info.length; i++){
						var s = '';
						if(info[i].isJs==0){
							s = '<span color="red">未结算</span>';
						}else{
							s = '<span color="green">已结算</span>';
						}
						str += '<tr><td>'+info[i].datetime+'</td><td>'+info[i].mrMoney+'</td><td>'+info[i].proName+'</td><td>'+info[i].mxId+'</td><td>'+s+'</td></tr>';
					}
					str += '</table></div>';
					layer.open({
						type : 1,
						skin : 'layui-layer-molv',
						area : [ '65%', '60%' ],
						title : '查看详情',
						id : 'show_todo',
						content : str,
						shade : [ 0.8, '#393D49' ],
						shadeClose : true,
						anim : 2
					});
				},
				error : function (){
					layer.msg('发生未知错误，请稍后再试!', {
						icon : 5,
						anim : 6
					});
				}
			});
		}
		
	</script>

</body>
<!-- 结算表单层 -->
<div style="display: none;" id="checkout_from">
	<div style="padding: 40px;">
		<form class="layui-form">
			<div class="layui-form-item">
				<label class="layui-form-label">起始时间</label>
				<div class="layui-input-inline">
					<input type="text" placeholder="起始时间" class="layui-input" id="startTime">
				</div>
				<div class="layui-form-mid layui-word-aux">
					<span id="start" style="color:red;">
						
					</span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">结束时间</label>
				<div class="layui-input-inline">
					<input type="text" placeholder="结束时间" class="layui-input" id="endTime">
				</div>
				<div class="layui-form-mid layui-word-aux">可结算至
					<span id="end" style="color:red;">
						
					</span>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">总金额</label> 
				<div class="layui-input-inline">
					<input type="text" lay-verify="required|sumMoney" placeholder="选择日期后自动结算" id="sumMoney"
						disabled="disabled" autocomplete="off" class="layui-input">  
				</div>
				<!-- <div class="layui-form-mid layui-word-aux">辅助文字</div> -->
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
			<div align="center" style="color: red;">
				注：当月15日后才能结算上个月的资金。
			</div>
		</form>
	</div>
</div>
<!--  -->
<div style="display: none;" id="checkout_from_two">
	<div style="padding: 40px;">
		<form class="layui-form">
			<div class="layui-form-item">
				<label class="layui-form-label">收款人</label>
				<div class="layui-input-inline">
					<input type="hidden" id="msId"/>
					<input type="text" lay-verify="required" placeholder="请填写收款人" id="msGetEmp"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">收款方式</label>
				<div class="layui-input-inline">
					<input type="text" lay-verify="required" placeholder="请填写收款方式" id="msGetType"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">收款时间</label> 
				<div class="layui-input-inline">
					<input type="text" lay-verify="required" placeholder="结束时间" class="layui-input" id="msGetTime">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemoTwo">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>
</div>
</html>