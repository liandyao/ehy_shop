<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>修改订单</title>
		<link rel="stylesheet" href="res/layui/css/layui.css">
		<script src="res/js/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script src="res/js/ehy_common.js" type="text/javascript" ></script>
		<script src="res/layui/layui.js"></script>
		<script src="res/js/back-order.js"></script>
	</head>
	<style type="text/css">
		#sousuo .layui-input {
			height: 30px;
		}
		#sousuo td {
			padding-right: 5px;
			padding-top: 5px;
		}
		body .layui-elem-quote {
			padding: 7px;
		}
		table .layui-input-inline{
			width: 160px;
		}
		td button{
			margin-left: 5px;
			float: right;
		}
		</style>
	<body>
		
		<div class="demoTable layui-elem-quote layui-quote-nm" id="searchDiv">
			<div class="layui-inline">
			  	<form class="layui-form" id="sousuo" style="float: left;">
			  		<table>
						<tr>
							<td>
								<div class="layui-input-inline">
							        <input id="minOrdSumMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="maxOrdSumMoney" placeholder="总金额"  autocomplete="off" 
							        	class="layui-input" type="text" onkeyup="number(this);"/>
							    </div>
							</td>
							<td>
								<div class="layui-input-inline">
							        <input id="startOrdTime" placeholder="下单时间"  autocomplete="off" 
							        	class="layui-input" type="text" />
							    </div>
							   -
							  	<div class="layui-input-inline"> 
							        <input id="endOrdTime" placeholder="下单时间"  autocomplete="off" 
							        	class="layui-input" type="text" />
							    </div>
							</td>
							<td>
								<div id="last" class="layui-input-inline" >
									当前站点：
									<div id="seachStation" style="width:80px;" class="layui-input-inline">
										<select name="stId" id="stId">
							 			
										</select>
									</div>
								</div>
								<div class="layui-input-inline" style="width:14 0px;">
									<input id="ordId" placeholder="订单号"  autocomplete="off" 
							        	class="layui-input" type="text" />
								</div>
								<button type="button" class="layui-btn layui-btn-small" onclick="reloadRec();"><i class="layui-icon">&#xe615;</i>搜索</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		
		<div style="font-size:14px;">
			<p> <font color="red">*</font>点击总金额列修改总金额，点击运费修列改运费，点击数量查看详情 </p>
		</div>
		
		<table class="layui-table" id="LAY_table_user" lay-filter="demoEvent"></table>
		
		
		<script type="text/html" id="barDemo">
  			<a class="layui-btn layui-btn-primary layui-btn-mini" lay-event="detail">查看</a>
  			<a class="layui-btn layui-btn-mini" lay-event="edit">编辑</a>
  			<a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="del">删除</a>
		</script>

	<script type="text/javascript">
		
		var table;
		var laydate;
		
		layui.use(['table','laydate'], function() {
			table = layui.table;
			/* 加载表格数据 */
			table.render({
				  loading: true
			      ,id: 'e_order'
				  ,elem: '#LAY_table_user'
			      ,url: 'orderItem/back/showList.action'
			      ,method: 'post'
			      ,where:{
			    	  "state" : "4"
			      }
			      ,cols: [[
			    	  {field:'ordId', title: '订单号', width:190, align:'center', fixed: 'left'},
			    	  /* {field:'stName', title: '站点名称', width:80, align:'center', fixed: 'left'}, */
			    	  {field:'ordSumMoney', title: '总金额', width:80, align:'center', event: 'editMoney', style:'cursor: pointer;'},
			    	  {field:'ordFreight', title: '运费', width:80, align:'center', event: 'setSign', style:'cursor: pointer;'},
			    	  {field:'ordSum', title: '总数量', width:80, align:'center', event: 'queryName', style:'cursor: pointer;'},
			    	  {field:'ordTime', title: '下单时间', width:145, align:'center'},
				      {field:'ordMember', title: '收货人', width:100, align:'center'},
				      {field:'ordPhone', title: '联系方式', width:110, align:'center'},
				      {field:'ordAdderss', title: '收货地址', width:340, align:'center'},
				      {field:'mxRemark', title: '备注', width:200, align:'center'},
				      {fixed: 'right', width:160, align:'center', toolbar: '#barDemo'}
			      ]] 
			      ,page: true
			      ,height: 400
			      ,size: "sm"
			      ,done: function(res, curr, count){
			    	  layer.closeAll();//加载层关闭  
			      }
			});
			
			//监听单元格事件
			table.on('tool(demoEvent)', function(obj) {
				var data = obj.data;
				if (obj.event === 'editMoney') {
					layer.prompt({
						formType : 2,
						title : '修改订单号为 [' + data.ordId + ']付款价格',
						value : data.ordSumMoney
					}, function(value, index) {
						//匹配最多两位小数的正则表达式
						var isNum=/^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
						if(!isNum.test(value)){
							layer.msg('请输入正确的数字', {icon: 5,anim: 6});
							return;//若输入的不是一个数字就不往后执行 
						}
						layer.confirm('确认将付款价格修改到<span style="color: red;margin: 0px 3px 0px 3px;font-size: 20px;">'+value+'</span>元？', function(index){
							//这里一般是发送修改的Ajax请求
							updateMoney(value, 1, data.ordId, index, obj);//调用修改价格方法，2：修改订单运费
						});
					});
				}else if(obj.event === 'queryName'){
					var s = '<div style="padding:40px;"><table class="layui-table">';
					var arr_1 = data.proName.split(','); 
					var arr_2 = data.mxNum.split(',');
					for(i=0; i<arr_1.length; i++){
						s += '<tr><td>'+arr_1[i]+'</td><td>'+arr_2[i]+' 件</td></tr>';
					}
					s += '</table></div>';
					layer.open({
						  type: 1,
						  title: false,
						  closeBtn: 0,
						  area: ["80%"],
						  shadeClose: true,
						  skin: 'yourclass',
						  content: s
						});
				}else if(obj.event === 'setSign'){
					layer.prompt({
						formType : 2,
						title : '修改订单号为 [' + data.ordId + ']运费',
						value : data.ordFreight
					}, function(value, index) {
						//匹配最多两位小数的正则表达式
						var isNum=/^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
						if(!isNum.test(value)){
							layer.msg('请输入正确的数字', {icon: 5,anim: 6});
							return;//若输入的不是一个数字就不往后执行 
						}
						layer.confirm('确认将运费修改到<span style="color: red;margin: 0px 3px 0px 3px;font-size: 20px;">'+value+'</span>元？', function(index){
							//这里一般是发送修改的Ajax请求
							updateMoney(value, 2, data.ordId, index);//调用修改价格方法，2：修改订单运费
						});
					});
				}else if(obj.event === 'del'){
					layer.confirm('真的删除行么', function(index){
						var index = layer.load();
						del(data.ordId, obj, index);
					});
				}
			});
			
			laydate = layui.laydate;
			//下单时间的日期控件
			var startOrdTime = laydate.render({
				elem : '#startOrdTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						endOrdTime.config.min.year = date.year;
						endOrdTime.config.min.month = date.month - 1;
						endOrdTime.config.min.date = date.date;
					}else{
						endOrdTime.config.min.year = '1900';
						endOrdTime.config.min.month = '01';
						endOrdTime.config.min.date = '01';
					}
				}
			});
			var endOrdTime = laydate.render({
				elem : '#endOrdTime',
				trigger : 'click',
				done : function(value, date, endDate) {
					if(value!=''){
						startOrdTime.config.max.year = date.year;
						startOrdTime.config.max.month = date.month - 1;
						startOrdTime.config.max.date = date.date;
					}else{
						startOrdTime.config.max.year = '2099';
						startOrdTime.config.max.month = '12';
						startOrdTime.config.max.date = '31';
					}
				}
			});
			
			
		});
		
		/**
			修改价格方法
			@param money 修改后的价格
			@param state 参数 1:修改订单总金额,2:修改订单运费
			@param ordId 订单id
			@param index 弹出层序号 
		*/
		function updateMoney(money, state, ordId, index, obj){
			var i = layer.load();
			$.ajax({
				url : 'orderItem/back/updateMoney.action',
				type : 'POST',
				data : {'money':money,'state':state,'ordId':ordId},
				async : false,
				success : function(obj){
					layer.closeAll();//当数据响应过来就关闭弹窗 
					layer.msg(obj.mes);
					//同步更新表格和缓存对应的值
					if(state==1){
						obj.update({
							ordSumMoney : value
						});
					}else if(state==2){
						obj.update({
							ordFreight : value
						});
					}
				},
				error : function(){
					layer.close(i);
					layer.closeAll();//当数据响应过来就关闭弹窗 
					layer.msg('发生未知错误，请稍后再试!', {icon: 5,anim: 6});
				}
			});
		}
		
		/**
			重新加载数据表格
		*/
		function reloadRec(){
			table.reload('e_order', {
				where: {
					'state' : "4",
					'stId' : $("#stId").val(),
					'startOrdTime' : $("#startOrdTime").val(),
					'endOrdTime' : $("#endOrdTime").val()==""?"":$("#endOrdTime").val()+" 23:59:59",
					'minOrdSumMoney' : $("#minOrdSumMoney").val(),
					'maxOrdSumMoney' : $("#maxOrdSumMoney").val(),
					'ordId' : $("#ordId").val()
				}
			});
		}
		
		function del(ordId, obj, index){
			$.ajax({
				url : 'order/front/deleteOrder.action',
				data : {'ordId':ordId},
				async : false, 
				success : function(info){
					layer.close(index);
					obj.del();
					layer.msg(info.mes);
				},
				error : function(){
					layer.close(index);
					layer.msg('发生未知错误，请稍后再试!', {icon: 5,anim: 6});
				}
			});
		}
		 
		$(function (){
			showStation('${manager.stId}');
			var queryStation = parent.queryStation;
			if(queryStation){
				$("#searchDiv").removeAttr('disable');
			}else{
				$("#searchDiv").attr('disable');
			}
		});
	</script>

</body>
</html>