<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>统计图</title>
<link rel="stylesheet" href="res/layui/css/layui.css">
<script src="res/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="res/layui/layui.js"></script>
<script src="res/js/ehy_common.js" type="text/javascript" ></script>
<script src="res/js/echarts.js"></script>
<style type="text/css">

body{margin: 10px;}
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
  
</style>
</head>
<body style="padding: 10px 55px 0px 60px;">
	<div class="layui-tab">
		<ul class="layui-tab-title">
			<li class="layui-this">折线图</li>
			<li>订单明细表</li>

		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show"> 
				年份：
				<div class="layui-inline" style="width: 150px">
					<form id="form" class="layui-form" lay-filter="from">
						<select name="selYear" id="selYear" lay-filter="aihao">
						</select> 
					</form>
				</div>
				<button class="layui-btn" data-type="reload" onclick="selectOrderMap()"><i class="layui-icon">&#xe615;</i>搜索</button>
				</br>
				<div id="main" style="height:280px"></div>
			</div>
			<div class="layui-tab-item">
				<div class="demoTable"> 
					   商品名称：
					  <div class="layui-inline">
					    <input class="layui-input" name="proName" id="proName" style="width: 150px" autocomplete="off">
					  </div>
					   时间：
					  <div class="layui-input-inline">
						 <input name="startTime" id="startTime" lay-verify="date"
							 placeholder="请选择起始日" style="width: 150px" autocomplete="off"
							 class="layui-input" type="text">
					  </div>
						~ 
					  <div class="layui-input-inline">
						 <input name="endTime" id="endTime" lay-verify="date"
							 placeholder="请选择结束日" style="width: 150px" autocomplete="off"
							 class="layui-input" type="text">
					  </div>
					  <button class="layui-btn" data-type="reload"><i class="layui-icon">&#xe615;</i>搜索</button>
			    </div>
				<table class="layui-hide" id="test" style="height:400px"></table>
			</div>
		</div>
	</div>
	<script>
	//获取从父页面传过来的参数的方法
	function GetQueryString(name){
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	
	
	
layui.use(['element','form','layer','laydate','table'], function(){
	form = layui.form;
	var table = layui.table;
	var laydate = layui.laydate;
	layer = layui.layer;
	form.render('select', 'from'); 
	var code = GetQueryString("code"); 
	 table.render({
	    elem: '#test'
	    ,url:'invitationCode/back/findOrderItem.action?code='+code
	    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增 
	    ,page:true 
	    ,method:'post'
	    ,id:'idTest'
	    ,cols: [[
	    	{field:'stName', title: '站点名称', width:95, align:'center'}
	       ,{field:'mbName', title: '购买人', width:95, align:'center'}
	       ,{field:'proName', title: '商品名称', width:150, align:'center'}
	       ,{field:'mxDatetime', title: '下单时间', width:165, align:'center'} 
	       ,{field:'mxNum', title: '数量', width:69, align:'center'}
	       ,{field:'mxPrice', title: '购买价格', width:105, align:'center'} 
	       ,{field:'mxMoneyFact', title: '实际付款金额', width:115, align:'center',templet: '#moneyTpl'}
		     
	    ]]
	});
	 //条件查询的方法
 	  var $ = layui.$, active = { 
		    reload: function(){  
		      var startTime = $('#startTime');
		      var endTiem = $('#endTime');
		      var proName = $('#proName');
		      table.reload('idTest', {
		        where: { 
		        	startTime: startTime.val(),
		        	endTiem: endTiem.val(),
		        	proName: proName.val()
		        } 
		      });
		    }
 		};
  	  //搜索按钮点击的方法
	  $('.demoTable .layui-btn').on('click', function(){ 
		    var type = $(this).data('type');
		    active[type] ? active[type].call(this) : '';
	  });  
	//日期
		var startTime = laydate.render({
			elem : '#startTime',
			trigger : 'click',
			done : function(value, date, endDate) {
				if(value!=''){
					endTime.config.min.year = date.year;
					endTime.config.min.month = date.month - 1;
					endTime.config.min.date = date.date;
				}else{
					endTime.config.min.year = '1900';
					endTime.config.min.month = '01';
					endTime.config.min.date = '01';
				}
				reloadRec();
			}
		});
		//日期
		var endTime = laydate.render({
			elem : '#endTime',
			trigger : 'click',
			done : function(value, date, endDate) {
				if(value!=''){
					startTime.config.max.year = date.year;
					startTime.config.max.month = date.month - 1;
					startTime.config.max.date = date.date;
				}else{
					startTime.config.max.year = '2099';
					startTime.config.max.month = '12';
					startTime.config.max.date = '31';
				}
				reloadRec();
			}
		}); 
});
function DateSelector(selYear) {
	this.selYear = selYear;
	this.selYear.Group = this;

	// 给年份、月份下拉菜单添加处理onchange事件的函数 
	if (window.document.all != null) // IE 
	{
		this.selYear.attachEvent("onchange", DateSelector.Onchange);

	} else // Firefox 
	{
		this.selYear.addEventListener("change", DateSelector.Onchange,
				false);

	}
	if (arguments.length == 4) // 如果传入参数个数为4，最后一个参数必须为Date对象 
		this.InitSelector(arguments[3].getFullYear());
	else if (arguments.length == 6) // 如果传入参数个数为6，最后三个参数必须为初始的年月日数值 
		this.InitSelector(arguments[3]);
	else // 默认使用当前日期 
	{
		var dt = new Date();
		this.InitSelector(dt.getFullYear());
	}
}
// 增加一个最大年份的属性 
DateSelector.prototype.MinYear = 1900;
// 增加一个最大年份的属性 
DateSelector.prototype.MaxYear = (new Date()).getFullYear();
// 初始化年份 
DateSelector.prototype.InitYearSelect = function() {
	// 循环添加OPION元素到年份select对象中 
	for ( var i = this.MaxYear; i >= this.MinYear; i--) {
		// 新建一个OPTION对象 
		var op = window.document.createElement("OPTION");
		// 设置OPTION对象的值 
		op.value = i;
		// 设置OPTION对象的内容 
		op.innerHTML = i;
		// 添加到年份select对象 
		this.selYear.appendChild(op);
	}
}

// 处理年份和月份onchange事件的方法，它获取事件来源对象（即selYear或selMonth） 
// 并调用它的Group对象（即DateSelector实例，请见构造函数）提供的InitDaySelect方法重新初始化天数 
// 参数e为event对象 
DateSelector.Onchange = function(e) {
	var selector = window.document.all != null ? e.srcElement : e.target;
	selector.Group.InitDaySelect();
}
// 根据参数初始化下拉菜单选项 
DateSelector.prototype.InitSelector = function(year) {
	// 由于外部是可以调用这个方法，因此我们在这里也要将selYear和selMonth的选项清空掉 
	// 另外因为InitDaySelect方法已经有清空天数下拉菜单，因此这里就不用重复工作了 
	this.selYear.options.length = 0;
	// 初始化年、月 
	this.InitYearSelect();
	// 设置年、月初始值 
	this.selYear.selectedIndex = this.MaxYear - year;

}
$(function() {
	
	selectOrderMap();
	
});
function selectOrderMap(){ 
	var year = $("#selYear").val();
	 // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
     
    var code = GetQueryString('code'); 
    var data = [];
    
    var url = 'invitationCode/back/selectOrderMap.action';
    var d = {code:code,year:year};
    
    $.post(url, d, function(info){
    	for ( var i = 0; i < info.list.length; i++) {
			data.push([ info.list[i].mxDatetime, info.list[i].mxNum ]);
		}
    	
        var dateList = data.map(function (item) {
            return item[0];
        });
        var valueList = data.map(function (item) {
            return item[1];
        });
        
        option = {

            // Make gradient line here
            visualMap: [{
                show: false,
                type: 'continuous',
                seriesIndex: 0,
                min: 0,
                max: 5
            } ],

            title: [{
                left: 'center',
                text: year+'年各月的订单数的统计折线图'
            }],
            tooltip: {
                trigger: 'axis'
            },
            xAxis: [{
                data: dateList
                
            } ],
            yAxis: [{
                splitLine: {show: false}
            } ],
            grid: [{
                bottom: '8%'
            }, {
                top: '10%'
            }],
            series: [{
                type: 'line',
                showSymbol: false,
                data: valueList
            } ]
        }; 
       
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        
    });
    
    
  
}
</script>
<script type="text/html" id="moneyTpl"> 
  	 <span style="color: #FF6737;">{{ d.mxMoneyFact }}</span> 
</script>
<script type="text/javascript">
var selYear = window.document.getElementById("selYear");
var selMonth = window.document.getElementById("selMonth");
// 新建一个DateSelector类的实例，将select对象传进去 
new DateSelector(selYear, selMonth, 2004, 2, 29);
</script>
</body>
</html>