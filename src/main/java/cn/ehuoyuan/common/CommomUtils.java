/**
 * 
 */
package cn.ehuoyuan.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

 
/**
 * 定义常量类,需要使用的常量在这里取到
 * @author liandyao
 * @date 2017年9月29日 上午11:52:25
 * @version 1.0
 */
public class CommomUtils {
	
	/**
	 * <p>类似如:http://ehuoyuan.cn/shop<p>
	 * <p>域名地址,注意域名后面的shop是本系统的上下文路径,如果系统直接放在ROOT下,那么shop可以不用.</p>
	 * 
	 */
	public static String DOMAIN_PATH = "";
	/**
	 * 系统配置
	 */
	public static Properties CONF = new Properties();
	
	static{
		try {
			CONF.load(CommomUtils.class.getClassLoader().getResourceAsStream("config/conf.properties"));
			String domain= CONF.getProperty("domain");
			if(!Tools.isEmpty(domain)) {
				DOMAIN_PATH = domain ;
			}
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	/**
	 * 支付类型:微信支付
	 */
	public final static String PAY_TYPE_WXPAY = "WXPAY";
	
	/**
	 * 支付类型:阿里支付
	 */
	public final static String PAY_TYPE_ALIPAY = "ALIPAY";
	
	
	/**
	 * 分页每页显示条数
	 */
	public final static int PAGES_MAX_RESULT=5; 
	
	/**
	 * 是否有效 有效
	 */
	public final static int ISVA_YES = 1 ;
	
	/**
	 * 是否有效 无效
	 */
	public final static int ISVA_NO = 0;
	
	/**
	 * 退货1,与数据库同步
	 */
	public final static int STATE_ORDER_TUIHUO = 1;
	
	
	/**
	 * 订单确认.待付款 10 
	 */
	public final static int STATE_ORDER_OK = 10;
	
	/**
	 * 已付款 20 
	 */
	public final static int STATE_PAY_OK = 20;
	
	/**
	 * 已发货 30  与数据库字段保持同步
	 */
	public final static int STATE_EXP = 30;
	
	/**
	 * 已签收 40 与数据库字段保持同步
	 */
	public final static int STATE_OK = 40;
	
	/**
	 * 退款申请,与数据库同步
	 */
	public final static int STATE_ORDER_TUIKUAN = 50;
	
	/**
	 * 退款申请,未发货的申请退款.与数据库同步
	 */
	public final static int STATE_ORDER_TUIKUAN2 = 51;
	
	
	/**
	 * 已退款成功,与数据库同步
	 */
	public final static int STATE_ORDER_TUIKUAN_OK = 60;
	
	/**
	 * 订单中文描述
	 */
	public final static Map<Integer, String> STATE_MAP = new HashMap<Integer, String>();
	static{
		STATE_MAP.put(STATE_ORDER_OK,"已下单未付款");
		STATE_MAP.put(STATE_PAY_OK,"已付款");
		STATE_MAP.put(STATE_EXP,"已发货");
		STATE_MAP.put(STATE_OK,"已签收"); 
	}
	
	/**
	 * 日期格式
	 * yyyy-MM-dd
	 */
	public static SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 时间格式
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static SimpleDateFormat SDF_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 订单的时间格式
	 * yyyyMMddHHmmss
	 */
	public static SimpleDateFormat SDF_ORDER_TIME = new SimpleDateFormat("yyyyMMddHHmmss");
	
	/**
	 * 支付订单显示字符
	 */
	public static final String PAY_ORDER_NAME = "E货源商品支付订单";
}
