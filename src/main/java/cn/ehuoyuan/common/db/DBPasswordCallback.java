/**
 * 
 */
package cn.ehuoyuan.common.db;

import java.util.Properties;

import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.util.DruidPasswordCallback;

import cn.ehuoyuan.common.Tools;

/**
 * 密码加密类
 * <p>
 * 使用方式:
 * </p>
 * <p>1 首先运行RSAKeysUtil.java类,生成公钥和私钥.私钥是用来给密码加密,公钥用来解密</p>
 * <p>2 使用该类的main方法生成密码.注意看main方法里面的例子,将生成的密码放入到db.properties文件中</p>
 * <p>3 在spring中配置该类,将该类作为加密的工具,重写其setPassword方法将密码解码之后传入</p>
 * @author liandyao
 * @date 2017年11月11日
 * @version 1.0
 */
public class DBPasswordCallback extends DruidPasswordCallback {

	 //上述生成的私钥
    private static final String PRIVATE_KEY_STRING = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAI8ep3/Nxby2o7Fa1vTrl6rvDuw8" + 
    		"ugGaTjrZY+PemyxnEs04mMXd5KjwKxH0IpTCMuelB2Pcocuo2LZwnu3q3bgra7mHgBOP32FLXhTg" + 
    		"q5cMYc9iNPnvg1DSptAhGKf2gpuSM40MQiXCd2mCBrGTTozCfF6ACx5Gnl4z/jY4cBw9AgMBAAEC" + 
    		"gYEAg+fq1L3gS0fBospoZ59jT+W4yT5G6QdApriJehFGzyvIhmBtXaBfCN0htRBfdt9tb1YIc597" + 
    		"d2sNIUi3Eexvn8mdfNcYLhFyc5jkvalkFzV7FiFFL07VeBf2b4i1DzZN2Ut+wgW7k5rmzhKGOtrd" + 
    		"mlwcANlGipgcYMzsuVeZDPkCQQD9KjN7qfxQw5DK5SiNZz7IhcRgbMR4xsQ8v2sv+FIwsyi/OVVJ" + 
    		"frocBvIPJil2wklIJJ1MhmtLxnoVoLH0rw8XAkEAkLj28VwXbRVM0CaA3hrziByEaA3vNc2MloU6" + 
    		"yIEUAYO83pcNhSYqohBN+dKDnUMhm1t9IRNz/xPVVHtG00kjywJAcYYXCAE1z9GigBk6kg4WLT0u" + 
    		"zj536+DV5pe/rQchdA0i3H0NLaPSOPc0XG60cTi2O8+Pad1tB9G8tS4WHq7RiQJBAJBVeCsU7mQj" + 
    		"Vx1NGT7p4DHCRydDpmuhWXIQ46RuX1Z3XVkdiyVdAC3rVukt04d3GUBJCU2AijI+RIdh/yvfiOsC" + 
    		"QQCt5B2rILe6J9xwtkTctog1+19aP9eeejZTq0g11eUo1KpQLEmMjcFwTp9MjXROpeQJYA+8h7+9" + 
    		"hzCNrlgx+ofP";

  //上述生成的公钥
    public static final String PUBLIC_KEY_STRING = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCPHqd/zcW8tqOxWtb065eq7w7sPLoBmk462WPj" + 
    		"3pssZxLNOJjF3eSo8CsR9CKUwjLnpQdj3KHLqNi2cJ7t6t24K2u5h4ATj99hS14U4KuXDGHPYjT5" + 
    		"74NQ0qbQIRin9oKbkjONDEIlwndpggaxk06MwnxegAseRp5eM/42OHAcPQIDAQAB";
    public void setProperties(Properties properties) {
        super.setProperties(properties);
        String pwd = properties.getProperty("password"); 
        if (!Tools.isEmpty(pwd)) {
            try {
                //这里的password是将jdbc.properties配置得到的密码进行解密之后的值
                //所以这里的代码是将密码进行解密
                //TODO 将pwd进行解密;
                String password = ConfigTools.decrypt(PUBLIC_KEY_STRING, pwd); 
                setPassword(password.toCharArray()); //解密之后传入真正的项目中
            } catch (Exception e) {
            	e.printStackTrace();
                setPassword(pwd.toCharArray());
            }
        }
    }
	
	public static void main(String[] args) {
		 String plainText = "liandyao";
	     try {
			
			String pwd = ConfigTools.encrypt(PRIVATE_KEY_STRING, plainText);
			System.out.printf(pwd);
			//String depwd = ConfigTools.decrypt(PUBLIC_KEY_STRING, pwd);
			//logger.info(depwd);
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	
}
