﻿package cn.ehuoyuan.common;
import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */
public class AlipayConfig {

	//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2017111900042803";

	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCvTOr6uPMY2MrVrQTJ1eeMKtKSbSFASm4m5Qs5YTI9OpYQGi9oTqOJ8keegPtsE5JfgO5wxhtKMBnGMc1fJXD8oNX3TwVNui3xDTPDKulDAes9Q5HlgYDJpTM/KEuBzb2AwTdmRJL3MsEZsvqzeELMxcQ+upy9tby5H4udb/y2aCjsTtg5jxVHt5M5eSekb5GAakAO7WDCR/pqvNSCDKNbf5f4EPQLamUjWWAKv/8cqk00TBJgnlrzuegs9UDhc7/oYouCzyVZpBEvRsdzRwvKChf4Q/Juo6Wxj7GBvuz/VQx8RQ0bQgD9BvIGWTsKSTIAZFoh4si4QGXta/39w8PJAgMBAAECggEAGTlCHm16XTrkhLZul/Q1aw/GWNdnD28mRmZypQD9T54cdZiu9Z1LHtBDyeTZSIXQa/mLVaLLznL5YnFMEcK7bT5fS0SNiM+fSK4ToqOMCN4/0QLl/VYTB/DDXPUZWEYYXGxw4kA/vV4KNv5v4F8PjZNvV29Wml2KGEEXv8GbZUt9gDR/Biru0LITZj1wFbTbDjXNO96NktHcqxDRjXHEJfR57efhB0qctdh629BHrdWc3vguk5Grj/Z6qcGck/zP+Aqy6brQGUOshMlg3rupI50wg1YAQqIash/Cm31Ps2xSg5bseVKzfHPV9hifVf5CWirHlAtYjxFWB8VjILKCuQKBgQD/VzuiBdrEhl11Vup0idCYDi031TSXZJcZEnZ8XACUWTRChJK/Rlhq78YBF0462Hu/kGpbPtjasMVSU7iPL0JVB4XaOGGWFy5Myiq2z4ahO4ASqOo9DS4qwrp51JItiDDmmN1++x0e8Wk58etIkNgOn7w2pqQp9w7JOcIeSxpPzwKBgQCvwMhOSnR+DBd+k1GOCzrjbYu4a8mULJHIgcUbHTcuBNREt80SNN97rqEya9bgmujtnhkN2PSZI3aauo2VlQ2jbixfE6bmSsX3a2keX4r+Xe+2IkWTe8gwfRKoyvwfKs1VEqgAWunXsrMZmFmaHrnwKn6AW4LoGICwmRMubE5A5wKBgDCiqf4jhVb3jOejxYarFWtRD/XZi2nmxA0HgkAPuiCHmTeuTtFWEdNUlm9ww/rbbz6+wDNfFikZkIws3DORud2+UBWPta7D/ZKs01P81VTyo2NqfytfzlJgwuofvjQkA8R4i3/7TsGtE/DwjnNE+5JVhKEYPp6rq0pS0RG6FmnrAoGAajaBVv8cA4jfMflp0gPrZMIxLBTHz6pXt+rk6Yvwavk0JwtE/XnCjYZtmIEfbMFxfxiKX5n7yAhshvmy0vEvOtM8MYE43ffWR7472sU2xJ6UXWe80HJ/+v27JxL05wkA6ibKLNzEcTxnjFJL5B4plIC5zmx/YjBcpVz9AJMnf4UCgYEAyjorezbGT4Q+g/HkdEoObXsf9LRXkBkNtvJ0p3SIaR4HVPlNtWVdMQkvJJOOia6xt8LKBixez0FGYZrB6phtKQsFCx43B7V9O4aSqTkOjfwSUyOQFo1Qx/LNLq46o4J0KQPuB4rh97M4ark0fI0G5Eaig95IZMRXhjkxH9OJJF8=";

	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
	public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkH2EzrqXkeG3xdi5W+Aioaox4rR4q0vc6F5WDX2hYD4PmRn1RIaUhKn0YJZQ5V33JUO3wbeD9LrrUR6hhrhZs/Z6hWkq532bkQJaUS1VPrbXxDhLETlk3w9BbFKQm9J/44jH9HpIiuxFd2+BO6aUqX2jVVGBxd6CDFHIP8eq8I1evSqsPrX6qxOZ28oFmCAnGnewNsh1wn4D99ntddxDDd4e3wFza4Qe/ENpxosH/J7zQdcTj8bMzXZ2JCmVqtan626VScKQdX5bFEm39I+maP1IfChmFjeuXSN+U6IXYMuHFzGShWHysOEpQvUswOJ42wrRhEVEun1Fjsq6UGcG7QIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = CommomUtils.DOMAIN_PATH+"/alipayAction/notify.action";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = CommomUtils.DOMAIN_PATH+"/alipayAction/result.action";

	// 签名方式
	public static String sign_type = "RSA2";

	// 字符编码格式
	public static String charset = "utf-8";

	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";

	// 支付宝网关
	public static String log_path = "C:\\";


	//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	/** 
	 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
	 * @param sWord 要写入日志里的文本内容
	 */
	public static void logResult(String sWord) {
		FileWriter writer = null;
		try {
			writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
			writer.write(sWord);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
