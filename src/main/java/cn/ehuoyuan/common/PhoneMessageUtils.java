/**
 * 
 */
package cn.ehuoyuan.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List; 
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;


/**
 * 短信发送端-新的平台
 * @author liandyao
 * @date 2018年8月23日
 * @version 1.0
 */
public class PhoneMessageUtils {

	static Logger log = Logger.getLogger(PhoneMessageUtils.class);
	/**
	 * 短信验证码的KEY
	 */
	public static final String account="17340171558" ;
	public static final String password ="xQCGasDEnBvBiP6kZPR9zg==";
	
	static String httpUrl = "http://sms.zhushou001.com/sms/api/v1/intf/send";
	static String httpArg = "?msgType=1&userId="+account; //msgType=1表示可以任何时候发送短信,msgType=0表示晚上22点-6点不允许发送短信
	
	public final static String MODEL = "【e货源】";
	
	public static void main(String[] args) {
		
		try {
			//String httpUrl = "http://sdk.shmyt.cn:8080/manyousms/sendsms";
			//String  result= SmsTest(httpUrl, account, password, "13357225692", "您的验证码是：4875【满友科技】"); 
			//logger.info(result); 
			
			String  result= sms("13357225692", "您的验证码是：5201314"+MODEL); 
			System.out.println(result);
			//String result2 = getBalance();
			
			//logger.info(result+"  余额：   "+result2); 
			
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	 
	public static String  getBalance() throws ClientProtocolException, IOException{
		 
		String httpUrl="http://sdk.shmyt.cn:8080/manyousms/getbalance";
		return requestPost(httpUrl, httpArg);
		
		
		//return SendMsg.sendBatch(sendUrl, account, password, mobile, content) ;
	}
	
	/**
	 * 发送信息
	 * @param mobile 手机号码
	 * @param content 发送的正文,注意不需要带【e货源】,本方法自己会加上
	 * @return 返回是否成功的json
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String  sms(String mobile,String content) throws ClientProtocolException, IOException{
		if(Tools.isEmpty(mobile)) return null;
		//content=URLEncoder.encode(content, "UTF-8");
		String outData = httpArg+"&phone="+mobile+"&msgContent="+MODEL+content;
		//httpUrl = "http://localhost:8080/wexin/pay/test.do";
		String result = requestPost(httpUrl, outData); 
		System.out.println(result);
		return result;
	} 
	 
    /**
     * 使用httpClient的post请求url
     * @param url
     * @param paramsStr
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public static String requestPost(String url,String paramsStr) throws ClientProtocolException, IOException {
        CloseableHttpClient httpclient = HttpClientBuilder.create().build();
        List<NameValuePair> params = new ArrayList<NameValuePair>();     
        
        if(paramsStr!=null && !"".equals(paramsStr)){
        	if(paramsStr.indexOf("?")>-1){
        		paramsStr = paramsStr.substring(1);//将?去掉
        	}
        	String p[] = paramsStr.split("&");
        	for(String s:p){
        		String pa[] = s.split("=");
        		//logger.info(pa[0]+"   "+pa[1]);
        		NameValuePair np = new BasicNameValuePair(pa[0], pa[1]);
        		params.add(np);
        	}
        	NameValuePair np = new BasicNameValuePair("sign",password);
    		params.add(np);
        }
       
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
         
        CloseableHttpResponse response = httpclient.execute(httppost);
        //logger.info(response.toString()); 
        HttpEntity entity = response.getEntity();
        String jsonStr = EntityUtils.toString(entity, "utf-8");
       // logger.info(jsonStr);
         
        httppost.releaseConnection();
		return jsonStr;
    }
	
}
