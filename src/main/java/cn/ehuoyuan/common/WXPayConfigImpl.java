package cn.ehuoyuan.common;

import java.io.InputStream;

import com.github.wxpay.sdk.IWXPayDomain;
import com.github.wxpay.sdk.WXPayConfig;
import com.github.wxpay.sdk.WXPayDomainSimpleImpl;

public class WXPayConfigImpl extends WXPayConfig{

    private static WXPayConfigImpl INSTANCE;
    
    public static WXPayConfigImpl getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (WXPayConfigImpl.class) {
                if (INSTANCE == null) {
                    INSTANCE = new WXPayConfigImpl();
                }
            }
        }
        return INSTANCE;
    }
    
    public String getAppID() {
        return "wx291dcc1ea85e0755"; // 微信支付商户号
    }

    public String getMchID() {
        return "1491576502";// 公众号APPID
    }

    public String getKey() {
        return "h1h2h3g4g3hhdsjfjfesgeg74hsyw83h";
    }

    public InputStream getCertStream() {
    	InputStream certBis;
        certBis =  this.getClass().getClassLoader().getResourceAsStream("config/apiclient_cert.p12");
         
        return certBis;
    }
    
    /**
     * 返回回调地址
     * @return
     */
    public String getNotifyPath() {
    	return CommomUtils.DOMAIN_PATH+"/wxpayAction/notify.action";
    }


    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

	@Override
	public IWXPayDomain getWXPayDomain() { 
		return WXPayDomainSimpleImpl.instance();
	}
    
   
}
