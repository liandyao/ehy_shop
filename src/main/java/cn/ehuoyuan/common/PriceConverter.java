/**
 * 
 */
package cn.ehuoyuan.common;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;


/**
 * 类的描述：金额元转分(String转BigDecimal)
 * @author 罗海兵
 * @dateTime 2017年10月25日 上午8:44:51
 * @version 1.0
 */
public class PriceConverter implements Converter<String, BigDecimal>{

	Logger logger = Logger.getLogger(getClass());
	@Override
	public BigDecimal convert(String price) {
		try {
			BigDecimal newPrice=new BigDecimal(Tools.moneyYuanToFen(price));
			logger.info("原始金额:"+price+"  转换之后的金额:"+newPrice.intValue());
            return newPrice;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}

}
