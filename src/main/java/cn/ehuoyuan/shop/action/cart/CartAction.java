/**
 * 
 */
package cn.ehuoyuan.shop.action.cart;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyCart;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.service.cart.EhyCartService;

/**
 * 类的描述：购物车的控制器
 * @author 罗海兵
 * @dateTime 2017年10月30日 上午8:24:34
 * @version 1.0
 */
@Controller
@RequestMapping("/cart")
public class CartAction {
	
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	EhyCartService ehyCartService;//购物车的Service
	
	/**
     * 加入购物车(可选部分)
     * @param Map的对象
     * @see map.cart 购物车对象
     * @see map.pspId 产品价格ID
     * @return 成功返回1,失败返回0
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:33:14
     * @versions 1.0
     */
	@RequestMapping("front/add")
	@ResponseBody
	public int add(EhyCart cart, HttpSession session){
		EhyMember mem=(EhyMember) session.getAttribute("login");
		cart.setMbId(mem.getMbId());
		cart.setOper(mem.getMbName());
		cart.setOptime(new Date());
		//logger.info("进入了addOrUpdate方法");
		CartVo cartVo = ehyCartService.findCart(cart);
		//logger.info("查询没有问题");
		int rows = 0;
		if(cartVo == null ){
			//logger.info("这是增加");
			rows = ehyCartService.add(cart,null);
			if(rows>0){
				int cartNum=(int) session.getAttribute("cartNum");
				session.setAttribute("cartNum", cartNum+rows);
			}
		}else{
			//logger.info("这是修改");
			EhyCart cart2 = new EhyCart();
			cart2.setCartId(cartVo.getCartId());
			cart2.setCartNum(cartVo.getCartNum()+1);
			rows = ehyCartService.update(cart2);
		}
		return rows;
	}
	
	public static void main(String[] args) {
		
		String password = "Gzlz5H4deDZ3kJTe+ygq1OEBs9bU3QnDyUhdiX2v85Kl2R4pQic9l4ocJrLPjbm50LWG9qNuWx0Sw1v+W5IG6z5/ZeZTkpFzL/U8piu4gHBACG2XpyjNHeDMGnF50t8ZsDA5t7EFKbtnq1QLwK/58HbQPfe/u/HxkfsP5hAyBnA=";
		//logger.info(Tools.decode(password));
	}
	
	/**
     * 根据会员ID查询
     * @param session HttpSession会话对象
     * @return 返回购物车数量
     * @author 罗海兵
     * @dateTime 2017年10月31日 下午4:03:08
     * @versions 1.0
     */
	@RequestMapping("front/findCartNum")
	@ResponseBody
	public int findCartNum(HttpSession session){
		int cartNum=(int) session.getAttribute("cartNum");
		return cartNum;
	}
	
	/**
	 * 根据会员ID查询购物车集合
	 * @param session HttpSession会话对象
	 * @return 返回一个购物车的集合
	 * @author 罗海兵
	 * @dateTime 2017年10月31日 下午4:43:10
	 * @versions 1.0
	 */
	@RequestMapping("front/findByMbId")
	@ResponseBody
	public List<CartVo> findByMbId(Boolean init, HttpSession session){
		EhyMember mem=(EhyMember) session.getAttribute("login");
		return ehyCartService.findByMbId(mem.getMbId());
	}
	
	/**
	 * 根据购物车ID删除一条记录
	 * @param cartId 购物车ID
	 * @return 成功返回1,失败返回0
	 * @author 罗海兵
	 * @dateTime 2017年11月2日 下午3:15:14
	 * @versions 1.0
	 */
	@RequestMapping("front/delete")
	@ResponseBody
	public int delete(String cartId, HttpSession session){
		int rows=ehyCartService.delete(cartId);
		if(rows>0){
			int cartNum=(int) session.getAttribute("cartNum");
			session.setAttribute("cartNum", cartNum-rows);
		}
		
		
		return rows;
	}
	
	/**
	 * 增加或者减少购物车的产品数量
	 * @param cart 购物车对象
	 * @return 成功返回1,失败返回0
	 * @author 罗海兵
	 * @dateTime 2017年11月2日 下午4:26:50
	 * @versions 1.0
	 */
	@RequestMapping("front/addOrCut")
	@ResponseBody
	public int addOrCut(EhyCart cart){
		int rows=ehyCartService.update(cart);
		return rows;
	}
	
	/**
	 * 购物车结算商品查询
     * <br/>根据购物车ID的数组查询购物车集合
     * @param cartIds 购物车ID的数组
     * @return 返回一个购物车对象的集合
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:34:49
     * @versions 1.0
     */
	@RequestMapping("front/findByCarIds")
	@ResponseBody
	public List<CartVo> findByCarIds(@RequestParam(value = "cartId") String[] cartId){
		return ehyCartService.findByCartIds(cartId);
	}
}
