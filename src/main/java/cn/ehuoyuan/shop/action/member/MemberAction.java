/**
 * 
 */
package cn.ehuoyuan.shop.action.member;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.FileTools;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.PhoneMessageUtils;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyArea;
import cn.ehuoyuan.shop.domain.EhyInvitationCode;
import cn.ehuoyuan.shop.domain.EhyManager;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyOrderItem;
import cn.ehuoyuan.shop.domain.EhyStation;
import cn.ehuoyuan.shop.service.base.area.EhyAreaService;
import cn.ehuoyuan.shop.service.cart.EhyCartService;
import cn.ehuoyuan.shop.service.invitationCode.InvitationCodeService;
import cn.ehuoyuan.shop.service.member.MemberService;
import cn.ehuoyuan.shop.service.station.StationService;

/**
*
* @author WangGuanfu
* @date 2017年10月2日
* version v1.0
*/
@Controller
@RequestMapping("/member")
public class MemberAction {
	
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	private MemberService memberService;
	/**
	 * 注入站点的service
	 */
	@Resource
	private StationService stationService;
	/**
	 * 注入邀请码的service
	 */
	@Resource
	private InvitationCodeService invitationCodeService;
	/**
	 * 注入地区的ID
	 */
	@Resource
	private EhyAreaService ehyAreaService;
	
	/**
	 * 注入购物车的Service
	 */
	@Resource
	private EhyCartService ehyCartService;
	
	EhyMessage mes = new EhyMessage();
	
	/**
	 * 验证会员是否登陆
	 * @param session HttpSession会话对象
	 * @return 已登陆返回true,未登陆返回false
	 * @author 罗海兵
	 * @dateTime 2017年10月30日 上午9:41:11
	 * @versions 1.0
	 */
	@RequestMapping("front/isLogin")
	@ResponseBody
	public boolean isLogin(HttpSession session){
		EhyMember login=(EhyMember) session.getAttribute("login");
		return login!=null;
	}
	
	/**
	 * 获取手机验证码
	 * @author WangGuanfu
	 * @param mbPhone
	 * @param session
	 * @param request
	 * @return
	 */
	@RequestMapping("/front/yzm")
	@ResponseBody
	public EhyMessage getYzm(String mbPhone,HttpSession session,HttpServletRequest request) {
		
		//logger.info(mbPhone);
		EhyMessage mes = new EhyMessage();
		String yzm = Tools.getRandomString(4);
		//logger.info("---------"+yzm);
		try {
			String sun =PhoneMessageUtils.sms(mbPhone,"您注册的验证码是:"+yzm);
			if(sun!=null){
				mes.setState(1);
				HttpSession  sessionYzm =request.getSession();
				sessionYzm.setAttribute("yzm", yzm);
			}
		} catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mes;
	}
	
	
	
	/**
	 * 前台注册
	 * @author WangGuanfu
	 * @param record
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/front/register")
	@ResponseBody
	public EhyMessage register(EhyMember record , HttpServletRequest request,HttpSession session){
			EhyMessage mes = new EhyMessage();
			HttpSession session1 = request.getSession();
			String mbyzm = request.getParameter("yzm");
			String se  =  (String) session1.getAttribute("yzm");
			//logger.info("========"+record.getMbPhone());
			//查询数据库手机号码注册的条数
			
			int row=memberService.selectPhone(record.getMbPhone());
			//logger.info("===========row==========="+row);
			if(Tools.isEmpty(record.getInvitationCode())){
				record.setLevelId("1");//默认会员等级
				record.setMbIsbzj(0);
				if(se!=null && se.equals(mbyzm) && row==0){
					String m = Tools.password(record.getMbLoginPwd());
					record.setMbName("E_"+Tools.getRandomString(5));//默认会员姓名
					record.setMbLoginPwd(m);//会员登录密码
					 
					record.setMbRemark("/upload/member/1508546596_xq92ugo5.jpg");
					record.setInvitationCode(record.getInvitationCode());//会员邀请码
					String path = "member";
				
					int rows =memberService.insertSelective(record);
					EhyMember member =memberService.selectByPrimaryKey(record.getMbId());
					
					HttpSession  sessionYzm =request.getSession();
					sessionYzm.setAttribute("login", member);
					mes.setState(1);
				}else if( row>0){
					mes.setState(3);
					//logger.info("到了");
					mes.setMes("手机号码已经被注册!");
				}else{
					mes.setState(0);
					mes.setMes("注册失败,验证码错误!");
				}
			}else{
				//logger.info("邀请码是"+record.getInvitationCode());
				int codeRow= invitationCodeService.selectByCode(record.getInvitationCode());
				//logger.info("邀请码条数是"+codeRow);
				if(codeRow>0){
					record.setLevelId("2");//默认会员等级
					record.setMbIsbzj(0);
					if(se.equals(mbyzm) && row==0){
						String m = Tools.password(record.getMbLoginPwd());
						record.setMbName("E_"+Tools.getRandomString(5));//默认会员姓名
						record.setMbLoginPwd(m);//会员登录密码
						 
						record.setInvitationCode(record.getInvitationCode());//会员邀请码
						record.setMbRemark("/upload/member/1508546596_xq92ugo5.jpg");
						int rows =memberService.insertSelective(record);
						EhyMember member =memberService.selectByPrimaryKey(record.getMbId());
						
						HttpSession  sessionYzm =request.getSession();
						sessionYzm.setAttribute("login", member);
						mes.setState(1);
					  
					}else if( row>0){
						mes.setState(3);
					}else{
						mes.setState(0);
					}	
				}else{
					mes.setState(2);
				}
				
				
			}

		return mes;
		
	}
	

	/**
	 * 获取手机验证码-找回密码
	 * @author WangGuanfu
	 * @param mbPhone
	 * @param session
	 * @param request
	 * @return
	 */
	@RequestMapping("/front/forgetPwd")
	@ResponseBody
	public EhyMessage forgetPwd(String mbPhone,HttpServletRequest request) {
		
		//logger.info(mbPhone);
		EhyMessage mes = new EhyMessage();
		int rows = memberService.selectPhone(mbPhone);
		if(rows==0) {
			mes.setState(0);
			mes.setMes("该手机号注册的账号不存在!");
			return mes ;
		}
		
		String yzm = Tools.getRandomString(4);
		//logger.info("----找回密码的验证码-----"+yzm);
		try {
			String sun =PhoneMessageUtils.sms(mbPhone,"您正在使用ehuoyuan网上商城找回密码功能!需要的验证码是:"+yzm);
			
			mes.setState(1);
			HttpSession  sessionYzm =request.getSession();
			sessionYzm.setAttribute("forgetPwdYzm", yzm);
			 
		} catch (Exception e){ 
			e.printStackTrace();
		}
		return mes;
	}
	
	/**
	 * 找回密码功能.发送一个随机密码给手机,然后重置密码
	 * @param phone
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@RequestMapping("/front/resetPwd")
	@ResponseBody
	public EhyMessage resetPwd(String phone,String yzm,HttpServletRequest request) throws ClientProtocolException, IOException{
		EhyMessage mes = new EhyMessage();
		HttpSession  sessionYzm =request.getSession();
		String yzmSession = (String) sessionYzm.getAttribute("forgetPwdYzm");
		if(yzmSession==null || !yzmSession.equalsIgnoreCase(yzm)) {
			mes.setState(0); 
			mes.setMes("您输入的验证码错误!");
			return mes ;
		}
		
		//得到随机8位的密码 
		String pwd = Tools.getRandomString(8);
		String mpwd = Tools.password(pwd); //加密之后的密码
		EhyMember mem = new EhyMember();
		mem.setMbLoginPwd(mpwd);
		mem.setMbPhone(phone);
		int row = memberService.updateMemberPwdByPhone(mem);
		
		if(row>0){
			
			String sun =PhoneMessageUtils.sms(phone,"您正在使用ehuoyuan网上商城找回密码功能!密码已经被重置为:"+pwd+",请使用手机号码登录!");
			 
			mes.setState(1); 
			mes.setMes("密码已经被重置为:"+pwd+",请使用手机号码登录,建议修改密码!");
			sessionYzm.removeAttribute("forgetPwdYzm");
			 
		}else{
			mes.setState(0);
			mes.setMes("密码重置失败,该手机号还没有注册!");
		}
		return mes;
	}
	
	/**
	 * 查询手机号码是否存在
	 * @param phone
	 * @return
	 */
	@RequestMapping("/front/selectPhoneRow")
	@ResponseBody
	public EhyMessage selectPhoneRow(String phone){
		EhyMessage mes = new EhyMessage();
		//logger.info("=============phone==========="+phone);
		int row=memberService.selectPhone(phone);
		if(row>0){
			mes.setState(1);
		}else{
			mes.setState(0);
		}
		return mes;
	}
	
	
	/**
	 * 前台登录
	 * @author WangGuanfu
	 * @param record
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/front/login")
	@ResponseBody
	public EhyMessage login(String mbPhones, String mbLoginPwd, HttpSession session){
		
			EhyMessage mes = new EhyMessage();
			String m = Tools.password(mbLoginPwd);
			EhyMember login= memberService.selectLogin(new EhyMember(mbPhones, mbPhones, m));
			
			if(login!=null){
				session.setAttribute("login", login);
				mes.setState(1);
				int cartNum=ehyCartService.findCartNum(login.getMbId());
				session.setAttribute("cartNum", cartNum);
			}else{
				mes.setState(0);
			}
			return mes;
		
	}
	
	
	
	/**
	 * 查询所有
	 * @param page 开始的页数
	 * @param limit 结束的页数
	 * @param member 查询的参数
	 * @return map 返回值
	 */
	@RequestMapping("/back/findAll")
	@ResponseBody  
	public Map<String,Object> findAll(int page,int limit,EhyMember member){
		Map<String,Object> map = new HashMap<String, Object>();
		/*Tools.isEmpty(memberLevel.getLevelName());*/
		if(Tools.isEmpty(member.getMbName())==true)member.setMbName(null);
		if(Tools.isEmpty(member.getMbPhone())==true)member.setMbPhone(null);
		if(Tools.isEmpty(member.getLevelId())==true)member.setLevelId(null);
		//if(Tools.isEmpty(member.getMbIsbzj()==true)member.setLevelId(null);
		 
		Pages pages=new Pages();
		pages.setCurPage(page);
		pages.setMaxResult(limit);
		Map<String,Object> mapParam = new HashMap<String, Object>();
		mapParam.put("firstRows", pages.getFirstRows());
		mapParam.put("maxResult", pages.getMaxResult());
		mapParam.put("mbName", member.getMbName());
		mapParam.put("mbPhone", member.getMbPhone());
		mapParam.put("levelId", member.getLevelId());
		mapParam.put("mbIsbzj", member.getMbIsbzj());
		List<Map> list  = memberService.findAll(mapParam);
		int totalRows = memberService.getTatolRows(member);


		map.put("count", totalRows);
		map.put("data", list);
		map.put("code",0);
		map.put("msg", "");
		return map;

	}
	
	/**
	 * 修改会员照片
	 */
	@RequestMapping("/back/updateMemberPhoto")
	@ResponseBody  //ajax注解
	public EhyMessage updateMemberPhoto(@RequestParam(value = "file", required = false) MultipartFile file, 
					HttpServletRequest request, ModelMap model,EhyMember member){
		/*logger.info("文件名："+file.getOriginalFilename());
	 	String path = request.getRealPath("upload/member/");
	 	logger.info("路径是："+path);*/
		String  photo =null;
	 	String fileName = file.getOriginalFilename();
	 	if(fileName!=null || "".equals(fileName)){
	 		String path="member";
	 		try {
				 photo =FileTools.saveFile(path, file);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
	 	}
       /* File targetFile = new File(path,fileName);
       
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        //保存
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        EhyManager ehyManager = (EhyManager) request.getSession().getAttribute("manager");
        member.setOper(ehyManager.getManUser());
        member.setMbRemark(photo);
        int rows= memberService.updateByPrimaryKeySelective(member);
        if(rows>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
        return mes;
	}
	
	
	/**
	 * 修改前台会员照片
	 */
	@RequestMapping("/front/updatePhoto")
	@ResponseBody
	public EhyMessage updatePhoto(@RequestParam(value = "file", required = false) MultipartFile file, 
					HttpServletRequest request, ModelMap model){
	
		String  photo =null;
	 	String fileName = file.getOriginalFilename();
	 	
	 	if(fileName!=null || "".equals(fileName)){
	 		String path="member";
	 		try {
				 photo =FileTools.saveFile(path, file);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
	 	}
      
        EhyMember member = (EhyMember) request.getSession().getAttribute("login");
       
        member.setMbId(member.getMbId());
        member.setMbRemark(photo);
        int rows= memberService.updateByPrimaryKeySelective(member);
       
        if(rows>0){
			mes.setMes(member.getMbRemark());
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
        return mes;
	}
	/**
	 * 根据id查对象
	 * @param levelId
	 * @return
	 */
	@RequestMapping("/back/findById")
	@ResponseBody  
	public EhyMember findById(String mbId){
		EhyMember member= memberService.selectByPrimaryKey(mbId);
		if(member!=null){
			return member;
		}else{
			return null;
		}
	}
	
	/**
	 * 根据id删除
	 * @param levelId
	 * @return
	 */
	@RequestMapping("/back/deleteRec")
	@ResponseBody  
	public EhyMessage deleteRec(String mbId,HttpServletRequest request){
		
		EhyManager ehyManager = (EhyManager) request.getSession().getAttribute("manager");
		
		int rows= memberService.deleteByPrimaryKey(mbId,ehyManager.getManUser());
		
		if(rows>0){
			int count = invitationCodeService.deleteRec(mbId);
			if(count>0){

				mes.setMes(EhyMessage.SUCCESS_MES);
				mes.setState(EhyMessage.SUCCESS);
			}else{
				mes.setMes(EhyMessage.ERROR_MES);
				mes.setState(EhyMessage.ERROR);
			}
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;

	}
	/**
	 * 查询分站
	 * @return
	 */
	@RequestMapping("/back/selectStation")
	@ResponseBody 
	public List<EhyStation> selectStation(){
		List<EhyStation> list= stationService.findStation();
		return list;
		
	}
	
	/**
	 * 修改的方法
	 * @param member
	 * @return
	 */
	@RequestMapping("/back/update")
	@ResponseBody 
	public EhyMessage update(EhyMember member,HttpServletRequest request){
		EhyManager ehyManager = (EhyManager) request.getSession().getAttribute("manager");
		if(ehyManager==null) {
			mes.setMes("操作超时,请您重新登录!");
			mes.setState(EhyMessage.ERROR);
			return mes ;
		}
		member.setOper(ehyManager.getManUser());
		if(!Tools.isEmpty(member.getMbLoginPwd())) {
			String memberPwd = Tools.password(member.getMbLoginPwd());
			member.setMbLoginPwd(memberPwd);
		}
		if(!Tools.isEmpty(member.getMbPayPwd())) {
			String memberzhifuPwd = Tools.password(member.getMbPayPwd()); 
			member.setMbPayPwd(memberzhifuPwd);
		}
		
		int rows = memberService.updateByPrimaryKeySelective(member);
		if(rows>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
		
	}
	
	/**
	 * 生成邀请码
	 * @param mbId 参数
	 * @return
	 */
	@RequestMapping("/back/addInvitationCode")
	@ResponseBody
	public EhyMessage addInvitationCode(EhyMember member,HttpServletRequest request,EhyInvitationCode ehyInvitationCode){
		
		EhyManager ehyManager = (EhyManager) request.getSession().getAttribute("manager");
		//会员id
		ehyInvitationCode.setMbId(member.getMbId());
		//会员姓名
		ehyInvitationCode.setMbName(member.getMbName());
		//操作人
		ehyInvitationCode.setOper(ehyManager.getManUser());
		//站点id
		ehyInvitationCode.setStId(ehyManager.getStId());
		//根据id查询
		EhyStation ehyStation = stationService.selectByPrimaryKey(ehyManager.getStId());
		//邀请码
		String invitationCode =ehyStation.getStCode()+""+getRandomNumber(5);
		//取邀请码
		ehyInvitationCode.setCode(invitationCode);
		member.setInvitationCode(invitationCode);
		//赋值来源分站
		member.setMbStation(ehyStation.getStName());
		
		EhyMember ehyMember=  memberService.selectByPrimaryKey(member.getMbId());
		if(Tools.isEmpty(ehyMember.getInvitationCode())==false){
			mes.setMes("该会员已有邀请码");
			mes.setState(3);
		}else{
			//增加邀请码 到邀请码表
			int rows = invitationCodeService.insertSelective(ehyInvitationCode);
			//增加到会员列表
			int count = memberService.updateByPrimaryKeySelective(member);
			if(rows>0 || count>0){
				mes.setMes("邀请码生成：-"+invitationCode);
				mes.setState(EhyMessage.SUCCESS);
			}else{
				mes.setMes("请登录，在完成此次操作");
				mes.setState(EhyMessage.ERROR);
			}
			
		}
		return mes;
		
	}
	
	/**
	 * 根据Id查询会员信息
	 * @param mbId
	 * @param request
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/front/findMemberById")
	public List<EhyMember> findMemberById(String mbId,HttpServletRequest request, HttpSession session){
		
			HttpSession sessionMember = request.getSession();
			
			
			EhyMember	member= (EhyMember) sessionMember.getAttribute("login");
			
			
			List<EhyMember> list = memberService.findMenberBy(member.getMbId());
			//logger.info(list.size()+"==============="+member.getMbId()+"==============."+member.getLevelId());
			return list ;
	}
	
	/**
	 * 修改会员信息
	 * @param member
	 * @return
	 */
	@RequestMapping("/front/updateMember")
	@ResponseBody
	public EhyMessage updateMember(EhyMember member ){
		int row = memberService.updateByPrimaryKey(member);
		if(row>0){
			mes.setState(1);
		}else{
			mes.setState(0);
		}
		return mes;
	}
	
	
	@RequestMapping("/front/loginPwdYzm")
	@ResponseBody
	public EhyMessage loginPwdYzm(HttpServletRequest request ,EhyMember record){
		HttpSession session1 = request.getSession();
		String se  =  (String) session1.getAttribute("yzm");
		String yzm =request.getParameter("mbPhoneYzm");
		String mbPhone = request.getParameter("mbPhone");
		if(se!=null && se.equals(yzm)){
			mes.setState(1);
		}else{
			mes.setState(0);
		}
		return mes;
		
	}
	/**
	 * 修改手机号码
	 * @param member
	 * @return
	 */
	@RequestMapping("/front/updatePhone")
	@ResponseBody
	public EhyMessage updatePhone(EhyMember member,HttpServletRequest request){
		String mbPhone = request.getParameter("mbPhone");
		String mbPhones = request.getParameter("mbPhones");
		String mbLoginPwd = request.getParameter("mbLoginPwd");
		String mbLoginPwds = request.getParameter("mbLoginPwds");
		String mbLoginPayPwd = request.getParameter("mbLoginPayPwd");
		String mbLoginPayPwds = request.getParameter("mbLoginPayPwds");
		
		if(Tools.isEmpty(mbPhones)){
			EhyMember session = (EhyMember) request.getSession().getAttribute("login");
			member.setMbPhone(session.getMbPhone());
			member.setMbPhones(session.getMbPhone());
		}else{
			member.setMbPhones(mbPhones);
		}
		
		if(Tools.isEmpty(mbLoginPwd)){
			EhyMember session = (EhyMember) request.getSession().getAttribute("login");
			member.setMbLoginPwd(session.getMbLoginPwd());
			member.setMbLoginPwds(session.getMbLoginPwd());
		}else{
			String mbLoginPwd1 = Tools.password(mbLoginPwds);
			member.setMbLoginPwds(mbLoginPwd1);
		}
		
		if(Tools.isEmpty(mbLoginPayPwds)){
			member.setMbPayPwd(mbLoginPayPwds);
			member.setMbPayPwds(mbLoginPayPwds);
		}else{
			String mbLoginPwd1 = Tools.password(mbLoginPayPwds);
			member.setMbPayPwd(mbLoginPwd1);
		}
		
		int row = memberService.updateMemberKey(member);
		
		
		if(row>0){
			mes.setState(1);
		}else{
			mes.setState(0);
		}
		return mes;
	}
	
	/**
	 * 查询登录名是否存在
	 * @return
	 */
	@RequestMapping("back/isMbLogin")
	@ResponseBody 
	public EhyMessage islevelName(String mbLogin,String mbId){
		//增加会员等级查询 名字是否存在
		int rows = memberService.isMbLogin(mbLogin,mbId);
		//当返回的值大于0
		if(rows>0){
			//返回去页面的消息类赋值
			mes.setState(EhyMessage.SUCCESS);
		}else{
			//返回去页面的消息类赋值
			mes.setState(EhyMessage.ERROR);
		}
		//返回的值
		return mes;
		
	}
	
	/**
	 * 查询地区
	 * @param area
	 * @return
	 */
	@ResponseBody
	@RequestMapping("front/findArea")
	public String findArea(EhyArea area){
		List<EhyArea> list = ehyAreaService.selectArea(area);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(jsonString);
		return jsonString;
	}
	

	/**
	 * 根据地区ID查询地区
	 * @param areaId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("front/findAreaById")
	public String findAreaById(String areaId){
		
		List<EhyArea> list = ehyAreaService.selectAreaById(areaId);
		for (EhyArea ehyArea : list) {
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		//logger.info(jsonString);
		return jsonString;
	}
	
	/**
	 * 根据时间查询注册人数
	 * @param optime
	 * @return
	 */
	@RequestMapping("/front/selectNumByTime")
	@ResponseBody
	public int selectNumByTime(HttpServletRequest request){
		String time = Tools.getCurDate();
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
		//logger.info("===================time================"+time);
		Map<String,Object> map = new HashMap<>();
		map.put("optime", time);
		map.put("invitationCode", ehyMember.getInvitationCode());
		int member=	memberService.selectNumByTime(map);
		
		return member;
	}
	/**
	 *查这个月注册人数
	 * @return
	 */
	@RequestMapping("/front/selectNumByMon")
	@ResponseBody
	public int selectNumByMon(HttpServletRequest request){
		
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		Date date = new Date();
		Map<String,Object> map = new HashMap<>();
		map.put("optime", sdf.format(new Date()));
		map.put("invitationCode", ehyMember.getInvitationCode());
		int member1 =	memberService.selectNumByTime(map);
		return member1;
	}
	
	/**
	 * 查上个月注册人数
	 * @return
	 */
	@RequestMapping("/front/selectNumByMon1")
	@ResponseBody
	public int selectNumByMon1(HttpServletRequest request){
		Calendar calendar = Calendar.getInstance();//日历对象  
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
       int year= calendar.get(Calendar.YEAR);//获取年份
       int mon = calendar.get(Calendar.MONTH);//获取上个月
       String date  =year+"-"+(mon>10?""+mon:"0"+mon);
        Map<String,Object> map = new HashMap<>();
		map.put("optime",date);
		map.put("invitationCode", ehyMember.getInvitationCode());
		int member2 =	memberService.selectNumByTime(map);
		return member2;
	}
	
	
	/**
	 * 根据时间跟是否付款查询订单
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/front/selectNumByOrder")
	public int selectNumByOrder(HttpServletRequest request){
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
		String time = Tools.getCurDate();//使用工具类
		Map<String,Object> map = new HashMap<>();
		map.put("optime", time);
		map.put("invitationCode", ehyMember.getInvitationCode());
		int order =	memberService.selectNumByOrder(map);
		return order;
	}
	
	
	/**
	 * 根据本月时间跟是否付款查询订单
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/front/selectNumByOrder1")
	public int selectNumByOrder1(HttpServletRequest request){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
		Date date = new Date();
		Map<String,Object> map = new HashMap<>();
		map.put("optime", sdf.format(new Date()));
		map.put("invitationCode", ehyMember.getInvitationCode());
		int order1 =	memberService.selectNumByOrder(map);
		return order1;
	}
	
	/**
	 * 根据上个月时间跟是否付款查询订单
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/front/selectNumByOrder2")
	public int selectNumByOrder2(HttpServletRequest request){
		Calendar calendar = Calendar.getInstance();//日历对象  
		EhyMember  ehyMember = (EhyMember) request.getSession().getAttribute("login");
	       int year= calendar.get(Calendar.YEAR);//获取年份
	       int mon = calendar.get(Calendar.MONTH);//获取上个月
	       String date  =year+"-"+(mon>10?""+mon:"0"+mon);
	        Map<String,Object> map = new HashMap<>();
			map.put("optime",date);
			map.put("invitationCode", ehyMember.getInvitationCode());
			int order2 =	memberService.selectNumByOrder(map);
			return order2;
	}
	
	
	/**
	 * 查询会员分页
	 * @param member
	 * @return
	 */
	@RequestMapping("/front/selectMemberPage")
	@ResponseBody
	public Map<String,Object> selectMemberPage(EhyMember member,  String curPage,HttpServletRequest request){
		Pages pages = new Pages();
		String time = Tools.getCurDate();
		Map<String,Object> map=new HashMap<>();
		Map<String,Object> map1=new HashMap<>();
		pages.setMaxResult(5);//复职当前行数
		EhyMember  ehyMember1 = (EhyMember) request.getSession().getAttribute("login");
		int row = memberService.selectNumPage();
		pages.setTotalRows(row);//总行数
		
		if(curPage==null){
			pages.setCurPage(1);//复职当前页数
		}else{
			pages.setCurPage(Integer.valueOf(curPage));//复职当前页数
		}
		map1.put("page", pages.getFirstRows());//复职到map
		map1.put("limit", pages.getMaxResult());//复职到map
		map1.put("invitationCode", ehyMember1.getInvitationCode());
		map1.put("optime", time);
		List<EhyMember> ehyMember = memberService.selectMemberPage(map1);
		//logger.info("+=============================="+ehyMember);
		map.put("list", ehyMember);
		map.put("pages", pages);
		return map;
	}
	/**
	 * 根据会员ID查询订单分页
	 * @param member
	 * @return
	 */
	@RequestMapping("/front/selectOrder")
	@ResponseBody
	public Map<String, Object> selectOrder(EhyOrderItem orderItem , String curPage,HttpServletRequest request){
		EhyMember session = (EhyMember) request.getSession().getAttribute("login");
		String time = Tools.getCurDate();
		Pages pages = new Pages();
		Map<String,Object> map=new HashMap<>();
		Map<String,Object> map1=new HashMap<>();
		pages.setMaxResult(5);//复职当前行数     
		int row = memberService.selectNumPage();
		pages.setTotalRows(row);//总行数
		
		if(curPage==null){
			pages.setCurPage(1);//复职当前页数
		}else{
			pages.setCurPage(Integer.valueOf(curPage));//复职当前页数
		}
		map1.put("page", pages.getFirstRows());//复职到map
		map1.put("limit", pages.getMaxResult());//复职到maps
		map1.put("invitationCode", session.getInvitationCode());
		map1.put("optime", time);
		List<EhyOrderItem> orderItem1 = memberService.selectOrder(map1);
		//logger.info(orderItem1);
		map.put("list", orderItem1);
		map.put("pages", pages);
		return map;
	}
	
	/**
	 * 更具邀请码 查询 用户名
	 * @param invitationCode 邀请码
	 * @return 用户名
	 */
	@RequestMapping("/back/findInvitationCode")
	@ResponseBody
	public EhyMember findInvitationCode(String invitationCode){
		EhyMember  ehyMember = new  EhyMember();
		String userName = memberService.findInvitationCode(invitationCode);
		ehyMember.setMbName(userName);
		return ehyMember;
		
	}
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMdhmsS");
	/**
	 * 得到根据时间生成的数字
	 * @param length 最多不能15位
	 */
	public  String getRandomNumber(int length) {
		Date date = new Date(); 
		String  formDate =sdf.format(date);
       // System.out.println(formDate);
        String no = formDate.substring(formDate.length()-length, formDate.length());
        logger.info(no);
        return no ;
	}
	
}
