/**
 * 
 */
package cn.ehuoyuan.shop.action.shelves;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyShelves;
import cn.ehuoyuan.shop.service.shelves.EhyShelvesService;

/**
 * 类的描述：货架的Action
 * @author 罗海兵
 * @dateTime 2018年2月22日 下午3:56:41
 * @version 1.0
 */
@Controller
@RequestMapping("shelves")
public class ShelvesAction {
	
	@Resource
	private EhyShelvesService ehyShelvesService;
	
	/**
	 * 加入货架
	 * @param shelves 货架的对象模型
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:43:51
	 * @versions 1.0
	 */
	@RequestMapping("front/add")
	@ResponseBody
	public EhyMessage add(EhyShelves shelves, HttpSession session){
		EhyMessage mes = new EhyMessage();
		if(Tools.isEmpty(shelves.getProId())) {
			mes.setState(0);
			mes.setMes("加入货架错误!");
			return mes;
		}
		EhyMember mem=(EhyMember) session.getAttribute("login");
		if(mem==null) {
			mes.setState(0);
			mes.setMes("请重新登录再操作!");
			return mes;
		}
		
		int mbLevel=Integer.valueOf(mem.getLevelId());
		shelves.setAddTime(Tools.getCurDateTime());
		shelves.setMbId(mem.getMbId());
		shelves.setOper(mem.getMbName());
		shelves.setMbStation(mem.getMbStation());
		//判断是否已经加入购物车
		if(ehyShelvesService.findByMbId(mem.getMbId(), shelves.getProId()).size()>0) {
			mes.setState(0);
			mes.setMes("您的货架中已经存在该商品,不能重复添加!");
			return mes;
		}
		//白银,黄金会员加入货架
		if(mbLevel>=2){
			//logger.info("黄金、钻石会员加入货架");
			mes.setState(ehyShelvesService.add(shelves));
			mes.setMes("加入货架成功!");
			return mes;
			 
		}
		 
		//logger.info("普通会员加入货架");
		
		mes.setState(0);
		mes.setMes("对不起,您没有权限使用加入货架功能!");
		return mes;
	}
	
	/**
	 * 删除货架上的某个产品
	 * @param sheId 货架id
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:45:19
	 * @versions 1.0
	 */
	@RequestMapping("delete")
	@ResponseBody
	public int delete(String sheId){
		return ehyShelvesService.delete(sheId);
	}
	
	/**
	 * 修改货架上的产品信息
	 * @param shelves 货架的对象模型
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:46:13
	 * @versions 1.0
	 */
	@RequestMapping("update")
	@ResponseBody
	public int update(EhyShelves shelves){
		return ehyShelvesService.update(shelves);
	}
	
	/**
	 * 根据货架ID查询货架
	 * @param sheId 货架ID
	 * @return 返回一个货架的对象模型
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:47:06
	 * @versions 1.0
	 */
	@RequestMapping("findById")
	@ResponseBody
	public EhyShelves findById(String sheId){
		return ehyShelvesService.findById(sheId);
	}
	
	/**
	 * 根据会员id查询货架
	 * @return 返回一个货架的集合
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:48:20
	 * @versions 1.0
	 */
	@RequestMapping("findByMbId")
	@ResponseBody
	public List<EhyShelves> findByMbId(String mbId,HttpSession session){
		if(mbId==null) { //如果为空,就取session中的会员ID
			EhyMember  ehyMember = (EhyMember) session.getAttribute("login");
			mbId = ehyMember.getMbId();
		}
		return ehyShelvesService.findByMbId(mbId);
	}
		
}
