/**
 * 
 */
package cn.ehuoyuan.shop.action.shelves.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 货架的vo
 * @author liandyao
 * @date 2018年6月9日
 * @version 1.0
 */
public class ShelvesVo implements java.io.Serializable{
	
	//货架ID
    private String sheId;
    //会员主键
    private String mbId;
    //产品ID
    private String proId;
    //加入时间
    private String addTime;
    //是否有效
    private Integer isva;
    //操作时间
    private Date optime;
    //操作人
    private String oper;
    //排序
    private Integer sort;
     
    //产品名称
    private String proName;
    //产品显示价格(元)
    private BigDecimal proPrice;
    //普通会员价格
    private BigDecimal proPrice0;
    //银牌会员价格
    private BigDecimal proPrice1;
    //金牌会员价格
    private BigDecimal proPrice2;
    
    
    private String imgPath ;
    private String stId ;
    
    
	public String getStId() {
		return stId;
	}
	public void setStId(String stId) {
		this.stId = stId;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public String getSheId() {
		return sheId;
	}
	public void setSheId(String sheId) {
		this.sheId = sheId;
	}
	public String getMbId() {
		return mbId;
	}
	public void setMbId(String mbId) {
		this.mbId = mbId;
	}
	public String getProId() {
		return proId;
	}
	public void setProId(String proId) {
		this.proId = proId;
	}
	public String getAddTime() {
		return addTime;
	}
	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}
	public Integer getIsva() {
		return isva;
	}
	public void setIsva(Integer isva) {
		this.isva = isva;
	}
	public Date getOptime() {
		return optime;
	}
	public void setOptime(Date optime) {
		this.optime = optime;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public BigDecimal getProPrice() {
		return proPrice;
	}
	public void setProPrice(BigDecimal proPrice) {
		this.proPrice = proPrice;
	}
	public BigDecimal getProPrice0() {
		return proPrice0;
	}
	public void setProPrice0(BigDecimal proPrice0) {
		this.proPrice0 = proPrice0;
	}
	public BigDecimal getProPrice1() {
		return proPrice1;
	}
	public void setProPrice1(BigDecimal proPrice1) {
		this.proPrice1 = proPrice1;
	}
	public BigDecimal getProPrice2() {
		return proPrice2;
	}
	public void setProPrice2(BigDecimal proPrice2) {
		this.proPrice2 = proPrice2;
	}
    
    

}
