/**
 * 
 */
package cn.ehuoyuan.shop.action.pay;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;

import cn.ehuoyuan.common.AlipayConfig;
import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.frontShow.OrderPay;
import cn.ehuoyuan.shop.service.pay.PayService;

/** 
 * 支付宝支付action
 * @author liandyao
 * @date 2017年11月28日
 * @version 1.0
 */
@Controller
@RequestMapping("/alipayAction")
public class AlipayAction {
	
	Logger logger = Logger.getLogger(AlipayAction.class);

	//获得初始化的AlipayClient
	public static AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

			
	@Resource
	PayService payService ;

	/**
	 * 支付请求地址
	 * @param request
	 * @param response
	 * @throws AlipayApiException
	 * @throws IOException
	 */
	@RequestMapping("pay")
	public void pay(HttpServletRequest request,HttpServletResponse response) throws AlipayApiException, IOException {
		response.setContentType("text/html;charset=utf-8");
		//得到订单ID
		String orderId = request.getParameter("orderId");
		OrderPay order = payService.getOrderPay(orderId);
		if(order==null){
			response.getWriter().write("订单不存在或者已经支付完成,操作失败!");//直接将完整的表单html输出到页面
			response.getWriter().flush();
			return ;
		}
		
		//设置请求参数
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
		alipayRequest.setReturnUrl(AlipayConfig.return_url);
		alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

		//商户订单号，商户网站订单系统中唯一订单号，必填
		//String out_trade_no = new String(request.getParameter("WIDout_trade_no").getBytes("ISO-8859-1"),"UTF-8");
		String out_trade_no = order.getOrdId();
		//付款金额，必填
		//String total_amount = new String(request.getParameter("WIDtotal_amount").getBytes("ISO-8859-1"),"UTF-8");
		String total_amount = order.getPayMoney();
		//订单名称，必填
		//String subject = new String(request.getParameter("WIDsubject").getBytes("ISO-8859-1"),"UTF-8");
		String subject = CommomUtils.PAY_ORDER_NAME;
		//商品描述，可空
		//String body = new String(request.getParameter("WIDbody").getBytes("ISO-8859-1"),"UTF-8");
		String body = "" ;

		alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\"," 
				+ "\"total_amount\":\""+ total_amount +"\"," 
				+ "\"subject\":\""+ subject +"\"," 
				+ "\"body\":\""+ body +"\"," 
				+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

		//若想给BizContent增加其他可选请求参数，以增加自定义超时时间参数timeout_express来举例说明
		//alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\"," 
		//		+ "\"total_amount\":\""+ total_amount +"\"," 
		//		+ "\"subject\":\""+ subject +"\"," 
		//		+ "\"body\":\""+ body +"\"," 
		//		+ "\"timeout_express\":\"10m\"," 
		//		+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
		//请求参数可查阅【电脑网站支付的API文档-alipay.trade.page.pay-请求参数】章节

		//请求
		String result = alipayClient.pageExecute(alipayRequest).getBody();


		response.getWriter().write(result);//直接将完整的表单html输出到页面
		response.getWriter().flush();

	}

	/**
	 * http://li.hk1.mofasuidao.cn/alipayAction/result.action?total_amount=0.01&timestamp=2017-11-29+19%3A04%3A09&sign=YkmGZ2zcvhxvInQ%2FtXtRBx%2Fsva1YpsK%2BEb37YciLrBsFq8ar9RFjSwYqgyKIr0142jtxVKKIlMHQ%2BkKQiYXwUiviWgQdDJN4N9fTw8XW1jvazGS%2Bs8AStazWds1fgXJZ4%2B8iJs8q%2BqRrBRz9hxJNA3NHZmxnu4%2F%2Bl%2FHRuxkmm1zfiwhkKdg7ILqYurgABVfE4vDJ4OvR6flYQY942V3Hey%2FLkb6qK9cVqyw9jlYyBSeAiEmygfW4446yHVf8CIbHmYindBZk9JCZ7vU3KrZ6CSdcrLXVC9x%2BhjhPrGPLZhMCD7MLwQ8zFU%2FZjT9SF72io0u%2F36ZL5SztMRrf9xwTkg%3D%3D&trade_no=2017112921001004460533471496&sign_type=RSA2&auth_app_id=2017111900042803&charset=utf-8&seller_id=2088121881775253&method=alipay.trade.page.pay.return&app_id=2017111900042803&out_trade_no=20171129190328p9ygtffk3w&version=1.0
	 * 支付之后跳转的页面,这里需要改变支付状态
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws AlipayApiException
	 */
	@RequestMapping("result")
	public ModelAndView result(HttpServletRequest request) throws UnsupportedEncodingException, AlipayApiException {
		ModelAndView mv = new ModelAndView("front/pay/result");

		//获取支付宝GET过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map<String,String[]> requestParams = request.getParameterMap();
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用
			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}

		boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

		//——请在这里编写您的程序（以下代码仅作参考）——
		if(signVerified) {
			mv.addObject("state", "1"); //成功
			//商户订单号
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
			mv.addObject("out_trade_no", out_trade_no);
			//支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
			mv.addObject("trade_no", trade_no);
			//付款金额
			String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"),"UTF-8");
			
			mv.addObject("total_amount", total_amount);
			
			//logger.info("=======支付宝支付,回调,将支付金额*100变成分"+total_amount);
			total_amount = Tools.moneyYuanToFen(total_amount) ;
			//logger.info("=======支付宝支付,回调,将支付金额*100变成分之后:"+total_amount);
			
			//out.println("trade_no:"+trade_no+"<br/>out_trade_no:"+out_trade_no+"<br/>total_amount:"+total_amount);

			
			int rows = payService.saveOrder(out_trade_no, trade_no, total_amount,CommomUtils.PAY_TYPE_ALIPAY);
			if(rows<=0){
				mv.addObject("state", "0"); //失败
				mv.addObject("mes","修改商家订单状态失败!");
			}else{
				mv.addObject("mes","支付成功!");
			}
		}else {
			//out.println("验签失败");
			mv.addObject("state", "0"); //失败
			mv.addObject("mes","验签失败!");
		}

		return mv ;
	}

	

	/**
	 * 通知页面
	 * @param request
	 * @return
	 * @throws IOException 
	 * @throws UnsupportedEncodingException
	 * @throws AlipayApiException
	 */
	@RequestMapping("notify")
	public void notify(HttpServletRequest request,HttpServletResponse response) throws IOException, AlipayApiException{
		/* *
		 * 功能：支付宝服务器异步通知页面
		 * 日期：2017-03-30
		 * 说明：
		 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
		 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。


		 *************************页面功能说明*************************
		 * 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
		 * 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
		 * 如果没有收到该页面返回的 success 
		 * 建议该页面只做支付成功的业务逻辑处理，退款的处理请以调用退款查询接口的结果为准。
		 */
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map<String,String[]> requestParams = request.getParameterMap();
		for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用
			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}
		logger.info("进来了-----------延时通知-----------------");
		boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

		//——请在这里编写您的程序（以下代码仅作参考）——

		/* 实际验证过程建议商户务必添加以下校验：
			1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
			2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
			3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
			4、验证app_id是否为该商户本身。
		 */
		if(signVerified) {//验证成功
			//商户订单号
			String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//支付宝交易号
			String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

			//交易状态
			String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

			//付款金额,回调的时候,支付宝回调的金额是元
			String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"),"UTF-8");
			logger.info("notify回调=======支付宝支付,回调,将支付金额*100变成分"+total_amount);
			total_amount = Tools.moneyYuanToFen(total_amount) ;
			logger.info("notify回调=======支付宝支付,回调,将支付金额*100变成分之后:"+total_amount);
			
			logger.info("延时发生交易状态: "+trade_status+"   商户交易订单号:"+out_trade_no+"          发送的金额是:"+total_amount);
			if(trade_status.equals("TRADE_FINISHED")){
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				int rows = payService.saveOrder(out_trade_no, trade_no, total_amount,CommomUtils.PAY_TYPE_ALIPAY);
				if(rows<=0){
					out.println("fail");
					return ;
				}
				//注意：
				//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
			}else if (trade_status.equals("TRADE_SUCCESS")){
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序

				//注意：
				//付款完成后，支付宝系统发送该交易状态通知
			}

			out.println("success");

		}else {//验证失败
			out.println("fail");

			//调试用，写文本函数记录程序运行情况是否正常
			//String sWord = AlipaySignature.getSignCheckContentV1(params);
			//AlipayConfig.logResult(sWord);
		}

		//——请在这里编写您的程序（以上代码仅作参考）——
	}
	
	 
	
	
	
	
	public static void main(String[] args) {
		
	}
}
