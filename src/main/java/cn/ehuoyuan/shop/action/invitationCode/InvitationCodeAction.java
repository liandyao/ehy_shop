/** 
 * 
 */
package cn.ehuoyuan.shop.action.invitationCode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyInvitationCode;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyOrderItem;
import cn.ehuoyuan.shop.domain.EhyStation;
import cn.ehuoyuan.shop.service.invitationCode.InvitationCodeService;
import cn.ehuoyuan.shop.service.member.MemberService;
import cn.ehuoyuan.shop.service.station.StationService;

/**
 * 站点代理人管理的action
 * @author denglijie
 * @2017年10月11日
 * @version v1.0
 */
@Controller
@RequestMapping("invitationCode")
public class InvitationCodeAction {
	     
	/**
	 * 注入代理人的service
	 */
	@Resource
	private InvitationCodeService invitationCodeService;
	
	/**
	 * 注入站点的service
	 */
	@Resource
	private StationService stationService;
	
	/**
	 * 注入会员的service
	 */
	@Resource
	private MemberService memberService;
	/**
	 * 显示列表
	 * @param page 
	 * @param limit
	 * @return map
	 */
	@RequestMapping("/back/showList")
	@ResponseBody
	public Map<String, Object> showList(int page,int limit,EhyInvitationCode code,EhyStation sta ){
		
		Calendar ca = Calendar.getInstance();//得到一个Calendar的实例 
		ca.setTime(new Date());//月份是从0开始的，所以11表示12月 
		Date now = ca.getTime(); 
		ca.add(Calendar.MONTH, -1); //月份减1 
		Date lastMonth = ca.getTime(); //结果 
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM"); 
		String thisMonth = sf.format(now);
		String oneMonth = sf.format(lastMonth);
		
		
		Map<String , Object> map = new HashMap<>();
		
		Pages p = new Pages();
		p.setCurPage(page);
		p.setMaxResult(limit);
		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		
		mapParam.put("thisMonth", thisMonth);
		mapParam.put("oneMonth", oneMonth);
		
		mapParam.put("stName", sta.getStName());
		mapParam.put("code", code.getCode());
		mapParam.put("mbName", code.getMbName());
		
		
		mapParam.put("firstRows", p.getFirstRows());
		mapParam.put("maxResult", p.getMaxResult());
		
		List<Map> list = invitationCodeService.findAll(mapParam);
		
		int count = invitationCodeService.findRowCount(mapParam);
		map.put("data", list);
		map.put("count", count);
	
		map.put("code",0);
		map.put("msg", "");
		return map;
	}
	
	/**
	 * 显示所有站点
	 * @return
	 */
	@RequestMapping("/back/findStation")
	@ResponseBody
	public Map<String, Object> findStation(){
		Map<String, Object> map = new HashMap<String, Object>();
		List<EhyStation> list = stationService.findStation();
		map.put("list",list);
		return map;
	}
	
	/**
	 * 显示所有会员
	 * @return
	 */
	@RequestMapping("/back/findMember")
	@ResponseBody
	public Map<String, Object> findMember(){
		Map<String, Object> map = new HashMap<String, Object>();
		List<EhyMember> list = memberService.findMember();
		map.put("list",list);
		return map;
	}
	
	/**
	 * 删除
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/delete")
	@ResponseBody
	public EhyMessage delete(EhyInvitationCode code){
		EhyMember mem = new EhyMember();
		mem.setMbId(code.getMbId());
		mem.setLevelId("1");
		mem.setMbStation("");
		mem.setInvitationCode("");
		memberService.updateByPrimaryKeySelective(mem); 
		
		code.setIsva(CommomUtils.ISVA_NO);
		int rows = invitationCodeService.updateByPrimaryKeySelective(code);
		EhyMessage message = new EhyMessage();
		if(rows>0){
			message.setState(EhyMessage.SUCCESS);
			message.setMes(EhyMessage.SUCCESS_MES);
		}else{
			message.setState(EhyMessage.ERROR);
			message.setMes(EhyMessage.ERROR_MES);
		}
		return message;
	}
	
	/**
	 * 增加或修改
	 * @param invitationCode
	 * @return
	 */
	@RequestMapping("/back/addOrUpdate")
	@ResponseBody
	public EhyMessage addOrUpdate(EhyInvitationCode invitationCode,HttpSession session){
		
		EhyMessage message = new EhyMessage();
		EhyMember member = memberService.selectByPrimaryKey(invitationCode.getMbId());
		invitationCode.setMbName(member.getMbName()); 
		
		if(Tools.isEmpty(invitationCode.getInviId())){
			//如果页面传过来的是小写字母则自动转化为大写
			invitationCode.setCode(invitationCode.getCode().toUpperCase());
			
			String code = invitationCode.getCode()+""+Tools.getSysJournalNo(6, true);
			
			EhyMember mem = new EhyMember();
			mem.setMbId(invitationCode.getMbId()); 
			if(invitationCode.getIsva()==1){
				mem.setLevelId("1");
			}else{
				mem.setLevelId("4");
			}
			
			EhyStation sta = stationService.findById(invitationCode.getStId());
			mem.setMbStation(sta.getStName());
			
			mem.setInvitationCode(code);
			memberService.updateByPrimaryKeySelective(mem);
			
			while(true){
				int row = invitationCodeService.selectByCode(code);
				if(row>0){
					code = invitationCode.getCode()+Tools.getSysJournalNo(6, true);
				}else{
					invitationCode.setCode(code);
					break;
				}
			}
			
			int rows = invitationCodeService.insertSelective(invitationCode);
			if(rows>0){
				message.setState(EhyMessage.SUCCESS);
				message.setMes(EhyMessage.SUCCESS_MES);
			}else{
				message.setState(EhyMessage.ERROR);
				message.setMes(EhyMessage.ERROR_MES);
			}
		}
		return message;
	}
	
	/**
	 * 显示修改信息
	 * @param stId
	 * @return
	 */
	@RequestMapping("/back/showUpdate")
	@ResponseBody
	public EhyInvitationCode showUpdate(String inviId){
		EhyInvitationCode invitationCode =  invitationCodeService.selectByPrimaryKey(inviId);
		return invitationCode;
	}
	
	/**
	 * 根据会员id判断邀请码是否存在
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/isCode")
	@ResponseBody
	public EhyMessage isCode(String mbId){
		EhyMessage message = new EhyMessage();
		int rows = memberService.isCode(mbId); 
		if(rows>0){
			message.setState(EhyMessage.SUCCESS);
		}else{
			message.setState(EhyMessage.ERROR);
		}
		return message;
	}
	
	/**
	 * 设为代理人
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/setAgent")
	@ResponseBody
	public EhyMessage setAgent(EhyInvitationCode code){
		EhyMember mem = new EhyMember();
		mem.setMbId(code.getMbId());
		mem.setLevelId("4");
		memberService.updateByPrimaryKeySelective(mem);
		 
		code.setIsva(2);
		int rows = invitationCodeService.updateByPrimaryKeySelective(code);
		EhyMessage message = new EhyMessage();
		if(rows>0){
			message.setState(EhyMessage.SUCCESS);
			message.setMes(EhyMessage.SUCCESS_MES);
		}else{
			message.setState(EhyMessage.ERROR);
			message.setMes(EhyMessage.ERROR_MES);
		}
		return message;
	}
	
	/**
	 * 取消代理人
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/cancelAgent")
	@ResponseBody
	public EhyMessage cancelAgent(EhyInvitationCode code){
		EhyMember mem = new EhyMember();
		mem.setMbId(code.getMbId());
		mem.setLevelId("1");
		memberService.updateByPrimaryKeySelective(mem);
		code.setIsva(1);
		
		int rows = invitationCodeService.updateByPrimaryKeySelective(code);
		EhyMessage message = new EhyMessage();
		if(rows>0){
			message.setState(EhyMessage.SUCCESS);
			message.setMes(EhyMessage.SUCCESS_MES);
		}else{
			message.setState(EhyMessage.ERROR);
			message.setMes(EhyMessage.ERROR_MES);
		}
		return message;
	}
	
	/**
	 * 排序
	 * @param start
	 * @param end
	 * @param inviId
	 * @return
	 */
	@RequestMapping("/back/sortInvitationCode")
	@ResponseBody
	public EhyMessage sortInvitationCode(int start, int end, String inviId) {
		EhyMessage mes = new EhyMessage();
		int rows = invitationCodeService.sortInvitationCode(start, end, inviId);
		if(rows>0){
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(EhyMessage.SUCCESS_MES);
		}else{
			mes.setState(EhyMessage.ERROR);
			mes.setMes(EhyMessage.ERROR_MES);
		}
		return mes;
	}
	
	/**
	 * 查询本月订单数的统计
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/thisOrderMap")
	@ResponseBody
	private Map<String, Object> thisOrderMap(String code){
		Calendar cla = Calendar.getInstance();
		Date date = cla.getTime(); 
		String thisMonth = Tools.getDateStr(date).substring(0, 7);
		
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("thisMonth", thisMonth);
		mapParam.put("code", code); 
		List<EhyOrderItem> list = invitationCodeService.thisOrderMap(mapParam);
		
		String year = Tools.getDateStr(date).split("-")[0];
		String month = Tools.getDateStr(date).split("-")[1];
		
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("year", year);
		map.put("month", month);
		return map; 
	}
	
	/**
	 * 查询上月订单数的统计
	 * @param code
	 * @return
	 */
	@RequestMapping("/back/oneOrderMap")
	@ResponseBody
	public Map<String, Object> oneOrderMap(String code){
		Calendar cla = Calendar.getInstance();
		cla.add(Calendar.MONTH, -1);
		Date date = cla.getTime(); 
		String oneMonth = Tools.getDateStr(date).substring(0, 7); 
		
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("oneMonth", oneMonth);
		mapParam.put("code", code); 
		List<EhyOrderItem> list = invitationCodeService.oneOrderMap(mapParam); 
		
		String year = Tools.getDateStr(date).split("-")[0];
		String month = Tools.getDateStr(date).split("-")[1];
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("year", year);
		map.put("month", month);
		return map; 
	}
	
	/**
	 * 查询每月的订单数的统计
	 * @param code
	 * @param year
	 * @return
	 */
	@RequestMapping("/back/selectOrderMap")
	@ResponseBody
	public Map<String, Object> selectOrderMap(String code,String year){
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("code", code);
		mapParam.put("year", year);
		List<EhyOrderItem> list = invitationCodeService.selectOrderMap(mapParam);
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		return map;		
	}
	
	/**
	 * 查询订单明细的记录的方法
	 * @param code 邀请码
	 * @param proName 商品名称
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @param page 当前页数
	 * @param limit 每页显示的最大条数
	 * @return
	 */
	@RequestMapping("/back/findOrderItem")
	@ResponseBody
	public Map<String, Object> findOrderItem(String code,String proName,String startTime,String endTime,int page,int limit){
		Map<String , Object> map = new HashMap<>();
		
		Pages p = new Pages();
		p.setCurPage(page);
		p.setMaxResult(limit);
		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		 
		mapParam.put("code", code);
		mapParam.put("proName", proName);
		mapParam.put("startTime", startTime);
		mapParam.put("endTime", endTime);
		mapParam.put("firstRows", p.getFirstRows());
		mapParam.put("maxResult", p.getMaxResult()); 
		List<Map> list = invitationCodeService.findOrderItem(mapParam);
		
		for (Map m : list) {
			m.put("mxPrice", Tools.moneyFenToYuan2(m.get("mxPrice").toString()));
			if(m.get("mxMoneyFact")!=null || "".equals(m.get("mxMoneyFact"))){
				m.put("mxMoneyFact", Tools.moneyFenToYuan2(m.get("mxMoneyFact").toString())); 
			}else{
				m.put("mxMoneyFact", 0);
			}
		}
		
		int count = invitationCodeService.findOrderItemRowCount(mapParam);
		map.put("data", list);
		map.put("count", count); 
		map.put("code",0);
		map.put("msg", "");
		return map;
	}
	
}
