package cn.ehuoyuan.shop.action.websocket;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;


/**
 * 消息类
 * @author Goofy
 * @Date 2015年6月12日 下午7:32:39
 */
public class Message {
	/**
	 *  消息类型
	 */
	private int type;
	
	/**
	 * 发送者
	 */
	private String host;
	
	/**
	 * 接收者
	 */
	private String[] dests;
	
	/**
	 * 发送的文本
	 */
	private String msg;
	
	/**
	 * 发送日期
	 */
	private Date date;
	
	/**
	 *  聊天室信息(内部类实例对象)
	 */
	private RoomInfo roomInfo;
	
	/**
	 * 类的描述：聊天室信息
	 * @author 罗海兵
	 * @dateTime 2017年12月4日 下午1:31:26
	 * @version 1.0
	 */
	public static class RoomInfo {
		/**
		 *  聊天室名称
		 */
		private String name;
		
		/**
		 *  创建人
		 */
		private String creater;
		
		/**
		 *  创建时间
		 */
		private String createTime;

		/**
		 * 构造器(一)
		 * @param name 聊天室名称
		 */
		public RoomInfo(String name) {
			this.name = name;
		}
		
		/**
		 * 构造器(二)
		 * @param creater 创建人
		 * @param createTime createTime
		 */
		public RoomInfo(String creater, String createTime) {
			this.creater = creater;
			this.createTime = createTime;
		}
		
		/**
		 * @return 返回聊天室名称 
		 */
		public String getName() {
			return name;
		}
		
		/**
		 * @param createTime 聊天室名称
		 */
		public void setName(String name) {
			this.name = name;
		}
		
		/**
		 * @return 返回创建人
		 */
		public String getCreater() {
			return creater;
		}
		
		/**
		 * @param createTime 创建人
		 */
		public void setCreater(String creater) {
			this.creater = creater;
		}
		
		/**
		 * @return 返回创建时间
		 */
		public String getCreateTime() {
			return createTime;
		}
		
		/**
		 * @param createTime 创建时间
		 */
		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}
	}
	
	/**
	 * 类的描述：通信连接
	 * @author 罗海兵
	 * @dateTime 2017年12月4日 下午1:31:46
	 * @version 1.0
	 */
	public class MsgConstant {
		/**
		 *  新连接
		 */
		public final static int Open = 1;
		
		/**
		 *  连接断开
		 */
		public final static int Close = 2;
		
		/**
		 *  发送给所有人
		 */
		public final static int MsgToAll = 3;
		
		/**
		 *  发送给指定用户
		 */
		public final static int MsgToPoints = 4;
		
		/**
		 *  需要登录
		 */
		public final static int RequireLogin = 5;
		
		/**
		 *  设置用户名
		 */
		public final static int setName = 6;
	}
	
	/**
	 * 空构造器
	 * <br/>参数默认值：
	 * <br/>type(消息类型)： MsgToAll(发送给所有人)
	 */
	public Message() {
		setType(MsgConstant.MsgToAll);//设置消息类型
	}
	
	/**
	 * 构造器一
	 * @param host 发送者：
	 * <br/>姓名/用户名/昵称/用户ID
	 * @param type 消息类型：
	 * <br/>1新连接	2断开连接	3发送给所有人	4发送给指定人	5需要登陆	6设置用户名
	 */
	public Message(String host, int type) {
		setHost(host);//设置发送者
		setType(type);//设置消息类型
	}
	
	
	/**
	 * 构造器二
	 * @param host 发送者
	 * @param type 消息类型
	 * @param msg 消息主题
	 */
	public Message(String host, int type, String msg) {
		this(host, type);
		setMsg(msg);
	}
	
	/**
	 * 构造器三
	 * @param host 发送者
	 * @param type 消息类型
	 * @param dests 接受者(数组)
	 */
	public Message(String host, int type, String[] dests) {
		this(host, type);
		setDests(dests);
	}

	/**  
	 * @return 发送者
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param host 发送者
	 */
	public void setHost(String host) {
		this.host = host;
	}
	/**  
	 * @return 发送日期
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date 发送日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * @return 返回聊天室信息对象
	 */
	public RoomInfo getRoomInfo() {
		return roomInfo;
	}
	
	/**
	 * @param roomInfo 聊天室信息对象
	 */
	public void setRoomInfo(RoomInfo roomInfo) {
		this.roomInfo = roomInfo;
	}
	/**
	 * @return 返回消息类型
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * @param type 消息类型
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	
	/**  
	 * @return 接收者
	 */
	public String[] getDests() {
		return dests;
	}

	/**
	 * @param dests 接收者
	 */
	public void setDests(String[] dests) {
		this.dests = dests;
	}

	/**  
	 * @return 发送的文本
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg 发送的文本
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 *  序列化成json串(一)
	 */
	@Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}

}
