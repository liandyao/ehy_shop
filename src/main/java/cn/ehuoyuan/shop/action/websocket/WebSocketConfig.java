/**
 * 
 */
package cn.ehuoyuan.shop.action.websocket;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * 类的描述：WebScoket通讯的配置处理器
 * @author 罗海兵
 * @dateTime 2017年12月11日 上午10:06:35
 * @version 1.0
 */
@Component
@EnableWebSocket
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer{
	
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	MyWebSocketHandler handler;
	
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(handler, "/ws.action").addInterceptors(new HandShake());
		logger.info("配置处理器初始化完成");
		registry.addHandler(handler, "/ws/sockjs").addInterceptors(new HandShake()).withSockJS();
	}

}
