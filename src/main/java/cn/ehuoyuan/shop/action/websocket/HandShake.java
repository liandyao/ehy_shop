/**
 * 
 */
package cn.ehuoyuan.shop.action.websocket;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import cn.ehuoyuan.shop.domain.EhyMember;

/**
 * 类的描述：WebSocket握手请求的拦截器. 检查握手请求和响应, 对WebSocketHandler传递属性
 * @author 罗海兵
 * @dateTime 2017年12月11日 上午10:57:52
 * @version 1.0
 */
public class HandShake implements HandshakeInterceptor {
	
	Logger logger = Logger.getLogger(getClass());
	
	/**
	 * 在握手之前执行该方法, 继续握手返回true, 中断握手返回false. 通过attributes参数设置WebSocketSession的属性
	 */
	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		//判断request是否为ServletServerHttpRequest的实例
		if (request instanceof ServletServerHttpRequest) {
			//将request转化成ServletServerHttpRequest的实例对象
			ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
			
			//取出HttpSession
			HttpSession session = servletRequest.getServletRequest().getSession(false);
			
			// 取出HttpSession中保存的用户对象
			EhyMember member=(EhyMember) session.getAttribute("login");
			
			//如果用户对象不为空
			if(member!=null){
				//将该用户对象添加到WebSocketSession的属性
				attributes.put("member", member);
				logger.info("Websocket：连接建立成功,用户名："+member.getMbName()+", 密码："+member.getMbLoginPwd());
			}else{
				logger.info("登陆用户为空");
				//如果登陆用户为空,返回中断握手协议
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 在握手之后执行该方法. 无论是否握手成功都指明了响应状态码和相应头.
	 */
	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception exception) {
		
	}

}