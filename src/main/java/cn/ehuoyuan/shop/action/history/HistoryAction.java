/**
 * 
 */
package cn.ehuoyuan.shop.action.history;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.shop.domain.EhyHistory;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.service.history.HistoryService;

/**
 * 类的描述：游览记录的action
 * @author xym
 * @version 1.0
 * @date 2018年1月23日
 */
@Controller
@RequestMapping("history")
public class HistoryAction {
	@Resource
	HistoryService historyService;
	/**
	 * 增加游览记录的方法
	 * @param history
	 */
	@ResponseBody
	@RequestMapping("front/addHistory")
	public void addHistory(EhyHistory history,HttpSession session){
		//得到当前登录会员的信息
		EhyMember mb=(EhyMember) session.getAttribute("login");
		//当会员有值时直接返回不再执行
		if(null==mb){
			return;
		}else{
			//给游览记录对象中的用户编号赋值
			history.setMemId(mb.getMbId());
			//给游览记录对象中的用户名赋值
			history.setMemName(mb.getMbName());
		}
		
		//调用增加游览记录的方法
		historyService.addHistory(history);
	}
	/**
	 * 显示登录会员全部游览记录的方法
	 * @param map 传入的所有参数
	 * @param pages 分页对象
	 * @return map 传出参数historyList
	 */
	@ResponseBody
	@RequestMapping("front/showAllHistory")
	public Map<String, Object> showAllHistory(@RequestParam Map<String, Object> param,Pages pages,HttpSession session){
		//得到当前登录会员的信息
		EhyMember mb=(EhyMember) session.getAttribute("login");
		//将mbId传入map集合中作为查询条件使用
		param.put("mbId", mb.getMbId());
		//调用查询全部的方法
		return historyService.showAllHistory(param);
	}
	/**
	 * 查询当前登录会员分享出去的商品游览总数量的方法
	 * @param session 
	 * @return 前登录会员分享出去的商品游览总数量
	 */
	@ResponseBody
	@RequestMapping("front/findLookCount")
	public int findLookCount(HttpSession session){
		//得到当前登录会员的信息
		EhyMember mb=(EhyMember) session.getAttribute("login");
		return historyService.findLookCount(mb.getMbId());
	}
}
