/**
 * 
 */
package cn.ehuoyuan.shop.action.stat;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.stat.vo.TodoVo;
import cn.ehuoyuan.shop.domain.EhyManager;
import cn.ehuoyuan.shop.service.stat.MoneyStatService;

/**
 * 资金汇总Action
 * @author zengren
 * @date 2018年1月18日
 * @version 1.0
 */
@Controller
@RequestMapping("stat")
public class MoneyStatAction {
	
	@Resource
	private MoneyStatService statService;
	
	/**
	 * 显示结算记录
	 * @param map
	 * @param limit
	 * @param page
	 * @param session
	 * @return
	 */
	@RequestMapping("back/showList")
	@ResponseBody
	public Map<String, Object> showList(@RequestParam Map<String, Object> map, Integer limit, Integer page, HttpSession session){
		
		if(map.get("stId")==null && (EhyManager)session.getAttribute("manager")!=null){
			map.put("stId", ((EhyManager)session.getAttribute("manager")).getStId());
		}
		
		if(limit!=null && page!=null){
			Pages pages = new Pages();
			pages.setMaxResult(limit);
			pages.setCurPage(page);
			map.put("pages", pages);
			//logger.info("firstRows:"+pages.getFirstRows()+", maxResult:"+pages.getMaxResult());
		}
		return statService.showList(map);
	}
	
	@RequestMapping("back/displayTodo")
	@ResponseBody
	public List<TodoVo> displayTodo(String id){
		
		return statService.displayTodo(id);
	}
	
	/**
	 * 添加结算记录
	 * @param map
	 * @return
	 */
	@RequestMapping("back/addCheckoutRecord")
	@ResponseBody
	public EhyMessage addCheckoutRecord(@RequestParam Map<String, Object> map, HttpSession session){
		EhyMessage mes = new EhyMessage();
		EhyManager manager = (EhyManager)session.getAttribute("manager");
		if(manager==null){
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
			return mes;
		}
		map.put("emp", manager.getManUser());
		map.put("stId", manager.getStId());
		if(statService.addCheckoutRecord(map)>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping("back/confirmCheckout")
	@ResponseBody
	public EhyMessage confirmCheckout(@RequestParam Map<String, Object> map){
		EhyMessage mes = new EhyMessage();
		if(statService.confirmCheckout(map)>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}
	
	/**
	 * 查询所选时间段可结算的金额
	 * @param map
	 * @return
	 */
	@RequestMapping("back/computeMoney")
	@ResponseBody
	public EhyMessage computeMoney(@RequestParam Map<String, Object> map){
		EhyMessage mes = new EhyMessage();
		String sumMoney = statService.computeMoney(map);
		if(Tools.isEmpty(sumMoney) || "0".equals(sumMoney)){
			mes.setMes("该时间段没有可结算订单");
		}else{
			mes.setMes(sumMoney);
		}
		return mes;
	}
	
	/**
     * 查询上次结算结束的时间
     * @return 上次结算结束的时间
     */
	@RequestMapping("back/queryEndCheckoutTime")
	@ResponseBody
	public EhyMessage queryEndCheckoutTime(){
		EhyMessage mes = new EhyMessage();
		String time = statService.queryEndCheckoutTime();
		if(Tools.isEmpty(time)){
			mes.setState(EhyMessage.ERROR);
			mes.setMes(time);
		}else{
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(time);
		}
		return mes;
	}
	
	/**
	 * 删除为结算记录同时清除详情
	 * @param id 
	 * @return
	 */
	@RequestMapping("back/delete")
	@ResponseBody
	public EhyMessage delete(String id){
		EhyMessage mes = new EhyMessage();
		if(statService.delete(id)>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}
	
}
