/**
 * 
 */
package cn.ehuoyuan.shop.action.stat.vo;

import java.io.Serializable;

/**
 * 后台资金结算记录显示的模型
 * @author zengren
 * @date 2018年1月18日
 * @version 1.0
 */
public class MoneyRecordVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -399769623461734526L;
	
	//资金结算ID
	private String mrId;
	//资金汇总ID
	private String msId;
	//结算时间
	private String mrDateTime;
	//总金额
	private String mrMoney;
	//结算说明
	private String mrRemark;
	//购买数量
	private String mxNum;
	//商品名称
	private String proName;
	//订单明细ID
	private String mxId;
	/**
	 * @return 资金结算ID
	 */
	public String getMrId() {
		return mrId;
	}
	/**
	 * @param mrId 资金结算ID
	 */
	public void setMrId(String mrId) {
		this.mrId = mrId;
	}
	/**
	 * @return 资金汇总ID
	 */
	public String getMsId() {
		return msId;
	}
	/**
	 * @param msId 资金汇总ID
	 */
	public void setMsId(String msId) {
		this.msId = msId;
	}
	/**
	 * @return 结算时间
	 */
	public String getMrDateTime() {
		return mrDateTime;
	}
	/**
	 * @param mrDateTime 结算时间
	 */
	public void setMrDateTime(String mrDateTime) {
		this.mrDateTime = mrDateTime;
	}
	/**
	 * @return 总金额
	 */
	public String getMrMoney() {
		return mrMoney;
	}
	/**
	 * @param mrMoney 总金额
	 */
	public void setMrMoney(String mrMoney) {
		this.mrMoney = mrMoney;
	}
	/**
	 * @return 结算说明
	 */
	public String getMrRemark() {
		return mrRemark;
	}
	/**
	 * @param mrRemark 结算说明
	 */
	public void setMrRemark(String mrRemark) {
		this.mrRemark = mrRemark;
	}
	/**
	 * @return 购买数量
	 */
	public String getMxNum() {
		return mxNum;
	}
	/**
	 * @param mxNum 购买数量
	 */
	public void setMxNum(String mxNum) {
		this.mxNum = mxNum;
	}
	/**
	 * @return 商品名称
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param proName 商品名称
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**
	 * @return 订单明细ID
	 */
	public String getMxId() {
		return mxId;
	}
	/**
	 * @param mxId 订单明细ID
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	
	@Override
	public String toString() {
		return "MoneyRecord [mrId=" + mrId + ", msId=" + msId + ", mrDateTime=" + mrDateTime + ", mrMoney=" + mrMoney
				+ ", mrRemark=" + mrRemark + ", mxNum=" + mxNum + ", proName=" + proName + ", mxId=" + mxId + "]";
	}
	
}
