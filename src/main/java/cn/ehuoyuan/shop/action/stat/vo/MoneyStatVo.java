/**
 * 
 */
package cn.ehuoyuan.shop.action.stat.vo;

import java.io.Serializable;

import cn.ehuoyuan.common.Tools;

/**
 * 后台资金汇总显示的模型
 * @author zengren
 * @date 2018年1月18日
 * @version 1.0
 */
public class MoneyStatVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -625117058522194765L;
	//资金汇总ID
	private String msId;
	//站点名称
	private String stName;
	//开始时间
	private String msStartTime;
	//结束时间
	private String msEndTime;
	//年份
	private String msYear;
	//统计人
	private String msEmp;
	//统计时间
	private String msDateTime;
	//总金额
	private String msMoney;
	//是否结算    0：否，1：是
	private String msIsjs;
	//收款人
	private String msGet;
	//收款方式
	private String msGetType;
	//收款时间
	private String msGetTime;
	
	/**
	 * @return 资金汇总ID
	 */
	public String getMsId() {
		return msId;
	}
	/**
	 * @param msId 资金汇总ID
	 */
	public void setMsId(String msId) {
		this.msId = msId;
	}
	/**
	 * @return 站点名称
	 */
	public String getStName() {
		return stName;
	}
	/**
	 * @param stName 站点名称
	 */
	public void setStName(String stName) {
		this.stName = stName;
	}
	/**
	 * @return 开始时间
	 */
	public String getMsStartTime() {
		return msStartTime;
	}
	/**
	 * @param msStartTime 开始时间
	 */
	public void setMsStartTime(String msStartTime) {
		this.msStartTime = msStartTime;
	}
	/**
	 * @return 结束时间
	 */
	public String getMsEndTime() {
		return msEndTime;
	}
	/**
	 * @param msEndTime 结束时间
	 */
	public void setMsEndTime(String msEndTime) {
		this.msEndTime = msEndTime;
	}
	/**
	 * @return 年份
	 */
	public String getMsYear() {
		return msYear;
	}
	/**
	 * @param msYear 年份
	 */
	public void setMsYear(String msYear) {
		this.msYear = msYear;
	}
	/**
	 * @return 统计人
	 */
	public String getMsEmp() {
		return msEmp;
	}
	/**
	 * @param msEmp 统计人
	 */
	public void setMsEmp(String msEmp) {
		this.msEmp = msEmp;
	}
	/**
	 * @return 统计时间
	 */
	public String getMsDateTime() {
		return msDateTime;
	}
	/**
	 * @param msDateTime 统计时间
	 */
	public void setMsDateTime(String msDateTime) {
		this.msDateTime = msDateTime;
	}
	/**
	 * @return 总金额
	 */
	public String getMsMoney() {
		if(!Tools.isEmpty(msMoney)){
			msMoney = Tools.moneyFenToYuan2(msMoney);
		}
		return msMoney;
	}
	/**
	 * @param msMoney 总金额
	 */
	public void setMsMoney(String msMoney) {
		this.msMoney = msMoney;
	}
	/**
	 * @return 是否结算    0：否，1：是
	 */
	public String getMsIsjs() {
		return msIsjs;
	}
	/**
	 * @param msIsjs 是否结算    0：否，1：是
	 */
	public void setMsIsjs(String msIsjs) {
		this.msIsjs = msIsjs;
	}
	/**
	 * @return 收款人
	 */
	public String getMsGet() {
		return msGet;
	}
	/**
	 * @param msGet 收款人
	 */
	public void setMsGet(String msGet) {
		this.msGet = msGet;
	}
	/** 
	 * @return 收款方式
	 */
	public String getMsGetType() {
		return msGetType;
	}
	/**
	 * @param msGetType 收款方式
	 */
	public void setMsGetType(String msGetType) {
		this.msGetType = msGetType;
	}
	/**
	 * @return 收款时间
	 */ 
	public String getMsGetTime() {
		return msGetTime;
	}
	/**
	 * @param msGetTime 收款时间
	 */
	public void setMsGetTime(String msGetTime) {
		this.msGetTime = msGetTime;
	}
	@Override
	public String toString() {
		return "MoneyStatVo [msId=" + msId + ", stName=" + stName + ", msStartTime=" + msStartTime + ", msEndTime=" + msEndTime
				+ ", msYear=" + msYear + ", msEmp=" + msEmp + ", msDateTime=" + msDateTime + ", msMoney=" + msMoney
				+ ", msIsjs=" + msIsjs + ", msGet=" + msGet + ", msGetType=" + msGetType + ", msGetTime=" + msGetTime
				+ "]";
	}
}	
