/**
 * 
 */
package cn.ehuoyuan.shop.action.stat.vo;

import cn.ehuoyuan.common.Tools;

/**
 * 
 * @author zengren
 * @date 2018年3月17日
 * @version 1.0
 */
public class TodoVo implements java.io.Serializable{
	
	private String datetime;
	
	private String isJs;
	
	private String mxId;
	
	private String proName;
	
	private String mrMoney;
	

	public String getMrMoney() {
		if(Tools.isEmpty(mrMoney)){
			this.mrMoney = Tools.moneyFenToYuan2(this.mrMoney);
		}
		return mrMoney;
	}

	public void setMrMoney(String mrMoney) {
		this.mrMoney = mrMoney;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getIsJs() {
		return isJs;
	}

	public void setIsJs(String isJs) {
		this.isJs = isJs;
	}

	public String getMxId() {
		return mxId;
	}

	public void setMxId(String mxId) {
		this.mxId = mxId;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}
	
	@Override
	public String toString() {
		return "TodoVo [datetime=" + datetime + ", isJs=" + isJs + ", mxId=" + mxId + ", proName=" + proName
				+ ", mrMoney=" + mrMoney + "]";
	}
}
