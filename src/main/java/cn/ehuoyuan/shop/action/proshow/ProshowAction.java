/**
 * 
 */
package cn.ehuoyuan.shop.action.proshow;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyManager;
import cn.ehuoyuan.shop.domain.EhyProShow;
import cn.ehuoyuan.shop.service.product.ProductService;
import cn.ehuoyuan.shop.service.proshow.ProshowService;
import cn.ehuoyuan.shop.service.specificationType.SpecificationTypeService;

/**
 * 产品展示action
 * @author dong
 * @da2017年10月18日
 * @version 1.0
 */
@Controller
@RequestMapping("proshow")
public class ProshowAction {
	
	@Resource	
	private ProshowService proshowService;//产品展示service
	
	@Resource
	private ProductService productService;//产品service
	
	@Resource
	private SpecificationTypeService specificationTypeService;//规格类型service
	
	/**
	 * 排序
	 * @param start
	 * @param end
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping("/back/sortModule")
	@ResponseBody
	public EhyMessage sortModule(int start, int end, String showId) {
		EhyMessage mes = new EhyMessage();//消息类
		int rows = proshowService.sortModule(start, end, showId);//排序方法
		if(rows>0){
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(EhyMessage.SUCCESS_MES);
		}else{
			mes.setState(EhyMessage.ERROR);
			mes.setMes(EhyMessage.ERROR_MES);
		}
		return mes;
	}
	/**
	 * 根据ID查询
	 * @param express
	 * @return
	 */
	@RequestMapping("/back/findById")
	@ResponseBody
	public EhyProShow findById(EhyProShow show){
		EhyProShow e=proshowService.selectByPrimaryKey(show.getShowId());
		return e;
		
	}
	
	/**
	 * 删除方法
	 * @param express
	 * @return
	 */
	@RequestMapping("/back/delete")
	@ResponseBody
	public EhyMessage delete(EhyProShow show){
		show.setIsva(0);//复职0
		int rows = proshowService.updateByPrimaryKeySelective(show);//删除方法
		EhyMessage message = new EhyMessage();//消息类
		if(rows>0){
			message.setState(EhyMessage.SUCCESS);
			message.setMes(EhyMessage.SUCCESS_MES);
		}else{
			message.setState(EhyMessage.ERROR);
			message.setMes(EhyMessage.ERROR_MES);
		}
		return message;
	}
	/**
	 * 查询所有方法
	 * @param page
	 * @param limit
	 * @param area
	 * @return
	 */
	@RequestMapping("/back/showList")
	@ResponseBody  //ajax注解
	public Map<String,Object> showList(int page ,int limit,EhyProShow show,String stName,String proName,String proCode){
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> pagmap = new HashMap<String, Object>();
		Pages ps=new Pages();//分页工具类
		ps.setCurPage(page);
		ps.setMaxResult(limit);
		pagmap.put("page", ps.getFirstRows());
		pagmap.put("limit", ps.getMaxResult());
		pagmap.put("proCode", proCode);//商品编码
		pagmap.put("showIsva", show.getShowIsva());//是否展示
		pagmap.put("stName", stName);//站点
		pagmap.put("proName", proName);//商品名称
		List<EhyProShow> list =proshowService.findAllshow(pagmap);//查询所有
		
		//layui数据表格需要返回的参数
		map.put("count", proshowService.findRowCount(pagmap));
		map.put("data", list);
		map.put("code",0);
		map.put("msg", "");
		return map;
	}
	
	
	/**
	 * 增加和修改
	 * @param express
	 * @return
	 */
	@RequestMapping("/back/addOrUpdate")
	@ResponseBody
	public EhyMessage addOrUpdate(EhyProShow proshow,HttpSession session){
		EhyMessage mes = new EhyMessage();//消息类
		
		EhyManager man = (EhyManager) session.getAttribute("manager");//取session中的用户名
		
		proshow.setOper(man.getManUser());//操作人
		proshow.setShowId(proshow.getShowId());//展示ID
		proshow.setProId(proshow.getProId());//产品ID
		proshow.setStId(proshow.getStId());//站点ID
		proshow.setShowType(proshow.getShowType());//展示类型
		proshow.setShowIsva(proshow.getShowIsva());//是否展示 
		if(Tools.isEmpty(proshow.getShowStartTime())){//如果不选开始时间就取当前时间
			proshow.setShowStartTime(Tools.getCurDateTime());//开始时间取当前时间
		}else{
			proshow.setShowStartTime(proshow.getShowStartTime());
		}
		
		proshow.setShowEndTime(proshow.getShowEndTime());//结束时间
		proshow.setIsva(1);//默认有效
		
		
		
		if(Tools.isEmpty(proshow.getShowId())){
			int rows=proshowService.insertSelective(proshow);//增加方法
			
			if(rows>0){
				mes.setMes(EhyMessage.SUCCESS_MES);
				mes.setState(EhyMessage.SUCCESS);
			}else{
				mes.setMes(EhyMessage.ERROR_MES);
				mes.setState(EhyMessage.ERROR);
			}
		}else{
			int rows=proshowService.updateByPrimaryKeySelective(proshow);//修改方法
			if(rows>0){
				mes.setMes(EhyMessage.SUCCESS_MES);
				mes.setState(EhyMessage.SUCCESS);
			}else{
				mes.setMes(EhyMessage.ERROR_MES);
				mes.setState(EhyMessage.ERROR);
			}
		}

		return mes;
		
	}
	 
}
