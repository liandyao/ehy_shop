/**
 * 
 */
package cn.ehuoyuan.shop.action.share;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.shop.action.share.Vo.ShareVo;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.service.share.ShareService;

/**
 * 类的描述：分享的action
 * @author xym
 * @date 2017年12月26日
 * @version 1.0
 */
@Controller
@RequestMapping("share")
public class ShareAction {
	/*
	 * 分享的service
	 */
	@Resource
	private ShareService shareService;
	/**
	 * 根据会员编号显示所有会员分享记录的方法
	 * @param session 
	 * @return 会员分享集合
	 */
	@ResponseBody
	@RequestMapping("front/showAll")
	public List<ShareVo> showAll(HttpSession session){
		return shareService.myShare(((EhyMember)session.getAttribute("login")).getMbId());
	}
}
