/**
 * 
 */
package cn.ehuoyuan.shop.action.share.Vo;

import java.io.Serializable;

/**
 * 类的描述：分享的宝贝的视图对象
 * @author xym
 *
 * @date 2017年12月26日
 */
public class ShareVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2002374054471308823L;
	//分享的编号
	private String shaId;
	//商品的编号
	private String proId;
	//商品名称
	private String proName;
	//分享价格的百分比（原分享价格一列）
	private Integer shaPrice;
	//商品的市场价
	private String proPrice;
	//商品厂家的直销价格
	private String proPrice0;
	//下单时间
	private String mxDateTime;
	//游览次数
	private String look;
	//已成交数量
	private String buy;
	//我的收益（会员）
	private String earnings;
	//分享的时间
	private String shareTime;
	//订单明细编号
	private String mxId;
	//图片（商品的一张图片和一张评论的图片使用","隔开）
	private String Photo;
	
	
	
	/**
	 * @return 分享价格的百分比（原分享价格一列）
	 * 
	 */
	public Integer getShaPrice() {
		/*if(shaPrice.indexOf(".")>-1)return shaPrice.split(".")[0];*/
		return shaPrice;
	}
	/**
	 * @param 分享价格的百分比（原分享价格一列）
	 */
	public void setShaPrice(Integer shaPrice) {
		this.shaPrice = shaPrice;
	}
	/**
	 * @return 分享编号
	 */
	public String getShaId() {
		return shaId;
	}
	/**
	 * @param 分享编号
	 */
	public void setShaId(String shaId) {
		this.shaId = shaId;
	}
	/**
	 * @return 图片（商品的一张图片和一张评论的图片使用","隔开）
	 */
	public String getPhoto() {
		return Photo;
	}
	/**
	 * @param 图片（商品的一张图片和一张评论的图片使用","隔开）
	 */
	public void setPhoto(String photo) {
		Photo = photo;
	}
	/**
	 * @return 商品的编号
	 */
	public String getProId() {
		return proId;
	}
	/**
	 * @param 商品的编号
	 */
	public void setProId(String proId) {
		this.proId = proId;
	}
	/**
	 * @return 商品名称
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param 商品名称
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**
	 * @return 商品的市场价
	 */
	public String getProPrice() {
		return proPrice;
	}
	/**
	 * @param 商品的市场价
	 */
	public void setProPrice(String proPrice) {
		this.proPrice = proPrice;
	}
	/**
	 * @return 商品厂家的直销价格
	 */
	public String getProPrice0() {
		return proPrice0;
	}
	/**
	 * @param 商品厂家的直销价格
	 */
	public void setProPrice0(String proPrice0) {
		this.proPrice0 = proPrice0;
	}
	/**
	 * @return 下单时间
	 */
	public String getMxDateTime() {
		return mxDateTime;
	}
	/**
	 * @param 下单时间
	 */
	public void setMxDateTime(String mxDateTime) {
		this.mxDateTime = mxDateTime;
	}
	/**
	 * @return 游览次数
	 */
	public String getLook() {
		return look;
	}
	/**
	 * @param 游览次数
	 */
	public void setLook(String look) {
		this.look = look;
	}
	/**
	 * @return 已成交数量
	 */
	public String getBuy() {
		return buy;
	}
	/**
	 * @param 已成交数量
	 */
	public void setBuy(String buy) {
		this.buy = buy;
	}
	/**
	 * @return 我的收益（会员）
	 */
	public String getEarnings() {
		return earnings;
	}
	/**
	 * @param 我的收益（会员）
	 */
	public void setEarnings(String earnings) {
		this.earnings = earnings;
	}
	/**
	 * @return 分享的时间
	 */
	public String getShareTime() {
		return shareTime;
	}
	/**
	 * @param 分享的时间
	 */
	public void setShareTime(String shareTime) {
		this.shareTime = shareTime;
	}
	/**
	 * @return 订单明细编号
	 */
	public String getMxId() {
		return mxId;
	}
	/**
	 * @param 订单明细编号
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	
	
}
