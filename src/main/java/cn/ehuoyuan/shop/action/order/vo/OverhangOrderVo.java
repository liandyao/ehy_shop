/**
 * 
 */
package cn.ehuoyuan.shop.action.order.vo;

import java.io.Serializable;

import cn.ehuoyuan.common.Tools;

/**
 * 后台待发货模型
 * @author zengren
 * @date 2018年1月2日
 * @version 1.0
 */
public class OverhangOrderVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3851230232829116233L;
	//订单id
	private String ordId;
	//收货人
	private String ordMember;
	//地址
	private String ordAdderss;
	//收货人联系方式
	private String ordPhone;
	//明细单号
	private String mxId;
	//数量
	private String mxNum;
	//价格
	private String mxMoney;
	//实际付款
	private String mxMoneyFact;
	//商品名包括规格
	private String proName;
	//站点名称
	private String stName;
	//付款时间
	private String payTime;
	//备注
	private String mxRemark;
	//下单时间
	private String mxDateTime;
	//
	private String proCode;
	
	/**
	 * @return 订单id
	 */
	public String getOrdId() {
		return ordId;
	}
	
	/** 
	 * @param ordId 订单id
	 */
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	
	/**
	 * @return 收货人
	 */
	public String getOrdMember() {
		return ordMember;
	}
	
	/**
	 * @param ordMember 收货人
	 */
	public void setOrdMember(String ordMember) {
		this.ordMember = ordMember;
	}
	
	/**
	 * @return 地址
	 */
	public String getOrdAdderss() {
		return ordAdderss;
	}
	
	/**
	 * @param ordAdderss 地址
	 */
	public void setOrdAdderss(String ordAdderss) {
		this.ordAdderss = ordAdderss;
	}
	
	/**
	 * @return 收货人联系方式
	 */
	public String getOrdPhone() {
		return ordPhone;
	}
	
	/**
	 * @param ordPhone 收货人联系方式
	 */
	public void setOrdPhone(String ordPhone) {
		this.ordPhone = ordPhone;
	}
	
	/**
	 * @return 明细单号
	 */
	public String getMxId() {
		return mxId;
	}
	
	/**
	 * @param mxId 明细单号
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	
	/**
	 * @return 数量
	 */
	public String getMxNum() {
		return mxNum;
	}
	
	/**
	 * @param mxNum 数量
	 */
	public void setMxNum(String mxNum) {
		this.mxNum = mxNum;
	}
	
	/**
	 * @return 价格
	 */
	public String getMxMoney() {
		if(this.mxMoney!=null){
			mxMoney = Tools.moneyFenToYuan2(mxMoney);
		}
		return mxMoney;
	}
	
	/**
	 * @param mxMoney 价格
	 */
	public void setMxMoney(String mxMoney) {
		this.mxMoney = mxMoney;
	}
	
	/**
	 * @return 实际付款
	 */
	public String getMxMoneyFact() {
		if(this.mxMoneyFact!=null){
			mxMoneyFact = Tools.moneyFenToYuan2(mxMoneyFact);
		}
		return mxMoneyFact;
	}
	
	/**
	 * @param mxMoneyFact 实际付款
	 */
	public void setMxMoneyFact(String mxMoneyFact) {
		this.mxMoneyFact = mxMoneyFact;
	}
	
	/**
	 * @return 商品名
	 */
	public String getProName() {
		return proName;
	}
	
	/**
	 * @param proName 商品名
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
	/**
	 * @return 站点名称
	 */
	public String getStName() {
		return stName;
	}
	
	/**
	 * @param stName 站点名称
	 */
	public void setStName(String stName) {
		this.stName = stName;
	}
	
	/**
	 * @return 备注
	 */
	public String getMxRemark() {
		return mxRemark;
	}
	
	/**
	 * @param mxRemark 备注
	 */
	public void setMxRemark(String mxRemark) {
		this.mxRemark = mxRemark;
	}
	
	/**
	 * @return 支付时间
	 */
	public String getPayTime() {
		return payTime;
	}
	
	/**
	 * @param payTime 支付时间
	 */
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	
	/**
	 * @return 下单时间
	 */
	public String getMxDateTime() {
		return mxDateTime;
	}
	
	/**
	 * @param mxDateTime 下单时间
	 */
	public void setMxDateTime(String mxDateTime) {
		this.mxDateTime = mxDateTime;
	}
	/**
	 * @return 产品编号
	 */
	public String getProCode() {
		return proCode;
	}
	/**
	 * @param proCode 产品编号
	 */
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	@Override
	public String toString() {
		return "OverhangOrderVo [ordId=" + ordId + ", ordMember=" + ordMember + ", ordAdderss=" + ordAdderss
				+ ", ordPhone=" + ordPhone + ", mxId=" + mxId + ", mxNum=" + mxNum + ", mxMoney=" + mxMoney
				+ ", mxMoneyFact=" + mxMoneyFact + ", proName=" + proName + ", stName=" + stName + ", payTime="
				+ payTime + ", mxRemark=" + mxRemark + ", mxDateTime=" + mxDateTime + ", proCode="+ proCode +"]";
	}
}
