/**
 * 
 */
package cn.ehuoyuan.shop.action.order.vo;

import java.io.Serializable;

import cn.ehuoyuan.common.Tools;

/**
 * 修改订单价格模型
 * @author zengren
 * @date 2018年1月22日
 * @version 1.0
 */
public class EditOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5893734195092820216L;
	
	//订单ID
	private String ordId;
	//明细ID
	private String mxId;
	//站点名称
	private String stName;
	//确认订单时间
	private String ordTime;
	//订单总金额
	private String ordSumMoney;
	//订单总数量
	private String ordSum;
	//收货人
	private String ordMember;
	//收货电话
	private String ordPhone;
	//收货地址
	private String ordAddress;
	//运费
	private String ordFreight;
	//全部商品名
	private String proName;
	//商品数量
	private String mxNum;
	//备注
	private String mxRemark;
	
	/**
	 * @return 订单ID
	 */
	public String getOrdId() {
		return ordId;
	}
	/**
	 * @param ordId 订单ID
	 */
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	/**
	 * @return 明细ID
	 */
	public String getMxId() {
		return mxId;
	}
	/** 
	 * @param mxId 明细ID
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	/**
	 * @return 站点名称
	 */
	public String getStName() {
		return stName;
	}
	/**
	 * @param stName 站点名称
	 */
	public void setStName(String stName) {
		this.stName = stName;
	}
	/** 
	 * @return 确认订单时间
	 */
	public String getOrdTime() {
		return ordTime;
	}
	/**
	 * @param ordTime 确认订单时间
	 */
	public void setOrdTime(String ordTime) {
		this.ordTime = ordTime;
	}
	/**
	 * @return 订单总金额
	 */ 
	public String getOrdSumMoney() {
		if(!Tools.isEmpty(ordSumMoney)){
			ordSumMoney = Tools.moneyFenToYuan2(ordSumMoney);
		}
		return ordSumMoney;
	}
	/**
	 * @param ordSumMoney 订单总金额
	 */
	public void setOrdSumMoney(String ordSumMoney) {
		this.ordSumMoney = ordSumMoney;
	}
	/**
	 * @return 订单总数量
	 */
	public String getOrdSum() {
		return ordSum;
	}
	/**
	 * @param ordSum 订单总数量
	 */
	public void setOrdSum(String ordSum) {
		this.ordSum = ordSum;
	}
	/**
	 * @return 收货人
	 */
	public String getOrdMember() {
		return ordMember;
	}
	/**
	 * @param ordMember 收货人
	 */
	public void setOrdMember(String ordMember) {
		this.ordMember = ordMember;
	}
	/**
	 * @return 收货电话
	 */
	public String getOrdPhone() {
		return ordPhone;
	}
	/**
	 * @param ordPhone 收货电话
	 */
	public void setOrdPhone(String ordPhone) {
		this.ordPhone = ordPhone;
	}
	/**
	 * @return 收货地址
	 */
	public String getOrdAddress() {
		return ordAddress;
	}
	/**
	 * @param ordAddress 收货地址
	 */
	public void setOrdAddress(String ordAddress) {
		this.ordAddress = ordAddress;
	}
	/**
	 * @return 运费
	 */
	public String getOrdFreight() {
		if(!Tools.isEmpty(ordFreight)){
			ordFreight = Tools.moneyFenToYuan2(ordFreight);
		}
		return ordFreight;
	}
	/**
	 * @param ordFreight 运费
	 */
	public void setOrdFreight(String ordFreight) {
		this.ordFreight = ordFreight;
	}
	/**
	 * @param proName 全部商品名
	 */
	public void setProName(String proName){
		this.proName = proName;
	}
	/**
	 * @return 全部商品名
	 */
	public String getProName(){
		return proName;
	}
	/**
	 * @param mxNum 商品数量
	 */
	public void setMxNum(String mxNum){
		this.mxNum = mxNum;
	}
	/**
	 * @return 商品数量
	 */
	public String getMxNum(){
		return mxNum;
	}
	/**
	 * @param mxRemark 备注
	 */
	public void setMxRemark(String mxRemark){
		this.mxRemark = mxRemark;
	}
	/**
	 * @return 备注
	 */
	public String getMxRemark(){
		return mxRemark;
	}
	
	@Override
	public String toString() {
		return "EditOrder [ordId=" + ordId + ", mxId=" + mxId + ", stName=" + stName + ", ordTime=" + ordTime
				+ ", ordSumMoney=" + ordSumMoney + ", ordSum=" + ordSum + ", ordMember=" + ordMember + ", ordPhone="
				+ ordPhone + ", ordAddress=" + ordAddress + ", ordFreight=" + ordFreight + ", proName=" + proName
				+ ", mxNum=" + mxNum + ", mxRemark=" + mxRemark + "]";
	}
	
}
