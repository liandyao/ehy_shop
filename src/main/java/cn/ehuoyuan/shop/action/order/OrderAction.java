/**
 * 
 */
package cn.ehuoyuan.shop.action.order;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.shop.domain.EhyCart;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyReturns;
import cn.ehuoyuan.shop.service.cart.EhyCartService;
import cn.ehuoyuan.shop.service.order.OrderService;
import cn.ehuoyuan.shop.service.returns.EhyReturnsService;

/**
 * 类的描述：订单的action
 * @author xym
 * @date 2017年10月19日
 * @version 1.0
 */
@Controller
@RequestMapping("/order")
public class OrderAction {
	@Resource
	private OrderService orderService;
	
	@Resource
	EhyCartService ehyCartService;//购物车的Service
	
	@Resource
	EhyReturnsService ehyReturnsService ;
	
	/**
	 * 直接购买
	 * @param cart 封装了商品信息的购物车对象
	 * @param session HttpSession对象
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年7月1日 下午2:44:35
	 * @versions 1.0
	 */
	@RequestMapping("front/addOrder")
	@ResponseBody
	public EhyMessage addOrder(EhyCart cart,String sheId, HttpSession session){
		EhyMember mem=(EhyMember) session.getAttribute("login");
		EhyMessage message = new EhyMessage(); // 消息类
		
		cart.setMbId(mem.getMbId());
		cart.setOper(mem.getMbName());
		cart.setOptime(new Date());
		int rows =   ehyCartService.add(cart, sheId);
		if(rows>0){
			message =orderService.addOrder(cart, mem);
		}
		
		return message;
	}
	
	
	/**
	 * 前端显示全部订单的方法
	 * @param session
	 * @param 当前的页数
	 * @return 订单和消息状态
	 */
	@RequestMapping("front/showList")
	@ResponseBody
	public Map<String, Object> frontShowList(HttpSession session,@RequestParam Map<String, Object> map,Pages page){
		EhyMember mb=(EhyMember) session.getAttribute("login");//得到session中的会员对象
		if(null!=mb){
			map.put("mbId", mb.getMbId());//将会员的id传入map中
		}
		
		map.put("page", page);
		return orderService.frontShowAll(map);
	}
	/**
	 * 确认收货的方法
	 * @param session
	 * @param map 需要的参数 state(订单状态), mbId(会员的编号) ,ordId(订单编号)
	 * @param ordId
	 * @return 消息结果
	 */
	@RequestMapping("front/receipt")
	@ResponseBody
	public EhyMessage receipt(HttpSession session,Map<String, Object> map,String ordId){
		//EhyMember mb=(EhyMember) session.getAttribute("login");//得到session中的会员对象
		map.put("state", CommomUtils.STATE_OK);
		//map.put("mbId", mb.getMbId());//将会员id传入map
		map.put("ordId", ordId);//将订单号传入map
		return orderService.receipt(map);
	}
	
	/**
	 * 退款申请的方法
	 * @param session
	 * @param map 需要的参数 state(订单状态), mbId(会员的编号) ,ordId(订单编号)
	 * @param ordId
	 * @return 消息结果
	 */
	@RequestMapping("front/tuikuanShenqing")
	@ResponseBody
	public EhyMessage tuikuanShenqing(HttpSession session,Map<String, Object> map,String ordId,String content){
		//EhyMember mb=(EhyMember) session.getAttribute("login");//得到session中的会员对象
		map.put("state", CommomUtils.STATE_ORDER_TUIKUAN);
		//map.put("mbId", mb.getMbId());//将会员id传入map
		map.put("ordId", ordId);//将订单号传入map
		map.put("content", content) ;//申请退款原因
		return orderService.saveReturns(map);
	}
	
	/**
	 * 退款申请的方法
	 * @param session
	 * @param map 需要的参数 state(订单状态), mbId(会员的编号) ,ordId(订单编号)
	 * @param ordId
	 * @return 消息结果
	 */
	@RequestMapping("front/tuikuanShenqing2")
	@ResponseBody
	public EhyMessage tuikuanShenqing2(HttpSession session,Map<String, Object> map,String ordId,String content){
		//EhyMember mb=(EhyMember) session.getAttribute("login");//得到session中的会员对象
		map.put("state", CommomUtils.STATE_ORDER_TUIKUAN2);
		//map.put("mbId", mb.getMbId());//将会员id传入map
		map.put("ordId", ordId);//将订单号传入map
		map.put("content", content+" _该商品未发货") ;//申请退款原因
		return orderService.saveReturns(map);
	}
	
	/**
	 * 退款审核通过的方法
	 * @param session
	 * @param map 需要的参数 state(订单状态), mbId(会员的编号) ,ordId(订单编号)
	 * @param ordId
	 * @return 消息结果
	 */
	@RequestMapping("front/tuikuanShenHe")
	@ResponseBody
	public EhyMessage tuikuanShenHe(HttpSession session,Map<String, Object> map,String ordId,String money){
		//EhyMember mb=(EhyMember) session.getAttribute("login");//得到session中的会员对象
		map.put("state", CommomUtils.STATE_ORDER_TUIKUAN_OK);
		
		map.put("ordId", ordId);//将订单号传入map
		
		map.put("money", money);//将退款金额传入map
		
		return orderService.saveReturnsOK(map);
	}
	
	/**
	 * 输入退款快递单号
	 * @param ordId 订单编号
	 * @param content 快递单号
	 * @return
	 */
	@RequestMapping("front/returnKuaiDi")
	@ResponseBody
	public EhyMessage returnKuaiDi(String ordId,String content) {
		EhyReturns ret = new EhyReturns(); 
		ret.setOrdId(ordId); 
		ret.setReExpressCode(content); //快递单号
		EhyMessage em = ehyReturnsService.saveReturns(ret) ;
		 
		return em ;
		 
	}
	
	/**
	 * 根据id查询订单详情
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping("front/findById")
	@ResponseBody
	public Map<String, Object> findById(@RequestParam Map<String, Object> map,HttpSession session){
		EhyMember mb=(EhyMember) session.getAttribute("login");
		map.put("mbId", mb.getMbId());
		return orderService.findByIdShare(map);
	}
	/**
	 * 删除订单明细的方法
	 * @param mxId 明细编号
	 * @return 消息类
	 */
	@ResponseBody
	@RequestMapping("front/deleteOrderItem")
	public EhyMessage deleteOrderItem(String mxId){
		return orderService.deleteOrderItem(mxId);
	}
	/**
	 * 删除订单的方法
	 * @param ordId 订单编号
	 * @return 消息类
	 */
	@ResponseBody
	@RequestMapping("front/deleteOrder")
	public EhyMessage deleteOrder(String ordId){
		return orderService.deleteOrder(ordId);
	}
}
