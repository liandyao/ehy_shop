/**
 * 
 */
package cn.ehuoyuan.shop.action.order;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.shop.domain.EhyReturns;
import cn.ehuoyuan.shop.service.returns.EhyReturnsService;

/**
 * 退货信息
 * @author liandyao
 * @date 2018年7月22日
 * @version 1.0
 */
@Controller
@RequestMapping("returns")
public class ReturnsAction {

	@Resource
	EhyReturnsService ehyReturnsService;
	
	@RequestMapping("/findByOrdId")
	@ResponseBody
	public EhyReturns findByOrdId(String ordId) {
		return ehyReturnsService.selectByOrdId(ordId);
	}
	
	@RequestMapping("/sendAddress")
	@ResponseBody
	public EhyMessage sendAddress(String ordId,String address) {
		EhyMessage mes = new EhyMessage();
		EhyReturns ret = new EhyReturns();
		ret.setOrdId(ordId);
		ret.setReAddress(address); //退货的收货地址
		ehyReturnsService.saveReturns(ret);
		
		mes.setState(EhyMessage.SUCCESS);
		mes.setMes(EhyMessage.SUCCESS_MES);
		return mes ;
	}
}
