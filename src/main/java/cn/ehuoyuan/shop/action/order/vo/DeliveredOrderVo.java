/**
 * 
 */
package cn.ehuoyuan.shop.action.order.vo;

import java.io.Serializable;

import cn.ehuoyuan.common.Tools;

/**
 * 已发货订单模型
 * @author zengren
 * @date 2018年1月9日
 * @version 1.0
 */
public class DeliveredOrderVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2437631598123178913L;
	
	//订单号
	private String ordId;
	//收件人
	private String ordMember;
	//地址
	private String ordAddress;
	//联系手机
	private String ordPhone;
	//快递公司
	private String ordExpress;
	//快递单号
	private String ordExpressCode;
	//实际付款
	private String mxMoneyFact;
	//数量
	private String mxNum;
	//站点名称
	private String stName;
	//商品名带规格
	private String proName;
	//支付时间
	private String payTime;
	//下单时间
	private String mxDateTime;
	//发货时间
	private String fhTime;
	//总金额
	private String mxMoney;
	//备注
	private String mxRemark;
	//明细id
	private String mxId;
	//产品编号
	private String proCode;
	
	//订单状态
	private Integer ordState;
	
	//运费
	private String ordFreight ;
	 
	 
	
	
	
	 
	public String getOrdFreight() {
		return Tools.moneyFenToYuan2(ordFreight);
	}
	public void setOrdFreight(String ordFreight) {
		this.ordFreight = ordFreight;
	}
	/**
	 * @return 订单号
	 */
	public String getOrdId() {
		return ordId;
	}
	/**
	 * @param ordId 订单号
	 */
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	/**
	 * @return 收件人
	 */
	public String getOrdMember() {
		return ordMember;
	}
	/**
	 * @param ordMember 收件人
	 */
	public void setOrdMember(String ordMember) {
		this.ordMember = ordMember;
	}
	/**
	 * @return 地址
	 */
	public String getOrdAddress() {
		return ordAddress;
	}
	/**
	 * @param ordAddress 地址
	 */
	public void setOrdAddress(String ordAddress) {
		this.ordAddress = ordAddress;
	}
	/**
	 * @return 联系手机
	 */
	public String getOrdPhone() {
		return ordPhone;
	}
	/**
	 * @param ordPhone 联系手机
	 */
	public void setOrdPhone(String ordPhone) {
		this.ordPhone = ordPhone;
	}
	/**
	 * @return 快递公司
	 */
	public String getOrdExpress() {
		return ordExpress;
	}
	/**
	 * @param ordExpress 快递公司
	 */
	public void setOrdExpress(String ordExpress) {
		this.ordExpress = ordExpress;
	}
	/**
	 * @return 快递单号
	 */
	public String getOrdExpressCode() {
		return ordExpressCode;
	}
	/**
	 * @param ordExpressCode 快递单号
	 */
	public void setOrdExpressCode(String ordExpressCode) {
		this.ordExpressCode = ordExpressCode;
	}
	/**
	 * @return 实际付款
	 */ 
	public String getMxMoneyFact() {
		if(this.mxMoneyFact!=null){
			mxMoneyFact = Tools.moneyFenToYuan2(mxMoneyFact);
		}
		return mxMoneyFact;
	}
	/**
	 * @param mxMoneyFact 实际付款
	 */
	public void setMxMoneyFact(String mxMoneyFact) {
		this.mxMoneyFact = mxMoneyFact;
	}
	/**
	 * @return 数量
	 */
	public String getMxNum() {
		return mxNum;
	}
	/**
	 * @param mxNum 数量
	 */
	public void setMxNum(String mxNum) {
		this.mxNum = mxNum;
	}
	/**
	 * @return 站点名称
	 */
	public String getStName() {
		return stName;
	}
	/**
	 * @param stName 站点名称 
	 */
	public void setStName(String stName) {
		this.stName = stName;
	}
	/**
	 * @return 商品名
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param proName 商品名
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**
	 * @return 支付时间
	 */
	public String getPayTime() {
		return payTime;
	}
	/**
	 * @param payTime 支付时间
	 */
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	/**
	 * @return 下单时间
	 */
	public String getMxDateTime() {
		return mxDateTime;
	}
	/**
	 * @param mxDateTime 下单时间
	 */
	public void setMxDateTime(String mxDateTime) {
		this.mxDateTime = mxDateTime;
	}
	/**
	 * @return 发货时间
	 */
	public String getFhTime() {
		/*if(fhTime.indexOf(".")>-1){
			fhTime = fhTime.split(".")[0];
		}*/
		return fhTime;
	}
	/**
	 * @param fhTime 发货时间
	 */
	public void setFhTime(String fhTime) {
		this.fhTime = fhTime;
	}
	/**
	 * @return 总金额
	 */
	public String getMxMoney() {
		if(this.mxMoney!=null){
			mxMoney = Tools.moneyFenToYuan2(mxMoney);
		}
		return mxMoney;
	}
	/**
	 * @param mxMoney 总金额
	 */
	public void setMxMoney(String mxMoney) {
		this.mxMoney = mxMoney;
	}
	/**
	 * @return 备注
	 */
	public String getMxRemark() {
		return mxRemark;
	}
	/**
	 * @param mxRemark 备注
	 */
	public void setMxRemark(String mxRemark) {
		this.mxRemark = mxRemark;
	}
	/**
	 * @param mxId 明细Id
	 */
	public void setMxId(String mxId){
		this.mxId = mxId;
	}
	/**
	 * @return 明细Id
	 */
	public String getMxId(){
		return mxId;
	}
	/**
	 * @return 产品编号
	 */
	public String getProCode() {
		return proCode;
	}
	
	
	public Integer getOrdState() {
		return ordState;
	}
	public void setOrdState(Integer ordState) {
		this.ordState = ordState;
	}
	/**
	 * @param proCode 产品编号
	 */
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	@Override
	public String toString() {
		return "DeliveredOrderVo [ordId=" + ordId + ", ordMember=" + ordMember + ", ordAddress=" + ordAddress
				+ ", ordPhone=" + ordPhone + ", ordExpress=" + ordExpress + ", ordExpressCode=" + ordExpressCode
				+ ", mxMoneyFact=" + mxMoneyFact + ", mxNum=" + mxNum + ", stName=" + stName + ", proName=" + proName
				+ ", payTime=" + payTime + ", mxDateTime=" + mxDateTime + ", fhTime=" + fhTime + ", mxMoney=" + mxMoney
				+ ", mxRemark=" + mxRemark + ", mxId=" + mxId + ", proCode="+ proCode +"]";
	}
	
}
