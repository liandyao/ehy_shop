package cn.ehuoyuan.shop.action.order.vo;

import java.io.Serializable;

import cn.ehuoyuan.common.Tools;

/**
 * 售后订单模型
 * @author zengren
 * @date 2018年1月13日
 * @version 1.0
 */
public class ReceivedOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3441967613482892968L;
	//订单id
	private String ordId;
	//明细id
	private String mxId;
	//状态
	private String ordState;
	//收货人
	private String ordMember;
	//联系方式
	private String ordPhone;
	//支付时间
	private String payTime;
	//发货时间
	private String fhTime;
	//下单时间
	private String mxDateTime;
	//实际付款
	private String mxMoneyFact;
	//备注
	private String mxRemark;
	//快递公司
	private String ordExpress;
	//快递单号
	private String ordExpressCode;
	//站点名称
	private String stName;
	//商品名带规格
	private String proName;
	//数量
	private String mxNum;
	//是否结算
	private String mrId;
	//总金额
	private String mxMoney;
	//产品编号
	private String proCode;
	//运费
	private String ordFreight ;
	
	
	
	
	public String getOrdFreight() {
		return Tools.moneyFenToYuan2(ordFreight);
	}
	public void setOrdFreight(String ordFreight) {
		this.ordFreight = ordFreight;
	}
	/**
	 * @return 订单id
	 */
	public String getOrdId() {
		return ordId;
	}
	/**
	 * @param ordId 订单id
	 */
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	/**
	 * @return 明细id
	 */
	public String getMxId() {
		return mxId;
	}
	/**
	 * @param mxId 明细id
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	/**
	 * @return 状态
	 */
	public String getOrdState() {
		return ordState;
	}
	/**
	 * @param ordStatr 状态
	 */
	public void setOrdState(String ordState) {
		this.ordState = ordState;
	}
	/**
	 * @return 收货人
	 */
	public String getOrdMember() {
		return ordMember;
	}
	/**
	 * @param ordMember 收货人
	 */
	public void setOrdMember(String ordMember) {
		this.ordMember = ordMember;
	}
	/**
	 * @return 联系方式
	 */
	public String getOrdPhone() {
		return ordPhone;
	}
	/**
	 * @param ordPhone 联系方式
	 */
	public void setOrdPhone(String ordPhone) {
		this.ordPhone = ordPhone;
	}
	/**
	 * @return 支付时间
	 */
	public String getPayTime() {
		return payTime;
	}
	/**
	 * @param payTime 支付时间
	 */
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	/**
	 * @return 发货时间
	 */
	public String getFhTime() {
		/*if(fhTime.indexOf(".")>-1){
			fhTime = fhTime.split(".")[0];
		}*/
		return fhTime;
	}
	/**
	 * @param fhTime 发货时间
	 */
	public void setFhTime(String fhTime) {
		this.fhTime = fhTime;
	}
	/**
	 * @return 下单时间
	 */
	public String getMxDateTime() {
		return mxDateTime;
	}
	/**
	 * @param mxDateTime 下单时间
	 */
	public void setMxDateTime(String mxDateTime) {
		this.mxDateTime = mxDateTime;
	}
	/**
	 * @return 实际付款
	 */
	public String getMxMoneyFact() {
		if(!Tools.isEmpty(mxMoneyFact)){
			mxMoneyFact = Tools.moneyFenToYuan2(mxMoneyFact);
		}
		return mxMoneyFact;
	}
	/**
	 * @param mxMoneyFact 实际付款
	 */
	public void setMxMoneyFact(String mxMoneyFact) {
		this.mxMoneyFact = mxMoneyFact;
	}
	/**
	 * @return 备注
	 */
	public String getMxRemark() {
		return mxRemark;
	}
	/**
	 * @param mxRemark 备注
	 */
	public void setMxRemark(String mxRemark) {
		this.mxRemark = mxRemark;
	}
	/**
	 * @return 快递公司
	 */
	public String getOrdExpress() {
		return ordExpress;
	}
	/**
	 * @param ordExpress 快递公司
	 */
	public void setOrdExpress(String ordExpress) {
		this.ordExpress = ordExpress;
	}
	/**
	 * @return 快递单号
	 */
	public String getOrdExpressCode() {
		return ordExpressCode;
	}
	/**
	 * @param ordExpressCode 快递单号
	 */
	public void setOrdExpressCode(String ordExpressCode) {
		this.ordExpressCode = ordExpressCode;
	}
	/**
	 * @return 站点名称
	 */
	public String getStName() {
		return stName;
	}
	/**
	 * @param stName 站点名称
	 */
	public void setStName(String stName) {
		this.stName = stName;
	}
	/**
	 * @return 商品名带规格
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param proName 商品名带规格
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**
	 * @return 数量
	 */
	public String getMxNum() {
		return mxNum;
	}
	/**
	 * @param mxNum 数量
	 */
	public void setMxNum(String mxNum) {
		this.mxNum = mxNum;
	}
	/**
	 * @return 是否结算 0：未结算，1：已结算
	 */ 
	public String getMrId() {
		return mrId;
	}
	/**
	 * @param mrId 是否结算 0：未结算，1：已结算
	 */
	public void setMrId(String mrId) {
		this.mrId = mrId;
	}
	/**
	 * @return 总金额
	 */
	public String getMxMoney() {
		if(!Tools.isEmpty(mxMoney)){
			mxMoney = Tools.moneyFenToYuan2(mxMoney);
		}
		return mxMoney;
	}
	/**
	 * @param mxMoney 总金额
	 */
	public void setMxMoney(String mxMoney) {
		this.mxMoney = mxMoney;
	}
	/**
	 * @return 产品编号
	 */
	public String getProCode() {
		return proCode;
	}
	/**
	 * @param proCode 产品编号
	 */
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	@Override
	public String toString() {
		return "ReceivedOrder [ordId=" + ordId + ", mxId=" + mxId + ", ordState=" + ordState + ", ordMember="
				+ ordMember + ", ordPhone=" + ordPhone + ", payTime=" + payTime + ", fhTime=" + fhTime + ", mxDateTime="
				+ mxDateTime + ", mxMoneyFact=" + mxMoneyFact + ", mxRemark=" + mxRemark + ", ordExpress=" + ordExpress
				+ ", ordExpressCode=" + ordExpressCode + ", stName=" + stName + ", proName=" + proName + ", mxNum="
				+ mxNum + ", mrId=" + mrId + ", mxMoney=" + mxMoney + ", proCode=" + proCode + "]";
	}
	
}
