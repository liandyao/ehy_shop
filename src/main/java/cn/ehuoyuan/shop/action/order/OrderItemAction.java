/**
 * 
 */
package cn.ehuoyuan.shop.action.order;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.domain.EhyManager;
import cn.ehuoyuan.shop.service.order.OrderItemService;

/**
 * 类的描述：
 * @author zengren
 * @date 2017年10月27日
 * @version 1.0
 */
@Controller
@RequestMapping("orderItem")
public class OrderItemAction {
	
	@Resource
	private OrderItemService orderItemService;
	
	/**
	 * 显示后台订单
	 * @param map
	 * @param page
	 * @param limit
	 * @param state 状态，1：待发货订单，2：已发货订单，3：已收货订单。
	 * @return
	 */
	@RequestMapping("back/showList")
	@ResponseBody
	public Map<String, Object> showList(@RequestParam Map<String, Object> map, int page, int limit, Integer state, HttpSession session){
		Pages pages = new Pages();
		pages.setMaxResult(limit);
		pages.setCurPage(page);
		map.put("pages", pages);
		//如果没有选择站点，设置站点为当前登录人的站点
		if(map.get("stId")==null || "".equals((map.get("stId").toString()))){
			map.put("stId", ((EhyManager)session.getAttribute("manager")).getStId());
		} 
		for(String key : map.keySet()){
			//当搜索条件有金额Money时并且值不为空，进行元转分的操作
			if(key.indexOf("Money")>-1 && !Tools.isEmpty(map.get(key).toString())){
				map.put(key, Tools.moneyYuanToFen(map.get(key).toString()));
			} 
		} 
		return orderItemService.showList(map, state);
	}
	
	/**
	 * 发货
	 * @param map
	 * @param arr
	 * @return
	 */
	@RequestMapping("back/deliverGoods")
	@ResponseBody
	public EhyMessage deliverGoods(@RequestParam Map<String, Object> map, String arr[]){
		EhyMessage mes = new EhyMessage();
		int rows = orderItemService.deliverGoods(map, arr);
		if(rows>0){
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(EhyMessage.SUCCESS_MES);
		}else{
			mes.setState(EhyMessage.ERROR);
			mes.setMes(EhyMessage.ERROR_MES);
		}
		return mes;
	}
	
	/**
	 * 修改价格。
	 * @param map 
	 * @return 
	 */
	@RequestMapping("back/updateMoney")
	@ResponseBody
	public EhyMessage updateMoney(@RequestParam Map<String, Object> map){
		EhyMessage mes = new EhyMessage();
		for(String key : map.keySet()){
			//当搜索条件有金额Money时并且值不为空，进行元转分的操作
			if(key.indexOf("oney")>-1 && !Tools.isEmpty(map.get(key).toString())){
				map.put(key, Tools.moneyYuanToFen(map.get(key).toString()));
			}
		}
		if(orderItemService.updateMoney(map)>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}
	
	
}
