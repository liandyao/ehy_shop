/**
 * 
 */
package cn.ehuoyuan.shop.action.order.vo;

import java.io.Serializable;

/**
 * 类的描述：分享页面的视图对象
 * @author xym
 * @date 2017年11月4日
 * @version 1.0
 */
public class ShareVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5788487945056745346L;
	//产品名称
	private String proName;
	//图片路径
	private String photo;
	//商品直销的最高价
	private String proMaxPrice;
	//商品直销的最低价
	private String proMinPrice;
	//商品的分享价格（最低价格）区间
	private String minPrice;
	//商品的分享价格(最高价格)区间
	private String maxPrice;
	//实际付款
	private String moneyFact;
	
	
	/**
	 * @return 商品直销的最高价
	 */
	public String getProMaxPrice() {
		return proMaxPrice;
	}
	/**
	 * @param 商品直销的最高价
	 */
	public void setProMaxPrice(String proMaxPrice) {
		this.proMaxPrice = proMaxPrice;
	}
	/**
	 * @return 商品直销的最低价
	 */
	public String getProMinPrice() {
		return proMinPrice;
	}
	/**
	 * @param 商品直销的最低价
	 */
	public void setProMinPrice(String proMinPrice) {
		this.proMinPrice = proMinPrice;
	}
	/**
	 * @return 实际付款
	 */
	public String getMoneyFact() {
		return moneyFact;
	}
	/**
	 * @param 实际付款
	 */
	public void setMoneyFact(String moneyFact) {
		this.moneyFact = moneyFact;
	}
	/**
	 * @return 产品名称
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param 产品名称
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**
	 * @return 图片路径
	 */
	public String getPhoto() {
		return photo;
	}
	/**
	 * @param 图片路径
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	/**
	 * @return 商品的分享价格（最低价格）
	 */
	public String getMinPrice() {
		return minPrice;
	}
	/**
	 * @param 商品的分享价格（最低价格）
	 */
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	/**
	 * @return 商品的分享价格(最高价格)
	 */
	public String getMaxPrice() {
		return maxPrice;
	}
	/**
	 * @param 商品的分享价格(最高价格)
	 */
	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}
	
}
