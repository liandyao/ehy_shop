/**
 * 
 */
package cn.ehuoyuan.shop.action.comment;


import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.shop.action.comment.vo.ShareDetailsVo;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyProductComment;
import cn.ehuoyuan.shop.domain.EhyShare;
import cn.ehuoyuan.shop.service.comment.CommentService;

/**
 * 类的描述：评论的action
 * @author xym
 * @date 2017年11月3日
 * @version 1.0
 */
@Controller
@RequestMapping("/comment")
public class CommentAction {
	@Resource
	private CommentService commentService;
	/**
	 * 增加评论和分享的方法
	 * @param comment 评论的对象
	 * @param share 评论的对象
	 * @param photoDesc 评论图片说明的数组
	 * @param photo 评论图片的数组
	 * @param session
	 * @return 消息对象
	 */
	@RequestMapping("front/comment")
	@ResponseBody
	public EhyMessage comment(EhyProductComment comment,EhyShare share,String[] photoDesc,@RequestParam MultipartFile[] photo,HttpSession session){
		EhyMember member=(EhyMember) session.getAttribute("login");
		comment.setMbId(member.getMbId());
		share.setMbId(member.getMbId());
		share.setMbName(member.getMbName());
		return commentService.comment(comment, share, photoDesc, photo);
	}
	/**
	 * 显示全部评论的方法
	 * @param map 查询所需条件
	 * @return 查询结果
	 */
	@RequestMapping("front/showList")
	@ResponseBody
	public Map<String, Object> showList(@RequestParam Map<String, Object> map){
		return commentService.showList(map);
	}
	/**
	 * 根据分享编号查询分享详情评论部分信息的方法
	 * @param shaId 分享的编号
	 * @return 分享详情的视图对象
	 * <p>所查询的评论图片为多张图片地址和图片说明合并的字符串字符串
	 * <br>分割图片使用','
	 * <br>分割图片地址和图片说明使用'|'</p>
	 */
	@RequestMapping("front/findShareDetails")
	@ResponseBody
	public ShareDetailsVo findShareDetails(String shaId){
		return commentService.findShareByShaId(shaId);
	}
}
