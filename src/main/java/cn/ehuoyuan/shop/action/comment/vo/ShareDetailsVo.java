/**
 * 
 */
package cn.ehuoyuan.shop.action.comment.vo;

import java.io.Serializable;

/**
 * 类的描述：分享详情的视图对象
 * @author xym
 * @date 2018年2月13日
 * @version 1.0
 */
public class ShareDetailsVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3016853347946764663L;
	//会员的名称
	private String mbName;
	//会员的编号
	private String mbId;
	//会员的等级
	private String mbLevel;
	//会员头像地址
	private String mbPhoto;
	//会员评论内容
	private String commDesc;
	//会员评价
	private String commLevel;
	//会员评论图片（内涵图片地址，图片附带评论使用‘|’作为切割字符串的符号在页面进行处理）
	private String commtPhoto;
	/**
	 * @return 会员的名称
	 */
	public String getMbName() {
		return mbName;
	}
	/**
	 * @param 会员的名称
	 */
	public void setMbName(String mbName) {
		this.mbName = mbName;
	}
	/**
	 * @return 会员的编号
	 */
	public String getMbId() {
		return mbId;
	}
	/**
	 * @param 会员的编号
	 */
	public void setMbId(String mbId) {
		this.mbId = mbId;
	}
	/**
	 * @return 会员的等级
	 */
	public String getMbLevel() {
		return mbLevel;
	}
	/**
	 * @param 会员的等级
	 */
	public void setMbLevel(String mbLevel) {
		this.mbLevel = mbLevel;
	}
	/**
	 * @return 会员头像地址
	 */
	public String getMbPhoto() {
		return mbPhoto;
	}
	/**
	 * @param 会员头像地址
	 */
	public void setMbPhoto(String mbPhoto) {
		this.mbPhoto = mbPhoto;
	}
	/**
	 * @return 会员评论内容
	 */
	public String getCommDesc() {
		return commDesc;
	}
	/**
	 * @param 会员评论内容
	 */
	public void setCommDesc(String commDesc) {
		this.commDesc = commDesc;
	}
	/**
	 * @return 会员评价
	 */
	public String getCommLevel() {
		return commLevel;
	}
	/**
	 * @param 会员评价
	 */
	public void setCommLevel(String commLevel) {
		this.commLevel = commLevel;
	}
	/**
	 * @return 会员评论图片（内涵图片地址，图片附带评论使用‘|’作为切割字符串的符号在页面进行处理）
	 */
	public String getCommtPhoto() {
		return commtPhoto;
	}
	/**
	 * @param 会员评论图片（内涵图片地址，图片附带评论使用‘|’作为切割字符串的符号在页面进行处理）
	 */
	public void setCommtPhoto(String commtPhoto) {
		this.commtPhoto = commtPhoto;
	}
	
}
