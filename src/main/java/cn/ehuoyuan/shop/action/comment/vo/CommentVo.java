/**
 * 
 */
package cn.ehuoyuan.shop.action.comment.vo;

import java.io.Serializable;

/**
 * 类的描述：评论区的视图模型
 * @author xym
 * @date 2017年11月26日
 * @version 1.0
 */
public class CommentVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4991546685574191368L;
	private String mxId;//订单详情编号
	private String mbName;//会员名称
	private String mbPhoto;//会员头像
	private int levels;//会员等级
	private String mbDesc;//买家评语
	private String img;//图片
	private String shaId;//分享的编号
	
	
	
	/**
	 * @return 分享的编号
	 */
	public String getShaId() {
		return shaId;
	}
	/**
	 * @param 分享的编号
	 */
	public void setShaId(String shaId) {
		this.shaId = shaId;
	}
	/**
	 * @return 订单编号
	 */
	public String getMxId() {
		return mxId;
	}
	/**
	 * @param 订单编号
	 */
	public void setMxId(String mxId) {
		this.mxId = mxId;
	}
	/**
	 * @return 会员头像
	 */
	public String getMbPhoto() {
		return mbPhoto;
	}
	/**
	 * @param 会员头像
	 */
	public void setMbPhoto(String mbPhoto) {
		this.mbPhoto = mbPhoto;
	}
	
	/**
	 * @return 会员名称
	 */
	public String getMbName() {
		return mbName;
	}
	/**
	 * @param 会员名称
	 */
	public void setMbName(String mbName) {
		this.mbName = mbName;
	}
	/**
	 * @return 会员等级
	 */
	public int getLevels() {
		return levels;
	}
	/**
	 * @param 会员等级
	 */
	public void setLevels(int levels) {
		this.levels = levels;
	}
	/**
	 * @return 买家评价
	 */
	public String getMbDesc() {
		return mbDesc;
	}
	/**
	 * @param 买家评价
	 */
	public void setMbDesc(String mbDesc) {
		this.mbDesc = mbDesc;
	}
	/**
	 * @return 评论图片
	 */
	public String getImg() {
		return img;
	}
	/**
	 * @param 评论图片
	 */
	public void setImg(String img) {
		this.img = img;
	}
	
}
