package cn.ehuoyuan.shop.action.frontShow;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.ehuoyuan.common.Tools;

/**
 * 类的描述：产品展示模型
 * @author 罗海兵
 * @dateTime 2017年10月19日 下午2:34:59
 * @version 1.0
 */
public class ProInfo2 implements java.io.Serializable{
	//产品ID
    private String proId;
	//产品名称
    private String proName;
    //是否上架  0 未上架 1 已上架
    private Integer proIsva;
    //产品滚动图片
    private String imgPath;
    //产品直销价格(元)
    private String proPrice0;
    //产品市场价格(元)
    private String proPrice;
    
    // 附加属性
    private BigDecimal minPspPrice;//最小直销价(普通会员价)
    private BigDecimal maxPspPrice;//最大直销价(普通会员价)
    private BigDecimal minPspFactoryPrice;//最小出厂价
    private BigDecimal maxPspFactoryPrice;//最大出厂价
    private String priceRange;//直销价区间
    private String price0Range;//市场价区间
    
    /**  
	 * @return 产品直销价格(元)
	 */
	public String getProPrice0() {
		if(Tools.isEmpty(proPrice0)){
			return "0.00";
		}
		return Tools.moneyFenToYuan2(proPrice0);
	}
	/**
	 * @param proPrice0 产品直销价格(元)
	 */
	public void setProPrice0(String proPrice0) {
		this.proPrice0 = proPrice0;
	}
	/**  
	 * @return 产品显示价格(元)
	 */
	public String getProPrice() {
		if(Tools.isEmpty(proPrice)){
			return "0.00";
		}
		return Tools.moneyFenToYuan2(proPrice);
	}
	/**
	 * @param proPrice 产品显示价格(元)
	 */
	public void setProPrice(String proPrice) {
		this.proPrice = proPrice;
	}
    
    /**  
	 * @return 产品滚动图片
	 */
	public String getImgPath() {
		return imgPath;
	}
	/**
	 * @param imgPath 产品滚动图片
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
    
	/**  
	 * @return 返回最小直销价
	 */
    @JsonIgnore
	public BigDecimal getMinPspPrice() {
		return minPspPrice;
	}
	/**
	 * @param minPspPrice 最小直销价
	 */
	public void setMinPspPrice(BigDecimal minPspPrice) {
		this.minPspPrice = minPspPrice;
	}
	/**  
	 * @return 返回最大直销价
	 */
	@JsonIgnore
	public BigDecimal getMaxPspPrice() {
		return maxPspPrice;
	}
	/**
	 * @param maxPspPrice 最大直销价
	 */
	public void setMaxPspPrice(BigDecimal maxPspPrice) {
		this.maxPspPrice = maxPspPrice;
	}
	/**  
	 * @return 返回产品的最小出厂价
	 */
	@JsonIgnore
	public BigDecimal getMinPspFactoryPrice() {
		return minPspFactoryPrice;
	}
	/**
	 * @param minPspFactoryPrice 产品的最大出厂价
	 */
	public void setMinPspFactoryPrice(BigDecimal minPspFactoryPrice) {
		this.minPspFactoryPrice = minPspFactoryPrice;
	}
	
	/**  
	 * @return 返回产品的最大出厂价
	 */
	@JsonIgnore
	public BigDecimal getMaxPspFactoryPrice() {
		return maxPspFactoryPrice;
	}
	/**
	 * @param maxPspFactoryPrice 产品的出厂价区间
	 */
	public void setMaxPspFactoryPrice(BigDecimal maxPspFactoryPrice) {
		this.maxPspFactoryPrice = maxPspFactoryPrice;
	}
	
	/**  
	 * @return 产品的直销价区间
	 */
	public String getPriceRange() {
		if(Tools.isEmpty(minPspPrice)){
			return "0.00";
		}
		priceRange=Tools.getPriceRange(minPspPrice, maxPspPrice);
		return priceRange;
	}
	/**
	 * @param priceRange 产品的直销价区间
	 */
	public void setPriceRange(String priceRange) {
		this.priceRange = priceRange;
	}
	/**  
	 * @return 产品的市场价区间
	 */
	public String getPrice0Range() {
		if(Tools.isEmpty(minPspFactoryPrice)){
			return "0.00";
		}
		price0Range=Tools.getPriceRange(minPspFactoryPrice, maxPspFactoryPrice, 1.5);
		return price0Range;
	}
	/**
	 * @param price0Range 产品的市场价区间
	 */
	public void setPrice0Range(String price0Range) {
		this.price0Range = price0Range;
	}
	/**  
	 * @return 产品ID
	 */
	public String getProId() {
		return proId;
	}
	/**
	 * @param proId 产品ID
	 */
	public void setProId(String proId) {
		this.proId = proId;
	}
	/**  
	 * @return 产品名称
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param proName 产品名称
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**  
	 * @return 是否上架  0 未上架 1 已上架
	 */
	public Integer getProIsva() {
		return proIsva;
	}
	/**
	 * @param proIsva 是否上架  0 未上架 1 已上架
	 */
	public void setProIsva(Integer proIsva) {
		this.proIsva = proIsva;
	}
	
}
