/**
 * 
 */
package cn.ehuoyuan.shop.action.frontShow;

/**
 * 类的描述：订单的Vo模型
 * @author 罗海兵
 * @dateTime 2017年11月19日 下午1:07:56
 * @version 1.0
 */
public class OrderVo implements java.io.Serializable{
    //备注(客户留言)
    private String mxRemark;
    //收货人
    private String ordMember;
    //收货地址
    private String ordAddress;
    //收货电话
    private String ordPhone;
    //订单id
    private String ordId;
    
	/**  
	 * @return 订单id
	 */
	public String getOrdId() {
		return ordId;
	}

	/**
	 * @param ordId 订单id
	 */
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}

	/**  
	 * @return 备注(客户留言)
	 */
	public String getMxRemark() {
		return mxRemark;
	}

	/**
	 * @param mxRemark 备注(客户留言)
	 */
	public void setMxRemark(String mxRemark) {
		this.mxRemark = mxRemark;
	}

	/**  
	 * @return 收货人
	 */
	public String getOrdMember() {
		return ordMember;
	}

	/**
	 * @param ordMember 收货人
	 */
	public void setOrdMember(String ordMember) {
		this.ordMember = ordMember;
	}

	/**  
	 * @return 收货地址
	 */
	public String getOrdAddress() {
		return ordAddress;
	}

	/**
	 * @param ordAddress 收货地址
	 */
	public void setOrdAddress(String ordAddress) {
		this.ordAddress = ordAddress;
	}

	/**  
	 * @return 收货电话
	 */
	public String getOrdPhone() {
		return ordPhone;
	}

	/**
	 * @param ordPhone 收货电话
	 */
	public void setOrdPhone(String ordPhone) {
		this.ordPhone = ordPhone;
	}
    
    
}
