package cn.ehuoyuan.shop.action.frontShow;
import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import cn.ehuoyuan.common.Tools;

/**
 * 
 * 类名称：TimerTask   
 * 类描述：定时器任务
 * 创建人：geekfly
 * 创建时间：Aug 29, 2016 10:56:27 AM      
 * @version  V1.0
 *
 */
@Component
public class TimerTask {
  int a=0;
 	 
  @Scheduled(cron = "0/60 * * * * ?")//每隔60秒隔行一次 
  public void test2(){
     //logger.info("job2 开始执行");
     a++;
     if(a>4){
    	 a=0;
     }
     WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
     ServletContext application = webApplicationContext.getServletContext();
     application.setAttribute("pageNum", a);
     
     //logger.info(a);
  } 
}