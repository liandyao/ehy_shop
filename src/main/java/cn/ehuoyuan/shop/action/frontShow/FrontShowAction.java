/**
 * 
 */
package cn.ehuoyuan.shop.action.frontShow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.cart.CartVo;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyOrder;
import cn.ehuoyuan.shop.domain.EhyOrderItem;
import cn.ehuoyuan.shop.service.cart.EhyCartService;
import cn.ehuoyuan.shop.service.member.MemberService;
import cn.ehuoyuan.shop.service.member.address.AddressService;
import cn.ehuoyuan.shop.service.order.OrderService;
import cn.ehuoyuan.shop.service.proType.ProTypeService;
import cn.ehuoyuan.shop.service.product.ProductService;
import cn.ehuoyuan.shop.service.productAttribute.ProductAttributeService;
import cn.ehuoyuan.shop.service.productImg.ProductImgService;
import cn.ehuoyuan.shop.service.productQc.ProductQcService;
import cn.ehuoyuan.shop.service.productSpecificationPrice.ProductSpecificationPriceService;
import cn.ehuoyuan.shop.service.productSpecificationValue.ProductSpecificationValueService;
import cn.ehuoyuan.shop.service.proshow.ProshowService;
import cn.ehuoyuan.shop.service.specificationType.SpecificationTypeService;
import cn.ehuoyuan.shop.service.station.StationService;

/**
 * 类的描述：前台展示查询的Action
 * @author 罗海兵
 * @dateTime 2017年11月11日 上午9:58:20
 * @version 1.0
 */
@Controller
@RequestMapping("frontShow")
public class FrontShowAction {
	@Resource
	private StationService stationService;//站点管理的Service接口
	
	@Resource	
	private ProshowService proshowService;//产品展示service
	
	@Resource
	private ProTypeService proTypeService ;//定义一个产品类型service
	
	@Resource
	private SpecificationTypeService specificationTypeService;//规格类型service
	
	@Resource
	private ProductSpecificationValueService productSpecificationValueService;//产品规格表Service接口
	
	@Resource
	private ProductService productService;//产品service接口
	
	@Resource
	private ProductImgService productImgService;//产品图片service接口
	
	@Resource
	private ProductSpecificationPriceService productSpecificationPriceService;//产品规格价格表Service接口
	
	@Resource
	private ProductAttributeService productAttributeService;//产品属性service接口
	
	@Resource
	private AddressService addressService;//收货地址的接口类
	
	@Resource
	private EhyCartService ehyCartService;//购物车的Service
	
	@Resource
	private OrderService orderService;//订单的Service接口
	
	@Resource
	private MemberService memberService ; // 会员的Service接口
	
	@Resource
	private ProductQcService productQcService ; // 清仓产品的Service接口
	
	
	
	/**
	 * 会员退出并且跳转到首页
	 * @return 
	 * @author 罗海兵
	 * @dateTime 2018年1月17日 下午2:38:47
	 * @versions 1.0
	 */
	@RequestMapping("/front/logout")
	public ModelAndView logout(HttpSession session){
		session.removeAttribute("login");
		session.removeAttribute("cartNum");
		return new ModelAndView("front/index");
	}
	
	/**
	 * 跳转到首页
	 * @return 
	 * @author 罗海兵
	 * @dateTime 2018年1月17日 下午2:38:47
	 * @versions 1.0
	 */
	@RequestMapping("/front/index")
	public ModelAndView index(){
		//logger.info("---------------");
		return new ModelAndView("front/index");
	}
	
	/**
     * 查询所有站点的站点ID,站点名称,站点类型
     * @return 返回一个Map的
     * @author 罗海兵
     * @dateTime 2017年11月13日 上午11:46:49
     * @versions 1.0
     */
	@RequestMapping("/front/findStationAll")
	@ResponseBody
	public List<Map<String, Object>> findStationAll(){
    	return stationService.findStationAll();
    }
	
	
	/**
	 * @title 查询全部展示产品
	 * @description 默认查询全部商品展示列表的模型，可分页，可附件条件查询
	 * @param pages 分页工具类对象
	 * @param proShow 封装了查询条件的商品展示列表的模型
	 * @return 返回一个封装好的Map对象
	 * @author 罗海兵
	 * @dateTime 2017年10月19日 上午10:05:33
	 * @versions 1.0
	 */
	@RequestMapping("/front/showAll")
	@ResponseBody
	public List<Map<String, Object>> showAll(Pages pages, String stId){
		return proshowService.showAll(stId);
	}
	
	/**
	 * 根据类型展示所有的商品
	 * @param pages
	 * @param stId
	 * @return 
	 * liandyao 2018年10月22日 显示清仓商品
	 */
	@RequestMapping("/front/showAllByType")
	@ResponseBody
	public Map<String, Object> showAllByType(Pages pages, String stId,String showType,String keyword){
		if(Tools.isEmpty(showType)) showType="2";
		return proshowService.showAllByType(stId,showType,keyword);//"1" 为推荐商品 "2"为清仓商品
	}
	
	
	@RequestMapping("/front/show2")
	@ResponseBody
	public List<Map<String, Object>> show2(String stId, String ptId, String typeId, 
			@RequestParam(value = "spValues[]", required = false)String[] spValues, 
			String proName, BigDecimal minPrice, BigDecimal maxPrice, HttpServletRequest request, int maxResult){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("stId", stId);
		map.put("ptId", ptId);
		map.put("typeId", typeId);
		map.put("spValues", spValues);
		map.put("proName", proName);
		map.put("minPrice", minPrice);
		map.put("maxPrice", maxPrice);
		 // 执行查询并分页,TbItemExample是逆向工程自动生成的，用来进行条件查询，这里不设置则表示无条件
        //TbItemExample example = new TbItemExample();
		Integer pageNum=(Integer) request.getServletContext().getAttribute("pageNum");
		
		Pages pages=new Pages();
		if(Tools.isEmpty(pageNum)){
			pageNum=1;
		}
		pages.setCurPage(pageNum.intValue());
		pages.setMaxResult(maxResult);
        //分页处理，显示第一页的10条数据
        PageHelper.startPage(pages.getCurPage(), pages.getMaxResult());
		return productService.searchAll(map);
	}
	/**
     * @title 根据站点ID查询最上级分类
     * @param stId 站点ID
     * @return 返回一个Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月24日 下午4:55:41
     * @versions 1.0
     */
	@RequestMapping("front/findByStId")
	@ResponseBody
    public List<Map<String, Object>> findByStId(String stId){
    	List<Map<String, Object>> superiorList=proTypeService.findByStId(stId);
    	return superiorList;
    }
	
	/**
	 * 根据上级ID查询产品类型集合
	 * @param ptId 最上级产品类型ID
	 * @return 返回一个List<Map<String, Object>>的集合
	 * @author 罗海兵
	 * @dateTime 2017年11月7日 下午2:10:38
	 * @versions 1.0
	 */
	@RequestMapping("front/findByPtId")
	@ResponseBody
	public Map<String, Object> findByPtId(String ptId){
		List<Map<String, Object>> subordinateList=proTypeService.findSubordinate(ptId);
		List<String> typeIds = new ArrayList<String>();
		typeIds.add(ptId);
		for(int i=0;i<subordinateList.size();i++){
			Map<String, Object> map=subordinateList.get(i);
			if(map.containsKey("typeId")){
				String typeId=(String) map.get("typeId");
				typeIds.add(typeId);
			}
		}
		List<Map<String, Object>> specificationList=specificationTypeService.findByTypeIds(typeIds,2);
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("subordinateList", subordinateList);
		map.put("specificationList", specificationList);
		return map;
	}
	
	/**
	 * 前台首页查询产品
	 * @param stId 站点ID
	 * @param ptId 父类型ID
	 * @param typeId 产品类型ID(可为空)
	 * @param spValues 产品属性的数组(可为空)
	 * @param proName 产品名称关键字
	 * @return 返回一个ProshowVo对象
	 * @author 罗海兵
	 * @dateTime 2017年11月8日 下午4:52:06
	 * @versions 1.0
	 */
	@RequestMapping("/front/searchAll")
	@ResponseBody
	public List<Map<String, Object>> searchAll(String stId, String ptId, String typeId, 
			@RequestParam(value = "spValues[]", required = false)String[] spValues, 
			String proName, BigDecimal minPrice, BigDecimal maxPrice){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("stId", stId);
		map.put("ptId", ptId);
		map.put("typeId", typeId);
		map.put("spValues", spValues);
		map.put("proName", proName);
		map.put("minPrice", minPrice);
		map.put("maxPrice", maxPrice);
		List<Map<String, Object>> showVoList=productService.searchAll(map);
		return showVoList;
	}
	
	/**
	 * @title 根据产品ID查询宝贝详情
	 * @description 查询产品图片、产品基本信息、规格类型、规格值、规格价格
	 * @param proId 产品ID
	 * @return 返回一个ProInfo对象
	 * @author 罗海兵
	 * @dateTime 2017年10月26日 上午9:05:23
	 * @versions 1.0
	 */
	@RequestMapping("front/findInfoById")
	@ResponseBody
	public ProInfo2 findInfoById2(String proId){
		return productService.findInfoById(proId);
	}
	
	/**
     * 根据产品ID查询产品详情的规格参数 
     * @param proId 产品ID
     * @return 返回一个Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月27日 上午10:27:21
     * @versions 1.0
     */
	@RequestMapping("front/findByProId")
	@ResponseBody
	public List<Map<String, Object>> findByProId(String proId){
    	return productSpecificationValueService.findByProId(proId);
    }
	
	/**
     * 根据产品ID和图片类型查询产品图片
     * @param map 封装了产品ID和图片类型的Map对象
     * @return 返回一个图片地址的Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月26日 下午2:57:03
     * @versions 1.0
     */
	@RequestMapping("front/findImgByProId")
	@ResponseBody
    public List<String> findImgByProId(String proId, Integer imgType){
		Map<String, Object> map=new HashMap<String, Object>();
    	map.put("proId", proId);
    	map.put("imgType", imgType);
    	return productImgService.findImgByProId(map);
    }
	
	 /**
     * 根据产品ID查询产品的所有规格价格
     * @param proId
     * @see Map对象：key=组合名称,value=ProPrice对象
     * @return 返回一个Map的对象集合
     * @author 罗海兵
     * @dateTime 2017年11月12日 下午2:33:09
     * @versions 1.0
     */
	@RequestMapping("front/findPriceByProId")
	@ResponseBody
    public Map<String, Object> findPriceByProId(String proId){
		List<ProPrice> list=productSpecificationPriceService.findPriceByProId(proId);
		Map<String, Object> map=new HashMap<String,Object>();
		for(int i=0;i<list.size();i++){
			ProPrice obj=list.get(i);
			String key=obj.getPspGroupName();
			map.put(key, obj);
		}
    	return map;
    }
	
	 /**
     * 根据产品ID查询该产品的所有属性
     * @param proId 产品ID
     * @return 返回一个Map集合
     * @author 罗海兵
     * @dateTime 2017年11月12日 下午3:16:36
     * @versions 1.0
     */
	@RequestMapping("front/findAttrByProId")
	@ResponseBody
    public List<Map<String, Object>> findAttrByProId(String proId){
    	return productAttributeService.findAttrByProId(proId);
    }
	
	/**
     * 根据会员ID查询收货地址(字段合并版)
     * @param mbId 会员ID
     * @return 返回一个收货地址的集合List<String>
     * @author 罗海兵
     * @dateTime 2017年11月16日 上午10:44:22
     * @versions 1.0
     */
	@RequestMapping("front/findAddrByMbId")
	@ResponseBody
	public List<Map<String, Object>> findAddrByMbId(HttpSession session){
		EhyMember mem=(EhyMember) session.getAttribute("login");
		return addressService.findAddrByMbId(mem.getMbId());
    }
	
	/**
	 * 增加订单和订单明细的集合，并且将已购买的购物车设置为无效
	 * @param orderVo 订单的Vo模型
	 * @param session HttpSession会话对象
	 * @return 返回一个消息类的模型
	 * @author 罗海兵
	 * @dateTime 2017年11月19日 下午1:18:53
	 * @versions 1.0
	 */
	@RequestMapping("front/addOrder")
	@ResponseBody
	public EhyMessage addOrder(OrderVo orderVo, @RequestParam(value = "cartId")String[] cartId, HttpSession session){
		EhyMember mem=(EhyMember) session.getAttribute("login");//从HttpSesion中取出登陆的会员对象
		String mbName=mem.getMbName();//取出会员姓名
		String mbId=mem.getMbId();//取出会员ID
		String mbLogin=mem.getMbLogin();//取出登陆账号
		String mxRemark=orderVo.getMxRemark();//取出订单备注(客户留言)
		String createTime=Tools.getCurDateTime();//得到下单时间

		List<EhyOrderItem> orderItemList=new ArrayList<EhyOrderItem>();//创建订单明细对象的集合
		BigDecimal ordSumMoney=new BigDecimal("0.00");//创建一个订单总金额的BigDecimal对象
		BigDecimal ordSum=new BigDecimal(0);//创建一个订单总数量的BigDecimal对象
		BigDecimal ordFreight=new BigDecimal("0.00");//创建一个订单运费的BigDecimal对象
		String createordCode=Tools.getDateOrderNo();//订单编号
		List<CartVo> cartList=ehyCartService.findByCartIds(cartId);//根据前台传来的购物车ID数组查询购物车集合
		for(int i=0;i<cartList.size();i++){//循环购物车集合
			CartVo cart=cartList.get(i);//取出购物车对象
			int cartNum=cart.getCartNum();//取出购买数量
			String cartPrice=cart.getCartPrice();//取出购买价格(元)
			String cartMoney=cart.getMoney();//取出运费(元)
			String proId=cart.getProId();//取出产品ID
			String proName=cart.getProName();//取出产品名称
			String stId=cart.getStId();//取出站点ID
			String stName=cart.getStName();//取出站点名称
			BigDecimal mxPrice=new BigDecimal(Tools.moneyYuanToFen(cartPrice));//建立订单明细购买价格(分)的BigDecimal型变量
			BigDecimal money=new BigDecimal(Tools.moneyYuanToFen(cartMoney));//建立订单明细运费(分)的BigDecimal型变量
			BigDecimal num = new BigDecimal(cartNum); //建立购买数量的BigDecimal型变量
			BigDecimal mxMoney=mxPrice.multiply(num).setScale(2);//计算出订单明细的总金额，结果保留2位小数
			ordSumMoney=ordSumMoney.add(mxMoney).setScale(2);//将订单明细的总金额累加起来，结果保留2位小数
			ordSum=ordSum.add(num);//将订单明细的购买数量累加起来
			//logger.info("订单明细的运费, 分 "+money+", 元："+cartMoney);
			ordFreight = ordFreight.add(money);//将订单明细的运费累加起来
			//logger.info("订单的运费, 分 "+ordFreight);
			String ordCode=createordCode;//定义一个订单号冗余
			if(i<100){
				ordCode+="00"+i;
			}else if(i<10){
				ordCode+="0"+i;
			}else{
				ordCode+=i;
			}
			String mxId=UUID.randomUUID().toString().replaceAll("-", "");//生成一个UUID唯一标示符,并且去掉'-'符号
			EhyOrderItem orderItem=new EhyOrderItem();//创建一个订单明细对象
			orderItem.setOrdId(createordCode);//给订单明细的订单ID赋值
			orderItem.setMxId(mxId);//给订单明细的ID赋值
			orderItem.setOrdCode(ordCode);//给订单明细的销售单号赋值
			orderItem.setMxDatetime(createTime);//给订单明细的下单时间赋值
			orderItem.setMxNum(cartNum);//给订单明细的购买数量赋值
			orderItem.setMxPrice(mxPrice);//给订单明细的购买价格赋值
			orderItem.setMxMoney(mxMoney);//给订单明细的总金额赋值
			orderItem.setMxRemark(mxRemark);//给订单明细的备注赋值
			orderItem.setMbId(mbId);//给订单明细的会员ID赋值
			orderItem.setMbName(mbName);//给订单明细的会员名称赋值
			//订单ID待生成
			orderItem.setProId(proId);//给订单明细的产品ID赋值
			orderItem.setOper(mbLogin);//给订单明细的操作人赋值
			orderItem.setOrdFreight(money);//给订单明细的运费赋值
			orderItem.setStId(stId);//给订单明细的站点ID赋值
			orderItem.setStName(stName);//给订单明细的站点名称赋值
			String newProName=getNewProName(proName, cart.getSpNames());//得到一个由规格值和产品名称组合而成的新名称
			orderItem.setProName(newProName);//给订单明细的产品名称赋值
			
			//2017年12月10日 liandyao增加
			orderItem.setOrdState(CommomUtils.STATE_ORDER_OK);//订单确认,待付款
			
			orderItemList.add(orderItem);//将订单明细对象添加到订单明细集合
		}
		
		EhyOrder order=new EhyOrder();//创建订单对象
		order.setOrdId(createordCode);//给订单ID赋值
		order.setOrdCode(createordCode);//给订单编号赋值
		order.setOrdTime(createTime);//给确认订单时间赋值//给订单赋值
		order.setOrdSumMoney(ordSumMoney);//给订单总金额赋值
		order.setOrdSum(ordSum);//给订单总数量赋值
		
		String ordMember = orderVo.getOrdMember(); // 收货人
		if(!Tools.isEmpty(ordMember)){
			order.setOrdMember(ordMember);
		}
		String ordAddress = orderVo.getOrdAddress(); // 收货地址
		if(!Tools.isEmpty(ordAddress)){
			order.setOrdAddress(ordAddress);
		}
		String ordPhone = orderVo.getOrdPhone(); // 收货电话号码
		if(!Tools.isEmpty(ordPhone)){
			order.setOrdPhone(ordPhone);
		}
		
		//BeanUtils.copyProperties(orderVo, order);//将订单vo模型中的数据Copy到订单对象中
		order.setOrdEndtime(Tools.getTimeToFuture("H", 2));//给订单支付截止时间赋值
		order.setOrdFreight(ordFreight);//给订单运费赋值
		//logger.info("订单的总运费是："+ordFreight);
		order.setOper(mem.getMbLogin());//给订单操作人赋值
		
		//2017年12月10日 liandyao增加
		order.setOrdState(CommomUtils.STATE_ORDER_OK); //订单确认,待付款
		
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("ord", order);
		//logger.info(order.getOrdId());
		map.put("itemList", orderItemList);
		EhyMessage message=orderService.insertSelective(order, orderItemList, cartId);
		if(message.getState()>0){
			int cartNum = ehyCartService.findCartNum(mbId);
			session.setAttribute("cartNum", cartNum);
		}
		System.out.println("==========会员等级========"+mem.getLevelId());
		if("1".equals(mem.getLevelId())) { //如果是普通会员,直接升级为银牌会员
			EhyMember em = new EhyMember();
			em.setMbId(mem.getMbId());
			em.setLevelId("2");
			memberService.updateByPrimaryKeySelective(em);
		}
		//String ordId=message.getMes();//取出订单ID
		//logger.info("\n\n\n===============================================");
		//logger.info("生成的订单ID："+ordId+", 返回行数："+message.getState());
		//logger.info("===============================================\n\n\n");
		return message;
	}
	
	/**
	 * 增加订单和订单明细的集合，并且将已购买的购物车设置为无效
	 * @param orderVo 订单的Vo模型
	 * @param session HttpSession会话对象
	 * @return 返回一个消息类的模型
	 * @author 罗海兵
	 * @dateTime 2017年11月19日 下午1:18:53
	 * @versions 1.0
	 */
	@RequestMapping("front/addOrder2")
	@ResponseBody
	public EhyMessage addOrder2(OrderVo orderVo){
		String ordId = orderVo.getOrdId(); // 订单id
		String mxRemark=orderVo.getMxRemark();//取出订单备注(客户留言)
		EhyOrder order=new EhyOrder();//创建订单对象
		BeanUtils.copyProperties(orderVo, order);//将订单vo模型中的数据Copy到订单对象中
		order.setOrdEndtime(Tools.getTimeToFuture("H", 2));//给订单支付截止时间赋值
		
		
		EhyOrderItem ehyOrderItem = new EhyOrderItem();
		ehyOrderItem.setOrdId(ordId);
		ehyOrderItem.setMxRemark(mxRemark);
		int rows = orderService.update(order, ehyOrderItem);
		return new EhyMessage(ordId, rows);
	}
	
	/**
	 * 得到一个新的产品名称
	 * @param proName 原来的产品名称
	 * @param spNames 规格组合名称
	 * @return 返回一个由产品名称和规格值组合后的新名称
	 * @author 罗海兵
	 * @dateTime 2018年1月2日 下午2:25:15
	 * @versions 1.0
	 */
	private static String getNewProName(String proName, String spNames){
		String[] strArr = spNames.split(",");
		StringBuffer strBuffer =  new StringBuffer(proName);
		for(int i=0;i<strArr.length;i++){
			strBuffer.append("_"+strArr[i]);
		}
		return strBuffer.toString();
	}
	
	/**
     * 支付页面查询
     * @param ordId 订单ID
     * @return 返回一个订单支付页面的VO模型
     * @author 罗海兵
     * @dateTime 2017年11月21日 上午9:35:05
     * @versions 1.0
     */
	@RequestMapping("front/selectPay")
	@ResponseBody
    public OrderPay selectPay(String ordId){
		return orderService.selectPay(ordId);
	}
	
	/**
	 * 查询清仓产品
	 * @param keyword 搜索关键字
	 * @param limit 长度限制
	 * @param curPage 当前页数
	 * @return 返回一个ProInfo2对象的集合
	 * @author 罗海兵
	 * @dateTime 2018年10月12日 
	 * @versions 1.0
	 */
	@RequestMapping("front/searchProductQc")
	@ResponseBody
	public Map<String, Object> searchProductQc(String keyword, int limit, int curPage){
		Map<String, Object> arg = new HashMap<>();
		arg.put("keyword", keyword);
		Page page = PageHelper.startPage(curPage, limit, true); // 开启分页
		List<ProInfo2> dataList = productQcService.searchProductQc(arg); // 查询清仓产品
		//PageInfo pageInfo = new PageInfo<>(page.getResult()); // 获取分页信息
		/*
		Pages pages=new Pages();
		pages.setCurPage(curPage);
		pages.setMaxResult(limit);
		pages.setTotalPage(page.getPageNum());
		pages.setTotalRows((int)page.getTotal());;*/
		Map<String, Object> result = new HashMap<>();
		result.put("dataList", dataList);
		result.put("curPage", curPage);
		result.put("total", page.getTotal());
		result.put("pageNum", page.getPages());
		return result;
	}
}
