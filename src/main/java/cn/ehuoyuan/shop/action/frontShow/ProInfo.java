package cn.ehuoyuan.shop.action.frontShow;

import java.math.BigDecimal;


import cn.ehuoyuan.common.Tools;

/**
 * 类的描述：产品展示模型
 * @author 罗海兵
 * @dateTime 2017年10月19日 下午2:34:59
 * @version 1.0
 */
public class ProInfo implements java.io.Serializable{
	//产品ID
    private String proId;
	//产品名称
    private String proName;
	//产品直销价格(元)
    private String proPrice0;
    //产品市场价格(元)
    private String proPrice;
    //产品滚动图片
    private String imgPath;
    //是否上架  0 未上架 1 已上架
    private Integer proIsva;
    
    // 附加属性
    private BigDecimal minPspPrice;//最小直销价(普通会员价)
    private BigDecimal maxPspPrice;//最大直销价(普通会员价)
    private BigDecimal minPspFactoryPrice;//最小出厂价
    private BigDecimal maxPspFactoryPrice;//最大出厂价
    private String pspPriceRange;//市场价区间
    private String pspPrice0Range;//直销价区间
    
    
	/**  
	 * @return 返回最小直销价
	 */
	public BigDecimal getMinPspPrice() {
		return minPspPrice;
	}
	/**
	 * @param minPspPrice 最小直销价
	 */
	public void setMinPspPrice(BigDecimal minPspPrice) {
		this.minPspPrice = minPspPrice;
	}
	/**  
	 * @return 返回最大直销价
	 */
	public BigDecimal getMaxPspPrice() {
		return maxPspPrice;
	}
	/**
	 * @param maxPspPrice 最大直销价
	 */
	public void setMaxPspPrice(BigDecimal maxPspPrice) {
		this.maxPspPrice = maxPspPrice;
	}
	/**  
	 * @return 返回产品的最小出厂价
	 */
	public BigDecimal getMinPspFactoryPrice() {
		return minPspFactoryPrice;
	}
	/**
	 * @param minPspFactoryPrice 产品的最大出厂价
	 */
	public void setMinPspFactoryPrice(BigDecimal minPspFactoryPrice) {
		this.minPspFactoryPrice = minPspFactoryPrice;
	}
	
	/**  
	 * @return 返回产品的最大出厂价
	 */
	public BigDecimal getMaxPspFactoryPrice() {
		return maxPspFactoryPrice;
	}
	/**
	 * @param maxPspFactoryPrice 产品的出厂价区间
	 */
	public void setMaxPspFactoryPrice(BigDecimal maxPspFactoryPrice) {
		this.maxPspFactoryPrice = maxPspFactoryPrice;
	}
	/**  
	 * @return 返回产品的市场价区间
	 */
	public String getPspPriceRange() {
		pspPriceRange=Tools.getPriceRange(minPspFactoryPrice, maxPspFactoryPrice, 1.5);
		return pspPriceRange;
	}
	/**
	 * @param pspPriceRange 产品的市场价区间
	 */
	public void setPspPriceRange(String pspPriceRange) {
		this.pspPriceRange = pspPriceRange;
	}
	/**  
	 * @return 返回产品的直销价区间
	 */
	public String getPspPrice0Range() {
		pspPrice0Range=Tools.getPriceRange(minPspPrice, maxPspPrice);
		return pspPrice0Range;
	}
	/**
	 * @param pspPrice0Range 产品的直销价区间
	 */
	public void setPspPrice0Range(String pspPrice0Range) {
		this.pspPrice0Range = pspPrice0Range;
	}
	/**  
	 * @return 产品ID
	 */
	public String getProId() {
		return proId;
	}
	/**
	 * @param proId 产品ID
	 */
	public void setProId(String proId) {
		this.proId = proId;
	}
	/**  
	 * @return 产品名称
	 */
	public String getProName() {
		return proName;
	}
	/**
	 * @param proName 产品名称
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	/**  
	 * @return 产品直销价格(元)
	 */
	public String getProPrice0() {
		return Tools.moneyFenToYuan2(proPrice0);
	}
	/**
	 * @param proFactoryPrice 产品直销价格(元)
	 */
	public void setProPrice0(String proPrice0) {
		this.proPrice0 = proPrice0;
	}
	/**  
	 * @return 产品显示价格(元)
	 */
	public String getProPrice() {
		return Tools.moneyFenToYuan2(proPrice);
	}
	/**
	 * @param proPrice 产品显示价格(元)
	 */
	public void setProPrice(String proPrice) {
		this.proPrice = proPrice;
	}
	/**  
	 * @return 产品滚动图片
	 */
	public String getImgPath() {
		return imgPath;
	}
	/**
	 * @param imgPath 产品滚动图片
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	/**  
	 * @return 是否上架  0 未上架 1 已上架
	 */
	public Integer getProIsva() {
		return proIsva;
	}
	/**
	 * @param proIsva 是否上架  0 未上架 1 已上架
	 */
	public void setProIsva(Integer proIsva) {
		this.proIsva = proIsva;
	}
	
}
