/**
 * 
 */
package cn.ehuoyuan.shop.action.frontShow;

import cn.ehuoyuan.common.Tools;

/**
 * 类的描述：订单支付页面的VO模型
 * @author 罗海兵
 * @dateTime 2017年11月21日 上午10:15:46
 * @version 1.0
 */
public class OrderPay implements java.io.Serializable{
	private String ordId ;//订单ID liandyao 2017年11月29日 加入订单ID
	private String ordCode;//订单编号
	private String payMoney;//订单应付金额
	private String ordAddress;//订单收货地址
	private String ordEndtime;//订单支付截止时间
	private String payType ;//订单支付类型
	private int ordState;
	
	private String payMoneyFen;//订单应付金额--分,这里是因为退款,不需要转换为元,所以加了这个字段
	
	
	public int getOrdState() {
		return ordState;
	}
	public void setOrdState(int ordState) {
		this.ordState = ordState;
	}
	public String getOrdId() {
		return ordId;
	}
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	/**  
	 * @return 订单编号
	 */
	public String getOrdCode() {
		return ordCode;
	}
	/**
	 * @param ordCode 订单编号
	 */
	public void setOrdCode(String ordCode) {
		this.ordCode = ordCode;
	}
	/**  
	 * @return 订单应付金额
	 */
	public String getPayMoney() {
		return Tools.moneyFenToYuan2(payMoney);
	}
	/**
	 * @param payMoney 订单应付金额
	 */
	public void setPayMoney(String payMoney) {
		this.payMoney = payMoney;
	}
	/**  
	 * @return 订单收货地址
	 */
	public String getOrdAddress() {
		return ordAddress;
	}
	/**
	 * @param ordAddress 订单收货地址
	 */
	public void setOrdAddress(String ordAddress) {
		this.ordAddress = ordAddress;
	}
	/**  
	 * @return 订单支付截止时间
	 */
	public String getOrdEndtime() {
		return ordEndtime;
	}
	/**
	 * @param ordEndtime 订单支付截止时间
	 */
	public void setOrdEndtime(String ordEndtime) {
		this.ordEndtime = ordEndtime;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getPayMoneyFen() {
		return payMoneyFen;
	}
	public void setPayMoneyFen(String payMoneyFen) {
		this.payMoneyFen = payMoneyFen;
	}
	
	
	
}
