package cn.ehuoyuan.shop.action.frontShow;

import cn.ehuoyuan.common.Tools;

/**
 * 类的描述：产品规格价格的模型
 * @author 罗海兵
 * @dateTime 2017年10月19日 下午2:34:59
 * @version 1.0
 */
public class ProPrice implements java.io.Serializable{
    //产品价格ID
    private String pspId;
    //规格值名字的集合
    private String pspGroupName;
	//产品出厂价格(元)
    private String pspFactoryPrice;
    //产品显示价格(元)
    private String pspPrice;
    
	/**  
	 * @return 产品价格ID
	 */
	public String getPspId() {
		return pspId;
	}
	/**
	 * @param pspId 产品价格ID
	 */
	public void setPspId(String pspId) {
		this.pspId = pspId;
	}
	/**  
	 * @return 规格值名字的集合
	 */
	public String getPspGroupName() {
		return pspGroupName;
	}
	/**
	 * @param pspGroupName 规格值名字的集合
	 */
	public void setPspGroupName(String pspGroupName) {
		this.pspGroupName = pspGroupName;
	}
	/**  
	 * @return 产品出厂价格(元)
	 */
	public String getPspFactoryPrice() {
		return Tools.moneyFenToYuan2(pspFactoryPrice);
	}
	/**
	 * @param psPfactoryPrice 产品出厂价格(元)
	 */
	public void setPspFactoryPrice(String pspFactoryPrice) {
		this.pspFactoryPrice = pspFactoryPrice;
	}
	/**  
	 * @return 产品显示价格(元)
	 */
	public String getPspPrice() {
		return Tools.moneyFenToYuan2(pspPrice);
	}
	/**
	 * @param PspPrice 产品显示价格(元)
	 */
	public void setPspPrice(String pspPrice) {
		this.pspPrice = pspPrice;
	}
    
    
	
}
