/**
 * 
 */
package cn.ehuoyuan.shop.action.proTypeSpecification;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.ehuoyuan.shop.service.proTypeSpecification.ProTypeSpecificationService;

/**
 * 产品类型规格action
 * @author 欧阳丰
 * @data 2017年10月10日
 */
@Controller
@RequestMapping("/proTypeSpecification")
public class ProTypeSpecificationAction {
	@Resource
	ProTypeSpecificationService service;
	
	/**
     * 查询该产品类型的所有规格
     * @return 影响行数
     * @author 欧阳丰
	 * @date 2017年10月11日 9:23:20
	 * @version 1.1
     */
	@ResponseBody
	@RequestMapping("/back/findAll")
	public List<Map<String, Object>> findAll(String proTypeId){
		return service.findAll(proTypeId);
	}
	
	
}
