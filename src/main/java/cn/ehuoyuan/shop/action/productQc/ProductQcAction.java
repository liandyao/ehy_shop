/**
 * 
 */
package cn.ehuoyuan.shop.action.productQc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import cn.ehuoyuan.shop.domain.EhyManager;
import cn.ehuoyuan.shop.domain.EhyProductQC;
import cn.ehuoyuan.shop.service.proType.ProTypeService;
import cn.ehuoyuan.shop.service.productQc.ProductQcService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 清仓产品action
 * @author 罗海兵
 * @data 2018年10月09日
 */
@Controller
@RequestMapping("/productQc")
public class ProductQcAction {
	
	@Resource
	private ProTypeService proTypeService ;//定义一个产品类型service
	
	@Resource
	private ProductQcService productQcService ;//定义一个清仓产品service
	
	/**
     * 删除
     * @param proId 产品ID
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
	@RequestMapping("/back/delProductQc")
	@ResponseBody
	public int delProductQc(String proId){
		int rows = productQcService.deleteByProId(proId);
		return rows;
	}
	
	/**
     * 增加
     * @param productQC 清仓产品的模型
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
	@RequestMapping("/back/addProductQc")
	@ResponseBody
	public int addProductQc(HttpSession session, EhyProductQC productQC){
		EhyManager manager=(EhyManager) session.getAttribute("manager");
		String oper = manager.getManId(); // 操作人
		productQC.setOper(oper);
		int rows = productQcService.add(productQC);
		return rows;
	}
	
   /**
     * 批量增加
     * @param proIdStr 产品id的字符串，多个产品id以逗号连接
     * @param session HttpSession会话对象
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
	@RequestMapping("/back/addBatchProductQc")
	@ResponseBody
	public int addBatchProductQc(HttpSession session, String dataArr){
		EhyManager manager=(EhyManager) session.getAttribute("manager");
		String oper = manager.getManId(); // 操作人
		List<Map<String, String>> argList = new ArrayList<>();
//		jsonArr=URLDecoder.decode(jsonArr,"UTF-8");//doGet请求需要编码解码
//		jsonArr=URLDecoder.decode(jsonArr,"UTF-8");
		
		JSONArray json= JSONArray.fromObject(dataArr);
		if(json.size()>0){
		  for(int i=0;i<json.size();i++){
		    JSONObject job = json.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
		    String proId = job.get("proId").toString();
		    String qcKc = job.get("qcKc").toString();
		    Map<String, String> arg = new HashMap<>();
			arg.put("oper", oper);
			arg.put("proId", proId);
			arg.put("qcKc", qcKc);
			argList.add(arg);
		  }
		}
		int rows = productQcService.addBatch(argList);
		return rows;
	}
	
	/**
	 * 修改
	 * @param productQC 清仓产品对象
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年10月17日 
	 * @versions 1.0
	 */
	@RequestMapping("/back/update")
	@ResponseBody
	public int update(HttpSession session, EhyProductQC productQC) {
		EhyManager manager=(EhyManager) session.getAttribute("manager");
		String oper = manager.getManId(); // 操作人
		productQC.setOper(oper);
		productQC.setOptime(new Date());
		int rows = productQcService.update(productQC);
		return rows;
	}
	
}
