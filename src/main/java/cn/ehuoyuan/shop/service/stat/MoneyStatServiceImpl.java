/**
 * 
 */
package cn.ehuoyuan.shop.service.stat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.action.stat.vo.TodoVo;
import cn.ehuoyuan.shop.dao.EhyMoneyStatMapper;
import cn.ehuoyuan.shop.dao.EhyOrderMapper;
import cn.ehuoyuan.shop.domain.EhyMoneyStat;

/**
 * 资金汇总Service实现类
 * @author zengren
 * @date 2018年1月18日
 * @version 1.0
 */
@Service
public class MoneyStatServiceImpl implements MoneyStatService{
	
	@Resource
	private EhyMoneyStatMapper statMapper;
	@Resource
	private EhyOrderMapper orderMapper;

	@Override
	public int add(EhyMoneyStat stat) {
		// TODO Auto-generated method stub
		return statMapper.insertSelective(stat);
	}

	@Override
	public int delete(String id) {
		// TODO Auto-generated method stub
		return statMapper.delete(id);
	}

	@Override
	public int update(EhyMoneyStat stat) {
		// TODO Auto-generated method stub
		return statMapper.updateByPrimaryKeySelective(stat);
	}

	@Override
	public EhyMoneyStat findById(String id) {
		// TODO Auto-generated method stub
		return statMapper.selectByPrimaryKey(id);
	}

	@Override
	public Map<String, Object> showList(Map<String, Object> map) {
		Map<String, Object> hash = new HashMap<String, Object>();
		hash.put("count", statMapper.countRows(map));
		hash.put("data", statMapper.showList(map));
		hash.put("msg", 0);
		hash.put("code", 0);
		return hash;
	}

	@Override
	public int addCheckoutRecord(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("mxIsJs", 0);
		map.put("startPayTime", map.get("startTime"));
		map.put("endPayTime", map.get("endTime"));
		return statMapper.addCheckoutRecord(map);
	}

	@Override
	public String computeMoney(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return statMapper.computeMoney(map);
	}

	@Override
	public String queryEndCheckoutTime() {
		// TODO Auto-generated method stub
		return statMapper.queryEndCheckoutTime();
	}

	@Override
	public int confirmCheckout(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("mxIdList", statMapper.selectItemById(map.get("msId").toString()));
		return statMapper.confirmCheckout(map);
	}

	@Override
	public List<TodoVo> displayTodo(String id) {
		// TODO Auto-generated method stub
		return statMapper.displayTodo(id);
	}
	
	
}
