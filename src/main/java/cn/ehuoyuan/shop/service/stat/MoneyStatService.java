package cn.ehuoyuan.shop.service.stat;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.stat.vo.TodoVo;
import cn.ehuoyuan.shop.domain.EhyMoneyStat;

/**
 * 资金汇总Service接口
 * @author zengren
 * @date 2018年1月18日
 * @version 1.0
 */
public interface MoneyStatService {
	
	public int add(EhyMoneyStat stat);
	
	/**
	 * 删除为结算记录同时清除详情
	 * @param id 
	 * @return
	 */
	public int delete(String id);
	
	public int update(EhyMoneyStat stat);
	
	public EhyMoneyStat findById(String id);
	
	/**
	 * 显示资金结算记录
	 * @param map
	 * @return
	 */
	public Map<String, Object> showList(Map<String, Object> map);
	
	/**
	 * 添加结算记录
	 * @param map
	 * @return
	 */
	public int addCheckoutRecord(Map<String, Object> map);
	
	/**
	 * 计算选择时间段的
	 * @param map
	 * @return
	 */
	public String computeMoney(Map<String, Object> map);
	
	/**
     * 查询上次结算结束的时间
     * @return 上次结算结束的时间
     */
	public String queryEndCheckoutTime();
	
	/**
	 * 确认结算
	 * @return
	 */
	public int confirmCheckout(Map<String, Object> map);
	
	public List<TodoVo> displayTodo(String id);
	
}
