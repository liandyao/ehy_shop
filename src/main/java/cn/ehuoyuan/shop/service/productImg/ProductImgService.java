/**
 * 
 */
package cn.ehuoyuan.shop.service.productImg;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductImg;

/**
 * 产品图片service接口
 * @author 欧阳丰
 * @data 2017年10月10日
 */
public interface ProductImgService {
	
	 /**
     * 根据图片ID修改图片为无效
     * @param imgId 图片ID
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月18日18:10:35
	 * @version 1.0
     */
    public int updateIsvaByImgId(String imgId);
	
	/**
     * 根据图片ID修改图片序号
     * @param imgId 图片ID数组
     * @param sort 排序数组
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月18日14:57:49
	 * @version 1.0
     */
    public int updateSortByImgId(String[] imgId,Integer[] sort);
	
    /**
     * 根据产品ID和类型ID查询图片
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 图片集合
     * @author 欧阳丰
     * @dateTime 2017年10月17日 20:47:20
	 * @version 1.0
     */
    public List<EhyProductImg> findImgByProIdAndImgType(String proId,int imgType);
    
    /**
     * 根据产品ID和类型ID查询图片数量
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 图片数量
     * @author 欧阳丰
     * @dateTime 2017年10月17日 20:47:20
	 * @version 1.0
     */
    public int findImgByProIdAndImgTypeSize(String proId,int imgType);
	
    /**
     * 增加产品图片
     * @param productImg 产品图片实体类(所有字段都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月10日 18:28:20
	 * @version 1.0
     */
    public int insert(EhyProductImg productImg);
    
    /**
     * 根据产品ID和图片类型修改图片为无效
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月10日 18:28:20
	 * @version 1.0
     */
    public int updateIsvaByProIdAndImgType(String proId,int imgType);
    
    /**
     * @title 根据产品ID和图片类型查询产品图片
     * @param map 封装了产品ID和图片类型的Map对象
     * @return 返回一个图片地址的Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月26日 下午2:57:03
     * @versions 1.0
     */
    List<String> findImgByProId(Map<String, Object> map);
}
