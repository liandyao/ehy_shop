/**
 * 
 */
package cn.ehuoyuan.shop.service.history;

import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyHistory;

/**
 * 类的描述：游览记录的service
 * @author xym
 * @version 1.0
 * @date 2018年1月23日
 */
public interface HistoryService {
	/**
	 * 增加游览记录的方法
	 * @param history 游览记录对象
	 */
	public void addHistory(EhyHistory history);
	/**
	 * 显示全部游览记录的方法
	 * @param param 查询所需参数：mbId 会员的编号
	 * @return
	 */
	public Map<String, Object> showAllHistory(Map<String, Object> param);
	/**
	 * 根据会员的编号查询出该会员分享出去的商品被游览数的总和
	 * @param mbId 会员编号
	 * @return 该会员分享出去的商品被游览数的总和
	 */
	public int findLookCount(String mbId);
}
