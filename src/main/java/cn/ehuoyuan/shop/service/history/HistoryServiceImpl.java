/**
 * 
 */
package cn.ehuoyuan.shop.service.history;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.dao.EhyHistoryMapper;
import cn.ehuoyuan.shop.domain.EhyHistory;

/**
 * 类的描述：游览记录service的实现类
 * @author xym
 * @version 1.0
 * @date 2018年1月23日
 */
@Service
public class HistoryServiceImpl implements HistoryService {
	
	@Resource
	EhyHistoryMapper ehyHistoryMapper;

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.history.HistoryService#addHistory(cn.ehuoyuan.shop.domain.EhyHistory)
	 */
	@Override
	public void addHistory(EhyHistory history) {
		//调增加游览记录的方法
		ehyHistoryMapper.addHistory(history);
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.history.HistoryService#showAllHistory(java.util.Map)
	 */
	@Override
	public Map<String, Object> showAllHistory(Map<String, Object> param) {
		//新建一个返回出去的对象
		Map<String, Object> result=new HashMap<String,Object>();
		//将查询出来的游览记录的list集合放入返回的map集合中
		result.put("HistoryList", ehyHistoryMapper.showAllHistory(param));
		
		return result;
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.history.HistoryService#findLookCount(java.lang.String)
	 */
	@Override
	public int findLookCount(String mbId) {
		// TODO Auto-generated method stub
		return ehyHistoryMapper.findLookCount(mbId);
	}

}
