/**
 * 
 */
package cn.ehuoyuan.shop.service.product;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.frontShow.ProInfo2;
import cn.ehuoyuan.shop.domain.EhyProduct;
import cn.ehuoyuan.shop.domain.EhyProductVo;

/**
 * 产品service接口
 * @author 欧阳丰
 * @data 2017年10月10日
 */
public interface ProductService {
	
	/**
     * 根据产品ID得到修改产品的信息(适用于修改产品页面)
     * @param proId 产品ID
     * @return map (key为数据库字段名)
     * @author 欧阳丰
	 * @dateTime 2017年10月15日 18:28:20
	 * @version 1.0
     */
    public Map<String,Object> findProductAndBandByProId(String proId);
	
    /**
     * 修改产品信息
     * @param product 产品对象(除产品ID，其他字段可以为空，如果为空，表示不修改该字段)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 20:32:20
	 * @version 1.0
     */
    public int updateByPrimaryKeySelective(EhyProduct product);
    
    /**
     * 增加产品
     * @param product 产品对象(除产品ID，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月10日 18:28:20
	 * @version 1.0
     */
	public int insertSelective(EhyProduct product);
	
	 /**
     * 查询所有产品(包括未上架的)
     * 此方法不适用于其他人
     * @param map 搜索的字段
     * @return 产品集合
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 10:23:20
	 * @version 1.1
     */
    public List<EhyProduct> findAll(Map<String,Object> map);
    
    /**
     * 查询所有产品(包括未上架的)
     * 此方法不适用于其他人
     * @param map 搜索的字段
     * @return 产品集合
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 10:23:20
	 * @version 1.1
     */
    public List<EhyProductVo> findAll2(Map<String,Object> map);
    
    /**
     * 查询所有产品(包括未上架的)的总行数
     * 此方法不适用于其他人
     * @param map 搜索的字段
     * @return 产品数量
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 10:23:20
	 * @version 1.0
     */
    public int findAllSize(Map<String,Object> map);
    
    /**
     * 查询所有产品(包括未上架的)的总行数
     * 此方法不适用于其他人
     * @param map 搜索的字段
     * @return 产品数量
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 10:23:20
	 * @version 1.0
     */
    public int findAllSize2(Map<String,Object> map);
	
    /**
     * 修改产品信息
     * @param product 产品对象(除产品ID，其他字段可以为空，如果为空，表示不修改该字段)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月13日 20:32:20
	 * @version 1.0
     */
	public int updateByProId(EhyProduct product);
	
	/**
	 * 根据产品id得到产品信息
	 * @author 胡鑫
	 * @date 2017年10月23日11:20:49
	 * @param 产品id
	 * @return 返回产品类
	 */
	public EhyProduct findByIdProduct(String proId);
	
	/**
	 * 根据ID查询商品是否有图片
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	public int findAllimg(String proId);
	
	
	/**
	 * 根据ID查询商品是否有属性
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	public int findAllproperty(String proId);
	
	
	/**
	 * 根据ID查询商品是否有价格，规格
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	public int findAllprice(String proId);
	
	/**
	 * 根据ID查询商品是否加入展示
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	public int findAllshow(String proId);
	/**
	 * 前台首页查询产品
	 * @param map 封装了查询条件的Map对象
	 * @return 返回一个ProshowVo对象
	 * @author 罗海兵
	 * @dateTime 2017年11月8日 下午4:52:06
	 * @versions 1.0
	 */
	public List<Map<String, Object>> searchAll(Map<String, Object> map);
	
	/**
	  * @title 产品ID查询产品详情
	  * @see 产品详情：产品基本信息、规格类型、规格值、规格价格、产品图片等信息  
	  * @param proId 产品ID
	  * @return 返回一个产品展示的视图层模型
	  * @author 罗海兵
	  * @dateTime 2017年10月20日 下午1:32:26
	  * @versions 1.0
	  */
	 public ProInfo2 findInfoById(String proId);

}
