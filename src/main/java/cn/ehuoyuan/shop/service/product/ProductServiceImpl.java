/**
 * 
 */
package cn.ehuoyuan.shop.service.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.action.frontShow.ProInfo2;
import cn.ehuoyuan.shop.dao.EhyProductMapper;
import cn.ehuoyuan.shop.domain.EhyProduct;
import cn.ehuoyuan.shop.domain.EhyProductVo;

/**
 * 产品service实现接口
 * @author 欧阳丰
 * @data 2017年10月12日
 */
@Service
public class ProductServiceImpl implements ProductService{
	@Resource
	EhyProductMapper dao;
	
	@Override
	public int insertSelective(EhyProduct record) {
		return dao.insertSelective(record);
	}

	@Override
	public int updateByProId(EhyProduct prodyct) {
		return dao.updateByPrimaryKeySelective(prodyct);
	}

	@Override
	public List<EhyProduct> findAll(Map<String,Object> map) {
		List<EhyProduct> list =dao.findAll(map);
		List<EhyProduct> listVo = new ArrayList<EhyProduct>();
		BigDecimal num = new BigDecimal("100");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			EhyProduct ehyProduct = (EhyProduct) iterator.next();
			EhyProduct vo = new EhyProduct();
			BeanUtils.copyProperties(ehyProduct, vo);
			if(ehyProduct.getProFactoryPrice()!=null){
				vo.setProFactoryPrice(new BigDecimal(ehyProduct.getProFactoryPrice().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice()!=null ){
				vo.setProPrice(new BigDecimal(ehyProduct.getProPrice().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice0()!=null){
				vo.setProPrice0(new BigDecimal(ehyProduct.getProPrice0().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice1()!=null){
				vo.setProPrice1(new BigDecimal(ehyProduct.getProPrice1().toString()).divide(num,2));
			}
			listVo.add(vo);
		}
		return listVo;
	}
	
	@Override
	public List<EhyProductVo> findAll2(Map<String,Object> map) {
		List<EhyProductVo> list =dao.findAll2(map);
		List<EhyProductVo> listVo = new ArrayList<EhyProductVo>();
		BigDecimal num = new BigDecimal("100");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			EhyProductVo ehyProduct = (EhyProductVo) iterator.next();
			EhyProductVo vo = new EhyProductVo();
			BeanUtils.copyProperties(ehyProduct, vo);
			if(ehyProduct.getProFactoryPrice()!=null  ){
				vo.setProFactoryPrice(new BigDecimal(ehyProduct.getProFactoryPrice().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice()!=null ){
				vo.setProPrice(new BigDecimal(ehyProduct.getProPrice().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice0()!=null ){
				vo.setProPrice0(new BigDecimal(ehyProduct.getProPrice0().toString()).divide(num,2));
			}
			if(ehyProduct.getProPrice1()!=null ){
				vo.setProPrice1(new BigDecimal(ehyProduct.getProPrice1().toString()).divide(num,2));
			}
			listVo.add(vo);
		}
		return listVo;
	}

	@Override
	public int findAllSize(Map<String,Object> map) {
		return dao.findAllSize(map);
	}
	
	@Override
	public int findAllSize2(Map<String,Object> map) {
		return dao.findAllSize2(map);
	}

	@Override
	public int updateByPrimaryKeySelective(EhyProduct product) {
		return dao.updateByPrimaryKeySelective(product);
	}
	
	/**
	 * copy map对象
	 * @param mapSource
	 * @param mapTarget
	 */
	public static void  copyMap(Map mapSource,Map mapTarget) {
		if(mapSource==null) mapTarget =  new HashMap();
		if(mapTarget==null) mapTarget = new HashMap();
		Iterator ite = mapSource.entrySet().iterator();
		while(ite.hasNext()) {
			Map.Entry ent = (Entry) ite.next();
			Object key = ent.getKey();
			mapTarget.put(key, mapSource.get(key));
		}
	}

	@Override
	public Map<String, Object> findProductAndBandByProId(String proId) {
		Map<String, Object> map =dao.findProductAndBandByProId(proId);
		HashMap<String, Object> mapVo = new HashMap<String, Object>();
		copyMap(map,mapVo);
		//把出厂价从分转换成元
		if(map.get("PRO_FACTORY_PRICE")!=null){
			mapVo.put("PRO_FACTORY_PRICE",new BigDecimal(map.get("PRO_FACTORY_PRICE").toString()).divide(new BigDecimal("100"),2));
		}
		//把市场价从分转换成元
		if(map.get("PRO_PRICE")!=null){
			mapVo.put("PRO_PRICE",new BigDecimal(map.get("PRO_PRICE").toString()).divide(new BigDecimal("100"),2));
		}
		//把普通会员价从分转换成元
		if(map.get("PRO_PRICE0")!=null){
			mapVo.put("PRO_PRICE0",new BigDecimal(map.get("PRO_PRICE0").toString()).divide(new BigDecimal("100"),2));
		}
		//把银牌会员价从分转换成元
		if(map.get("PRO_PRICE1")!=null){
			mapVo.put("PRO_PRICE1",new BigDecimal(map.get("PRO_PRICE1").toString()).divide(new BigDecimal("100"),2));
		}
		//把金牌会员价从分转换成元
		if(map.get("PRO_PRICE2")!=null){
			mapVo.put("PRO_PRICE2",new BigDecimal(map.get("PRO_PRICE2").toString()).divide(new BigDecimal("100"),2));
		}
		return mapVo;
	}
	/**
	 * 根据产品id得到产品信息
	 * @author 胡鑫
	 * @date 2017年10月23日11:20:49
	 * @param 产品id
	 * @return 返回产品类
	 */
	@Override
	public EhyProduct findByIdProduct(String proId) {
		EhyProduct product = dao.findByIdProduct(proId);//定义产品实体类
		EhyProduct vo = new EhyProduct();
		BeanUtils.copyProperties(product, vo);
		BigDecimal num = new BigDecimal("100");
		vo.setProPrice0(new BigDecimal(product.getProPrice0().toString()).divide(num,2));
		vo.setProFactoryPrice(new BigDecimal(product.getProFactoryPrice().toString()).divide(num,2));
		return vo;
	}

	/**
	 * 根据ID查询商品是否有图片
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	@Override
	public int findAllimg(String proId) {
		
		return dao.findAllimg(proId);
	}
	/**
	 * 根据ID查询商品是否有属性
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	@Override
	public int findAllproperty(String proId) {
		// TODO Auto-generated method stub
		return dao.findAllproperty(proId);
	}
	
	/**
	 * 根据ID查询商品是否有价格，规格
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	@Override
	public int findAllprice(String proId) {
		// TODO Auto-generated method stub
		return dao.findAllprice(proId);
	}
	
	
	
	/**
	 * 根据ID查询商品是否加入展示
	 * @author 刘东
	 * @param proId 产品ID
	 * @return 行数
	 */
	@Override
	public int findAllshow(String proId) {
		// TODO Auto-generated method stub
		return dao.findAllshow(proId);
	}
	

	public List<Map<String, Object>> searchAll(Map<String, Object> map) {
		return dao.searchAll(map);
	}

	@Override
	public ProInfo2 findInfoById(String proId) {
		return dao.findInfoById(proId);
	}

	


}
