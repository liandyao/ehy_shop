/**
 * 
 */
package cn.ehuoyuan.shop.service.shelves;

import java.util.List;

import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyShelves;

/**
 * 类的描述：货架接口
 * @author 罗海兵
 * @dateTime 2018年2月22日 下午3:42:36
 * @version 1.0
 */
public interface EhyShelvesService {
	
	/**
	 * 加入货架
	 * @param shelves 货架的对象模型
	 * @param mem 登录的会员对象
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:43:51
	 * @versions 1.0
	 */
	public int add(EhyShelves shelves);
	
	/**
	 * 删除货架上的某个产品
	 * @param sheId 货架id
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:45:19
	 * @versions 1.0
	 */
	public int delete(String sheId);
	
	/**
	 * 修改货架上的产品信息
	 * @param shelves 货架的对象模型
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:46:13
	 * @versions 1.0
	 */
	public int update(EhyShelves shelves);
	
	/**
	 * 根据货架ID查询货架
	 * @param sheId 货架ID
	 * @return 返回一个货架的对象模型
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:47:06
	 * @versions 1.0
	 */
	public EhyShelves findById(String sheId);
	
	/**
	 * 根据会员id查询货架
	 * @return 返回一个货架的集合
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:48:20
	 * @versions 1.0
	 */
	public List<EhyShelves> findByMbId(String mbId);
	
	
	/**
	 * 根据会员id和商品id查询货架
	 * @return 返回一个货架的集合
	 * @author liandyao
	 * @dateTime 2018年8月22日 下午3:48:20
	 * @versions 1.0
	 */
	public List<EhyShelves> findByMbId(String mbId,String proId);
}
