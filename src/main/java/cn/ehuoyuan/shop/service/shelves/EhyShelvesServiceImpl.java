package cn.ehuoyuan.shop.service.shelves;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.dao.EhyShelvesMapper;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyShelves;

/**
 * 类的描述：货架接口的实现类
 * @author 罗海兵
 * @dateTime 2018年2月22日 下午3:42:36
 * @version 1.0
 */
@Service
public class EhyShelvesServiceImpl implements EhyShelvesService{
	
	@Resource
	private EhyShelvesMapper ehyShelvesMapper;
	
	@Override
	public int add(EhyShelves shelves) {
		return ehyShelvesMapper.insertSelective(shelves);
	}

	@Override
	public int delete(String sheId) {
		return ehyShelvesMapper.deleteByPrimaryKey(sheId);
	}

	@Override
	public int update(EhyShelves shelves) {
		return ehyShelvesMapper.updateByPrimaryKeySelective(shelves);
	}

	@Override
	public EhyShelves findById(String sheId) {
		return ehyShelvesMapper.selectByPrimaryKey(sheId);
	}

	@Override
	public List<EhyShelves> findByMbId(String mbId) {
		return ehyShelvesMapper.findByMbId(mbId);
	}

	@Override
	public List<EhyShelves> findByMbId(String mbId, String proId) {
		
		return ehyShelvesMapper.findByMbIdAndProId(mbId, proId);
	}

}
