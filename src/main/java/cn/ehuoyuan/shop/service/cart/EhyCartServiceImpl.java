package cn.ehuoyuan.shop.service.cart;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.cart.CartVo;
import cn.ehuoyuan.shop.dao.EhyCartMapper;
import cn.ehuoyuan.shop.dao.EhyProductSpecificationPriceMapper;
import cn.ehuoyuan.shop.dao.EhyShareMapper;
import cn.ehuoyuan.shop.domain.EhyCart;
import cn.ehuoyuan.shop.domain.EhyProductSpecificationPrice;

/**
 * 类的描述：购物车的Service实现类
 * @author 罗海兵
 * @dateTime 2017年10月30日 上午9:15:21
 * @version 1.0
 */
@Service
public class EhyCartServiceImpl implements EhyCartService{
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	EhyCartMapper ehyCartMapper;//购物车的DAO
	
	@Resource
	EhyProductSpecificationPriceMapper ehyProductSpecificationPriceMapper;//产品规格价格dao
	
	@Resource
	EhyShareMapper ehyShareMapper;

	@Override
	public int delete(String cartId) {
		return ehyCartMapper.delete(cartId);
	}

	@Override
	public int add(EhyCart cart, String sheId) {
		cart.setCartType(0);
		cart.setCartDatetime(new Date());
		String pspId=cart.getPspId();
		if(!Tools.isEmpty(pspId)){
			logger.info("进来了，产品规格价格ID不为空");
			EhyProductSpecificationPrice psp=ehyProductSpecificationPriceMapper.selectByPrimaryKey(pspId);
			if(Tools.isEmpty(sheId)){
				BigDecimal cartPrice=psp.getPspPrice();
				cart.setCartPrice(cartPrice);
				
			}else{
				BigDecimal cartPrice=psp.getPspFactoryPrice();
				BigDecimal bignum2 = new BigDecimal("1.05");  
				cart.setCartPrice(cartPrice.multiply(bignum2));
			}
			logger.info("加入价格是："+cart.getCartPrice());
		}
		String shaId=cart.getShaId();
		if(!Tools.isEmpty(shaId)){
			logger.info("进来了，分享ID不为空");
			BigDecimal cartPrice=cart.getCartPrice();
			int shaPrice=ehyShareMapper.selectByPrimaryKey(shaId).getShaPrice();
			
			BigDecimal percent=new BigDecimal((100-shaPrice)*0.01);
			cart.setCartPrice(cartPrice.multiply(percent));
			logger.info("分享价格是："+cart.getCartPrice());
		}
		return ehyCartMapper.insertSelective(cart);
	}

	@Override
	public CartVo findById(String cartId) {
		return ehyCartMapper.findByCartId(cartId);
	}

	@Override
	public List<CartVo> findByMbId(String mbId) {
		return ehyCartMapper.findByMbId(mbId);
	}

	@Override
	public int update(EhyCart cart) {
		return ehyCartMapper.updateByPrimaryKeySelective(cart);
	}

	@Override
	public int findCartNum(String mbId) {
		return ehyCartMapper.findCartNum(mbId);
	}

	@Override
	public List<CartVo> findByCartIds(String[] cartIds) {
		return ehyCartMapper.findByCartIds(cartIds);
	}

	@Override
	public CartVo findCart(EhyCart cart) {
		
		return ehyCartMapper.findCart(cart);
	}
	
}
