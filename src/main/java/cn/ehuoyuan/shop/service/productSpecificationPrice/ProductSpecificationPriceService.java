/**
 * 
 */
package cn.ehuoyuan.shop.service.productSpecificationPrice;

import java.util.List;

import cn.ehuoyuan.shop.action.frontShow.ProPrice;
import cn.ehuoyuan.shop.domain.EhyProductSpecificationPrice;

/**
 * 产品规格价格表Service接口
 * @author 欧阳丰
 * @data 2017年10月28日16:23:08
 */
public interface ProductSpecificationPriceService {
	
	 /**
     * 根据产品ID查找该产品的规格价格 
     * @param proId 产品ID
     * @return 规格价格集合
     * @author 欧阳丰
     * @dateTime 2017年10月31日15:45:15
	 * @version 1.0
     */
    public List<EhyProductSpecificationPrice> findAllByProId(String proId);
	
    /**
     * 增加产品规格价格
     * @param record 产品规格价格实体类(除主键ID外，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
	public int insertSelective(EhyProductSpecificationPrice record);
	
	/**
     * 批量增加产品规格价格
     * @param list 产品规格价格集合(集合中所有对象属性值都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月20日11:48:00
	 * @version 1.0
     */
    public int addSpecificationPriceList(List<EhyProductSpecificationPrice> list);
	
    /**
     * 根据产品ID修改该产品的所有规格价格为无效
     * @param proId 产品ID
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月28日17:01:30
	 * @version 1.0
     */
    public int updateIsvaByProId(String proId);
    
    /**
     * 根据产品ID查询产品的所有规格价格
     * @param proId
     * @return 
     * @author 罗海兵
     * @dateTime 2017年11月12日 下午2:33:09
     * @versions 1.0
     */
    public List<ProPrice> findPriceByProId(String proId);
}
