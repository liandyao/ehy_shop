/**
 * 
 */
package cn.ehuoyuan.shop.service.returns;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.shop.domain.EhyReturns;

/**
 * 退货申请
 * @author liandyao
 * @date 2018年7月20日
 * @version 1.0
 */
public interface EhyReturnsService {
	
	EhyReturns selectByOrdId(String ordId);
	
	/**
	 * 新增退货信息
	 * @param ret
	 * @return
	 */
	EhyMessage saveReturns(EhyReturns ret) ;
	 

}
