/**
 * 
 */
package cn.ehuoyuan.shop.service.returns;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.dao.EhyReturnsMapper;
import cn.ehuoyuan.shop.domain.EhyReturns;

/**
 * 退货信息
 * @author liandyao
 * @date 2018年7月20日
 * @version 1.0
 */
@Service
public class EhyReturnsServiceImpl implements EhyReturnsService{

	@Resource
	EhyReturnsMapper ehyReturnsMapper;
	/**
	 * 根据订单添加退货信息
	 */
	@Override
	public EhyMessage saveReturns(EhyReturns ret) {
		
		EhyMessage mes = new EhyMessage();
		
		String ordId = ret.getOrdId() ;//订单ID
		if(Tools.isEmpty(ordId)) {
			mes.setMes("订单号为空,操作失败!");
			mes.setState(EhyMessage.ERROR);
			return mes ;
		}
		EhyReturns returns = ehyReturnsMapper.selectByOrdId(ordId);
		if(returns!=null) { //如果已存在,则不需要更改
			ret.setReId(returns.getReId());
			
			mes.setState(EhyMessage.SUCCESS);
			
			ehyReturnsMapper.updateByPrimaryKeySelective(ret);
			
		}else {
			ehyReturnsMapper.insert(ret);
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes("申请退货成功");
		}
		 
		return mes;
	}
	@Override
	public EhyReturns selectByOrdId(String ordId) {
		 
		return ehyReturnsMapper.selectByOrdId(ordId); 
	}

	
}
