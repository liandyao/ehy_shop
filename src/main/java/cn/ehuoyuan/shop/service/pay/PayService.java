package cn.ehuoyuan.shop.service.pay;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.frontShow.OrderPay;
import cn.ehuoyuan.shop.dao.EhyOrderItemMapper;
import cn.ehuoyuan.shop.dao.EhyOrderMapper;
import cn.ehuoyuan.shop.domain.EhyOrder;
import cn.ehuoyuan.shop.domain.EhyOrderItem;
import cn.ehuoyuan.shop.service.order.OrderItemService;

/**
 * 支付的服务类
 * <p>目前仅提供两种支付方式,WXPAY微信支付 ALIPAY 支付宝支付</p>
 * @author liandyao
 * @date 2017年12月10日
 * @version 1.0
 */
@Service
public class PayService {
	
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	private EhyOrderMapper ehyOrderMapper; //订单接口
	
	@Resource
	private EhyOrderItemMapper ehyOrderItemMapper ;//订单明细
	
	@Resource
	OrderItemService orderItemService ;//订单明细接口
	
	/**
	 * 得到订单对象
	 * @param orderId
	 * @return
	 */
	public OrderPay getOrder(String orderId) {
		OrderPay order = ehyOrderMapper.selectPay(orderId);
		 
		return order ;
	}
	
	/**
	 * 得到订单对象
	 * @param orderId
	 * @return
	 */
	public OrderPay getOrderPay(String orderId) {
		OrderPay order = ehyOrderMapper.selectPay(orderId);
		if(order.getOrdState()!=CommomUtils.STATE_ORDER_OK) { //如果订单不是待付款,则不允许付款
			return null ;
		}
		return order ;
	}
	
 
	/**
	 *  修改订单信息
	 * @param out_trade_no 支付订单号(ehuoyuan方的订单号)
	 * @param trade_no 支付订单号(支付宝或者微信方的订单号)
	 * @param total_amount 支付金额(分)
	 * @param payType 支付类型    WXPAY  ALIPAY 常量
	 * @return
	 */
	public int saveOrder(String out_trade_no, String trade_no, String total_amount,String payType) { 
		/*
		 * 将支付信息加入到订单表中,目前订单表中的状态没有用了,
		 * 都是使用订单明细中的状态,但是付款是一起付的,所以,订单表中的支付类型和支付订单号还是需要用到
		 * 
		 * 以下代码修改订单类型和支付订单号
		 */
		EhyOrder order = ehyOrderMapper.selectByPrimaryKey(out_trade_no);
		//order.setPayType(CommomUtils.PAY_TYPE_ALIPAY);//支付类型
		order.setPayType(payType);//支付类型
		order.setOrdState(CommomUtils.STATE_PAY_OK); //订单已付款
		order.setPayCode(trade_no);	//支付宝订单号
		order.setPayMoney(new BigDecimal(total_amount)); //支付金额
		order.setPayTime(Tools.getCurDateTime()); //支付时间
		ehyOrderMapper.updateByPrimaryKeySelective(order); //修改订单表
		
		/*
		 * 修改订单明细信息
		 * 如果没有优惠券,实际付款价格就是购买的价格
		 * 如果使用了优惠券,则所有的商品需要减去优惠券的平均值
		 * 举例:
		 * 一个订单包含两个商品:商品A 价格 10元  , 商品B 价格 20元 ,总共30元,使用优惠券5元,实际支付15元
		 * 那么,订单明细表中的实际支付金额为商品A 7.5元 ,商品B 17.5元.
		 */
		
		List<EhyOrderItem> items = ehyOrderMapper.selectItenByOrderId(out_trade_no);
		BigDecimal result = new BigDecimal("0");//结果
		
		BigDecimal sumMoney = order.getOrdSumMoney();//实际结算金额 100
		BigDecimal payMoney = order.getPayMoney();//支付金额 90
		BigDecimal cha = sumMoney.subtract(payMoney);//相减的差.如果差为0,则商品不需要平均减价.如果不为0,则需要减去相应的平均价格
		if(cha.intValue()>0) {
			result = cha.divide(new BigDecimal(items.size())); //取平均数 
		}
		//返回成功的行数
		int rows = 0;
		for(EhyOrderItem item : items) {
			logger.info("==============getMxMoney======================"+item.getMxMoney());
			item.setMxMoneyFact(item.getMxMoney().subtract(result)); //减掉优惠券的金额
			logger.info("==============getMxMoneyFact======================"+item.getMxMoneyFact());
			item.setOrdState(CommomUtils.STATE_PAY_OK);
			ehyOrderItemMapper.updateByPrimaryKeySelective(item); //修改结果
			rows++;
		}
		
		/*
		 * 修改订单明细信息结束
		 */ 
		//这里改变支付状态
//		Map<String,Object> map = new HashMap<String,Object>();
//		map.put("ordId", out_trade_no);
//		map.put("state", CommomUtils.STATE_PAY_OK);//已付款,将订单明细里面的状态更新
//		int rows = orderItemService.updateStateByOrderId(map);
		return rows;
	}
}
