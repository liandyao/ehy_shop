/**
 * 
 */
package cn.ehuoyuan.shop.service.invitationCode;
 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.dao.EhyInvitationCodeMapper;
import cn.ehuoyuan.shop.dao.EhyOrderItemMapper;
import cn.ehuoyuan.shop.domain.EhyInvitationCode;
import cn.ehuoyuan.shop.domain.EhyOrderItem;

/**
 * 站点代理人的service实现类
 * @author denglijie
 * @2017年10月11日
 * @version v1.0
 */ 
@Service
public class InvitationCodeServiceImpl implements InvitationCodeService{

	@Resource
	private EhyInvitationCodeMapper ehyInvitationCodeMapper;
	
	@Resource
	private EhyOrderItemMapper ehyOrderItemMapper;
	
	/**
	 * 查询代理人
	 */
	public List<Map> findAll(Map<String, Object> map) {
		return ehyInvitationCodeMapper.findAll(map);
	}

	/**
	 * 查询总行数
	 */
	public int findRowCount(Map<String, Object> map) {
		return ehyInvitationCodeMapper.findRowCount(map);
	}

	/**
	 * 修改或删除
	 */
	public int updateByPrimaryKeySelective(EhyInvitationCode record) {
		
		return ehyInvitationCodeMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 根据邀请码查询邀请码是否存在
	 */
	public int selectByCode(String code) {
		return ehyInvitationCodeMapper.selectByCode(code);
	}

	/**
	 * 根据代理人id查询对象
	 */
	public EhyInvitationCode selectByPrimaryKey(String inviId) {
		return ehyInvitationCodeMapper.selectByPrimaryKey(inviId);
	}

	/**
	 * 增加
	 */
	public int insertSelective(EhyInvitationCode invitationCode) {
		return ehyInvitationCodeMapper.insertSelective(invitationCode);
	}

	@Override
	public int deleteRec(String mbId) {
		// TODO Auto-generated method stub
		return ehyInvitationCodeMapper.deleteRec(mbId);
	}

	/**
	 * 排序
	 */
	public int sortInvitationCode(Integer startNum, Integer endNum, String inviId) {
		return ehyInvitationCodeMapper.sortInvitationCode(startNum, endNum, inviId);
	}

	/**
	 * 查询本月订单的统计图
	 */
	public List<EhyOrderItem> thisOrderMap(Map<String, Object> map) {
		//实例化对象
		Calendar rightNow = Calendar.getInstance();
		String thisMonth= (String) map.get("thisMonth");
		//如果写成年月日的形式的话，要写小d，如："yyyy-MM-dd"
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		try {
			//给rightNow对象里的时间赋值，要计算你想要的月份，改变这里即可
			rightNow.setTime(sdf.parse(thisMonth));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//计算出这个月的天数
		int days = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		List<EhyOrderItem> list = new ArrayList<EhyOrderItem>();
		EhyOrderItem oi = null;
		List<EhyOrderItem> list2 = ehyOrderItemMapper.thisOrderMap(map);
		for(int i =1;i<days+1;i++){ 
			oi = new EhyOrderItem();
			//当循环小于十时
			if(i<10){ 
				oi.setMxDatetime(thisMonth+"-0"+i); 
				oi.setMxNum(0);
			}else{
				oi.setMxDatetime(thisMonth+"-"+i); 
				oi.setMxNum(0);
			}
			//循环list集合
			for(int j=0;j<list2.size();j++){ 
				EhyOrderItem eoi = list2.get(j); 
				String ny = eoi.getMxDatetime(); 
				if(ny.equals(oi.getMxDatetime())){ 
					oi.setMxNum(eoi.getMxNum());
				}
			} 
			list.add(oi);
		} 
		return list; 
	}

	@Override
	public List<EhyOrderItem> oneOrderMap(Map<String, Object> map) {
		//实例化对象
				Calendar rightNow = Calendar.getInstance();
				String oneMonth= (String) map.get("oneMonth");
				//如果写成年月日的形式的话，要写小d，如："yyyy-MM-dd"
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				try {
					//给rightNow对象里的时间赋值，要计算你想要的月份，改变这里即可
					rightNow.setTime(sdf.parse(oneMonth));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				//计算出这个月的天数
				int days = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
				
				List<EhyOrderItem> list = new ArrayList<EhyOrderItem>();
				EhyOrderItem oi = null;
				List<EhyOrderItem> list2 = ehyOrderItemMapper.oneOrderMap(map);
				for(int i =1;i<days+1;i++){ 
					oi = new EhyOrderItem();
					//当循环小于十时
					if(i<10){ 
						oi.setMxDatetime(oneMonth+"-0"+i); 
						oi.setMxNum(0);
					}else{
						oi.setMxDatetime(oneMonth+"-"+i); 
						oi.setMxNum(0);
					}
					
					for(int j=0;j<list2.size();j++){ 
						EhyOrderItem eoi = list2.get(j); 
						String ny = eoi.getMxDatetime(); 
						if(ny.equals(oi.getMxDatetime())){ 
							oi.setMxNum(eoi.getMxNum());
						}
					} 
					list.add(oi);
				} 
				return list; 
	}

	@Override
	public List<EhyOrderItem> selectOrderMap(Map<String, Object> map) {
		List<EhyOrderItem> list = new ArrayList<EhyOrderItem>();
		EhyOrderItem oi = null;
		List<EhyOrderItem> list2 = ehyOrderItemMapper.selectOrderMap(map);
		String year= (String) map.get("year");
		for(int i=1;i<13;i++){
			oi = new EhyOrderItem();
			//当循环小于十时
			if(i<10){ 
				oi.setMxDatetime(year+"-0"+i); 
				oi.setMxNum(0);
			}else{
				oi.setMxDatetime(year+"-"+i); 
				oi.setMxNum(0);
			}
			for(int j=0;j<list2.size();j++){
				EhyOrderItem eoi = list2.get(j); 
				String ny = eoi.getMxDatetime(); 
				if(ny.equals(oi.getMxDatetime())){ 
					oi.setMxNum(eoi.getMxNum());
				}
			} 
			list.add(oi);
		} 
		return list; 
	}

	@Override
	public List<Map> findOrderItem(Map<String, Object> map) { 
		return ehyOrderItemMapper.findOrderItem(map);
	}

	@Override
	public int findOrderItemRowCount(Map<String, Object> map) { 
		return ehyOrderItemMapper.findOrderItemRowCount(map);
	} 
}
