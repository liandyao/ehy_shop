/**
 * 
 */
package cn.ehuoyuan.shop.service.share;

import java.util.List;

import cn.ehuoyuan.shop.action.share.Vo.ShareVo;

/**
 * 类的描述：分享的service
 * @author xym
 * @date 2017年12月26日
 * @version 1.0
 */
public interface ShareService {
	public List<ShareVo> myShare(String mbId);
}
