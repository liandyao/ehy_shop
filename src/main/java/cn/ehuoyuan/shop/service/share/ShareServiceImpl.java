/**
 * 
 */
package cn.ehuoyuan.shop.service.share;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.share.Vo.ShareVo;
import cn.ehuoyuan.shop.dao.EhyShareMapper;

/**
 * 类的描述：分享service的实现类
 * @author xym
 * @date 2017年12月26日
 * @version 1.0
 */
@Service
public class ShareServiceImpl implements ShareService {
	
	@Resource
	EhyShareMapper ehyShareMapper;

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.share.ShareService#myShare()
	 */
	@Override
	public List<ShareVo> myShare(String mbId) {
		//得到会员分享的集合
		List<ShareVo> list=ehyShareMapper.myShare(mbId);
		//循环将分享视图中的商品市场价取出进行分转元操作
		for (ShareVo shareVo : list) {
			shareVo.setProPrice0(Tools.moneyFenToYuan2(shareVo.getProPrice0()));
		}
		return list;
	}

	
}
