/**
 * 
 */
package cn.ehuoyuan.shop.service.comment;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.FileTools;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.comment.vo.ShareDetailsVo;
import cn.ehuoyuan.shop.dao.EhyProductCommentImgMapper;
import cn.ehuoyuan.shop.dao.EhyProductCommentMapper;
import cn.ehuoyuan.shop.dao.EhyShareMapper;
import cn.ehuoyuan.shop.domain.EhyProductComment;
import cn.ehuoyuan.shop.domain.EhyProductCommentImg;
import cn.ehuoyuan.shop.domain.EhyShare;

/**
 * 类的描述：评论Service的实现类
 * @author xym
 * @date 2017年11月3日
 * @version 1.0
 */
@Service
public class CommentServiceImpl implements CommentService{
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	EhyProductCommentMapper ehyProductCommentMapper;
	@Resource
	EhyShareMapper ehyShareMapper;
	@Resource
	EhyProductCommentImgMapper ehyProductCommentImgMapper;
	@Override
	public EhyMessage comment(EhyProductComment comment, EhyShare share, String[] photoDesc, MultipartFile[] photo) {
		EhyMessage mes=new EhyMessage();
		//得到当前时间格式化成字符串存入评论对象中的评论时间中
		comment.setCommTime(Tools.getTimeStr(new Date()));
		//调用评论mapper中的选择添加有只就添加没有值补null并返回增加时所生成的uuid
		int comm=ehyProductCommentMapper.insertSelective(comment);
		/*//由于拦截器自动转换了大数据类型存入数据库时自动转换成了分所以分转元
		share.setShaPrice(new BigDecimal(Tools.moneyFenToYuan2(share.getShaPrice().toString())));*/
		//设置由于分享的收益一开始没有收入设定默认值为0
		share.setShaYield(new BigDecimal(0));
		logger.info(share.getShaYield());
		//增加分享
		int shar=ehyShareMapper.add(share)+comm;
		//循环存入图片
		for(int i=0;i<photo.length;i++){
			//当图片为空时跳过
			if(null!=photo[i].getOriginalFilename()&&!"".equals(photo[i].getOriginalFilename())){
				//建立评论图片对象
				EhyProductCommentImg img=new EhyProductCommentImg();
				//取出反射出来的评论id存入图片模型中
				img.setCommId(comment.getCommId());
				try {
					//将图片上传到本地并将得到返回的路径存入对象
					img.setPath(FileTools.saveFile("comment", photo[i]));
					img.setType(photoDesc[i]);
					ehyProductCommentImgMapper.insertSelective(img);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if(shar>2){
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(EhyMessage.SUCCESS_MES);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}
	@Override
	public Map<String, Object> showList(Map<String, Object> map) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("comment", ehyProductCommentMapper.showList(map));
		return result;
	}
	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.comment.CommentService#findShareByShaId(java.lang.String)
	 */
	@Override
	public ShareDetailsVo findShareByShaId(String shaId) {
		// TODO Auto-generated method stub
		return ehyProductCommentMapper.findShareByShaId(shaId);
	}


}
