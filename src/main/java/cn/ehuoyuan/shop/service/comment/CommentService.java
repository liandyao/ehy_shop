/**
 * 
 */
package cn.ehuoyuan.shop.service.comment;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.shop.action.comment.vo.ShareDetailsVo;
import cn.ehuoyuan.shop.domain.EhyProductComment;
import cn.ehuoyuan.shop.domain.EhyShare;

/**
 * 类的描述：评论的Service
 * @author xym
 * @date 2017年11月3日
 * @version 1.0
 */
public interface CommentService {
	/**
	 * 评论和分享的方法
	 * @author xym
	 * @param comment 评论对象
	 * @param share 分享对象
	 * @param photoDesc 图片的备注数组
	 * @param photo 图片的数组
	 * @return 消息对象
	 */
	public EhyMessage comment(EhyProductComment comment,EhyShare share,String[] photoDesc,@RequestParam MultipartFile[] photo);
	/**
	 * 查询所有评论的方法
	 * @author xym
	 * @param map 查询所需条件集合
	 * @return 查询结果Map集合  所包含的值comment 评论的视图对象（CommentVo）
	 */
	public Map<String, Object> showList(Map<String, Object> map);
	/**
	 * 根据分享编号查询分享详情评论部分信息的方法
	 * @param shaId 分享的编号
	 * @return 分享详情的视图对象
	 * <p>所查询的评论图片为多张图片地址和图片说明合并的字符串字符串
	 * <br>分割图片使用','
	 * <br>分割图片地址和图片说明使用'|'</p>
	 */
    public ShareDetailsVo findShareByShaId(String shaId);
}
