/**
 * 
 */
package cn.ehuoyuan.shop.service.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.CommomUtils;
import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.cart.CartVo;
import cn.ehuoyuan.shop.action.frontShow.OrderPay;
import cn.ehuoyuan.shop.action.order.vo.OrderVo;
import cn.ehuoyuan.shop.action.order.vo.ProdutVo;
import cn.ehuoyuan.shop.action.order.vo.ShareVo;
import cn.ehuoyuan.shop.dao.EhyCartMapper;
import cn.ehuoyuan.shop.dao.EhyOrderItemMapper;
import cn.ehuoyuan.shop.dao.EhyOrderMapper;
import cn.ehuoyuan.shop.dao.EhyProductSpecificationPriceMapper;
import cn.ehuoyuan.shop.dao.EhyShareMapper;
import cn.ehuoyuan.shop.domain.EhyCart;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyOrder;
import cn.ehuoyuan.shop.domain.EhyOrderItem;
import cn.ehuoyuan.shop.domain.EhyReturns;
import cn.ehuoyuan.shop.service.cart.EhyCartService;
import cn.ehuoyuan.shop.service.member.MemberService;
import cn.ehuoyuan.shop.service.returns.EhyReturnsService;

/**
 * 类的描述：订单service的实现类
 * @author xym
 * @date 2017年10月19日
 * @version 1.0
 */
@Service
public class OrderServiceImpl implements OrderService {
	Logger logger = Logger.getLogger(getClass());
	
	@Resource
	private EhyOrderMapper ehyOrderMapper;//订单表的DAO
	
	@Resource
	private EhyOrderItemMapper ehyOrderItemMapper;//订单明细表的DAO
	
	@Resource
	private EhyCartMapper ehyCartMapper; //购物车的DAO
	
	@Resource
	private EhyProductSpecificationPriceMapper ehyProductSpecificationPriceMapper; // 规格价格的DAO
	
	@Resource
	private EhyShareMapper ehyShareMapper; // 分享的DAO
	
	@Resource
	private EhyCartService ehyCartService; // 购物车的Service
	
	@Resource
	private EhyReturnsService ehyReturnsService ;//退货信息
	
	@Resource
	private MemberService memberService ;

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#showAll()
	 */
	@Override
	public Map<String, Object> frontShowAll(Map<String, Object> parameter) {
		//新建一个map对象
		Map<String, Object> map =new HashMap<String, Object>();
		//使用当前页数算出起始行数放入参数中
		/*parameter.put("firstRows", parameter.get("curPage")==null?0:(((int)parameter.get("curPage")-1)*10));*/
		//得到会员订单的集合
		List<OrderVo> orderlist=ehyOrderItemMapper.frontShowAll(parameter);
		//循环将所有的订单查出将内部字符串分割成一个对象
		for (Iterator<OrderVo> iterator = orderlist.iterator(); iterator.hasNext();) {
			OrderVo orderVo = (OrderVo) iterator.next();
			List<ProdutVo> produtlist=new ArrayList<ProdutVo>();
			String zf=orderVo.getProduct();
			String[] products=null;
			//将查询出来的字符串使用","分割成一个数组
			if(zf!=null){
				products=zf.split(",");
				//将分割出来的字符串继续拆分将它包装成一个对象
				//for(int i=0;i<products.length;i++){
					ProdutVo produt=new ProdutVo();
					//使用'|'将字符串进行分割因为使用"|"进行分割会出现问题所以使用"\\"进行转义
					String[] strProduct=zf.split("\\|");
					
					produt.setProId(strProduct[0]);
					produt.setProName(strProduct[1]);
					//produt.setProPhotoPath(strProduct[2]); //2018年7月16日 为了减少查询时间,将sql中的图片表的关联删除,可以节约很多时间
					 
					produt.setPayment(Tools.moneyFenToYuan2(strProduct[3]));
					
					produt.setCost(Tools.moneyFenToYuan2(strProduct[5]));
					produt.setMxNum(strProduct[4]);
					produt.setMxId(strProduct[6]);
					produt.setPayType(strProduct[7]);
					produt.setOrdFreight(strProduct[8]);
					produtlist.add(produt);
					orderVo.setProlist(produtlist);
					//将查询出来的字符串清空
					orderVo.setProduct(null);
				//}
			}
			
		}
		int totalRows=ehyOrderItemMapper.ordersCount(parameter);
		Pages page=new Pages();
		//将当前页数传入
		page.setCurPage(((Pages)parameter.get("page")).getCurPage());
		page.setTotalRows(totalRows);
		map.put("orderList", orderlist);
		map.put("pages", page);
		return map;
	}

	@Override
	public Map<String, Object> backShowAll(Map<String, Object> map, Pages pages, int state) {
		// TODO Auto-generated method stub
		Map<String, Object> hashMap = new HashMap<String, Object>();
		map.put("firstRows", pages.getFirstRows());
		map.put("maxResult", pages.getMaxResult());
		if(state==1){
			hashMap.put("data", ehyOrderItemMapper.overhangShowAll(map));
		}else if(state==2){
			hashMap.put("data", ehyOrderItemMapper.deliveredShowAll(map));
		}else if(state==3){
			hashMap.put("data", ehyOrderItemMapper.receivedShowAll(map));
		}
		hashMap.put("code", 0);
		hashMap.put("count", 10);
		hashMap.put("msg", "");
		return hashMap;
	}

	@Override
	public Map<String, Object> deliveredShowAll(Map<String, Object> map, Pages pages) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> overhangShowAll(Map<String, Object> map, Pages pages) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> receivedShowAll(Map<String, Object> map, Pages pages) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * 退款申请
	 * @param map
	 * @return
	 */
	public EhyMessage saveReturns(Map<String,Object> map) {
		EhyMessage mes=new EhyMessage(); 
		if(map.get("ordId")==null) {
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
			return mes ;
		}
		EhyReturns ret = new EhyReturns();
		ret.setReId(Tools.getRandomString());
		ret.setOrdId(map.get("ordId").toString());
		ret.setIsva(CommomUtils.ISVA_YES);
		ret.setReDesc(map.get("content").toString()); //退款说明
		ret.setReTime(Tools.getCurDateTime());
		ehyReturnsService.saveReturns(ret) ;
		//保存订单状态
		ehyOrderItemMapper.receipt(map);
		mes.setMes(EhyMessage.SUCCESS_MES);
		mes.setState(EhyMessage.SUCCESS);
		return mes ;
	}
	

	/**
	 * 退款申请确认
	 * @param map
	 * @return
	 */
	public EhyMessage saveReturnsOK(Map<String,Object> map) {
		EhyMessage mes=new EhyMessage(); 
		if(map.get("ordId")==null) {
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
			return mes ;
		}
		EhyReturns ret = new EhyReturns(); 
		ret.setOrdId(map.get("ordId").toString());
		ret.setIsva(CommomUtils.ISVA_YES);
		ret.setReCode(Tools.moneyYuanToFen(map.get("money").toString())); //退款金额 
		ehyReturnsService.saveReturns(ret) ;
		
		//将订单的实际支付金额减少,如果已经退款,则实际支付金额为0,如果只退了部分,则减去已经退款的金额
		Map<String,Object> mapOrd = new HashMap<>();
		
		mapOrd.put("state", CommomUtils.STATE_ORDER_TUIKUAN_OK);
		mapOrd.put("ordId", map.get("ordId").toString());
		EhyOrder ord = this.findById(map.get("ordId").toString()) ;
		//logger.info(ord.getPayMoney()+"     退款金额 :"+map.get("money").toString());
		if(ord.getPayMoney().intValue()>0) {
			//将之前的实际付款减去已经退款的
			BigDecimal money = ord.getPayMoney().subtract(new BigDecimal(Tools.moneyYuanToFen(map.get("money").toString()))) ;
			mapOrd.put("money", money);
			//logger.info("减掉之后的金额"+money);
			ehyOrderMapper.updateTuiKuan(mapOrd);
		}
		//保存订单状态
		ehyOrderItemMapper.receipt(map);
		mes.setMes(EhyMessage.SUCCESS_MES);
		mes.setState(EhyMessage.SUCCESS);
		return mes ;
	}
	 
	
	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#receipt(java.util.Map)
	 */
	@Override
	public EhyMessage receipt(Map<String, Object> map) {
		//map.put("state",CommomUtils.STATE_OK);
		EhyMessage mes=new EhyMessage();
		//调用方法并取得返回值做比较。执行行数大于0则返回成功消息。否则返回失败消息
		if(ehyOrderItemMapper.receipt(map)>0){
			mes.setMes(EhyMessage.SUCCESS_MES);
			mes.setState(EhyMessage.SUCCESS);
		}else{
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}
		return mes;
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#findById()
	 */
	@Override
	public Map<String, Object> findByIdShare(Map<String, Object> parameter) {
		//新建消息对象
		EhyMessage mes=new EhyMessage();
		//新建返回出去的对象
		Map<String, Object> map=new HashMap<String,Object>();
		//得到根据订单订单详情编号查询出来的对象
		ShareVo share=(ShareVo)ehyOrderItemMapper.findById(parameter);
		if(share==null){
			mes.setMes(EhyMessage.ERROR_MES);
			mes.setState(EhyMessage.ERROR);
		}else{
			if(!Tools.isEmpty(share.getProMaxPrice())&&!Tools.isEmpty(share.getProMinPrice())&&!Tools.isEmpty(share.getMoneyFact())){
				//将查询出来的厂家最高直销价进行分转元放回分享对象中
				share.setProMaxPrice(Tools.moneyFenToYuan2(share.getProMaxPrice()));
				//将查询出来的厂家最低直销价进行分转元放回分享对象中
				share.setProMinPrice(Tools.moneyFenToYuan2(share.getProMinPrice()));
				//将实际付款的价格进行分转元再放回对象中
				share.setMoneyFact(Tools.moneyFenToYuan2(share.getMoneyFact()));
				//设置成功消息的信息
				mes.setMes(EhyMessage.SUCCESS_MES);
				mes.setState(EhyMessage.SUCCESS);
			}else{
				//设置失败消息
				mes.setMes("订单完成之后才能分享^_^");
				mes.setState(EhyMessage.ERROR);
			}
			
		}
		
		map.put("share", share);
		
		
		map.put("mes", mes);
		return map;
	}

	@Override
	public EhyMessage insertSelective(EhyOrder order, List<EhyOrderItem> orderItemlist, String[] cartIds) {
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("ord", order);
		map.put("orderItemlist", orderItemlist);
		map.put("cartIds", cartIds);
		int rows=ehyOrderMapper.insertSelective(map);
		return new EhyMessage(order.getOrdId(),rows);
	}

	@Override
	public OrderPay selectPay(String ordId) {
		return ehyOrderMapper.selectPay(ordId);
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#findById(java.lang.String)
	 */
	@Override
	public EhyOrder findById(String orderId) {
		return ehyOrderMapper.selectByPrimaryKey(orderId);
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#deleteOrderItem(java.lang.String)
	 */
	@Override
	public EhyMessage deleteOrderItem(String mxId) {
		//新建消息类的对象
		EhyMessage mes=new EhyMessage();
		//判断该订单行数是否大于以如果大于一则使用普通方法删除订单。否则连同订单一起删除
		if(ehyOrderItemMapper.OrderItemNum(mxId)>1){
			//执行删除订单明细的方法根据方法得到的结果赋值
			if(ehyOrderItemMapper.deleteOrderItem(mxId)>0){
				mes.setState(EhyMessage.SUCCESS);
				mes.setMes(EhyMessage.SUCCESS_MES);
			}else{
				mes.setState(EhyMessage.ERROR);
				mes.setMes("订单不存在，请刷新后重试");
			}
			
		}else{
			//执行在只有一条明细的情况下删除订单明细的方法根据方法得到的结果赋值
			if(ehyOrderItemMapper.deleteOneOrderItem(mxId)>0){
				mes.setState(EhyMessage.SUCCESS);
				mes.setMes(EhyMessage.SUCCESS_MES);
			}else{
				mes.setState(EhyMessage.ERROR);
				mes.setMes("订单不存在，请刷新后重试");
			}
		}
		return mes;
		
	}

	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderService#deleteOrder(java.lang.String)
	 */
	@Override
	public EhyMessage deleteOrder(String ordId) {
		//新建消息类的对象
		EhyMessage mes=new EhyMessage();
		//执行删除订单的方法根据方法得到的结果赋值
		if(ehyOrderItemMapper.deleteOrder(ordId)>0){
			mes.setState(EhyMessage.SUCCESS);
			mes.setMes(EhyMessage.SUCCESS_MES);
		}else{
			mes.setState(EhyMessage.ERROR);
			mes.setMes(EhyMessage.ERROR_MES);
		}
		return mes;
	}

	@Override
	public EhyMessage addOrder(EhyCart cart, EhyMember mem) {
		
		String[] cartId = {cart.getCartId()};
		String mbName=mem.getMbName();//取出会员姓名
		String mbId=mem.getMbId();//取出会员ID
		String mbLogin=mem.getMbLogin();//取出登陆账号
		String mxRemark="";//取出订单备注(客户留言)
		String createTime=Tools.getCurDateTime();//得到下单时间
		
		List<EhyOrderItem> orderItemList=new ArrayList<EhyOrderItem>();//创建订单明细对象的集合
		BigDecimal ordSumMoney=new BigDecimal("0.00");//创建一个订单总金额的BigDecimal对象
		BigDecimal ordSum=new BigDecimal(0);//创建一个订单总数量的BigDecimal对象
		BigDecimal ordFreight=new BigDecimal("0.00");//创建一个订单运费的BigDecimal对象
		String createordCode=Tools.getDateOrderNo();//订单编号
		List<CartVo> cartList=ehyCartService.findByCartIds(cartId);//根据前台传来的购物车ID数组查询购物车集合
		for(int i=0;i<cartList.size();i++){//循环购物车集合
			CartVo cartVo=cartList.get(i);//取出购物车对象
			int cartNum=cart.getCartNum();//取出购买数量
			String cartPrice=cartVo.getCartPrice();//取出购买价格(元)
			String cartMoney=cartVo.getMoney();//取出运费(元)
			String proId=cartVo.getProId();//取出产品ID
			String proName=cartVo.getProName();//取出产品名称
			String stId=cartVo.getStId();//取出站点ID
			String stName=cart.getStName();//取出站点名称
			BigDecimal mxPrice=new BigDecimal(Tools.moneyYuanToFen(cartPrice));//建立订单明细购买价格(分)的BigDecimal型变量
			BigDecimal money=new BigDecimal(Tools.moneyYuanToFen(cartMoney));//建立订单明细运费(分)的BigDecimal型变量
			BigDecimal num = new BigDecimal(cartNum); //建立购买数量的BigDecimal型变量
			BigDecimal mxMoney=mxPrice.multiply(num).setScale(2);//计算出订单明细的总金额，结果保留2位小数
			ordSumMoney=ordSumMoney.add(mxMoney).setScale(2);//将订单明细的总金额累加起来，结果保留2位小数
			ordSum=ordSum.add(num);//将订单明细的购买数量累加起来
			logger.info("订单明细的运费, 分 "+money+", 元："+cartMoney);
			ordFreight = ordFreight.add(money);//将订单明细的运费累加起来
			logger.info("订单的运费, 分 "+ordFreight);
			String ordCode=createordCode;//定义一个订单号冗余
			if(i<100){
				ordCode+="00"+i;
			}else if(i<10){
				ordCode+="0"+i;
			}else{
				ordCode+=i;
			}
			String mxId=UUID.randomUUID().toString().replaceAll("-", "");//生成一个UUID唯一标示符,并且去掉'-'符号
			EhyOrderItem orderItem=new EhyOrderItem();//创建一个订单明细对象
			orderItem.setOrdId(createordCode);//给订单明细的订单ID赋值
			orderItem.setMxId(mxId);//给订单明细的ID赋值
			orderItem.setOrdCode(ordCode);//给订单明细的销售单号赋值
			orderItem.setMxDatetime(createTime);//给订单明细的下单时间赋值
			orderItem.setMxNum(cartNum);//给订单明细的购买数量赋值
			orderItem.setMxPrice(mxPrice);//给订单明细的购买价格赋值
			orderItem.setMxMoney(mxMoney);//给订单明细的总金额赋值
			orderItem.setMxRemark(mxRemark);//给订单明细的备注赋值
			orderItem.setMbId(mbId);//给订单明细的会员ID赋值
			orderItem.setMbName(mbName);//给订单明细的会员名称赋值
			//订单ID待生成
			orderItem.setProId(proId);//给订单明细的产品ID赋值
			orderItem.setOper(mbLogin);//给订单明细的操作人赋值
			orderItem.setOrdFreight(money);//给订单明细的运费赋值
			orderItem.setStId(stId);//给订单明细的站点ID赋值
			orderItem.setStName(stName);//给订单明细的站点名称赋值
			String newProName=getNewProName(proName, cart.getSpNames());//得到一个由规格值和产品名称组合而成的新名称
			orderItem.setProName(newProName);//给订单明细的产品名称赋值
			
			//2017年12月10日 liandyao增加
			orderItem.setOrdState(CommomUtils.STATE_ORDER_OK);//订单确认,待付款
			
			orderItemList.add(orderItem);//将订单明细对象添加到订单明细集合
		}
		
		EhyOrder order=new EhyOrder();//创建订单对象
		order.setOrdId(createordCode);//给订单ID赋值
		order.setOrdCode(createordCode);//给订单编号赋值
		order.setOrdTime(createTime);//给确认订单时间赋值//给订单赋值
		order.setOrdSumMoney(ordSumMoney);//给订单总金额赋值
		order.setOrdSum(ordSum);//给订单总数量赋值
		//BeanUtils.copyProperties(orderVo, order);//将订单vo模型中的数据Copy到订单对象中
		order.setOrdEndtime(Tools.getTimeToFuture("H", 2));//给订单支付截止时间赋值
		order.setOrdFreight(ordFreight);//给订单运费赋值
		logger.info("订单的总运费是："+ordFreight);
		order.setOper(mem.getMbLogin());//给订单操作人赋值
		
		//2017年12月10日 liandyao增加
		order.setOrdState(CommomUtils.STATE_ORDER_OK); //订单确认,待付款
		
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("ord", order);
		map.put("orderItemlist", orderItemList);
		map.put("cartIds", cartId);
		int row=ehyOrderMapper.insertSelective(map);
		
		logger.info("==========会员等级========"+mem.getLevelId());
		if("1".equals(mem.getLevelId())) { //如果是普通会员,直接升级为银牌会员
			EhyMember em = new EhyMember();
			em.setMbId(mem.getMbId());
			em.setLevelId("2");
			memberService.updateByPrimaryKeySelective(em);
		}
		
		String mes = "cartId="+cart.getCartId()+"&ordId="+order.getOrdId();
		return new EhyMessage(mes,row);
	}
	

	/**
	 * 得到一个新的产品名称
	 * @param proName 原来的产品名称
	 * @param spNames 规格组合名称
	 * @return 返回一个由产品名称和规格值组合后的新名称
	 * @author 罗海兵
	 * @dateTime 2018年1月2日 下午2:25:15
	 * @versions 1.0
	 */
	private static String getNewProName(String proName, String spNames){
		String[] strArr = spNames.split(",");
		StringBuffer strBuffer =  new StringBuffer(proName);
		for(int i=0;i<strArr.length;i++){
			strBuffer.append("_"+strArr[i]);
		}
		return strBuffer.toString();
	}

	@Override
	public int update(EhyOrder ehyOrder, EhyOrderItem ehyOrderItem) {
		int rows = ehyOrderMapper.updateByPrimaryKeySelective(ehyOrder);
		int rows2 = ehyOrderItemMapper.updateByOrdIdSelective(ehyOrderItem);
		return rows+rows2;
	}

	 
}
