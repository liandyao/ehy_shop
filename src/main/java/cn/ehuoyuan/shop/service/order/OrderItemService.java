/**
 * 
 */
package cn.ehuoyuan.shop.service.order;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyOrderItem;

/**
 * 类的描述：
 * @author zengren
 * @date 2017年10月27日
 * @version 1.0
 */
public interface OrderItemService {
	
	/**
	 * 显示
	 * @param map
	 * @param state 状态，1：待发货订单，2：已发货订单，3：已收货订单。
	 * @return
	 */
	Map<String, Object> showList(Map<String, Object> map, Integer state);
	
	/**
	 * 资金结算
	 * @param map 
	 * @return 
	 */
	int jsMoney(Map<String, Object> map);
	
	/**
	 * 发货
	 * @param map 
	 * @param arr 订单明细id的数组
	 * @return
	 */
	int deliverGoods(Map<String, Object> map,String[] arr);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	String findMoney(Map<String, Object> map);
	
	/**
	 * 根据订单明细编号查询订单明细的方法
	 * @author xym
	 * @param mxId 订单明细编号
	 * @return 订单明细对象
	 */
	public EhyOrderItem findById(String mxId);
	
	/**
     * 根据订单id修改订单明细状态
     * @param map 
     * @description map.ordId 订单id
     * @description map.ordState 订单状态
     * @return
     */
    int updateStateByOrderId(Map<String, Object> map);
    
    /**
     * 根据明细id修改订单明细状态
     * @param map 
     * @description map.mxId 明细id
     * @description map.ordState 订单状态
     * @return
     */
    int updateStateByItemId(Map<String, Object> map);
    
    /**
     * 根据订单id查询订单明细
     * @param ordId 订单ID
     * @return
     */
    List<EhyOrderItem> selectItenByOrderId(String ordId);
    
    /**
     * 修改订单价格
     * @param map
     * @return
     */
    int updateMoney(Map<String, Object> map);
	
}
