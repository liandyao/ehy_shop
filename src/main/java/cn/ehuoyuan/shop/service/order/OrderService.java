/**
 * 
 */
package cn.ehuoyuan.shop.service.order;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import cn.ehuoyuan.common.EhyMessage;
import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.shop.action.frontShow.OrderPay;
import cn.ehuoyuan.shop.domain.EhyCart;
import cn.ehuoyuan.shop.domain.EhyMember;
import cn.ehuoyuan.shop.domain.EhyOrder;
import cn.ehuoyuan.shop.domain.EhyOrderItem;

/**
 * 类的描述：订单的Service接口
 * @author xym
 * @date 2017年10月19日
 * @version 1.0
 */
public interface OrderService {
	/**
	 * 根据订单编号查询订单的方法
	 * @param orderId 订单的编号
	 * @return 订单对象
	 */
	public EhyOrder findById(String orderId);
	/**
	 * 根据编号查询该订单分享所需要的参数
	 * @author xym 
	 * @param map 所需参数 mbId 会员的编号  mxId订单明细的编号
	 * @return 返回一个map对象
	 */
	public Map<String, Object> findByIdShare(Map<String, Object> map);
	/** 
	 * 前端登录显示
	 * @param 会员编号
	 * @param 分页对象
	 * @return 会员订单集合与状态
	 */
	public Map<String, Object> frontShowAll(Map<String, Object> map);
	
	/**
	 * 后台订单显示
	 * @param map
	 * @param pages
	 * @param state 状态，1：待发货订单，2：已发货订单，3：已收货订单。
	 * @return
	 */
	public Map<String, Object> backShowAll(Map<String, Object> map, Pages pages, int state);
	
	/**
	 * 显示全部已发货订单
	 * @param map
	 * @param pages
	 * @return
	 */
	public Map<String, Object> deliveredShowAll(Map<String, Object> map, Pages pages);
	
	/**
	 * 显示全部待发货订单
	 * @param map
	 * @param pages
	 * @return
	 */
	public Map<String, Object> overhangShowAll(Map<String, Object> map, Pages pages);
	
	/**
	 * 显示全部已收货订单
	 * @param map
	 * @param pages
	 * @return
	 */
	public Map<String, Object> receivedShowAll(Map<String, Object> map, Pages pages);
	/**
	 * 修改订单状态的方法
	 * @param map
	 * @return
	 */
	public EhyMessage receipt(Map<String, Object> map);
	
	/**
     * 增加订单和订单明细的集合，并且将已购买的购物车设置为无效
     * @param order订单对象 
     * @param orderItemlist 订单明细对象的集合
     * @param cartIds 购物车ID的数组
     * @return 返回消息类对象
     * @author 罗海兵
     * @dateTime 2017年11月20日 上午11:33:26
     * @versions 1.0
     */
	public EhyMessage insertSelective(EhyOrder order, List<EhyOrderItem> orderItemlist, String[] cartIds);
	
	/**
     * 支付页面查询
     * @param ordId 订单ID
     * @return 返回一个订单支付页面的VO模型
     * @author 罗海兵
     * @dateTime 2017年11月21日 上午9:35:05
     * @versions 1.0
     */
	public OrderPay selectPay(String ordId);
	/**
	 * 删除订单明细的方法
	 * @param mxId 明细编号
	 * @return 消息类
	 */
	public EhyMessage deleteOrderItem(String mxId);
	/**
	 * 删除订单的方法
	 * @param ordId 订单编号
	 * @return 消息类
	 */
	public EhyMessage deleteOrder(String ordId);
	
	/**
	 * 直接购买
	 * @param cart 封装了产品信息的购物车临时对象
	 * @param mem 登陆账号
	 * @return 消息类
	 * @author 罗海兵
	 * @dateTime 2018年7月1日 下午3:56:30
	 * @versions 1.0
	 */
	public EhyMessage addOrder(EhyCart tempCart, EhyMember mem);
	
	/**
	 * 修改收货地址和客户备注
	 * @param ehyOrder 封装修改字段和修改条件的订单对象
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年7月2日 上午12:17:25
	 * @versions 1.0
	 */
	public int update(EhyOrder ehyOrder, EhyOrderItem ehyOrderItem);
	 
	/**
	 * 退款申请
	 * @param map
	 * @return
	 */
	public EhyMessage saveReturns(Map<String,Object> map);
	
	/**
	 * 退款申请确认
	 * @param map
	 * @return
	 */
	public EhyMessage saveReturnsOK(Map<String, Object> map);
}
