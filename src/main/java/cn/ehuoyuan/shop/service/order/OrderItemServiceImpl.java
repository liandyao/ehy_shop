/**
 * 
 */
package cn.ehuoyuan.shop.service.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.dao.EhyOrderItemMapper;
import cn.ehuoyuan.shop.dao.EhyOrderMapper;
import cn.ehuoyuan.shop.domain.EhyOrderItem;

/**
 * 类的描述：订单Service
 * @author zengren
 * @date 2017年10月27日
 * @version 1.0
 */
@Service
public class OrderItemServiceImpl implements OrderItemService{
	
	@Resource
	private EhyOrderMapper ehyOrderMapper;
	@Resource
	private EhyOrderItemMapper ehyOrderItemMapper;
	
	@Override
	public Map<String, Object> showList(Map<String, Object> map, Integer state) {
		Map<String, Object> hashMap = new HashMap<String, Object>();
		List<?> list = null;
		int count = 0;
		if(state==1){//状态为1，显示待发货订单
			count = ehyOrderMapper.showOverhangOrderCount(map);
			list = ehyOrderMapper.showOverhangOrder(map);
		}else if(state==2){//状态为2，显示所有订单
			count = ehyOrderMapper.showDeliveredOrderCount(map);
			list = ehyOrderMapper.showDeliveredOrder(map);
		}else if(state==3){//状态为3，显示售后订单
			count = ehyOrderMapper.showReceivedOrderCount(map);
			list = ehyOrderMapper.showReceivedOrder(map);
		}else if(state==4){//状态为4，显示代付款订单
			count = ehyOrderMapper.showEditOrderCount(map);
			list = ehyOrderMapper.showEditOrder(map);
		}
		hashMap.put("data", list);
		hashMap.put("count", count);
		hashMap.put("msg", "");
		hashMap.put("code", 0);
		return hashMap;
	}
	
	
	@Override
	public int jsMoney(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ehyOrderMapper.jsMoney(map);
	}

	@Override
	public int deliverGoods(Map<String, Object> map, String[] arr) {
		int rows = 0;
		if(arr!=null){
			for(int i=0; i<arr.length; i++){
				map.put("mxId", arr[i]);//发货的明细ID
				map.put("fhTime", Tools.getCurDateTime());//设置发货时间
				rows += ehyOrderMapper.deliverGoods(map);
			}
		}
		return rows;
	}


	@Override
	public String findMoney(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ehyOrderMapper.findMoney(map);
	}


	/* (non-Javadoc)
	 * @see cn.ehuoyuan.shop.service.order.OrderItemService#findById(java.lang.String)
	 */
	@Override
	public EhyOrderItem findById(String mxId) {
		return ehyOrderItemMapper.selectByPrimaryKey(mxId);
	}


	@Override
	public int updateStateByOrderId(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ehyOrderMapper.updateStateByOrderId(map);
	}


	@Override
	public int updateStateByItemId(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ehyOrderMapper.updateStateByItemId(map);
	}


	@Override
	public List<EhyOrderItem> selectItenByOrderId(String ordId) {
		// TODO Auto-generated method stub
		return ehyOrderMapper.selectItenByOrderId(ordId);
	}


	@Override
	public int updateMoney(Map<String, Object> map) {
		// TODO Auto-generated method stub
		if(Integer.valueOf(map.get("state").toString())==1)return ehyOrderMapper.updateSumMoney(map);else if(Integer.valueOf(map.get("state").toString())==2)return ehyOrderMapper.updateFreight(map);
		return 0;
	}
	
}
