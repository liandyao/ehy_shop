/**
 * 
 */
package cn.ehuoyuan.shop.service.proshow;


import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.frontShow.ProshowVo;
import cn.ehuoyuan.shop.domain.EhyProShow;

/**
 * 产品展示service
 * @author dong
 * @da2017年10月18日
 * @version 1.0
 */
public interface ProshowService {
	
	
	/**
	 * 增加方法
	 * @param record
	 * @return
	 */
	 int insertSelective(EhyProShow record);
	 
	 /**
	  * 查询所有
	  * @param map
	  * @return
	  */
	 List<EhyProShow> findAllshow(Map<String, Object> map);

	 
 /**
	* 排序
	* @return
	*/
	int sortModule(int start, int end, String showId);
	 
	 /**
	  * 总行数
	  * @param map
	  * @return
	  */
	 int findRowCount(Map<String, Object> map);
	 
	 
	 /**
	  * 根据ID查询
	  * @param showId
	  * @return
	  */
	EhyProShow selectByPrimaryKey(String showId);
	
	/**
	 * 修改或删除方法
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeySelective(EhyProShow record);
	 /**
	  * @title 查询全部
	  * @description 查询全部的商品展示列表数据 
	  * @return 返回一个商品展示列表的模型的集合
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午9:34:15
	  * @versions 1.0
	  */
	 public List<EhyProShow> findAll(EhyProShow proShow);
	 
	 /**
	  * @title 查询总行数
	  * @description  根据查询条件查询总行数
	  * @param proShow 封装了查询条件的商品展示列表的模型对象
	  * @return 返回总行数
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午10:02:55
	  * @versions 1.0
	  */
	 public int findTotalRows(EhyProShow proShow);
	 
	 /**
	  * @title 查询全部
	  * @description 查询全部的商品展示列表数据 
	  * @return 返回一个商品展示列表的模型的集合
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午9:34:15
	  * @versions 1.0
	  */
	 public List<Map<String, Object>> showAll(String stId);

	 /**
	  * 根据展示类型查询所有的展示的商品
	  * @param stId
	  * @param showType
	  * @return
	  */
	Map<String, Object> showAllByType(String stId, String showType,String keyWord);
	 
    
    
}
