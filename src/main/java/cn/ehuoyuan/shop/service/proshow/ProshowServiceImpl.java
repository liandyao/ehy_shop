/**
 * 
 */
package cn.ehuoyuan.shop.service.proshow;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.common.Tools;
import cn.ehuoyuan.shop.action.frontShow.ProshowVo;
import cn.ehuoyuan.shop.dao.EhyProShowMapper;
import cn.ehuoyuan.shop.dao.EhyProductImgMapper;
import cn.ehuoyuan.shop.dao.EhyProductSpecificationValueMapper;
import cn.ehuoyuan.shop.domain.EhyProShow;

/**
 * 产品展示service接口
 * @author dong
 * @da2017年10月18日
 * @version 1.0
 */
@Service
public class ProshowServiceImpl implements ProshowService{

	@Resource
	private EhyProShowMapper ehyProShowMapper;
	
	@Resource
	private EhyProductImgMapper ehyProductImgMapper;
	
	@Resource
	private EhyProductSpecificationValueMapper ehyProductSpecificationValueMapper;
	
	/**
	 * 增加
	 */
	@Override
	public int insertSelective(EhyProShow record) {
		
		return ehyProShowMapper.insertSelective(record);
	}

	/**
	 * 查询所有
	 */
	@Override
	public List<EhyProShow> findAllshow(Map<String, Object> map) {
		List<EhyProShow> list=ehyProShowMapper.findAllshow(map);
		return list;
	}

	/**
	 * 总行数
	 */
	@Override
	public int findRowCount(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return ehyProShowMapper.findRowCount(map);
	}

	/**
	 * 根据ID查询
	 */
	@Override
	public EhyProShow selectByPrimaryKey(String showId) {
		EhyProShow list=ehyProShowMapper.selectByPrimaryKey(showId);
		return list;
	}

	/**
	 * 修改或删除方法
	 */
	@Override
	public int updateByPrimaryKeySelective(EhyProShow record) {
		
		return ehyProShowMapper.updateByPrimaryKeySelective(record);
	}

	
	/**
	 * 排序
	 */
	@Override
	public int sortModule(int start, int end, String showId) {
		// TODO Auto-generated method stub
		return ehyProShowMapper.sortModule(start, end, showId);
	}
	
	@Override
	public List<EhyProShow> findAll(EhyProShow proShow) {
		return ehyProShowMapper.findAll(proShow);
	}

	@Override
	public int findTotalRows(EhyProShow proShow) {
		return ehyProShowMapper.findTotalRows(proShow);
	}

	@Override
	public List<Map<String, Object>> showAll(String stId) {
		List<Integer> typeList=ehyProShowMapper.findTypeAll(stId);
		List<Map<String, Object>> mapList=new ArrayList<Map<String, Object>>();
		for(int i=0;i<typeList.size();i++){
			Integer type=typeList.get(i);
			Map<String, Object> mapParam = new HashMap<>();
			mapParam.put("stId", stId);
			mapParam.put("showType", type);
			List<ProshowVo> showVoList=ehyProShowMapper.showAllByType(mapParam);
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("type", getTypeText(type));
			map.put("list", showVoList);
			mapList.add(map);
		}
		return mapList;
	}
	
	@Override
	public  Map<String, Object>  showAllByType(String stId,String showType,String keyWord) { 
		Map<String, Object> mapParam = new HashMap<>();
		mapParam.put("stId", stId);
		mapParam.put("showType", Integer.valueOf(showType));
		if(!Tools.isEmpty(keyWord))
			mapParam.put("keyWord", keyWord);
		List<ProshowVo> showVoList=ehyProShowMapper.showAllByType(mapParam);
		Map<String, Object> map=new HashMap<String, Object>(); 
		map.put("list", showVoList);
		 return map ;
	}
	
	/**
	 * 得到展示类型的文字说明
	 * @param showType
	 * @return 
	 * @author 罗海兵
	 * @dateTime 2018年2月2日 上午10:44:56
	 * @versions 1.0
	 */
	private String getTypeText(int showType){
		if(showType == 1){
			return "推荐商品";
		}
		if(showType == 2){
			return "清仓商品";
		}
		if(showType == 3){
			return "特价商品";
		}
		if(showType == 4){
			return "热卖商品";
		}
		return null;
	}
	
}
