/**
 * 
 */
package cn.ehuoyuan.shop.service.productQc;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ehuoyuan.shop.action.frontShow.ProInfo2;
import cn.ehuoyuan.shop.dao.EhyProductQcMapper;
import cn.ehuoyuan.shop.domain.EhyProductQC;

/**
 * 清仓产品service实现接口
 * @author 罗海兵
 * @data 2018年10月09日
 */
@Service
public class ProductQcServiceImpl implements ProductQcService{
	@Resource
	EhyProductQcMapper qcDao;

	@Override
	public int deleteByProId(String proId) {
		return qcDao.deleteByProId(proId);
	}

	
	@Override
	public int add(EhyProductQC productQC) {
		return qcDao.add(productQC);
	}


	@Override
	public int addBatch(List<Map<String, String>> argList) {
		return qcDao.addBatch(argList);
	}


	@Override
	public List<ProInfo2> searchProductQc(Map<String, Object> arg) {
		return qcDao.searchProductQc(arg);
	}


	@Override
	public int update(EhyProductQC productQC) {
		return qcDao.update(productQC);
	}

}
