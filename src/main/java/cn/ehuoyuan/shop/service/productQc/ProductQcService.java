/**
 * 
 */
package cn.ehuoyuan.shop.service.productQc;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.frontShow.ProInfo2;
import cn.ehuoyuan.shop.domain.EhyProductQC;

/**
 * 产品service接口
 * @author 欧阳丰
 * @data 2017年10月10日
 */
public interface ProductQcService {
	
	/**
     * 删除
     * @param proId 产品ID
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
    public int deleteByProId(String proId);
	
    /**
     * 增加
     * @param productQC 清仓产品的模型
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
    public int add(EhyProductQC productQC);
    
    /**
     * 批量增加
     * @param argList 参数集合
     * @return 成功返回1，失败返回0
     * @author 罗海兵
	 * @dateTime 2018年10月09日 22:27:20
	 * @version 1.0
     */
    public int addBatch(List<Map<String, String>> argList);
    
    /**
	 * 查询更多产品
	 * @param arg 封装了查询条件的Map对象
	 * @return 返回一个Map对象的集合
	 * @author 罗海兵
	 * @dateTime 2018年10月12日 
	 * @versions 1.0
	 */
	public List<ProInfo2> searchProductQc(Map<String, Object> arg);
	
	
	/**
	 * 修改
	 * @param productQC 清仓产品对象
	 * @return 成功返回1，失败返回0
	 * @author 罗海兵
	 * @dateTime 2018年10月17日 
	 * @versions 1.0
	 */
	public int update(EhyProductQC productQC);
    
}
