/**
 * 
 */
package cn.ehuoyuan.shop.service.productSpecificationValue;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductSpecificationValue;

/**
 * 产品规格表Service接口
 * @author 欧阳丰
 * @data 2017年10月28日16:23:08
 */
public interface ProductSpecificationValueService {
	
	/**
     * 根据产品ID查找该产品的规格
     * @param proId 产品ID
     * @return 产品规格集合
     * @author 欧阳丰
     * @dateTime 2017年10月31日15:45:15
	 * @version 1.0
     */
    public List<EhyProductSpecificationValue> findAllByProId(String proId);
	
    /**
     * 增加产品规格
     * @param record 产品规格实体类(除主键ID外，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
    public int insertSelective(EhyProductSpecificationValue record);
    
    /**
     * 批量增加产品规格
     * @param list 产品规格集合(集合中所有对象属性值都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月20日09:54:53
	 * @version 1.1
     */
    public int addSpecificationValueList(List<EhyProductSpecificationValue> list);
    
    /**
     * 根据产品ID修改该产品的所有规格为无效
     * @param proId 产品ID
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月28日17:01:30
	 * @version 1.0
     */
    public int updateIsvaByProId(String proId);
    
    /**
     * 根据产品ID查询产品详情的规格参数 
     * @param proId 产品ID
     * @return 返回一个Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月27日 上午10:27:21
     * @versions 1.0
     */
    List<Map<String, Object>> findByProId(String proId);
}
