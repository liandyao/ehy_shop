package cn.ehuoyuan.shop.service.productAttribute;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductAttribute;

/**
 * 产品属性service接口
 * @author 欧阳丰
 * @dataTime 2017年11月2日10:40:52
 */
public interface ProductAttributeService {
	/**
	 * 增加产品属性
	 * 此方法分为两步，第一步把该产品的之前属性全部设为无效，第二步再增加
	 * @param list 产品属性集合
	 * @return 影响行数
	 * @author 欧阳丰
	 * @dateTime 2017年11月2日10:34:10
	 * @versions 1.0
	 */
   public int addList(List<EhyProductAttribute> list,String proId);
   
   /**
	* 根据产品ID查询该产品的所有属性
	* @param proId 产品ID
	* @return 属性集合
	* @author 欧阳丰
	* @dateTime 2017年11月2日10:35:59
	* @versions 1.0
	*/
   public List<EhyProductAttribute> findAllByProId(String proId);
   
   /**
    * 根据产品ID查询该产品的所有属性
    * @param proId 产品ID
    * @return 返回一个Map集合
    * @author 罗海兵
    * @dateTime 2017年11月12日 下午3:16:36
    * @versions 1.0
    */
   public List<Map<String, Object>> findAttrByProId(String proId);
}
