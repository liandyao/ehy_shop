package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.comment.vo.CommentVo;
import cn.ehuoyuan.shop.action.comment.vo.ShareDetailsVo;
import cn.ehuoyuan.shop.domain.EhyProductComment;

public interface EhyProductCommentMapper {
    int deleteByPrimaryKey(String commId);

    int insert(EhyProductComment record);

    int insertSelective(EhyProductComment record);

    EhyProductComment selectByPrimaryKey(String commId);

    int updateByPrimaryKeySelective(EhyProductComment record);

    int updateByPrimaryKey(EhyProductComment record);
    /**
     * 显示所有评论的方法
     * @param map 查询的条件
     * @return 评论的视图模型集合
     */
    public List<CommentVo> showList(Map<String, Object> map);
    /**
	 * 根据分享编号查询分享详情评论部分信息的方法
	 * @param shaId 分享的编号
	 * @return 分享详情的视图对象
	 * <p>所查询的评论图片为多张图片地址和图片说明合并的字符串字符串
	 * <br>分割图片使用','
	 * <br>分割图片地址和图片说明使用'|'</p>
	 */
    public ShareDetailsVo findShareByShaId(String shaId);
}