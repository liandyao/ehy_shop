package cn.ehuoyuan.shop.dao;


import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyNews;


/**
 * 公告DAO
 * @author dong
 * @da2017年10月12日
 * @version 1.0
 */
public interface EhyNewsMapper {
    int deleteByPrimaryKey(String newsId);
    
    /**
     * 增加
     * @param record
     * @return
     */
    int insert(EhyNews record);

    /**
     * 查询所有(模糊查询，分页行数)
     * @param map 返回模糊查询，分页行数
     * @return
     */
    List<EhyNews> findAll(Map<String, Object> map);
    
    /**
     * 总行数
     * @param map 返回模糊查询
     * @return
     */
    int findRowCount(Map<String, Object> map);
    
    /**
     * 删除或修改方法(删除把是否有效改成0)
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(EhyNews record);
    
    /**
     * 根据ID查询公告
     * @param newsId 公告ID
     * @return
     */
    EhyNews selectByPrimaryKey(String newsId);
    
    /**
     * 修改，取消置顶
     * @param newsId 公告ID
     * @return
     */
    int updateByPrimaryKey(String newsId);
    
    /**
     * 修改，置顶
     * @param newsId 公告ID
     * @return
     */
    int update(String newsId);
    
    /**
     * 根据站点查询是否该站点存在置顶 
     * @param station
     * @return
     */
    String showstation(String station);
    
    /**
     * 根据站点标识码查询公告
     * @param station
     * @return
     */
    List<EhyNews> shownews(String station);
    
    /**
     * 根据站点标识码查询更多公告
     * @param map
     * @return
     */
    List<EhyNews> show(Map<String, Object> map);
    
    /**
     * 更多公告总行数
     * @param newsId 
     * @return
     */
    int showRowCount(String newsId);
    
    
    int insertSelective(EhyNews record);

   

  

    
}