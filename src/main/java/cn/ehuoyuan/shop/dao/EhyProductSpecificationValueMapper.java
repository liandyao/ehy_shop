package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductSpecificationValue;
/**
 * 产品规格dao
 * @author 欧阳丰
 * @dateTime 2017年10月28日17:07:00
 */
public interface EhyProductSpecificationValueMapper {

    /**
     * 根据产品ID查找该产品的规格
     * @param proId 产品ID
     * @return 产品规格集合
     * @author 欧阳丰
     * @dateTime 2017年10月31日15:45:15
	 * @version 1.0
     */
    public List<EhyProductSpecificationValue> findAllByProId(String proId);
    
    /**
     * 根据产品ID修改该产品的所有规格为无效
     * @param proId 产品ID
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月28日17:01:30
	 * @version 1.0
     */
    public int updateIsvaByProId(String proId);
    
    
    /**
     * 增加产品规格
     * @param record 产品规格实体类(所有字段均不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
    int insert(EhyProductSpecificationValue record);
    
    /**
     * 增加产品规格
     * @param record 产品规格实体类(除主键ID外，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
    public int insertSelective(EhyProductSpecificationValue record);
    
    /**
     * 批量增加产品规格
     * @param list 产品规格集合(集合中所有对象属性值都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月20日09:54:53
	 * @version 1.1
     */
    public int addSpecificationValueList(List<EhyProductSpecificationValue> list);
    
    /**
     * 根据规格ID查询产品规格
     * @param priId 主键ID
     * @return 产品规格对象
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:54:15
	 * @version 1.0
     */
    EhyProductSpecificationValue selectByPrimaryKey(String priId);
    
    /**
     * 根据主键ID修改产品规格
     * @param record 产品规格对象(除图片ID外，其他字段可以为空，如果为空则表示不修改该字段)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:54:53
	 * @version 1.0
     */
    int updateByPrimaryKeySelective(EhyProductSpecificationValue record);
    
    /**
     * 根据主键ID修改产品规格
     * @param record 产品规格对象(所有字段都不能为空，如果为空，则数据库字段修改为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:55:45
	 * @version 1.0
     */
    int updateByPrimaryKey(EhyProductSpecificationValue record);
    
    /**
     * 根据产品ID查询产品详情的规格参数 
     * @param proId 产品ID
     * @return 返回一个Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月27日 上午10:27:21
     * @versions 1.0
     */
    List<Map<String, Object>> findByProId(String proId);
}