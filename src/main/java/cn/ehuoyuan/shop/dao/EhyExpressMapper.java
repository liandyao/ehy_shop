package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyExpress;

/**
 * 物流DAO
 * @author dong
 * @da2017年10月7日
 * @version 1.0
 */
public interface EhyExpressMapper {
    int deleteByPrimaryKey(String expreessId);

    /**
     * 增加
     * @param record
     * @return
     */
    int insert(EhyExpress record);

    int insertSelective(EhyExpress record);
    /**
     * 根据ID查询物流公司
     * @param expreessId(快递公司ID)
     * @return
     */
    EhyExpress selectByPrimaryKey(String expreessId);
    /**
     * 删除方法(修改是否显示状态)
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(EhyExpress record);
    /**
     * 修改方法
     * @param record
     * @return
     */
    int update(EhyExpress record);

    /**
     * 查询所有(模糊查询，分页行数)
     * @param map 返回模糊查询，分页行数
     * @return
     */
   	List<EhyExpress> findAll(Map<String, Object> map);
    
   	/**
   	 * 总行数
   	 * @param map
   	 * @return
   	 */
  	int findRowCount(Map<String, Object> map);
  	
    int updateByPrimaryKey(EhyExpress record);
   
    /**
     * 查询所有的快递
     * @author 邓丽杰
     * @date 2017年10月18日13:58:10
	 * @version 1.1
     */
	List<EhyExpress> findExpress();
    
}