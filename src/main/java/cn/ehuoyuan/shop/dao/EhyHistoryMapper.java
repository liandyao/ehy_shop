/**
 * 
 */
package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyHistory;

/**
 * 类的描述：游览记录的dao
 * @author xym
 * @version 1.0
 * @date 2018年1月22日
 */
public interface EhyHistoryMapper {
	/**
	 * 增加游览记录的方法
	 * @param ehyHistory 游览记录的对象
	 * @return 执行行数
	 */
	public int addHistory(EhyHistory ehyHistory);
	/**
	 * 查询全部游览记录
	 * @param param Map集合 所需的参数：mbId 会员编号
	 * @return 该会员未被删除的游览记录集合
	 */
	public List<EhyHistory> showAllHistory(Map<String, Object> param);
	/**
	 * 根据会员的编号查询出该会员分享出去的商品被游览数的总和
	 * @param mbId 会员编号
	 * @return 该会员分享出去的商品被游览数的总和
	 */
	public int findLookCount(String mbId);
}
