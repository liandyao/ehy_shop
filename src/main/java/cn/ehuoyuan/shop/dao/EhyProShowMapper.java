package cn.ehuoyuan.shop.dao;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.ehuoyuan.shop.action.frontShow.ProshowVo;
import cn.ehuoyuan.shop.domain.EhyProShow;


/**
 * 产品展示DAO
 * @author dong
 * @da2017年10月18日
 * @version 1.0
 */
public interface EhyProShowMapper {
    int deleteByPrimaryKey(String showId);

    
    int insert(EhyProShow record);

    
    /**
     * 增加
     * @param record
     * @return
     */
    int insertSelective(EhyProShow record);

    /**
     * 查询所有(分页行数，模糊查询)
     * @param map 返回分页行数，模糊查询
     * @return
     */
    List<EhyProShow> findAllshow(Map<String, Object> map);
    
  /**
   * 总行数
   * @param map 返回模糊查询
   * @return
   */
  	int findRowCount(Map<String, Object> map);
  	
  	/**
  	 * 根据ID查询
  	 * @param showId 展示ID
  	 * @return
  	 */
  	EhyProShow selectByPrimaryKey(String showId);
  	
  	/**
  	 * 修改或删除(删除把是否有效改成0)
  	 * @param record
  	 * @return
  	 */
  	 int updateByPrimaryKeySelective(EhyProShow record);
  	
  	 /**
  	  * 排序
  	  * @param startNum
  	  * @param endNum
  	  * @param showId
  	  * @return
  	  */
  	int sortModule(@Param("startNum")Integer startNum, @Param("endNum")Integer endNum, @Param("showId")String showId);
  	
  	int updateByPrimaryKey(EhyProShow record);
  	
 
    

   

   
    
    /**
	  * @title 查询全部
	  * @description 查询全部的商品展示列表数据 
	  * @return 返回一个商品展示列表的模型的集合
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午9:34:15
	  * @versions 1.0
	  */
	 public List<EhyProShow> findAll(EhyProShow proShow);
	 
	 /**
	  * @title 查询总行数
	  * @description  根据查询条件查询总行数
	  * @param proShow 封装了查询条件的商品展示列表的模型对象
	  * @return 返回总行数
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午10:02:55
	  * @versions 1.0
	  */
	 public int findTotalRows(EhyProShow proShow);
	 
	 /**
	  * @title 查询总行数
	  * @description  根据查询条件查询总行数
	  * @param proShow 封装了查询条件的商品展示列表的模型对象
	  * @return 返回总行数
	  * @author 罗海兵
	  * @dateTime 2017年10月19日 上午10:02:55
	  * @versions 1.0
	  */
	 public List<ProshowVo> showAll(int showType);
	 
	 /**
	  * 查询某一个站点的全部展示产品的类型
	  * @param stId 站点ID
	  * @return 返回一个展示类型的字符串集合
	  * @author 罗海兵
	  * @dateTime 2018年2月1日 下午7:56:57
	  * @versions 1.0
	  */
	 public List<Integer> findTypeAll(String stId);
	 
	 public List<Map<String, Object>> show2(Map<String, Object> map);


	 /**
	  * 根据类型查询展示的商品
	  * @param stId
	  * @param showType
	  * @return
	  */
	List<ProshowVo> showAllByType(Map<String, Object> map);
	 
	 
}
	 
