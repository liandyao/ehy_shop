package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.common.Pages;
import cn.ehuoyuan.shop.action.order.vo.OrderVo;
import cn.ehuoyuan.shop.action.order.vo.ShareVo;
import cn.ehuoyuan.shop.domain.EhyOrderItem;

public interface EhyOrderItemMapper {
    int deleteByPrimaryKey(String mxId);

    int insert(EhyOrderItem record);

    int insertSelective(EhyOrderItem record);

    EhyOrderItem selectByPrimaryKey(String mxId);

    int updateByPrimaryKeySelective(EhyOrderItem record);

    int updateByPrimaryKey(EhyOrderItem record);
    /**
     * 查询所有的方法，前台
     * @param 会员的id
     * @return 订单的集合
     */
    List<OrderVo> frontShowAll(Map<String, Object> map);
    /**
     * 确认收货的方法
     * @param 参数 ：会员编号。订单编号
     * @return 修改行数
     */
    int receipt(Map<String, Object> map);
    
    /**
     * 查询所有方法，后台
     * @param map
     * @return
     */
    List<EhyOrderItem> backShowAll(Map<String, Object> map);
    
    /**
	 * 显示全部已发货订单
	 * @param map
	 * @return
	 */
	public List<EhyOrderItem> deliveredShowAll(Map<String, Object> map);
	
	/**
	 * 显示全部待发货订单
	 * @param map
	 * @return
	 */
	public List<EhyOrderItem> overhangShowAll(Map<String, Object> map);
	
	/**
	 * 显示全部已收货订单
	 * @param map
	 * @return
	 */
	public List<EhyOrderItem> receivedShowAll(Map<String, Object> map);
	/**
	 * 前台查询订单总条数
	 * @param map
	 * @return
	 */
	public int ordersCount(Map<String,Object> map);
    
	/**
	 * 分享时根据id查询
	 * @param map
	 * @return 分享视图对象
	 */
    public ShareVo findById(Map<String, Object> map);
    
    /**
     * 后台查询本月订单的统计图
     * @param map
     * @author 邓丽杰
	 * @date 2017年11月16日10:10:02
     * @return
     */
    public List<EhyOrderItem> thisOrderMap(Map<String, Object> map);
    
    /**
     * 后台查询上月订单的统计图
     * @param map
     * @author 邓丽杰
	 * @date 2017年11月16日10:10:02
     * @return
     */
    public List<EhyOrderItem> oneOrderMap(Map<String, Object> map);
    
    /**
     * 后台查询历史订单的统计图
     * @param map
     * @author 邓丽杰
	 * @date 2017年11月16日10:10:02
     * @return
     */
    public List<EhyOrderItem> selectOrderMap(Map<String, Object> map);
    
    /**
     * 后台根据邀请码查询订单明细
     * @param map
     * @author 邓丽杰
     * @date 2017年11月20日10:49:32
     * @return
     */
    public List<Map> findOrderItem(Map<String, Object> map);
    
    /**
     * 后台根据邀请码查询订单明细总行数
     * @param map
     * @author 邓丽杰
     * @date 2017年11月20日10:49:32
     * @return
     */
    public int findOrderItemRowCount(Map<String, Object> map);
    /**
     * 删除订单明细的方法
     * @param mxId 订单明细的编号
     * @return 执行行数
     */
    public int deleteOrderItem(String mxId);
    /**
     * 该明细所在的该订单总明细数量
     * @param mxId 订单明细的编号
     * @return 查询出来的总行数
     */
    public int OrderItemNum(String mxId);
    /**
     * 当订单明细只有一条时删除订单的方法
     * @param mxId 订单明细的编号
     * @return 执行行数
     */
    public int deleteOneOrderItem(String mxId);
    /**
     * 删除订单明细的方法
     * @param ordId 订单的编号
     * @return 执行行数
     */
    public int deleteOrder(String ordId);
    
    /**
     * 根据订单id修改订单明细
     * @param ehyOrderItem 封装了修改字段和修改条件的订单明细对象
     * @return 成功返回1，失败返回0
     * @author 罗海兵
     * @dateTime 2018年7月2日 上午12:29:27
     * @versions 1.0
     */
    public int updateByOrdIdSelective(EhyOrderItem ehyOrderItem);
}