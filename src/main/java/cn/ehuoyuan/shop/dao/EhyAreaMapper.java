package cn.ehuoyuan.shop.dao;


import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyArea;

/**
 * 类的描述：地区的DAO
 * @author dong
 * @dateTime 2017年11月1日 下午9:52:34
 * @version 1.0
 */
public interface EhyAreaMapper {
    int deleteByPrimaryKey(String areaId);
    /**
     * 增加
     * @param record
     * @return
     */
   int insert(EhyArea record);

    int insertSelective(EhyArea record);
    /**
     * 根据ID查询地区
     * @param areaId 地区ID
     * @return
     */
    EhyArea selectByPrimaryKey(String areaId);
    /**
     * 删除方法,(修改状态)
     * @param record 
     * @return
     */
    int updateByPrimaryKeySelective(EhyArea record);
    /**
     * 下拉框查询(地区表_地区ID为0，是否有效为1的)
     * @return
     */
    List<EhyArea> findshow();
    /**
     * 修改方法地区
     * @param record 可以为空
     * @return
     */
    int update(EhyArea record);
    
    int updateByPrimaryKey(EhyArea record);
    /**
     * 查询方法
     * @param map(模糊查询，必须需要分页)
     * @return
     */
	List<EhyArea> findAll(Map<String, Object> map);
	/**
	 * 地区总行数
	 * @param map(模糊查询)
	 * @return
	 */
	int findRowCount(Map<String, Object> map);
	
	 List<EhyArea> selectArea(EhyArea record);
	 
	 List<EhyArea> selectAreaById(String record);
	 
	/**
	 * 查询省区(地区表_地区ID为0)
	 * @return
	 */
	List<Map<String, Object>> findProvince();
	
	List<Map<String, Object>> findCity(String provinceId);
	
	List<Map<String, Object>> findArea(String cityId);
}