package cn.ehuoyuan.shop.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.ehuoyuan.shop.domain.EhyShelves;

public interface EhyShelvesMapper {
	public int deleteByPrimaryKey(String sheId);

    public int insert(EhyShelves record);

    public int insertSelective(EhyShelves record);

    public EhyShelves selectByPrimaryKey(String sheId);

    public int updateByPrimaryKeySelective(EhyShelves record);

    public int updateByPrimaryKey(EhyShelves record);
    
    /**
	 * 根据会员id查询货架
	 * @return 返回一个货架的集合
	 * @author 罗海兵
	 * @dateTime 2018年2月22日 下午3:48:20
	 * @versions 1.0
	 */
	public List<EhyShelves> findByMbId(String mbId);
	
	/**
	 * 根据会员id和商品id查询货架
	 * @return 返回一个货架的集合
	 * @author liandyao
	 * @dateTime 2018年8月22日 下午3:48:20
	 * @versions 1.0
	 */ 
	public List<EhyShelves> findByMbIdAndProId(@Param("mbId") String mbId,@Param("proId")String proId);
}