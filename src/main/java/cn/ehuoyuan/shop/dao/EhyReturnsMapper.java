package cn.ehuoyuan.shop.dao;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import cn.ehuoyuan.shop.domain.EhyReturns;

public interface EhyReturnsMapper {
    int deleteByPrimaryKey(String reId);

    int insert(EhyReturns record);

    int insertSelective(EhyReturns record);

    EhyReturns selectByPrimaryKey(String reId);

    int updateByPrimaryKeySelective(EhyReturns record);

    int updateByPrimaryKey(EhyReturns record);
    
    @ResultMap("BaseResultMap")
    @Select("select * from ehy_returns where ORD_ID=#{ordId}")
    EhyReturns selectByOrdId(String ordId);
}