package cn.ehuoyuan.shop.dao;

import java.util.List;

import cn.ehuoyuan.shop.action.share.Vo.ShareVo;
import cn.ehuoyuan.shop.domain.EhyShare;

public interface EhyShareMapper {
    int deleteByPrimaryKey(String shaId);

    int insert(EhyShare record);

    int insertSelective(EhyShare record);

    EhyShare selectByPrimaryKey(String shaId);

    int updateByPrimaryKeySelective(EhyShare record);

    int updateByPrimaryKey(EhyShare record);
    /**
     * 增加分享的方法
     * @param record 分享对象
     * @return 影响行数
     */
    public int add(EhyShare record);

	/**
	 * 显示全部订单（视图模型）的方法
	 * @param 会员的编号
	 * @return 订单的视图集合
	 * <p>视图模型使用 cn.ehuoyuan.shop.action.share.Vo.ShareVo</p>
	 */
	public List<ShareVo> myShare(String mbId);
}