package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.frontShow.OrderPay;
import cn.ehuoyuan.shop.action.order.vo.DeliveredOrderVo;
import cn.ehuoyuan.shop.action.order.vo.OverhangOrderVo;
import cn.ehuoyuan.shop.action.order.vo.ReceivedOrder;
import cn.ehuoyuan.shop.domain.EhyOrder;
import cn.ehuoyuan.shop.domain.EhyOrderItem;

public interface EhyOrderMapper {
	int deleteByPrimaryKey(String ordId);

	int insert(EhyOrder record);

	/**
	 * 增加订单和订单明细的集合，并且将已购买的购物车设置为无效
	 * @param map 封装订单对象、订单详情对象集合、购物车ID数组的Map对象
	 * @description map.order 订单对象
	 * @description map.orderItemlist 订单明细对象的集合
	 * @description map.cartIds 购物车ID的数组
	 * @return 返回增加行数
	 * @author 罗海兵
	 * @dateTime 2017年11月20日 上午11:33:26
	 * @versions 1.0
	 */
	int insertSelective(Map<String, Object> map);

	EhyOrder selectByPrimaryKey(String ordId);

	int updateByPrimaryKeySelective(EhyOrder record);

	int updateByPrimaryKey(EhyOrder record);
	
	/**
	 * 修改运费
	 * @param map
	 * @return
	 */
	int updateFreight(Map<String, Object> map);
	
	/**
	 * 修改总价格
	 * @param map
	 * @return
	 */
	int updateSumMoney(Map<String, Object> map);
	
	/**
	 * 显示可编辑价格订单
	 * @param map
	 * @return
	 */
	List<EhyOrder> showEditOrder(Map<String, Object> map);
	
	/**
	 * 显示可编辑价格订单的总行数
	 * @param map
	 * @return
	 */
	int showEditOrderCount(Map<String, Object> map);
	
	/**
	 * 显示售后订单
	 * @param map
	 * @return
	 */
	List<ReceivedOrder> showReceivedOrder(Map<String, Object> map);
	
	/**
	 * 显示售后订单总行数
	 * @param map
	 * @return
	 */
	int showReceivedOrderCount(Map<String, Object> map);
	
	/**
	 * 显示已发货订单
	 * @param map
	 * @return
	 */
	List<DeliveredOrderVo> showDeliveredOrder(Map<String, Object> map);

	/**
	 * 显示已发货订单订单的总行数
	 * @param map
	 * @return
	 */
	int showDeliveredOrderCount(Map<String, Object> map);

	/**
	 * 显示待发货订单
	 * @param map
	 * @return
	 */
	List<OverhangOrderVo> showOverhangOrder(Map<String, Object> map);

	/**
	 * 显示待发货订单的总行数
	 * @param map
	 * @return
	 */
	int showOverhangOrderCount(Map<String, Object> map);

	/**
	 * 资金结算
	 * @param map
	 * @description mrId 资金结算记录id
	 * @description startTime 开始时间
	 * @description endTime 结束时间
	 * @return
	 */
	int jsMoney(Map<String, Object> map);

	/**
	 * 发货
	 * @param map
	 * @description ordExpress 快递公司
	 * @description ordExpressCode 快递单号
	 * @description mxId 订单明细id
	 * @return
	 */
	int deliverGoods(Map<String, Object> map);

	/**
	 * 根据时间段查询结算资金
	 * @param map
	 * @description startTime 开始时间
	 * @description endTime 结束时间
	 * @return
	 */
	String findMoney(Map<String, Object> map);

	/**
	 * 支付页面查询
	 * @param ordId 订单ID
	 * @return 返回一个订单支付页面的VO模型
	 * @author 罗海兵
	 * @dateTime 2017年11月21日 上午9:35:05
	 * @versions 1.0
	 */
	OrderPay selectPay(String ordId);

	/**
	 * 根据订单id修改订单明细状态
	 * @param map 
	 * @description map.ordId 订单id
	 * @description map.state 订单状态
	 * @return
	 */
	int updateStateByOrderId(Map<String, Object> map);

	/**
	 * 根据明细id修改订单明细状态
	 * @param map 
	 * @description map.mxId 明细id
	 * @description map.state 订单状态
	 * @return
	 */
	int updateStateByItemId(Map<String, Object> map);

	/**
	 * 根据订单id查询订单明细
	 * @param ordId 订单ID
	 * @return
	 */
	List<EhyOrderItem> selectItenByOrderId(String ordId);
	
	/**
	 * 修改退款金额
	 * @param map
	 * @return
	 */
	int updateTuiKuan(Map<String, Object> map);

}