package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.cart.CartVo;
import cn.ehuoyuan.shop.domain.EhyCart;

/**
 * 类的描述：购物车的DAO
 * @author 罗海兵
 * @dateTime 2017年11月1日 下午9:52:34
 * @version 1.0
 */
public interface EhyCartMapper {
	/**
	 * 根据ID删除购物车
	 * @param cartId 购物车ID
	 * @return 成功返回1,失败返回0
	 * @author 罗海兵
	 * @dateTime 2017年10月30日 上午8:32:32
	 * @versions 1.0
	 */
	public int delete(String cartId);
    
    /**
     * 加入购物车(可选部分)
     * @param record 购物车对象
     * @return 成功返回1,失败返回0
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:33:14
     * @versions 1.0
     */
	public int insertSelective(EhyCart cart);
    
    /**
     * 根据购车ID查询购物车
     * @param cartId 购车ID
     * @return 找到一个返回购物车对象,找不到返回null
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:34:49
     * @versions 1.0
     */
	public CartVo findByCartId(String cartId);
    
    /**
     * 根据会员ID查询购物车
     * @param mbId 会员ID
     * @return 找到一个返回购物车对象,找不到返回null
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:34:49
     * @versions 1.0
     */
	public List<CartVo> findByMbId(String mbId);
    
    /**
     * 根据购物车ID的数组查询购物车集合
     * @param cartIds 购物车ID的数组
     * @return 返回一个购物车对象的集合
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:34:49
     * @versions 1.0
     */
    public List<CartVo> findByCartIds(String[] cartIds);
    
    /**
     * 根据购物车ID的数组查询购物车集合
     * @param cartIds 购物车ID的数组
     * @return 返回一个购物车对象的集合
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:34:49
     * @versions 1.0
     */
    public List<CartVo> findByCartIds2(List<Map<String, Object>> cartIds);
    
    /**
     * 根据购物车ID修改购物车(可选部分)
     * @param record 购物车对象
     * @return 成功返回1,失败返回0
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:36:46
     * @versions 1.0
     */
    public int updateByPrimaryKeySelective(EhyCart record);
    
    /**
     * 根据购物车ID修改购物车(全选)
     * @param record 购物车对象
     * @return 成功返回1,失败返回0
     * @author 罗海兵
     * @dateTime 2017年10月30日 上午8:36:46
     * @versions 1.0
     */
    public int updateByPrimaryKey(EhyCart record);
    
    /**
     * 根据会员ID查询
     * @param mbId 会员ID
     * @return 返回购物车数量
     * @author 罗海兵
     * @dateTime 2017年10月31日 下午4:03:08
     * @versions 1.0
     */
    public int findCartNum(String mbId);
    
    /**
     * 查询购物车
     * @param cart购物车对象
     * @return 返回购物车对象
     * @author 罗海兵
     * @dateTime 2018年1月10日 上午10:42:13
     * @versions 1.0
     */
    public CartVo findCart(EhyCart cart);
}