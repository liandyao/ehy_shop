package cn.ehuoyuan.shop.dao;

import java.util.List;

import cn.ehuoyuan.shop.action.frontShow.ProPrice;
import cn.ehuoyuan.shop.domain.EhyProductSpecificationPrice;
/**
 * 产品规格价格dao
 * @author 欧阳丰
 * @dateTime 2017年10月28日17:07:00
 */
public interface EhyProductSpecificationPriceMapper {
    
    /**
     * 根据产品ID查找该产品的规格价格 
     * @param proId 产品ID
     * @return 规格价格集合
     * @author 欧阳丰
     * @dateTime 2017年10月31日15:45:15
	 * @version 1.0
     */
    public List<EhyProductSpecificationPrice> findAllByProId(String proId);
    
    /**
     * 根据产品ID修改该产品的所有规格价格为无效
     * @param proId 产品ID
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月28日17:01:30
	 * @version 1.0
     */
    public int updateIsvaByProId(String proId);
    
    /**
     * 增加产品规格价格
     * @param record 产品规格价格实体类(所有字段均不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
    int insert(EhyProductSpecificationPrice record);
    
    /**
     * 增加产品规格价格
     * @param record 产品规格价格实体类(除主键ID外，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月28日16:37:09
	 * @version 1.0
     */
    public int insertSelective(EhyProductSpecificationPrice record);
    
    /**
     * 批量增加产品规格价格
     * @param list 产品规格价格集合(集合中所有对象属性值都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月20日11:48:00
	 * @version 1.0
     */
    public int addSpecificationPriceList(List<EhyProductSpecificationPrice> list);
    
    /**
     * 根据主键ID查询规格价格
     * @param pspId 主键ID
     * @return 规格价格对象
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:46:00
	 * @version 1.0
     */
    EhyProductSpecificationPrice selectByPrimaryKey(String pspId);
    
    /**
     * 根据主键ID修改规格价格
     * @param record 规格价格对象(除图片ID外，其他字段可以为空，如果为空则表示不修改该字段)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:47:44
	 * @version 1.0
     */
    int updateByPrimaryKeySelective(EhyProductSpecificationPrice record);
    
    /**
     * 根据主键ID修改规格价格
     * @param record 规格价格对象(所有字段都不能为空，如果为空，则数据库字段修改为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:48:09
	 * @version 1.0
     */
    int updateByPrimaryKey(EhyProductSpecificationPrice record);
    
    /**
     * 根据产品ID查询产品的所有规格价格
     * @param proId
     * @return 
     * @author 罗海兵
     * @dateTime 2017年11月12日 下午2:33:09
     * @versions 1.0
     */
    public List<ProPrice> findPriceByProId(String proId);
}