package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cn.ehuoyuan.shop.domain.EhySpecificationType;

public interface EhySpecificationTypeMapper {
    int deleteByPrimaryKey(String sptId);

    int insert(EhySpecificationType record);

    int insertSelective(EhySpecificationType record);
    
    /**
     * 根据规格类型id得到该规格的所有信息
     * @author 胡鑫
     * @date 2017年10月19日09:54:16
     * @param map map集合用于存放sql参数
     * @return 返回规格类型
     */
    public EhySpecificationType selectByPrimaryKey(Map<String, Object> map);

    int updateByPrimaryKeySelective(EhySpecificationType record);

    int updateByPrimaryKey(EhySpecificationType record);
    /**
     * 得到规格类型集合
     * @author 胡鑫
     * @param map 
     * @date 2017年10月10日16:26:12
     * @return 返回规格类型集合
     */
    public List<EhySpecificationType> findSpecificationTypeList(Map<String, Object> map);
    
    /**
     * 得到查询到的行数
     * @author 胡鑫
     * @param map2 
     * @date 2017年10月18日09:05:48
     * @return 返回查询到的行数
     */
    public int selectCountSpecificationType(Map<String, Object> map);
    
    /**
     * 分页、模糊查询规格类型集合
     * @author 胡鑫
     * @date 2017年10月18日10:05:42
     * @return 返回规格类型集合
     */
    public List<EhySpecificationType> selectAll(Map<String, Object> map);
    
    /**
     * 删除查询 执行修改状态为0之前进行该方法查询返回的值大于0则不能删除
     * @author 胡鑫
     * @date 2017年10月20日01:00:51
     * @param sptId 规格类型id
     * @return 返回执行的行数
     */
    public int deleteSelect(String sptId);
    
    /**
     * 根据规格id进行规格删除
     * @author 胡鑫
     * @date 2017年10月21日09:46:51
     * @param map 传入的map集合
     * @return 返回执行的行数
     */
    public int deleteByPrimaryKey(Map<String, Object> map);
    
    /**
     * 根据产品类型ID的集合来查询规格类型
     * @param typeIds 产品类型ID的集合
     * @return 返回一个Map集合
     * @author 罗海兵
     * @dateTime 2017年10月25日 下午4:45:03
     * @versions 1.0
     */
    public List<Map<String, Object>> findByTypeIds(Map<String, Object> map);
    /**
     * 拖动排序方式
     * @author 胡鑫
     * @date 2017年11月9日14:35:09
     * @param start
     * @param end
     * @param modId
     * @return 返回执行的行数
     */
    public int sortModule(@Param("startNum")Integer startNum, @Param("endNum")Integer endNum, @Param("sptId")String sptId);
    
    /**
     * 得到最大行数
     * @author 胡鑫
     * @date 2017年11月9日14:35:09
     * @return 返回行数
     */
    public int maxSort();
    
}