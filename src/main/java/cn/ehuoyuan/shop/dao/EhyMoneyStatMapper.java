package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.action.stat.vo.MoneyStatVo;
import cn.ehuoyuan.shop.action.stat.vo.TodoVo;
import cn.ehuoyuan.shop.domain.EhyMoneyRecord;
import cn.ehuoyuan.shop.domain.EhyMoneyStat;

public interface EhyMoneyStatMapper {
    int deleteByPrimaryKey(String msId);

    int insert(EhyMoneyStat record);

    int insertSelective(EhyMoneyStat record);

    EhyMoneyStat selectByPrimaryKey(String msId);

    int updateByPrimaryKeySelective(EhyMoneyStat record);

    int updateByPrimaryKey(EhyMoneyStat record);
    
    /**
     * 删除未确认的结算记录
     * @param id
     * @return
     */
    int delete(String id);
    
    /**
     * 显示资金结算记录
     * @param map
     * @return
     */
    List<MoneyStatVo> showList(Map<String, Object> map);
    
    /**
     * 查询资金结算记录的总行数
     * @param map
     * @return
     */
    int countRows(Map<String, Object> map);
    
    /**
     * 添加结算记录
     * @param map
     * @return
     */
    int addCheckoutRecord(Map<String, Object> map);
    
    /**
     * 
     * @param map
     * @return
     */
    String computeMoney(Map<String, Object> map);
    
    /**
     * 查询上次结算结束的时间
     * @return 上次结算结束的时间
     */
    String queryEndCheckoutTime();
    
    /**
     * 根据id查询详de订单id和详情id
     * @param msId
     * @return
     */
    List<EhyMoneyRecord> selectItemById(String msId);
    
    /**
     * 确认结算
     * @param map
     * @return
     */
    int confirmCheckout(Map<String, Object> map);
    
    List<TodoVo> displayTodo(String id);
}