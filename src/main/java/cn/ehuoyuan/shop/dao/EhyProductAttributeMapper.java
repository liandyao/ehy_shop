package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductAttribute;
/**
 * 产品属性dao
 * @author 欧阳丰
 * @dateTime 2017年11月2日10:34:10
 */
public interface EhyProductAttributeMapper {
	/**
	  * 增加产品属性 
	  * @param attribute 产品属性对象(所有字段都不能为空)
	  * @return 影响行数
	  * @author 欧阳丰
	  * @dateTime 2017年11月30日08:38:18
	  * @versions 1.0
	  */
    int insert(EhyProductAttribute attribute);
    
    /**
	  * 增加产品属性
	  * @param attribute 产品属性对象(除属性ID外，其他字段可以为空)
	  * @return 影响行数
	  * @author 欧阳丰
	  * @dateTime 2017年11月2日10:34:10
	  * @versions 1.0
	  */
    public int insertSelective(EhyProductAttribute attribute);
    
    /**
	  * 根据产品ID查询该产品的所有属性
	  * @param proId 产品ID
	  * @return 属性集合
	  * @author 欧阳丰
	  * @dateTime 2017年11月2日10:35:59
	  * @versions 1.0
	  */
    public List<EhyProductAttribute> findAllByProId(String proId);
    
    /**
	  * 修改某产品的所有属性为无效
	  * @param proId 产品ID
	  * @return 影响行数
	  * @author 欧阳丰
	  * @dateTime 2017年11月2日19:13:41
	  * @versions 1.0
	  */
    public int updateIsvaByProId(String proId);
    
    /**
	  * 根据产品ID修改某产品的所有属性
	  * @param attribute 产品属性对象(除属性ID外，其他字段可以为空，如果为空表示不修改该字段)
	  * @return 影响行数
	  * @author 欧阳丰
	  * @dateTime 2017年11月2日19:13:41
	  * @versions 1.0
	  */
    int updateByPrimaryKeySelective(EhyProductAttribute attribute);
    
    /**
	  * 根据产品ID修改某产品的所有属性
	  * @param attribute 产品属性对象(所有字段都不能为空，如果为空则修改数据库该字段为空)
	  * @return 影响行数
	  * @author 欧阳丰
	  * @dateTime 2017年11月2日19:13:41
	  * @versions 1.0
	  */
    int updateByPrimaryKey(EhyProductAttribute attribute);
    
    /**
     * 根据产品ID查询该产品的所有属性
     * @param proId 产品ID
     * @return 返回一个Map集合
     * @author 罗海兵
     * @dateTime 2017年11月12日 下午3:16:36
     * @versions 1.0
     */
    public List<Map<String, Object>> findAttrByProId(String proId);
}