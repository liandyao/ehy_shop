package cn.ehuoyuan.shop.dao;

import java.util.List;
import java.util.Map;

import cn.ehuoyuan.shop.domain.EhyProductImg;
/**
 * 产品图片dao
 * @author 欧阳丰
 * @dateTime 2017年10月18日14:57:49
 */
public interface EhyProductImgMapper {
    
    /**
     * 根据图片ID修改图片序号
     * @param productImg 产品图片对象(只需图片ID和图片序号两个参数，其他参数无效)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月18日14:57:49
	 * @version 1.0
     */
    public int updateSortByImgId(EhyProductImg productImg);
    
    /**
     * 增加产品图片
     * @param productImg 产品图片实体类(所有字段都不能为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年10月10日 18:28:20
	 * @version 1.0
     */
    int insert(EhyProductImg productImg);
    
    /**
     * 增加产品图片
     * @param productImg 产品图片实体类(除图片ID外，其他字段可以为空)
     * @return 影响行数
     * @author 欧阳丰
	 * @dateTime 2017年11月30日09:00:47
	 * @version 1.0
     */
    int insertSelective(EhyProductImg record);
    
    /**
     * 根据产品ID和图片类型修改图片为无效
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月10日 18:28:20
	 * @version 1.0
     */
    public int updateIsvaByProIdAndImgType(String proId,int imgType);
    
    /**
     * 根据产品ID和类型ID查询图片
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 图片集合
     * @author 欧阳丰
     * @dateTime 2017年10月17日 20:47:20
	 * @version 1.0
     */
    public List<EhyProductImg> findImgByProIdAndImgType(String proId,int imgType);
    
    /**
     * 根据产品ID和类型ID查询图片数量
     * @param proId 产品ID
     * @param imgType 图片类型
     * @return 图片数量
     * @author 欧阳丰
     * @dateTime 2017年10月17日 20:47:20
	 * @version 1.0
     */
    public int findImgByProIdAndImgTypeSize(String proId,int imgType);

    
    /**
     * 根据图片ID查询图片
     * @param imgId 图片ID
     * @return 图片对象
     * @author 欧阳丰
     * @dateTime 2017年11月30日09:03:09
	 * @version 1.0
     */
    EhyProductImg selectByPrimaryKey(String imgId);
    
    /**
     * 根据图片ID修改图片信息
     * @param productImg 产品图片实体类(除图片ID外，其他字段可以为空，如果为空则表示不修改该字段)
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月18日18:10:35
	 * @version 1.0
     */
    int updateByPrimaryKeySelective(EhyProductImg productImg);
    
    /**
     * 根据图片ID修改图片信息
     * @param productImg 产品图片实体类(所有字段都不能为空，如果为空，则数据库字段修改为空)
     * @return 影响行数
     * @author 欧阳丰
     * @dateTime 2017年10月18日18:10:35
	 * @version 1.0
     */
    int updateByPrimaryKey(EhyProductImg record);
    
    /**
     * @title 根据产品ID和图片类型查询产品图片
     * @param map 封装了产品ID和图片类型的Map对象
     * @return 返回一个图片地址的Map的List集合
     * @author 罗海兵
     * @dateTime 2017年10月26日 下午2:57:03
     * @versions 1.0
     */
    List<String> findImgByProId(Map<String, Object> map);
}