package cn.ehuoyuan.shop.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 清仓产品表的模型
 */
public class EhyProductQC implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 413883839047019079L;
	
	//清仓ID
    private String qcId;
	//产品ID
    private String proId;
    //属性01(库存数量)
    private String qcAttr01;
    //属性02
    private String qcAttr02;
    //属性03
    private String qcAttr03;
    //属性04
    private String qcAttr04;
    //属性05
    private String qcAttr05;
    //属性06
    private String qcAttr06;
    //属性07
    private String qcAttr07;
    //属性08
    private String qcAttr08;
    //是否有效
    private Integer isva;
    //操作时间
    private Date optime;
	//操作人
    private String oper;
    //排序
    private Integer sort;
    /**
     * 
     * @return 清仓ID
     */
    public String getQcId() {
        return qcId;
    }
    /**
     * 
     * @param 清仓ID
     */
    public void setQcId(String qcId) {
        this.qcId = qcId == null ? null : qcId.trim();
    }
    /**
     * 
     * @return 产品ID
     */
    public String getProId() {
        return proId;
    }
    /**
     * 
     * @param 产品ID
     */
    public void setProId(String proId) {
        this.proId = proId == null ? null : proId.trim();
    }
    /**
     * 
     * @return 属性01(库存数量)
     */
    public String getQcAttr01() {
        return qcAttr01;
    }
    /**
     * 
     * @param 属性01(库存数量)
     */
    public void setQcAttr01(String qcAttr01) {
        this.qcAttr01 = qcAttr01 == null ? null : qcAttr01.trim();
    }
    /**
     * 
     * @return 属性02
     */
    public String getQcAttr02() {
        return qcAttr02;
    }
    /**
     * 
     * @param 属性02
     */
    public void setQcAttr02(String qcAttr02) {
        this.qcAttr02 = qcAttr02 == null ? null : qcAttr02.trim();
    }
    /**
     * 
     * @return 属性03
     */
    public String getQcAttr03() {
        return qcAttr03;
    }
    /**
     * 
     * @param 属性03
     */
    public void setQcAttr03(String qcAttr03) {
        this.qcAttr03 = qcAttr03 == null ? null : qcAttr03.trim();
    }
    /**
     * 
     * @return 属性04
     */
    public String getQcAttr04() {
        return qcAttr04;
    }
    /***
     * 
     * @param 属性04
     */
    public void setQcAttr04(String qcAttr04) {
        this.qcAttr04 = qcAttr04 == null ? null : qcAttr04.trim();
    }
    /**
     * 
     * @return 属性05
     */
    public String getQcAttr05() {
        return qcAttr05;
    }
    /**
     * 
     * @param 属性05
     */
    public void setQcAttr05(String qcAttr05) {
        this.qcAttr05 = qcAttr05 == null ? null : qcAttr05.trim();
    }
    /**
     * 
     * @return 属性06
     */
    public String getQcAttr06() {
        return qcAttr06;
    }
    /**
     * 
     * @param 属性06
     */
    public void setQcAttr06(String qcAttr06) {
        this.qcAttr06 = qcAttr06 == null ? null : qcAttr06.trim();
    }
    /**
     * 
     * @return 属性07
     */
    public String getQcAttr07() {
        return qcAttr07;
    }
    /**
     * 
     * @param 属性07
     */
    public void setQcAttr07(String qcAttr07) {
        this.qcAttr07 = qcAttr07 == null ? null : qcAttr07.trim();
    }
    /**
     * 
     * @return 属性08
     */
    public String getQcAttr08() {
        return qcAttr08;
    }
    /**
     * 
     * @param 属性08
     */
    public void setQcAttr08(String qcAttr08) {
        this.qcAttr08 = qcAttr08 == null ? null : qcAttr08.trim();
    }
    /**
     * 
     * @return 是否有效
     */
    public Integer getIsva() {
        return isva;
    }
    /**
     * 
     * @param 是否有效
     */
    public void setIsva(Integer isva) {
        this.isva = isva;
    }
    /**
     * 
     * @return 操作时间
     */
    public Date getOptime() {
        return optime;
    }
    /**
     * 
     * @param 操作时间
     */
    public void setOptime(Date optime) {
        this.optime = optime;
    }
    /**
     * 
     * @return 操作人
     */
    public String getOper() {
        return oper;
    }
    /**
     * 
     * @param 操作人
     */
    public void setOper(String oper) {
        this.oper = oper == null ? null : oper.trim();
    }
    /**
     * 
     * @return 排序
     */
    public Integer getSort() {
        return sort;
    }
    /**
     * 
     * @param 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }
}