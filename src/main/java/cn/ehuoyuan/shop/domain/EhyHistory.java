/**
 * 
 */
package cn.ehuoyuan.shop.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 类的描述：游览记录的模型类
 * @author xym
 * @version 1.0
 * @date 2018年1月22日
 */
public class EhyHistory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8882469486842890376L;
	//游览主键
	private String hisId;
	//会员id
	private String memId;
	//会员名称
	private String memName;
	//商品编号
	private String ProId;
	//商品所在站点
	private String station;
	//商品名称
	private String ProName;
	//游览时间
	private Date hisTime;
	//是否删除
	private Integer isva;
	//分享的编号
	private String shaId;
	
	/**
	 * @return 分享的编号
	 */
	public String getShaId() {
		return shaId;
	}
	/**
	 * @param 分享的编号
	 */
	public void setShaId(String shaId) {
		this.shaId = shaId;
	}
	/**
	 * @return 浏览主键
	 */
	public String getHisId() {
		return hisId;
	}
	/**
	 * @param 浏览主键
	 */
	public void setHisId(String hisId) {
		this.hisId = hisId;
	}
	/**
	 * @return 会员ID
	 */
	public String getMemId() {
		return memId;
	}
	/**
	 * @param 会员ID
	 */
	public void setMemId(String memId) {
		this.memId = memId;
	}
	/**
	 * @return 会员名称
	 */
	public String getMemName() {
		return memName;
	}
	/**
	 * @param 会员名称
	 */
	public void setMemName(String memName) {
		this.memName = memName;
	}
	/**
	 * @return 商品ID
	 */
	public String getProId() {
		return ProId;
	}
	/**
	 * @param 商品ID
	 */
	public void setProId(String proId) {
		ProId = proId;
	}
	/**
	 * @return 商品所在站点(便于统计)(保留中文)
	 */
	public String getStation() {
		return station;
	}
	/**
	 * @param 商品所在站点(便于统计)(保留中文)
	 */
	public void setStation(String station) {
		this.station = station;
	}
	/**
	 * @return 商品名称
	 */
	public String getProName() {
		return ProName;
	}
	/**
	 * @param 商品名称
	 */
	public void setProName(String proName) {
		ProName = proName;
	}
	/**
	 * @return 浏览时间
	 */
	public Date getHisTime() {
		return hisTime;
	}
	/**
	 * @param 浏览时间
	 */
	public void setHisTime(Date hisTime) {
		this.hisTime = hisTime;
	}
	/**
	 * @return 是否删除(可以将自己的浏览记录删除)
	 */
	public Integer getIsva() {
		return isva;
	}
	/**
	 * @param 是否删除(可以将自己的浏览记录删除)
	 */
	public void setIsva(Integer isva) {
		this.isva = isva;
	}
	
}
