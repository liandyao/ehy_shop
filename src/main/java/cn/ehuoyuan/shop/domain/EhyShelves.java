package cn.ehuoyuan.shop.domain;

import java.io.Serializable;
import java.util.Date;
/**
 * 我的货架表模型
 */
public class EhyShelves implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6543503270437646888L;
	//货架ID
    private String sheId;
    //会员主键
    private String mbId;
    //产品ID
    private String proId;
    //加入时间
    private String addTime;
    //是否有效
    private Integer isva;
    //操作时间
    private Date optime;
    //操作人
    private String oper;
    //排序
    private Integer sort;
    
    
    
    //前台拓展属性
    //会员来源分站
    private String mbStation;
    //产品来源分站
    private String proStName;
    
    
    /**  
	 * @return 会员来源分站
	 */
	public String getMbStation() {
		return mbStation;
	}
	/**
	 * @param mbStation 会员来源分站
	 */
	public void setMbStation(String mbStation) {
		this.mbStation = mbStation;
	}
	/**  
	 * @return 产品来源分站
	 */
	public String getProStName() {
		return proStName;
	}
	/**
	 * @param proStName 产品来源分站
	 */
	public void setProStName(String proStName) {
		this.proStName = proStName;
	}
	/**
     * 
     * @return 货架ID
     */
    public String getSheId() {
        return sheId;
    }
    /**
     * 
     * @param 货架ID
     */
    public void setSheId(String sheId) {
        this.sheId = sheId == null ? null : sheId.trim();
    }
    /**
     * 
     * @return 会员主键
     */
    public String getMbId() {
        return mbId;
    }
    /**
     * 
     * @param 会员主键
     */
    public void setMbId(String mbId) {
        this.mbId = mbId == null ? null : mbId.trim();
    }
    /**
     * 
     * @return 产品ID
     */
    public String getProId() {
        return proId;
    }
    /**
     * 
     * @param 产品ID
     */
    public void setProId(String proId) {
        this.proId = proId == null ? null : proId.trim();
    }
    /**
     * 
     * @return 加入时间
     */
    public String getAddTime() {
        return addTime;
    }
    /**
     * 
     * @param 加入时间
     */
    public void setAddTime(String addTime) {
        this.addTime = addTime == null ? null : addTime.trim();
    }
    /**
     * 
     * @return 是否有效
     */
    public Integer getIsva() {
        return isva;
    }
    /**
     * 
     * @param 是否有效
     */
    public void setIsva(Integer isva) {
        this.isva = isva;
    }
    /**
     * 
     * @return 操作时间
     */
    public Date getOptime() {
        return optime;
    }
    /**
     * 
     * @param 操作时间
     */
    public void setOptime(Date optime) {
        this.optime = optime;
    }
    /**
     * 
     * @return 操作人
     */
    public String getOper() {
        return oper;
    }
    /**
     * 
     * @param 操作人
     */
    public void setOper(String oper) {
        this.oper = oper == null ? null : oper.trim();
    }
    /**
     * 
     * @return 排序
     */
    public Integer getSort() {
        return sort;
    }
    /**
     * 
     * @param 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }
    
    /**
     * 判断产品和会员的来源站点是否相同
     * @return 
     * @author 罗海兵
     * @dateTime 2018年2月22日 下午9:14:35
     * @versions 1.0
     */
    public boolean equal(){
    	return mbStation.equals(proStName);
    }
}