/**
 * 
 */
package shop;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author liandyao
 * @date 2018年9月7日
 * @version 1.0
 */
public class Test {
	public static void main(String[] args) {
		for(int i=0;i<100;i++) {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMdhmsS");
			String  formDate =sdf.format(date);
	        System.out.println(formDate);
	        String no = formDate.substring(formDate.length()-5, formDate.length());
	        System.out.println(no);
		}
	}

}
